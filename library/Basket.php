<?php

require_once PATH . 'library/Abstract.php';

require_once PATH . 'library/Interface.php';

class Basket extends Main_Abstract implements Main_Interface {

	
    public function factory() {
        return true;
    }      
        
    public function main() { 
    	
    	return $this->showBasket();
    }    
    
    public function delete() {
    	return $this->showBasket('deleted');
    }

    public function back() {
    	return $this->showBasket();
    }

    public function update() {
        if (isset($_POST['count_id']) && count($_POST['count_id']) > 0) {
            $goodsSession = new Zend_Session_Namespace('goods');
            foreach ($_POST['count_id'] as $id=>$value) {
            	
                if (isset($goodsSession->array[$id]['count'])) {
                	if ($value <= 0) {
                    	$goodsSession->array[$id]['status'] = 'deleted';
                    	$goodsSession->array[$id]['count'] = $goodsSession->array[$id]['count'];
                	} else {
                		$goodsSession->array[$id]['count'] = $value;
                		 
                	}
                }
            }
        }
        return $this->showBasket();
    }

    public function clear() {
	
		   	if ($_COOKIE[ua]!=1){
       $this->setMetaTags('Очистка корзины');    	
       $this->setWay('Очистка корзины');
		}
			else{
			   $this->setMetaTags('Очищення кошика');    	
			   $this->setWay('Очищення кошика');
				}

       
       $goodsSession = new Zend_Session_Namespace('goods');
       $goodsSession->array = array();
       return $this->showBasket();
    }

    protected function showBasket($action='active') {
       
	   	if ($_COOKIE[ua]!=1){
        $this->setMetaTags('Корзина');    	
        $this->setWay('Корзина');
		}
			else{
				$this->setMetaTags('Кошик');    	
				$this->setWay('Кошик');
				}
	   
    	$goodsSession = new Zend_Session_Namespace('goods');
    	$basketLength = count($goodsSession->array);    	
    	
		$ua=$this->getUa();
		
    	$this->tpl->define_dynamic('_basket', "$ua".'basket.tpl');
    	
    	if ($basketLength > 0) {
    		
    		$this->tpl->define_dynamic('basket', '_basket');
    		$this->tpl->define_dynamic('basket_items', 'basket');	
    		
    		$allSumm = 0;
    		$counter = 1;
			
    		foreach ($goodsSession->array as $key=>$item) {
    			//print_r($item); echo '<br>';
    			if (isset($item['cost']) ) {
    		
    				$summ = $item['cost'];

                     if (count($this->url) > 0 && end($this->url) == $item['artikul'] ) {

                            $goodsSession->array[$key]['status'] = $action;
                            $item['status'] = $action;
                        }

                        if ($item['status'] != 'deleted'  && $this->regionData['kod']==$item['region']) {

                            $summ *= $item['count'];                       
                            $allSumm += $summ;
                        }
                        
                        
    			
    			//number_format($item['cost'], 2, ',', " ")
    			
    			$url = "/catalog/";
    			
    			if (($dataTree = $this->dataTreeManager($item['id']))) {                	
    				$url .= $dataTree['links'];
    			}
    	
                        $goodsInfo = $this->db->fetchRow("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `artikul`='".$item['artikul']."'");
                        $tVivozaList = $this->db->fetchAll("SELECT * FROM `spravochnik` WHERE `region`='".$this->regionData['kod']."' AND vis='1' order by sort ASC, tvivoz ASC");
						
                        $tVivozaListOpt = '';
						
                            $tvivozaSel = $this->getVar('tvivoza','');
							
		if ($_COOKIE[ua]!=1){
			$tVivozaListOpt = '<tr>
			<td>Точка самовывоза<span>*</span>:</td>
			<td><select name="tvivoza" class="tvivoza">';   
		}
			else{
					$tVivozaListOpt = '<tr>
					<td>Точка самовивозу<span>*</span>:</td>
					<td><select name="tvivoza" class="tvivoza">';   
				}
							

                            foreach ($tVivozaList as $value) {
                                $tVivozaListOpt .= '<option value="'.$value[id].'" '.($tvivozaSel == $value?' selected="selected" ':'').'>'.$value[tvivoz].'</option>';
                            }
                            $tVivozaListOpt .= '</select></td></tr>';
                        
                        
                        
                        //var_dump($tVivozaListOpt); die;
                        if ($item['region']==$this->regionData['kod']) {
                        //echo $tVivozaList;
						
						
					if ($_COOKIE[ua]!=1){
					$this->tpl->assign(array(
										'PROBA_CATALOG'     =>  (''),
										'PRINT_REG'=>'',
										'BASKET_ITEM_ID'=>$item['id'],
							'BASKET_ITEM_COUNTER'=>$counter,
							'BASKET_HREF'=>$url,
							'BASKET_ITEM_NAME'=>$item['name'].'-'.$item['id'],
										'BASKET_ITEM_PRODUCER'=>$goodsInfo['producer'],
							'BASKET_ITEM_ARTIKUL'=>$item['artikul'],
							'BASKET_ITEM_COUNT'=>$item['count'],
							'BASKET_ITEM_COST'=>number_format($item['cost'], 2, ', ', " "),
							'BASKET_ITEM_SUMM'=>number_format($summ, 2, ', ', " "),
										'BASKET_ITEM_CLASS'=>($item['status'] == 'deleted' ? "class='deleted'" : ''),
										'BASKET_ITEM_BUTTON_CLASS'=>($item['status'] == 'deleted' ? 'back' : 'delete'),
										'BASKET_ITEM_BUTTON_TEXT'=>($item['status'] == 'deleted' ? 'Вернуть' : '<img src="/img/delete2.gif" width="15" height="15" alt="" />'),
							'TEST'=>'<div id="span_goods_123">123</div>',
										'BASKET_ITEM_REGION'=>$this->regionData['kod'],
										'TMP'=>'<span id="span_goods_'.$item['id'].'"></span>',
										'BASKET_TVIVOZA'=>$tVivozaListOpt
							
							));   
				}
					else{
					$name_ua = $this->db->fetchRow("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `id`='".$item['id']."'");
					
								
								if($name_ua['name_ua']==""){$name_ua['name_ua']=$name_ua['name'];}
					
							$this->tpl->assign(array(
										'PROBA_CATALOG'     =>  (''),
										'PRINT_REG'=>'',
										'BASKET_ITEM_ID'=>$item['id'],
							'BASKET_ITEM_COUNTER'=>$counter,
							'BASKET_HREF'=>$url,
							'BASKET_ITEM_NAME'=>$name_ua['name_ua'].'-'.$item['id'],
										'BASKET_ITEM_PRODUCER'=>$goodsInfo['producer'],
							'BASKET_ITEM_ARTIKUL'=>$item['artikul'],
							'BASKET_ITEM_COUNT'=>$item['count'],
							'BASKET_ITEM_COST'=>number_format($item['cost'], 2, ', ', " "),
							'BASKET_ITEM_SUMM'=>number_format($summ, 2, ', ', " "),
										'BASKET_ITEM_CLASS'=>($item['status'] == 'deleted' ? "class='deleted'" : ''),
										'BASKET_ITEM_BUTTON_CLASS'=>($item['status'] == 'deleted' ? 'back' : 'delete'),
										'BASKET_ITEM_BUTTON_TEXT'=>($item['status'] == 'deleted' ? 'Повернути' : '<img src="/img/delete2.gif" width="15" height="15" alt="" />'),
							'TEST'=>'<div id="span_goods_123">123</div>',
										'BASKET_ITEM_REGION'=>$this->regionData['kod'],
										'TMP'=>'<span id="span_goods_'.$item['id'].'"></span>',
										'BASKET_TVIVOZA'=>$tVivozaListOpt
							
							));   
						}
						
    			
    				$counter++;
    				$this->tpl->parse('BASKET_ITEMS', '.basket_items');
                        } else {
                            $this->tpl->assign(array(
											'PROBA_CATALOG'     =>  (''),
                                'PRINT_REG'=>'hide',
                                'BASKET_ITEM_ID'=>$item['id'],
    				'BASKET_ITEM_COUNTER'=>'',
    				'BASKET_HREF'=>'',
    				'BASKET_ITEM_NAME'=>'',
    				'BASKET_ITEM_ARTIKUL'=>'',
    				'BASKET_ITEM_COUNT'=>'',
    				'BASKET_ITEM_COST'=>'',
    				'BASKET_ITEM_SUMM'=>'',
                                'BASKET_ITEM_CLASS'=>'',
                                'BASKET_ITEM_BUTTON_CLASS'=>'',
                                'BASKET_ITEM_BUTTON_TEXT'=>'',
                                'BASKET_ITEM_REGION'=>$item['region'],
    				));
    				$counter++;
    				$this->tpl->parse('BASKET_ITEMS', '.basket_items');
                        }
                        
    			}
    		}
                
                 
                $fio = $this->getVar('fio','');
                $adr = $this->getVar('adr','');
                $email = $this->getVar('email','');
                $mphone = $this->getVar('mphone','');
                $phone = $this->getVar('phone','');
                $info = $this->getVar('info','');
                $tvivoza = $this->getVar('tvivoza','');

                
                $this->tpl->assign(array(
								'PROBA_CATALOG'     =>  (''),
                                'BASKET_FORM_FIO'=>$fio,
                                'BASKET_FORM_ADR'=> $adr,
    				'BASKET_FORM_EMAIL'=>$email,
    				'BASKET_FORM_MPHONE'=>$mphone,
    				'BASKET_FORM_PHONE'=>$phone,
    				'BASKET_FORM_INFO'=>$info
    				));
							
                if (!empty($_POST['go'])){
								
		if ($_COOKIE[ua]!=1){
                    if (empty ($fio)){
                        $this->addErr('Поле <b>Ф.И.О.</b> не должно быть пустым');
                    }
                    
                    
                    if (empty ($adr)){
                        $this->addErr('Поле <b>адрес</b> не должно быть пустым');
                    }
                    if (empty ($mphone)){
                        $this->addErr('Поле <b>мобильный телефон</b> не должно быть пустым');
                    }
                    $validate = new Zend_Validate_EmailAddress();
                    if (!$validate->isValid($email)) {
                        $this->addErr('E-mail указан не правильно');
                    }
                    if ((empty ($tvivoza) || $tvivoza == 'Выберите точку самовывоза' && count($tVivozaList)>1)){
                        $this->addErr('Выберите точку самовывоза');
                    }
		}
			else{
                    if (empty ($fio)){
                        $this->addErr('Поле <b>П.І.Б.</b> повинно бути заповненим');
                    }
                    
                    if (empty ($adr)){
                        $this->addErr('Поле <b>адреси</b> повинно бути заповненим');
                    }
                    if (empty ($mphone)){
                        $this->addErr('Поле <b>мобільный телефон</b> повинно бути заповненим');
                    }
                    $validate = new Zend_Validate_EmailAddress();
                    if (!$validate->isValid($email)) {
                        $this->addErr('E-mail введений не вірно');
                    }
                    if ((empty ($tvivoza) || $tvivoza == 'Выберите точку самовывоза' && count($tVivozaList)>1)){
                        $this->addErr('Выберите точку самовывоза');
                    }
				}
				
				if ($this->_err) {
                        $this->viewErrBasket();
                    } else {
                        unset($_SESSION['userInfo']);
                        $_SESSION['userInfo']['fio']    =   $this->getVar('fio', '');
                        $_SESSION['userInfo']['adr']    =   $this->getVar('adr', '');
                        $_SESSION['userInfo']['email']  =   $this->getVar('email', '');
                        $_SESSION['userInfo']['mphone'] =   $this->getVar('mphone', '');
                        $_SESSION['userInfo']['phone']  =   $this->getVar('phone', '');
                        $_SESSION['userInfo']['info']   =   $this->getVar('info', '');
                        $_SESSION['userInfo']['tvivoza']   =   $this->getVar('tvivoza', '');

                        header ('location: /'.$this->regionData['url'].'/order/');
                    }
                }
                 else {
                    $this->tpl->assign(array('BASKET_FORM_ERR'=>''));
                }
                
    		$this->tpl->assign(array('BASKET_ITEM_ALL_SUMM'=>number_format($allSumm, 2, ', ', " ")));
    		$this->tpl->parse('CONTENT', '.basket');
    	} else {
    		$this->tpl->define_dynamic('basket_empty', '_basket');
    		$this->tpl->parse('CONTENT', '.basket_empty');
    	}
    	
    	return true;
    }
    
}