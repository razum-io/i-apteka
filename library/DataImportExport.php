<?php
require_once PATH.'excel/PHPExcel.php';
require_once PATH.'excel/PHPExcel/IOFactory.php';

abstract class DataImportExportAbstract {
		protected $config;	
		protected $errors = array();
		protected $db;
		protected $tpl;
		protected $_locale;
		
	public function __construct($config = array()) {
		$this->_locale = new Zend_Locale('ru_UA');
		$this->config = $config;
		$this->testConfig();
	}
	
	public function setDb($db) { 
		$this->db = $db;
		
	}
	
	public function setTpl($tpl) {
		$this->tpl = $tpl;
	}
	
	protected function addError($string, $key='all') {
		$this->errors[] = array($key => $string);
	}
	
	public function getErrors($key = null) {
		if ($key === null) { 
			return $this->errors;
		}
		if (isset($this->errors[$key])) {
			return $this->errors[$key];
		}
		return false;
	}
	
	protected function testConfig() {
		
		if (!is_array($this->errors)) {
			$this->addError("Неправильныый формат конфигурации. Информация должна находиться в массиве", 'sys');
			return false;
		}
		
		
		if (count($this->config) > 0) {
			
		} else {
			$this->addError("Массив с конфигурацией не должен быть пустым", 'sys');
		}
	}
	
	protected function ru2Lat($str) {
    	
        $rus = array('ё','ж','ц','ч','ш','щ','ю','я','Ё','Ж','Ц','Ч','Ш','Щ','Ю','Я',  'Ї', 'ї', 'Є', 'є', 'І', 'і', 'ь', 'Ь', 'Ъ', 'ъ');
        $lat = array('yo','zh','tc','ch','sh','sh','yu','ya','YO','ZH','TC','CH','SH','SH','YU','YA', 'YI', 'yi', 'E', 'e', 'I', 'i', '', '','','');
        $prototype = array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '-', '_', ' ', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', ':', '/', '.', '?', '&');
        
        /*if ($type == 'link') {
            array_push($prototype, ':', '/', '.', '?', '&');
        }*/
        
        $str = str_replace($rus,$lat,$str);
        
        $str = strtr(iconv('utf-8', 'cp1251',$str),
        iconv('utf-8', 'cp1251', "АБВГДЕЗИЙКЛМНОПРСТУФХЪЫЬЭабвгдезийклмнопрстуфхыэ"),
        "ABVGDEZIJKLMNOPRSTUFH_I_Eabvgdezijklmnoprstufhie");

        $size = strlen($str);
        
        $temp = "";
        for ($i=0; $i<$size; $i++) {
            if(in_array($str[$i], $prototype)) $temp .= $str[$i];
        }
        
        $str = $temp;       
       // $str = str_ireplace(' ', '-', trim($str));       
		$str = preg_replace('/\W/', '-', trim($str));
       
        return (strtolower($str));
    }
    
    protected function convertDate($date = null, $format = "d.m.Y")  {
    	
        if (null === $date || !is_numeric($date)) {
            $date = mktime();
        }
                
        $d = new Zend_Date($date, false, $this->_locale);
        
        return $d->toString($format);
    }
	
	abstract public function run();
	
	
}

/*$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory;
PHPExcel_Settings::setCacheStorageMethod($cacheMethod);

 * 
 */

class DataImport extends DataImportExportAbstract {
		protected $objPHPExcel;
		protected $isRun = false;
		private $sysFields = array();
		protected $tmpFileName;
		protected $isFile = true;
		protected $chengedFields = array();
	
	public function __construct($config = array()) {
		parent::__construct($config);				                
		if (!empty($_FILES)  ) {
           	$file = ($_FILES['file']['size'] > 0) ? $_FILES['file'] : null;                
                
           	$this->tmpFileName = $_FILES[$this->config['htmlFormFieldName']]['tmp_name'];               
           	if($file['type'] != "application/vnd.ms-excel" && $file['type'] != "application/octet-stream" && $file['type'] != "application/x-msexcel") {
               	$this->addError("<br>Неправильный формат файла каталога");
           	} else {            		           	
           		$this->objPHPExcel = PHPExcel_IOFactory::load($this->tmpFileName);           		           		
				$this->isRun = true;
           	}           	
        }		
	}
	
	public function isFile() {
		return $this->isFile();
	}
	
	protected function testSheetTitles($sheetArray, $listName) {		
		if (isset($this->config['fileFields'][$listName])) {
			
			$fieldStartIndex = 0;
			
			if (isset($this->config['fieldsTitle']) && is_numeric($this->config['fieldsTitle'])) {
				$fieldStartIndex = intval($this->config['fieldsTitle']);
				//$fieldStartIndex;				
			}
			if ($_SERVER['REMOTE_ADDR'] == '94.178.133.26') {
				//var_dump($sheetArray);
			}
			if (!isset($sheetArray[$fieldStartIndex])) {
				$this->addError("Не могу найти стоку с заголовками столбцов в файле импорта. Возможно неправильно указано значение в настройках параметра fieldsTitle. Сейчас это значение =  $fieldStartIndex", 'sys');
				return false;
			}
			
			// Проверка системных столбцов
			
			$sysFieldsLength = count($this->config['fileFields'][$listName]['systemFields']);
			$counter = 0;
			
			
			foreach ($this->config['fileFields'][$listName]['systemFields'] as $filedName) {								
				if (!isset($sheetArray[$fieldStartIndex][$counter]) || $sheetArray[$fieldStartIndex][$counter] != $filedName) {
					$this->addError("Описания полей в настройках и файле импорта не совпадают. В настройках [$filedName] в файле импорта [".$sheetArray[$fieldStartIndex][$counter]."]. Колонка № ".($counter +1), 'sys');
					return false;
				} else {
					$this->sysFields[] = $filedName;
						
				}
				$counter++;
			}
			
			// Проверка остальных столбцов
			
			if (isset($this->config['fileFields'][$listName]['fields'])) {
				foreach ($this->config['fileFields'][$listName]['fields'] as $key=>$value) {					
					$title = $value;
					if (is_array($value)) { 
						if (isset($value['title'])) {
							$title = $value['title'];
						} else {
							$title = false;
						}						
					}
					
					if (!$title) {
						$this->addError("Заголовок для поля $key в настройках не указан.");
						return false;
					} else {
						if (isset($sheetArray[$fieldStartIndex][$counter])) {	 				
							if ($sheetArray[$fieldStartIndex][$counter] != $title) {
								$this->addError("Описания полей в настройках и файле импорта не совпадают. В настройках [$title] в файле импорта [".$sheetArray[$fieldStartIndex][$counter]."] Колонка № ".($counter + 1), 'sys');
								return false;
							}
						} else {
							$this->addError("Не могу найти колонку [$title] Колонка № ".($counter + 1), 'all');
							return false;							
						}
					}
					$counter++;					
				}				
			} 
			
		} else {
			$this->addError("Настроек для листа $listName не найдено", 'sys');
			return false;
		}
		return true;			
	}
	
	protected function testSheetValues($sheetArray, $listName) {		
		
		if (isset($this->config['fileFields'][$listName])) {			
			$fieldStartIndex = 1;
			
			if (isset($this->config['startRead']) && is_numeric($this->config['startRead'])) {
				$fieldStartIndex = intval($this->config['startRead']);				
				//$fieldStartIndex--;				
			}
			
			$dataLength = 0;
		
			if (!isset($sheetArray[$fieldStartIndex])) {
				$this->addError("Не могу найти стоку со значениями столбцов в файле импорта. Возможно неправильно указано значение в настройках параметра startRead. Сейчас это значение =  $fieldStartIndex", 'sys');
				return false;
			} 
			
			$dataLength = count($sheetArray) ;
			
			// Проверка системных столбцов
			
			$sysFieldsLength = count($this->config['fileFields'][$listName]['systemFields']);
			$counter = 0;
			
			$uniqueFieldsValues = array();
			
			// Массив с параметрами для остальных столбцов.
			$freeTypeFields = array_values($this->config['fileFields'][$listName]['fields']);
			
						
			for ($fieldStartIndex; $fieldStartIndex < $dataLength; ++$fieldStartIndex) { 
				if (isset($sheetArray[$fieldStartIndex])) {
				
					
					$collCounter = 0;
					$collFreeTypeCount = 0;
					
					foreach ($sheetArray[$fieldStartIndex] as $sheetFieldValue) {
						// Проверка системных столбцов 
						
						if ($collCounter < $sysFieldsLength) {
							$systemFieldTitle = $this->config['fileFields'][$listName]['systemFields'][$collCounter];
							
							if ($systemFieldTitle == 'Раздел' && empty($sheetFieldValue)) {
								//$this->addError("Значение для поля [Раздел] не может быть пустым строка № ".($fieldStartIndex + 1) .' Колонка № '.($collCounter + 1));
								return true;
							}
							
							if ($systemFieldTitle == 'Подраздел' && empty($sheetFieldValue)) {
								$this->addError("Значение для поля [Подраздел] не может быть пустым строка № ".($fieldStartIndex + 1) .' Колонка № '.($collCounter + 1));
								return false;
							}
							
							if ($systemFieldTitle == 'Название' && empty($sheetFieldValue)) {
								$this->addError("Значение для поля [Название] не может быть пустым строка № ".($fieldStartIndex + 1) .' Колонка № '.($collCounter + 1));
								return false;
							}
							
							if ($systemFieldTitle == 'Артикул' && in_array($sheetFieldValue, $uniqueFieldsValues)) {								
								$this->addError("Значение для поля [Артикул] должно быть уникальным $sheetFieldValue. Строка № ".($fieldStartIndex + 1) .' Колонка № '.($collCounter + 1));
								return false;
							}
						
							if ($systemFieldTitle == 'Артикул' && empty($sheetFieldValue)) {
                                                         
								$this->addError("Значение для поля [Артикул] не может быть пустым строка № ".($fieldStartIndex + 1) .' Колонка № '.($collCounter + 1));								
								return false;
							} elseif ($systemFieldTitle == 'Артикул') {
								$uniqueFieldsValues[] = $sheetFieldValue;
							}
													
						} else { // Проверка остальных столбцов 
						
							$param = '';
							if (isset($freeTypeFields[$collFreeTypeCount])) {
								$param = $freeTypeFields[$collFreeTypeCount];
								if (is_array($param)) {
									
									$isEmpty = (isset($param['isEmpty']) && $param['isEmpty'] === false  && empty($sheetFieldValue));
									$replace = false;
									$params =$param;
									
									if (isset($param['replace'])) {
										$replace = $param['replace'];
									}
																		
									if (isset($param['title']) ) { 
										$param = $param['title'];
									} else {
										$param = '';
										$this->addError("Не могу найти заголовок для стобца [".$freeTypeFields[$collFreeTypeCount]."]. Заполните параметр title в настроках столбца.", 'sys');
										return false;
									}
									
								    
									if ($replace !== false) {
										if (is_array($replace) && count($replace) > 0) {
											$fieldReplaceValue = array_values($replace);
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                        if ($isEmpty) {
												$this->addError("Занчение поля [$param] = ".$sheetFieldValue." Не должно быть пустым Строка № ".($fieldStartIndex + 1) .' Колонка № '.($collCounter + 1));
                                                                                                return false;
												
                                                                                        } elseif (!in_array($sheetFieldValue, $fieldReplaceValue) && (isset($params['isEmpty']) && $params['isEmpty'] === false))  {  
                                                                                        
                                                                                                //$this->addError("Неправильное занчение поля [$param] = ".$sheetFieldValue." Должно быть одно из: (".implode(', ', $fieldReplaceValue).") Строка № ".($fieldStartIndex + 1) .' Колонка № '.($collCounter + 1));
                                                                                            
												//return false;
												
                                                                                            }
											}
										
									}
									
									if ($isEmpty) {
										$this->addError("Поле [$param] не должно быть пустым Строка № ".($fieldStartIndex + 1) .' Колонка № '.($collCounter + 1));
										return false;
									}
									
									
								}
							} else {								
								if (!empty($param)) {
									$this->addError("Не могу найти настрокйки для столбца, который следует за [$param]", 'sys');
									return false;
								}	
								
							}
							++$collFreeTypeCount;
						}						
						
						++$collCounter;
					}
					
				} else {
					$this->addError("Значение для строки № ".($fieldStartIndex + 1) ." не найдено.");
					return false;
				}
			}		
			
			
			
		} else {
			$this->addError("Настроек для листа $listName не найдено", 'sys');
			return false;
		}
		return true;			
	}
	
	protected function testFile($allSheets) {
		$configFieldsTitles = array_keys($this->config['fileFields']);				
		if (count($allSheets) > 0 && count($this->config['fileFields']) > 0) {
			$counter = 0;
			foreach ($allSheets as $sheet) {	
				$title = $sheet->getTitle();				
				$sheetArr = $sheet->toArray();
		
				if(isset($this->config['fileFields'][$title])) {				
					// Проверка заголовков ячеек таблицы
					if ($this->testSheetTitles($sheetArr, $title)) { 
						// Проверка данных в файле импорта.
						if ($this->testSheetValues($sheetArr, $title)) { 
							$this->insertSheetValues($sheetArr, $title); 
							return true;
						}		
						
					}						
					
				} else {
					
					if (isset($sheetArr[0][0]) && $sheetArr[0][0] !== null) {
						$this->addError("Не могу найти настройки для листа с именем $title");	
						return false;
					}
										
				}
				
			}
		}
		
		return false;
	}
	
	protected function insertSheetValues($sheetArray, $listName) {		
	
		if (isset($this->config['fileFields'][$listName])) {			
			$fieldStartIndex = 1;
			
			if (isset($this->config['startRead']) && is_numeric($this->config['startRead'])) {
				$fieldStartIndex = intval($this->config['startRead']);				
				//$fieldStartIndex--;				
			}
			
			$dataLength = 0;
		
			if (!isset($sheetArray[$fieldStartIndex])) {
				$this->addError("Не могу найти стоку со значениями столбцов в файле импорта. Возможно неправильно указано значение в настройках параметра startRead. Сейчас это значение =  $fieldStartIndex", 'sys');
				return false;
			} 
			
			$dataLength = count($sheetArray) ;
			// Проверка системных столбцов
			
			$sysFieldsLength = count($this->config['fileFields'][$listName]['systemFields']);
			$counter = 0;
			
			$uniqueFieldsValues = array();
			
			// Массив с параметрами для остальных столбцов.
			$freeTypeFields = array_values($this->config['fileFields'][$listName]['fields']);
			$freeTypeKeys = array_keys($this->config['fileFields'][$listName]['fields']);
			
			
			for ($fieldStartIndex; $fieldStartIndex <= $dataLength; ++$fieldStartIndex) {
				if (isset($sheetArray[$fieldStartIndex])) {
				
				
					$collCounter = 0;
					$collFreeTypeCount = 0;
					$sectionArr = array();
					$sectionHref = '';
					$subSectionHref = '';					
					$goodsData = array();
					
					foreach ($sheetArray[$fieldStartIndex] as $key=>$sheetFieldValue) {
						// Добавление системных столбцов 
						
												
						$data = array();
						
						$systemFieldTitle = '';
						
						if (isset($this->config['fileFields'][$listName]['systemFields'][$key])) {
							$systemFieldTitle = $this->config['fileFields'][$listName]['systemFields'][$key];	
						} 
						
						$systemNextFieldTitle = '';
							
							
						if (isset($this->config['fileFields'][$listName]['systemFields'][($key + 1)])) {
							$systemNextFieldTitle = $this->config['fileFields'][$listName]['systemFields'][($key + 1)];
						}
							
						$sheetNextFieldValue = '';
							
						if (isset($sheetArray[$fieldStartIndex] [($key + 1)])) {
							$sheetNextFieldValue = $sheetArray[$fieldStartIndex] [($key + 1)];
						}
													
							
						if ($systemFieldTitle == 'Раздел') {
							$sectionHref = $this->ru2Lat($sheetFieldValue);							
							if (empty($sheetFieldValue)) {
								
								return true;
							}
								
							if (!isset($sectionArr[$sectionHref])) {
                                                                $sheetFieldValue = addslashes($sheetFieldValue);
								$data['level'] = 0;
								$data['href'] = $sectionHref;
								$data['type'] = 'section';
								$data['name'] = $sheetFieldValue;
								$data['header'] = $sheetFieldValue;
								$data['title'] = $sheetFieldValue;
								$data['keywords'] = $sheetFieldValue;
								$data['description'] = $sheetFieldValue;
								$collCounter ++;	
								if ($systemNextFieldTitle == 'Позиция') {
									if (is_numeric($sheetNextFieldValue)) {
										$data['position'] = $sheetNextFieldValue;											
									}	
									$collCounter ++;
								}
									
								
								
								$id = $this->db->fetchOne("SELECT `id` FROM `catalog` WHERE `href` = '$sectionHref'");
								
								if (!$id) {								
									$this->db->insert('catalog', $data);
									$level = $this->db->lastInsertId();
								} else {
									$level = $id;
								}
								$sectionArr[$sectionHref] = array();
										
							}									
							
						} elseif ($systemFieldTitle == 'Подраздел') {
                                                    $sheetFieldValue = addslashes($sheetFieldValue);
							$subSectionHref = $this->ru2Lat($sheetFieldValue);
								
							if (!isset($sectionArr[$sectionHref][$subSectionHref])) {									
								$data['name'] = $sheetFieldValue;
								$data['level'] = $level;
								$data['href'] = $subSectionHref;
								$data['type'] = 'section';
								$data['header'] = $sheetFieldValue;
								$data['title'] = $sheetFieldValue;
								$data['keywords'] = $sheetFieldValue;
								$data['description'] = $sheetFieldValue;
								$collCounter ++;	
								
								if ($systemNextFieldTitle == 'Позиция') {
									if (is_numeric($sheetNextFieldValue)) {
										$data['position'] = "$sheetNextFieldValue";											
									}	
									$collCounter++;
								}
								
								$id = $this->db->fetchOne("SELECT `id` FROM `catalog` WHERE `href` = '$subSectionHref' AND `level` = '$level'");
								
								if (!$id) {									
									$this->db->insert('catalog', $data);
									$level = $this->db->lastInsertId();
								} else {
									$level = $id;
								}
								$sectionArr[$sectionHref][$subSectionHref] = array();
									
							}
						} elseif ($systemFieldTitle == 'Название') {
							
							$goodsHref = $this->ru2Lat($sheetFieldValue);
							if (!isset($sectionArr[$sectionHref][$subSectionHref][$goodsHref])) {
								$goodsData['name'] = $sheetFieldValue;
								$goodsData['href'] = $goodsHref;								
								
								$collCounter++;
								if ($systemNextFieldTitle == 'Позиция' ) {
									if (is_numeric($sheetNextFieldValue)) {
										$goodsData['position'] = "$sheetNextFieldValue";											
									}	
									$collCounter++;
									
								}
																								
								$sectionArr[$sectionHref][$subSectionHref][$goodsHref] = $sheetFieldValue;
							}	
						} elseif ($systemFieldTitle == 'Артикул') {																					
							
							$goodsData['artikul'] = "$sheetFieldValue";	
														
							$collCounter++;
						} elseif ($key >= $collCounter ) { 
							
							if (isset($freeTypeFields[$collFreeTypeCount])) {
								$param = $freeTypeFields[$collFreeTypeCount];
								$fields = $freeTypeKeys[$collFreeTypeCount];
																
								if (is_array($param)) {
								
									$isEmpty = (isset($param['isEmpty']) && $param['isEmpty'] === false && empty($sheetFieldValue));								
									$replace = false;									
									$iseReplaced = false;
									
									if (isset($param['replace'])) {
										$replace = $param['replace'];
											
									}
																	
									if (isset($param['title']) ) {
										$param = $param['title'];
									} 
								
									if ($replace !== false) {
										if (is_array($replace) && count($replace) > 0) {											
											foreach ($replace as $key1=>$value1) {
												if ($sheetFieldValue == $value1) {
													$goodsData[$fields] = $key1;
													$iseReplaced = true;
												}
											}
											
										} 
									
										if (is_string($replace) && !empty($replace)) { 	
											if (isset($goodsData[$replace])) { 
												$goodsData[$fields] = $goodsData[$replace];
												$iseReplaced = true;
											}
										} 
										if (!$isEmpty && !$iseReplaced) { 
											$goodsData[$fields] = '';											
										}
									} 
																		
									//print "key: $key == $collCounter $fields => velue: $sheetFieldValue <br>"; 
								} else {
                                                                    $fieldName = substr($fields, 0, strpos('line_feature', $fields));
                                                                    if ($fieldName == 'line_feature' || $fields == 'body' || $fields == 'preview') {
                                                                        $sheetFieldValue = addslashes($sheetFieldValue);
                                                                    }
                                                                    $goodsData[$fields] = "$sheetFieldValue";									
								}
								
								++$collFreeTypeCount;	
							}
							
						}			 
						
					}
				
					// Если нужно к ссылке добавить значение другого поля
					if (isset($this->config['hrefConnectField'])) {
						if (is_array($this->config['hrefConnectField'])) {			
							$hrefConnectFieldLength = count($this->config['hrefConnectField']);
							$fieldVas = '';											
							for ($f = 0; $f < $hrefConnectFieldLength; ++$f) {																								
								if (isset($goodsData[$this->config['hrefConnectField'][$f]])) {		
									$slider = '-';
									if (isset($this->config['hrefConnectFieldSlider'])) {
										$slider = $this->config['hrefConnectFieldSlider'];
									}
									$goodsData['href'] .= $slider.$this->ru2Lat($goodsData[$this->config['hrefConnectField'][$f]]);
								}
							}
						}
						
					}
					$goodsData['level'] = $level;
					$goodsData['type'] = 'page';
					$goodsData['changed'] = '0';
					
					$row = $this->db->fetchRow("SELECT * FROM `catalog` WHERE `artikul` = '$goodsData[artikul]' AND `level`='$level' LIMIT 1");
					
					if (!$row) { 	                                        
                                            $this->db->insert('catalog', $goodsData);						
					} else {						
                                            
                                            if ($row['changed'] != '0') {
                                                $this->addError($row, 'chenged');
                                                $this->chengedFields[$row['artikul']] = $goodsData;													
                                            } else {                                                
                                                unset($goodsData['href']);
                                                unset($goodsData['artikul']);
                                                $this->db->update('catalog', $goodsData, "id=$row[id]");						
                                            }
                                            
                                            
					}
					
					
				} 
			 }
			
		} 

		return true;			
	}
	
	protected function import() {
			
		if (!$this->isRun) { return false;}	
		
		$allSheets = $this->objPHPExcel->getAllSheets();
		$sheetsLength = count($allSheets);
		
		if ($this->testFile($allSheets) ) {
			// Если тестирование прошло удачно, импортируем данные
			
		} else {			
			
			return false;
		}
		
	
	}
	
	protected function drowChangedList() { 

		$this->tpl->define_dynamic('_data_import', 'adm/data_import.tpl');	
		$this->tpl->define_dynamic('data_import', '_data_import');	
		$this->tpl->define_dynamic('data_import_list', 'data_import');			
		$isShow = false;
		if (count($this->errors) > 0) {			
			$chFields = "";						
			
			foreach ($this->errors as $error) {
				if (isset($error['chenged'])) {					
					
					foreach ($error as $fields) {
						if (count($fields) > 0) {
							
							foreach ($fields as $fName=>$fVal) {	
								if (isset($this->chengedFields[$fields['artikul']][$fName])) {
								
 									$val = $this->chengedFields[$fields['artikul']][$fName];
									if ($fName != 'artikul' && $fName != 'id' && $fName != 'changed' && $fName != 'href') {
										 
										$chFields .= "<input type = 'hidden' name='".$fields['artikul']."[$fName]' value = '$val' />";
									}		
								
									if ($fName == 'changed') {
										$chFields .= "<input type = 'hidden' name='".$fields['artikul']."[$fName]' value = '0' />";
									}
								} else {
									
								}
							}	
						}  
					}
					
					$isShow = true;
					$this->tpl->assign(
						array(
							'ADM_FIELDS' =>$chFields,
							'ADM_GOODS_NAME'=>$error['chenged']['name'],
							'ADM_ART'=>$error['chenged']['artikul'],
							'ADM_DATE'=>$this->convertDate($error['chenged']['changed'], 'm/d/Y H:i')
							)
							);
					$this->tpl->parse('DATA_IMPORT_LIST', '.data_import_list');	
				}
				
			}
			
		}
		if ($isShow) {
			$this->tpl->parse('CONTENT', '.data_import');
		}	
		
	}
	
	public function run() { 		
		if (!$this->isRun) { return false;}	
		
		$this->import();
	
		$this->drowChangedList();
	}	
}

class DataExport extends DataImportExportAbstract  {
		protected $data;
		protected $objPHPExcel;
		protected $systemFields = array();
		protected $fields = array();
		protected $fieldsTitlesArray = array();
		protected $fieldsDataArray = array();
		protected $fieldsTitle = 1;
		protected $sheet = array();
		
				
	public function __construct($config = array()) {
		parent::__construct($config);			
		require_once 'PHPExcel.php';
		$this->objPHPExcel = new PHPExcel();
			
	}
	
	public function writeMetaTags($array) {
		$objPHPExcel->getProperties()->setCreator("PHP")
			->setLastModifiedBy("Алексей")
			->setTitle("Office 2007 XLSX Тестируем")
			->setSubject("Office 2007 XLSX Тестируем")
			->setDescription("Тестовый файл Office 2007 XLSX, сгенерированный PHPExcel.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Тестовый файл");
			$objPHPExcel->getActiveSheet()->setTitle('Демо');
	}
	
	public function writeTitles () {
		
		$this->fieldsTitle = 0;
		if (isset($this->config['fieldsTitle'])) {
			$this->fieldsTitle = $this->config['fieldsTitle'];
			
			$sheets = $this->config['fileFields'];			
						
			foreach ($sheets as $sheetName=>$sheet) {
				
				$fieldsTitlesArray = array();
				if (isset($sheet['systemFields']) && is_array($sheet['systemFields'])) {
					$systemFieldsLength = count($sheet['systemFields']);
					for ($i = 0; $i < $systemFieldsLength; $i++) {						
						$this->fieldsTitlesArray[] = $sheet['systemFields'][$i];			
						$this->systemFields[] = $sheet['systemFields'][$i];
					}
					
				}
				
				if (isset($sheet['fields']) && is_array($sheet['fields']) && count($sheet['fields']) > 0) {					
					foreach ($sheet['fields'] as $key=>$value) {
						$title = $value;
						$this->fields[] = $key;
						if (is_array($value)) {
							if (isset($value['title'])) {
								$title = $value['title'];
							}							
						}
						$this->fieldsTitlesArray[] = $title;
					}
				}					
			}
		}		
	}
	
	public function readDataFromDb($activeSheetIndex = 0) { 
		
				
		$fields = "";
		$tables = "";
		$where = "";
		$subLevel = 2;
		
		
		$counter = 0;
		
		if (isset($this->systemFields[$counter]) && $this->systemFields[$counter] == 'Раздел') {
			$fields .=" c1.name as section";
			$tables .= " catalog as c1";
			$where .= "c1.level = 0";
			$counter++;
		}
		
		if (isset($this->systemFields[$counter]) && $this->systemFields[$counter] == 'Позиция') {
			$fields .=", c1.position section_position";			
			$counter++;
		}
		
		if (isset($this->systemFields[$counter]) && $this->systemFields[$counter] == 'Подраздел') { 
			$fields .=", c2.name as sub_section";
			$tables .= ", catalog as c2";
			$where .= " AND c2.level = c1.id";
			$subLevel = 3;
			$counter++;
		}
		
		if (isset($this->systemFields[$counter]) && $this->systemFields[$counter] == 'Позиция') {
			$fields .=", c2.position sub_section_position";
			$counter++;
		}
		
		if (isset($this->systemFields[$counter]) && $this->systemFields[$counter] == 'Название') { 
			$fields .=", c".$subLevel.".name as name";			
			$tables .= ", catalog as c".$subLevel." ";			
			
			$counter++;
		}
		
		if (isset($this->systemFields[$counter]) && $this->systemFields[$counter] == 'Позиция') {
			$fields .=", c".$subLevel.".position";
			$counter++;
		}
		
		
		
		
		array_unshift($this->fields, 'artikul');
		$fields .=", c".$subLevel.".".implode(', c'.$subLevel.'.', $this->fields);
		
		$where .= " AND c".$subLevel.".level = c".($subLevel-1).".id";
		
		$sql = "SELECT $fields FROM $tables WHERE $where";
		
		$items = $this->db->fetchAll($sql);
		
		$fieldsConfigs = array_values($this->config['fileFields']);
		
		if (isset($fieldsConfigs[$activeSheetIndex]['fields'])) {
			$fieldsConfigs = $fieldsConfigs[$activeSheetIndex]['fields'];
			
		}
		
		
		
		if ($items) {
			$line = 0;
			foreach ($items as $item) { 
				$column = 0;
				if (count($item) > 0) {
					foreach ($item as $key=>$row) { 
						
						if ($key == 'position' && $row == '9999') {
							$row = '';
						}
						
						if (isset($fieldsConfigs[$key])) {
							$param = $fieldsConfigs[$key];
							if (is_array($param)) {								
								
								$isEmpty = (isset($param['isEmpty']) && $param['isEmpty'] === false && empty($row));
								$replace = false;
																	
								if (isset($param['replace'])) {
									$replace = $param['replace'];
								}
																		
								if (isset($param['title']) ) {
									$param = $param['title'];
								} else {
									$param = '';								
								}	
								    
								if ($replace !== false) {
									if (is_array($replace) && count($replace) > 0) {
										foreach ($replace as $key1=>$value1) {
											if ($key1 == $row) {
												$row = $value1;
											}
										}
											
									}
								}									
							}
						}
						
										
						$this->fieldsDataArray[$line][$column] = $row;
						$column++;
					}
					
				}
				$line++;				
			}
		}		
		
	}
	
	public function run() {
		$this->writeTitles();
		$this->readDataFromDb();
		
		$fieldsTitlesArrayLength = count($this->fieldsTitlesArray);
		
		$this->objPHPExcel->setActiveSheetIndex(0)->setTitle('Лист1');
		
		for ($i = 0; $i < $fieldsTitlesArrayLength; ++$i) {	
			$this->objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($i, $this->fieldsTitle,  $this->fieldsTitlesArray[$i]);	
		}
	
		$fieldsDataArrayLength = count($this->fieldsDataArray);
		
		$writeLine = ( $this->fieldsTitle +1);
	
		
		for ($i = 0, $writeLine; $i < $fieldsDataArrayLength; ++$i, ++$writeLine) { 						
			for ($f = 0; $f < count($this->fieldsDataArray[$i]); ++$f) {		
				
				$this->objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($f, $writeLine,  $this->fieldsDataArray[$i][$f]);	
				
			}			
		}
		
		
		
		
		//$this->objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($column, $line, $row);								
		$objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel5');
 
   		header('Content-Type: application/vnd.ms-excel');
   		header('Content-Disposition: attachment;filename="Файл экспорта.xls"');
   		header('Cache-Control: max-age=0');
 
   		$objWriter->save('php://output');
   		
		return $this;
		
	}
}

class DataImportExport extends DataImportExportAbstract {
	
		protected $dataImportObj;
		protected $dataExportObj;
		
	public function __construct($config = array()) {
		parent::__construct($config);
		$this->dataImportObj = new DataImport($this->config);		
		$this->dataExportObj = new DataExport($this->config);
		
		
	}
	
	public function import() {
		$this->dataImportObj->setDb($this->db);
		$this->dataImportObj->setTpl($this->tpl);
		$this->dataImportObj->run();	
		$this->errors = $this->dataImportObj->getErrors();
		
	}
	
	public function export() {		
		$this->dataExportObj->setDb($this->db);
		$this->dataExportObj->setTpl($this->tpl);		
		$this->dataExportObj->run();	
		$this->errors = $this->dataExportObj->getErrors();
	}
	
	public function run() {
		
	}
		
}



?>