<?php

require_once PATH . 'library/Templates.php';

abstract class Main_Abstract {

   private $_config = null;
   private $_locale = null;
   private $_way = null;
   private $sepWay = '<span>&nbsp;</span>';
   protected $db = null;
   protected $tpl = null;
   protected $settings = null;
   protected $lookups = null;
   protected $lookupsRegion = null;
   protected $auth = null;
   protected $url = array();
   protected $basePath = '';
   protected $lang = '';
   protected $region = '';
   protected $getParam = array();
   protected $_err = '';
   protected $isUseDbProfiler = true;
   protected $dbProfiler = null;
   protected $regionData='';

   public function __construct(array $config) {
      if ($config instanceof Zend_Config) {
         $this->_config = $config->toArray();
      } else {
         if (!is_array($config)) {
            $config = (array) $config;
         }

         $this->_config = $config;
      }

      if (empty($this->_config)) {
         throw new Exception('Config cannot be empty!');
      }

      $this->_locale = new Zend_Locale('ru_UA');
      Zend_Date::setOptions(array('format_type' => 'php'));


      $this->getRegistry();

      $this->connectionToDatabase();

      $this->loadTemplates();
      //Обнуляем вывод модального окна
      //для выбора региона
      $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'PROMPTED_SELECT_REGION' => ''));
      // startIP
      $this->startIP();
      
      //Вся информация о регионе
      $this->regionData = $this->getRegionData($this->region);
        
      $this->newIP();

      
      $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'ALERT_REGION' => ''));
      $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'SERVER_NAME' => $_SERVER['SERVER_NAME']));
      $this->loadSettings();
      $this->loadLookups();
      $this->loadMetaTags();

      $this->checkUser();

      $this->loadAdminMenu();
      $this->loadPoll();


      $this->loadHorisontalMenu();
      //$this->loadVerticalMenu();
      
      //Тормозит?
      //$this->loadCatalogMenu();
      $this->loadLeftCatalogMenu();
      $this->listUserRegion();
      
      //Последние новости
      $this->lastNewsToMain();
      
      //загрузки информации о корзине
      $this->basketStart();
      
      //Русский алфавит
      $this->printListSearchABC();
      
      $this->alertChangeRegion();
      
      $this->loadTOPCatalogMenuLP('Лекарственные препараты', 'LP');
      $this->loadTOPCatalogMenuLP('Витамины', 'VITAMIN');
      $this->loadTOPCatalogMenuLP('Биологически активные добавки (БАД)', 'BAD');
      $this->loadTOPCatalogMenuLP('Изделия медицинского назначения', 'IMN');
      $this->loadTOPCatalogMenuLP('Медицинская техника', 'MT');
      $this->loadTOPCatalogMenuLP('Сопутствующие товары', 'ST');
      $this->loadTOPCatalogMenuLP('Косметика', 'KOSMETIKA');
      
      
      
      //var_dump($this->regionData); die;
      
   }
   
         public function getUa() {
		if ($_COOKIE[ua]==1){$ua="ua_";}else{$ua="";}
		return $ua;
   }     

   public function getZak() {
		if ($_COOKIE[ua]==1){$zakazat="Замовити";}else{$zakazat="Заказать";}
		return $zakazat;
   }

   protected function getCatOption($key) {
      if (isset($this->_config['catalog'])) {
         return $this->gpm($this->_config['catalog'], $key, false);
      }
      return false;
   }

   protected function getConfig($key=null) {
      if ($key == null) {
         return $this->_config;
      }

      if (isset($this->_config[$key])) {
         return $this->_config[$key];
      }
      return false;
   }

   private function getRegistry() {
      $array = Zend_Registry::get('run');

      if (!is_array($array)) {
         throw new Exception('Unexpected Error: Base variables not defined');
      }

      foreach ($array as $key => $value) {
         if (isset($this->$key)) {
            $this->$key = $value;
         } else {
            throw new Exception("Variable \"$key\" is not defined on Abstract Class!");
         }
      }
   }

   private function connectionToDatabase() {
      try {
         $database = Zend_Db::factory($this->_config['database']['adapter'], $this->_config['database']['params']);
         $this->dbProfiler = $database->getProfiler($this->isUseDbProfiler);
         $database->getConnection();
      } catch (Zend_Db_Adapter_Exception $e) {
         throw new Exception('возможно, неправильные параметры соединения или СУРБД не запущена');
      } catch (Zend_Exception $e) {
         throw new Exception('возможно, попытка загрузки требуемого класса адаптера потерпела неудачу');
      }

      $this->db = $database;
   }

   public function getProfiler() {
      return $this->dbProfiler;
   }

   private function loadTemplates() {
   
   $ua = $this->getUa();
   
      $templates = new Templates('tpl/');
      $templates->define_dynamic('page', "$ua"."design.tpl");
      $templates->define_dynamic('null', 'page');

      $templates->define_dynamic('p_header', 'page');

      $templates->define_dynamic('basket_block', 'page');
      $templates->define_dynamic('index_top_bnnrs', 'page');

      $templates->define_dynamic('administration', 'page');
      $templates->define_dynamic('show_order', 'administration');


      $templates->define_dynamic('adminjslib', 'page');

      $templates->define_dynamic('horisontal', 'page');
      $templates->define_dynamic('horisontal_sep', 'horisontal');

      $templates->define_dynamic('vertical', 'page');
      $templates->define_dynamic('vertical_single', 'vertical');
      $templates->define_dynamic('vertical_complex', 'vertical');
      $templates->define_dynamic('vertical_complex_sub', 'vertical_complex');

      $templates->define_dynamic('catalog_menu', 'page');
      $templates->define_dynamic('catalog_menu_new_goods', 'page');
      $templates->define_dynamic('catalog_menu_hit_goods', 'page');
      $templates->define_dynamic('catalog_menu_action_goods', 'page');

      $templates->define_dynamic('catalog_menu_single', 'catalog_menu');
      $templates->define_dynamic('catalog_menu_complex', 'catalog_menu');
      $templates->define_dynamic('catalog_menu_complex_sub', 'catalog_menu_complex');

      $templates->define_dynamic('catalog_left_menu', 'page');
      $templates->define_dynamic('catalog_left_menu_new_goods', 'page');
      $templates->define_dynamic('catalog_left_menu_hit_goods', 'page');
      $templates->define_dynamic('catalog_left_menu_action_goods', 'page');

      $templates->define_dynamic('catalog_left_menu_single', 'catalog_left_menu');
      $templates->define_dynamic('catalog_left_menu_complex', 'catalog_left_menu');
      $templates->define_dynamic('catalog_left_menu_complex_sub', 'catalog_left_menu_complex');

      
      
      //Последние новости на главной
      $templates->define_dynamic('last_news', 'page');
      $templates->define_dynamic('last_news_inner', 'last_news');

      
      $catalogOptions = $this->getConfig('catalog');

      
      
      $templates->parse('INDEX_TOP_BNNRS', 'null');

      if ($this->getStatus('new') == '0') {
         $templates->parse('CATALOG_MENU_NEW_GOODS', 'null');
      }
      if ($this->getStatus('hit') == '0') {
         $templates->parse('CATALOG_MENU_HIT_GOODS', 'null');
      }

      if ($this->getStatus('action') == '0') {
         $templates->parse('CATALOG_MENU_ACTION_GOODS', 'null');
      }

      if ($this->getCatOption('isStore')) {
         $templates->parse('BASKET_BLOCK', '.basket_block');
         $templates->assign('SHOW_SEARCH_TEXT_IF_NO_STORE', '');
      } else {
         $templates->parse('BASKET_BLOCK', 'null');
         $templates->assign('SHOW_SEARCH_TEXT_IF_NO_STORE', '<p><span id="find-text">Поиск</span> <br /><span>Поиск продукции по каталогу</span></p>');
      }

	  //ставим канонические урлы дабы убрать дубликаты страниц при разных регионах (только для товаров)
	  
				$ar_uri=explode("/",$_SERVER[REQUEST_URI]);
				$count_ar_uri=count($ar_uri);
		
		if(preg_match("/catalog/",$_SERVER[REQUEST_URI]) && $count_ar_uri>=6){
	//  print_r($ar_uri);
				$repl=preg_replace("/(.+)\/catalog/","/catalog",$_SERVER[REQUEST_URI]);
				$canon_url="http://$_SERVER[HTTP_HOST]$repl";
				$templates->assign('CANONICAL_URL', '<link href="'.$canon_url.'" rel="canonical" />');} 
			else {$templates->assign('CANONICAL_URL', '');}
		
      $this->tpl = $templates;
   }

   private function getStatus($status='new') {
      //return $this->db->fetchOne("SELECT COUNT(id) FROM `".$this->regionData['kod']."_catalog` WHERE `status`='$status'");
   }

   private function loadSettings() {
      if (null === $this->db) {
         throw new Exception('Database not initialised!');
      }

      $settings = $this->db->fetchAll("SELECT `key`, `value` FROM `settings` WHERE `language` = '" . $this->lang . "'");

      if ($settings) {
         $this->settings = array();

         foreach ($settings as $set) {
            $this->settings[$set['key']] = $set['value'];
            
         }
         
      }
   }

   private function loadLookups() {
      $lookups = $this->db->fetchAll("SELECT * FROM `lookups` WHERE `language` = '" . $this->lang . "' ORDER BY `position`");

      if ($lookups) {
         $this->lookups = $lookups;
		 
				 if ($_COOKIE[ua]!=1){
						 foreach ($lookups as $item) {
							//$this->lookups[$item['key']] = $item['value'];
							$this->tpl->assign(strtoupper($item['key']), stripslashes($item['value']));

							if ($item['key'] == 'FIRST_WAY') {
							   $this->setWay($item['value'], 'http://' . $_SERVER['HTTP_HOST'] . '/');
							}
						 }
				}
				else{
						foreach ($lookups as $item) {
							//$this->lookups[$item['key']] = $item['value'];
							$this->tpl->assign(strtoupper($item['key']), stripslashes($item['value_ua']));

							if ($item['key'] == 'FIRST_WAY') {
							   $this->setWay($item['value_ua'], 'http://' . $_SERVER['HTTP_HOST'] . '/');
							}
						 }
				
				}
      }
      
      $lookupsRegion = $this->db->fetchAll("SELECT * FROM `lookups_region` WHERE `language` = '" . $this->lang . "' AND `region`='".$this->regionData['kod']."' ORDER BY `position`");

	  if ($_COOKIE[ua]!=1){
			  if ($lookupsRegion) {
				 $this->lookupsRegion = $lookupsRegion;
				 foreach ($lookupsRegion as $item) {

					$this->tpl->assign(strtoupper($item['key']), stripslashes($item['value']));

				 }
			  }
	  }
	  else{
	  
	  if ($lookupsRegion) {
				 $this->lookupsRegion = $lookupsRegion;
				 foreach ($lookupsRegion as $item) {

					$this->tpl->assign(strtoupper($item['key']), stripslashes($item['value_ua']));

				 }
			  }
	  }
   }

   private function loadMetaTags() {
      $meta = $this->db->fetchRow("SELECT * FROM `meta_tags` WHERE `href` = '" . $this->url[0] . "' AND `language` = '" . $this->lang . "'");

      if ($meta) {
         $this->setMetaTags($meta);
		 
		if ($_COOKIE[ua]!=1){
			$this->setWay(stripslashes($meta['title']), $this->basePath . $meta['href']);
		}
		else{
			$this->setWay(stripslashes($meta['title_ua']), $this->basePath . $meta['href']);
		}
         if (!isset($this->url[1])) {
            $this->tpl->assign('CONTENT', stripslashes($meta['body']));
         }
      }
			//==========	Для динамического поиска
	  	  		$this->tpl->assign('NUMBER', $this->regionData['kod']);
			//============================================================
	}

   private function checkUser() {
      $auth = Zend_Auth::getInstance();

      if ($auth->hasIdentity()) {
         $this->auth = $auth->getIdentity();

         if ($this->_isAdmin()) {
            $this->loadAdminMenu();
         }
      }
   }

   private function loadAdminMenu() {
      if ($this->_isAdmin()) {
         $this->tpl->parse('ADMINISTRATION', 'administration');
         $this->tpl->parse('ADMINJSLIB', 'adminjslib');

         if (!$this->getCatOption('isStore')) {
            $this->tpl->parse('SHOW_ORDER', 'null');
         }
      } else {
         $this->tpl->parse('ADMINISTRATION', 'null');
         $this->tpl->parse('ADMINJSLIB', 'null');
      }
   }

   private function loadHorisontalMenu() {
   
      if(preg_match("/\/search\//",$_SERVER[REQUEST_URI])){
   
		  if ($_COOKIE[ua]!=1){
						   $search_from_google=" <div style=\"width:600px\"><gcse:search></gcse:search>
													<small><font color=\"red\">Если вы не нашли того что искали - воспользуйтесь поиском по нашему сайту при помощи сервиса от <b>Google</b></font></small>
												</div>
												<br />";
						}
						else{
						   $search_from_google=" <div style=\"width:600px\"><gcse:search></gcse:search>
													<small><font color=\"red\">Якщо ви не знайшли того що шукали - скористайтесь пошуком по нашому сайту за допомогою сервіса від <b>Google</b></font></small>
												</div>
												<br />";
						}
}
   
   else{
   $search_from_google="";
   }
   
   			//==========	Для поиска по гугллу
	  	  		$this->tpl->assign('GOOGLE_SEARCH', $search_from_google);
			//============================================================
			
   
   if ($_COOKIE[ua]!=1){
   
      $menu = $this->db->fetchAll("SELECT `href`, `header`, `type` FROM `page` WHERE `level` = '0' AND `menu` = 'horisontal' AND `language` = '" . $this->lang . "' AND `href`!='mneniya' ORDER BY `position`, `header`");

      if ($menu) {
         $size = sizeof($menu);

         for ($i = 0; $i < $size; $i++) {
            $this->tpl->assign(
                    array(
                        'MENU_HREF' => $menu[$i]['type'] === 'link' ? $menu[$i]['href'] : $this->basePath . $menu[$i]['href'],
                        'MENU_NAME' => stripslashes($menu[$i]['header'])
                    )
            );

            if ($i < $size - 1) {
               $this->tpl->parse('HORISONTAL_SEP', 'horisontal_sep');
            } else {
               $this->tpl->parse('HORISONTAL_SEP', 'null');
            }

            $this->tpl->parse('HORISONTAL', '.horisontal');
         }
      } else {
         $this->tpl->parse('HORISONTAL', 'null');
      }
		}
			else{
                         $menu = $this->db->fetchAll("SELECT `href`, `header`, `header_ua`, `type` FROM `page` WHERE `level` = '0' AND `menu` = 'horisontal' AND `language` = '" . $this->lang . "' AND `href`!='mneniya' ORDER BY `position`, `header`");

						  if ($menu) {
							 $size = sizeof($menu);

							 for ($i = 0; $i < $size; $i++) {
								$this->tpl->assign(
										array(
											'MENU_HREF' => $menu[$i]['type'] === 'link' ? $menu[$i]['href'] : $this->basePath . $menu[$i]['href'],
											'MENU_NAME' => stripslashes($menu[$i]['header_ua'])
										)
								);

								if ($i < $size - 1) {
								   $this->tpl->parse('HORISONTAL_SEP', 'horisontal_sep');
								} else {
								   $this->tpl->parse('HORISONTAL_SEP', 'null');
								}

								$this->tpl->parse('HORISONTAL', '.horisontal');
							 }
						  } else {
							 $this->tpl->parse('HORISONTAL', 'null');
						  }
				}
   

   }

   public function beforeAction($isRun = false) {
      if ($isRun) {
         $this->loadBanners();
      }
   }

   protected function loadBanners($isIndexOnliy = false) {
      $showAs = '';
      if ($isIndexOnliy) {
         $showAs = " OR `show_as`='index'";
      }

      $banners = $this->db->fetchAll("SELECT * FROM `banners` WHERE `show_as`='all' $showAs  AND `show_as` != 'hide' ORDER BY `layout`, `position`, `name`");

      if ($banners) {
         $topLeft = '';
         $topRight = '';

         $left = '';
         $bottom = '';


         foreach ($banners as $banner) {

            if ($banner['type'] == 'img') {

               if ($banner['layout'] == 'top' && !empty($topLeft)) {
                  $topRight = "<a href='$banner[href]' title='$banner[title]'  target=\"_blank\"><img src='/img/bnrs/top/$banner[pic]' width='468' height='60' alt='$banner[alt]' title='$banner[title]' /></a>";
               }

               if ($banner['layout'] == 'top' && empty($topLeft)) {
                  $topLeft = "<a href='$banner[href]' title='$banner[title]'  target=\"_blank\"><img src='/img/bnrs/top/$banner[pic]' width='468' height='60' alt='$banner[alt]' title='$banner[title]' /></a>";
               }


               $ifLeftImg = true;
               if ($banner['layout'] == 'left') {
                  $left .= "<a href='$banner[href]' title='$banner[title]'  target=\"_blank\"><img src='/img/bnrs/left/$banner[pic]' alt='$banner[alt]' title='$banner[title]' /></a>";
               }

               if ($banner['layout'] == 'bottom') {
                  $bottom .= "<li><a href='$banner[href]' title='$banner[title]'  target=\"_blank\"><img src='/img/bnrs/bottom/$banner[pic]' alt='$banner[alt]' title='$banner[title]' width='88' height='31' /></a></li> ";
               }
            }

            if ($banner['type'] == 'swf') {
               if ($banner['layout'] == 'top' && !empty($topLeft)) {
                  $topRight = (!empty($banner['href']) ? " <a href=\"$banner[href]\"  title='$banner[title]' target=\"_blank\">" : '') . "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0\"  width=\"468\" height=\"60\"> <param name=\"movie\" value=\"/img/bnrs/top/$banner[pic]\" /> <param name=\"quality\" value=\"high\" /> <param name=\"allowScriptAccess\" value=\"always\" /><param name=\"wmode\" value=\"transparent\">  <embed src=\"/img/bnrs/top/$banner[pic]\" quality=\"high\" type=\"application/x-shockwave-flash\"  WMODE=\"transparent\" width=\"468\" height=\"60\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" allowScriptAccess=\"always\" /></object>" . (!empty($banner['href']) ? " </a>" : '');
               }

               if ($banner['layout'] == 'top' && empty($topLeft)) {
                  $topLeft = (!empty($banner['href']) ? " <a href=\"$banner[href]\"  title='$banner[title]' target=\"_blank\">" : '') . "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0\"  width=\"468\" height=\"60\"> <param name=\"movie\" value=\"/img/bnrs/top/$banner[pic]\" /> <param name=\"quality\" value=\"high\" /> <param name=\"allowScriptAccess\" value=\"always\" /><param name=\"wmode\" value=\"transparent\">  <embed src=\"/img/bnrs/top/$banner[pic]\" quality=\"high\" type=\"application/x-shockwave-flash\"  WMODE=\"transparent\" width=\"468\" height=\"60\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" allowScriptAccess=\"always\" /></object>" . (!empty($banner['href']) ? " </a>" : '');
               }


               if ($banner['layout'] == 'left') {
                  $left .= ( !empty($banner['href']) ? " <a href=\"$banner[href]\"  title='$banner[title]' target=\"_blank\">" : '') . "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0\" > <param name=\"movie\" value=\"/img/bnrs/left/$banner[pic]\" /> <param name=\"quality\" value=\"high\" /> <param name=\"allowScriptAccess\" value=\"always\" /><param name=\"wmode\" value=\"transparent\">  <embed src=\"/img/bnrs/left/$banner[pic]\" quality=\"high\" type=\"application/x-shockwave-flash\"  WMODE=\"transparent\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" allowScriptAccess=\"always\" /></object>" . (!empty($banner['href']) ? " </a>" : '');
               }

               if ($banner['layout'] == 'bottom') {
                  $top .= ( !empty($banner['href']) ? " <li><a href=\"$banner[href]\"  title='$banner[title]' target=\"_blank\">" : '') . "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0\"  width=\"88\" height=\"31\"> <param name=\"movie\" value=\"/img/bnrs/bottom/$banner[pic]\" /> <param name=\"quality\" value=\"high\" /> <param name=\"allowScriptAccess\" value=\"always\" /><param name=\"wmode\" value=\"transparent\">  <embed src=\"/img/bnrs/bottom/$banner[pic]\" quality=\"high\" type=\"application/x-shockwave-flash\"  WMODE=\"transparent\" width=\"88\" height=\"31\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" allowScriptAccess=\"always\" /></object>" . (!empty($banner['href']) ? " </a></li>" : '');
               }
            }

            if ($banner['type'] == 'html') {
               if ($banner['layout'] == 'top' && !empty($topLeft)) {
                  $topRight = $banner['html_code'];
               }

               if ($banner['layout'] == 'top') {
                  $topLeft = $banner['html_code'];
               }


               if ($banner['layout'] == 'left') {
                  $left .= $banner['html_code'];
               }

               if ($banner['layout'] == 'bottom') {
                  $bottom .= '<li>' . stripcslashes($banner['html_code']) . '</li>';
               }
            }
         }

         $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'TOP_BANNER_LEFT' => (!empty($topLeft) ? stripcslashes($topLeft) : '')));
         $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'TOP_BANNER_RIGHT' => (!empty($topRight) ? stripcslashes($topRight) : '')));
         $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'LEFT_BANNER' => (!empty($left) ? stripcslashes($left) : '')));
         $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'PARTNERS' => (!empty($topLeft) ? stripcslashes($bottom) : '')));


         if (!empty($topRight) || !empty($topLeft)) {
            $this->tpl->parse('INDEX_TOP_BNNRS', 'index_top_bnnrs');
         }

         if (!empty($topRight)) {
            $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'TOP_BANNER_RIGHT' => stripcslashes($topRight)));
         }

         if (!empty($left)) {
            $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'LEFT_BANNER' => stripcslashes($left)));
         }



         if (!empty($bottom)) {
            $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'PARTNERS' => stripcslashes($bottom)));
         }
      }
   }

   private function loadVerticalMenu() {
      $menu = $this->db->fetchAll("SELECT `id`, `href`, `header`, `type` FROM `page` WHERE `level` = '0' AND `menu` = 'vertical' AND `language` = '" . $this->lang . "' ORDER BY `position`, `header`");

      if ($menu) {
         foreach ($menu as $m) {
            $this->tpl->parse('VERTICAL_COMPLEX', 'null');
            $this->tpl->parse('VERTICAL_COMPLEX_SUB', 'null');

            if ($m['type'] == 'section') {
               $sub = $this->db->fetchAll("SELECT `href`, `header`, `type` FROM `page` WHERE `level` = '" . $m['id'] . "' ORDER BY `position`, `header`");
               if ($sub) {
                  foreach ($sub as $s) {
                     $url = $s['href'];
                     if ($s['type'] !== 'link') {
                        $url = $this->basePath . $m['href'] . '/' . $url;
                     }

                     $this->tpl->assign(
                             array(
                                 'MENU_HREF' => $url,
                                 'MENU_NAME' => stripslashes($s['header'])
                             )
                     );

                     $this->tpl->parse('VERTICAL_COMPLEX_SUB', '.vertical_complex_sub');
                  }
               } else {
                  $this->tpl->parse('VERTICAL_COMPLEX_SUB', 'null');
               }
            }

            $this->tpl->assign(
                    array(
                        'MENU_HREF' => $m['type'] === 'link' ? $m['href'] : $this->basePath . $m['href'],
                        'MENU_NAME' => stripslashes($m['header'])
                    )
            );

            if ($m['type'] == 'section' && $sub) {
               $this->tpl->parse('VERTICAL_COMPLEX', '.vertical_complex');
               $this->tpl->parse('VERTICAL_SINGLE', 'null');
            } else {
               $this->tpl->parse('VERTICAL_COMPLEX', 'null');
               $this->tpl->parse('VERTICAL_SINGLE', 'vertical_single');
            }

            $this->tpl->parse('VERTICAL', '.vertical');
         }
      } else {
         $this->tpl->parse('VERTICAL', 'null');
      }
   }

   private function loadCatalogMenu() {
//echo $this->regionData['kod']; die;
      $visibleQuery = '';

      if (!$this->_isAdmin()) {
         $visibleQuery = " AND `visibility` = '1'";
      }

      $menu = $this->db->fetchAll("SELECT `id`, `href`, `name`, `type` FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '0' $visibleQuery  AND   `language` = '" . $this->lang . "' ORDER BY `position` ,`name` LIMIT 10");
      //echo "SELECT `id`, `href`, `name`, `type` FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '0' $visibleQuery  AND   `language` = '" . $this->lang . "' ORDER BY `position` ,`name`";
      //die;
      if ($menu) {
         foreach ($menu as $m) {
            $this->tpl->parse('CATALOG_MENU_COMPLEX', 'null');
            $this->tpl->parse('CATALOG_MENU_COMPLEX_SUB', 'null');

            if ($m['type'] == 'section') {
               $sub = $this->db->fetchAll("SELECT `id`, `href`, `name`, `type` FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '" . $m['id'] . "' $visibleQuery ORDER BY `position` ,`name`");


               if (!in_array($m['href'], $this->url)) {
                  $sub = false;
               }

               if ($sub) {
                  foreach ($sub as $s) {
                     $url = $s['href'];
                     if ($s['type'] !== 'link') {
                        $url = $this->basePath . $m['href'] . '/' . $url;
                     }
                     
                     $url2 = explode('/',$url);
                     
                     $this->tpl->assign(
                             array(
                                 'CATALOG_MENU_HREF' => $url,
                                 'CATALOG_MENU_NAME' => stripslashes($s['name']),
                                 'CATALOG_MENU_CL'=> (end($this->url)==end ($url2)?'active':''),
                                 'CATALOG_MENU_IMG'=> (end($this->url)==end ($url2)?'two_main_menu_row_hover.jpg':'two_main_menu_row.jpg')
                             )
                     );

                     $this->tpl->parse('CATALOG_MENU_COMPLEX_SUB', '.catalog_menu_complex_sub');
                  }
               } else {
                  $this->tpl->parse('CATALOG_MENU_COMPLEX_SUB', 'null');
               }
            }

            $this->tpl->assign(
                    array(
                        'CATALOG_MENU_HREF' => $m['type'] === 'link' ? $m['href'] : $this->basePath . $m['href'],
                        'CATALOG_MENU_NAME' => stripslashes($m['name'])
                    )
            );

            if ($m['type'] == 'section' && $sub) {
               $this->tpl->parse('CATALOG_MENU_COMPLEX', '.catalog_menu_complex');
               $this->tpl->parse('CATALOG_MENU_SINGLE', 'null');
            } else {
               $this->tpl->parse('CATALOG_MENU_COMPLEX', 'null');
               $this->tpl->parse('CATALOG_MENU_SINGLE', 'catalog_menu_single');
            }

            $this->tpl->parse('CATALOG_MENU', '.catalog_menu');
         }
      } else {
         $this->tpl->parse('CATALOG_MENU', 'null');
      }
   }

   protected function _isAdmin() {
      if (null === $this->auth) {
         return false;
      }

      if (!isset($this->auth->privilege) || $this->auth->privilege !== 'admin') {
         return false;
      }

      return true;
   }

   protected function setMetaTags($meta = null) {
      if (null === $meta) {
         return true;
      }

	  
		$url1 = end($this->url);

		if(preg_match("/catalog/",$_SERVER[REQUEST_URI]) && !preg_match("/listABC/",$_SERVER[REQUEST_URI])){
			$metas = $this->db->fetchRow("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `href` = '$url1'");
		//print_r($metas);
			
			if ($_COOKIE[ua]!=1){
			$this->tpl->assign(
					 array(
						 'TITLE' => stripslashes($metas['name']),
						 'KEYWORDS' => isset($metas['keywords']) ? stripslashes($metas['keywords']) : stripslashes($metas['name']),
						 'DESCRIPTION' => isset($metas['description']) ? stripslashes($metas['description']) : stripslashes($metas['name']),
						 'HEADER' => isset($metas['header']) ? stripslashes($metas['header']) : stripslashes($metas['name'])
					 )
			 );
			 }
			 else{
			 $this->tpl->assign(
					 array(
						 'TITLE' => stripslashes($metas['name_ua']),
						 'KEYWORDS' => isset($metas['keywords_ua']) ? stripslashes($metas['keywords_ua']) : stripslashes($metas['name_ua']),
						 'DESCRIPTION' => isset($metas['description_ua']) ? stripslashes($metas['description_ua']) : stripslashes($metas['name_ua']),
						 'HEADER' => isset($metas['header_ua']) ? stripslashes($metas['header_ua']) : stripslashes($metas['name_ua'])
					 )
			 );
			 }
		}
		else{
	  
      if (is_array($meta) && isset($meta['title'])) {
	//  print_r($meta);

	 if ($_COOKIE[ua]!=1){
	

         $this->tpl->assign(
                 array(
                     'TITLE' => stripslashes($meta['title']),
                     'KEYWORDS' => isset($meta['keywords']) ? stripslashes($meta['keywords']) : stripslashes($meta['title']),
                     'DESCRIPTION' => isset($meta['description']) ? stripslashes($meta['description']) : stripslashes($meta['title']),
                     'HEADER' => isset($meta['header']) ? stripslashes($meta['header']) : stripslashes($meta['title'])
                 )
         );
	}
	else{
					$this->tpl->assign(
							 array(
								 'TITLE' => stripslashes($meta['title_ua']),
								 'KEYWORDS' => isset($meta['keywords_ua']) ? stripslashes($meta['keywords_ua']) : stripslashes($meta['title_ua']),
								 'DESCRIPTION' => isset($meta['description_ua']) ? stripslashes($meta['description_ua']) : stripslashes($meta['title_ua']),
								 'HEADER' => isset($meta['header_ua']) ? stripslashes($meta['header_ua']) : stripslashes($meta['title_ua'])
							 )
					 );
	}
	
				 if(count($this->url)<4){
								if ($_COOKIE[ua]==1){
											if($this->url[2]==""){$url_this=$this->url[1];} else {$url_this=$this->url[2];}
										$metag = $this->db->fetchRow("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `href` = '" . $url_this . "'");
										//print_r($metag);
										$this->tpl->assign(
											 array(
												 'HEADER' => isset($metag['name_ua']) ? stripslashes($metag['name_ua']) : stripslashes($meta['title'])
													)
												);
										}	 
										
							}
							
				if(count($this->url)<2){
								if ($_COOKIE[ua]==1){
											$url_this=$this->url[1];
										$metag = $this->db->fetchRow("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `href` = '" . $url_this . "'");
										//print_r($metag);
										$this->tpl->assign(
											 array(
												 'HEADER' => isset($metag['name_ua']) ? stripslashes($metag['name_ua']) : stripslashes($meta['title_ua'])
													)
												);
										}	 
										
							}
							
	//	 print_r($meta);
      } else {
		//	print_r($meta);
			$url1 = end($this->url);
				if($url1=="index"){
				$url1="mainpage";
					$metas = $this->db->fetchRow("SELECT * FROM `page` WHERE `href` = '$url1' ");
					
					 $this->tpl->assign(
							 array(
								 'TITLE' => stripslashes($metas['title_ua']),
								 'KEYWORDS' => isset($metas['keywords_ua']) ? stripslashes($metas['keywords_ua']) : stripslashes($metas['title_ua']),
								 'DESCRIPTION' => isset($metas['description_ua']) ? stripslashes($metas['description_ua']) : stripslashes($metas['title_ua']),
								 'HEADER' => isset($metas['header_ua']) ? stripslashes($metas['header_ua']) : stripslashes(title_ua)
							 )
					 );
				}
				else{
			 $this->tpl->assign(
					 array(
						 'TITLE' => $meta,
						 'KEYWORDS' => $meta,
						 'DESCRIPTION' => $meta,
						 'HEADER' => $meta
					 )
			 );
		}
      }
	 }
   }

   protected function setWay($title = null, $url = null) {
      if (null === $title) {
         return true;
      }

      $sep = ($this->_way) ? $this->sepWay : '';

      if (null === $url) {
         $this->_way .= $sep . stripslashes($title);
      } else {
         $this->_way .= ( $sep . '<a href="' . $url . '">' . stripslashes($title) . '</a>');
      }
   }

   protected function generateWayFromUri() {
      if (sizeof($this->url) < 2) {
         return true;
      }

      $arrayUrl = $this->url;
      array_pop($arrayUrl);

      $in = '';

      foreach ($arrayUrl as $uri) {
         $in .= '"' . $uri . '",';
      }

      $in = substr($in, 0, -1);

      if (!$in) {
         return true;
      }

      $pages = $this->db->fetchAll("SELECT * FROM `page` WHERE `language` = '" . $this->lang . "' AND `href` IN ($in)");

      if (sizeof($pages) != sizeof($this->url) - 1) {
         return $this->error404();
      }

      $href = $this->basePath;
	  
		if ($_COOKIE[ua]!=1){
			  foreach ($pages as $page) {
				 $href .= $page['href'] . '/';
				 $this->setWay(stripslashes($page['header']), $href);
			  }
		}
		else{
			foreach ($pages as $page) {
				 $href .= $page['href'] . '/';
				 $this->setWay(stripslashes($page['header_ua']), $href);
			  }
		}
   }

   protected function getAdminEdit($target = null, $id = null, $hasDelete = true) {
      if (!$this->_isAdmin()) {
         return '';
      }

      if (null === $target || null === $id) {
         return '';
      }

      $admin = '<a href="' . $this->basePath . 'admin/edit' . $target . '/' . $id . '" title="Редактировать"><img src="/img/admin_icons/edit.png" width="12" height="12" alt="Редактировать" /></a>';

      if ($hasDelete) {
         $admin .= '&nbsp;&nbsp;';
         $admin .= '<a href="' . $this->basePath . 'admin/delete' . $target . '/' . $id . '" title="Удалить" onClick="return confirm(\'Вы уверены что хотите удалить?\'); return false;"><img src="/img/admin_icons/delete.png" width="12" height="12" alt="Удалить" /></a>&nbsp;&nbsp;';
      }

      return $admin;
   }

   protected function getAdminAdd($target = null, $id = null) {
      if (!$this->_isAdmin()) {
         return '';
      }

      if (null === $target) {
         return '';
      }

      if ($target == 'news') {
         $admin = '<a href="' . $this->basePath . 'admin/addnews/" title="Добавить новость"><img src="/img/admin_icons/add_page.png" border="0" alt="Добавить новость"></a>';
      } elseif ($target == 'section_meta_tags') {
         $admin = '<a href="' . $this->basePath . 'admin/section_meta_tags/' . $id . '" title="Добавить метатеги для раздела"><img src="/img/admin_icons/add_page.png" border="0" alt="Добавить метатеги для раздела"></a>';

         return $admin;
      } elseif ($target == 'catsection') {
         $admin = '<a href="' . $this->basePath . 'admin/addcatsection/' . $id . '" title="Создать раздел"><img src="/img/admin_icons/add_group.png" border="0" alt="Создать раздел"> Создать раздел</a>&nbsp;';
         return $admin;
      } elseif ($target == 'catpage') {
         $admin = '<a href="' . $this->basePath . 'admin/addcatpage/' . $id . '" title="Добавить товар"><img src="/img/admin_icons/add_page.png" border="0" alt="Добавить товар">Добавить товар</a>&nbsp;';
         return $admin;
      } else {
         if (null === $id) {
            return '';
         }

         $admin = '<a href="' . $this->basePath . 'admin/addsection/' . $id . '" title="Создать раздел"><img src="/img/admin_icons/add_group.png" border="0" alt="Создать раздел"></a>&nbsp;';
         $admin .= '<a href="' . $this->basePath . 'admin/addpage/' . $id . '" title="Создать страницу"><img src="/img/admin_icons/add_page.png" border="0" alt="Создать страницу"></a>&nbsp;';
         $admin .= '<a href="' . $this->basePath . 'admin/addlink/' . $id . '" title="Создать ссылку"><img src="/img/admin_icons/add_link.png" border="0" alt="Создать ссылку"></a>';
      }

      return $admin;
   }

   protected function convertDate($date = null, $format = "d.m.Y") {
      if (null === $date || !is_numeric($date)) {
         $date = mktime();
      }

      $d = new Zend_Date($date, false, $this->_locale);

      return $d->toString($format);
   }

   protected function loadPaginator($allPages = null, $pages = null, $url = null) {
       
      if (null === $allPages || null === $pages || null === $url) {
         return '';
      }

      if ($pages > $allPages) {
         $pages = 1;
      }

      $symbol = (preg_match("/\?/", $url) ? '&' : '?');

      $add = $del = 0;
      $start = $pages - 10;


      if ($start < 1) {
         $add = abs($start);
         $start = 1;
      }

      $end = $pages + 10 + $add;

      if ($end > $allPages) {
         $del = $end - $allPages;
         $end = $allPages;
         $start = ($start - $del > 0) ? $start - $del : 1;
      }

      $navbar = '<div class="page_p">';


	  if ($_COOKIE[ua]!=1){
                      if ($pages > 1) {
						 $navbar .= '<a class="nomarg" href="' . $url . '?vis='.$_GET[vis].'&manuf='.$_GET[manuf].'">Первая страница</a>&nbsp;&nbsp;';
						 $navbar .= '<a href="' . $url . ((($pages - 1) == 1) ? ("") : ($symbol . "page=" . ($pages - 1))) .((($pages - 1) == 1) ? ("?") : ("&")). '&vis='.$_GET[vis].'&manuf='.$_GET[manuf].'" class="an"><<</a>&nbsp;&nbsp;';
					  }
		}
			else{
			$vopr="?";
			$amp="&";
					      if ($pages > 1) {
							 $navbar .= '<a class="nomarg" href="' . $url . '?vis='.$_GET[vis].'&manuf='.$_GET[manuf].'">Перша сторінка</a>&nbsp;&nbsp;';
							 $navbar .= '<a href="' . $url . ((($pages - 1) == 1) ? ("") : ($symbol . "page=" . ($pages - 1))) .((($pages - 1) == 1) ? ("?") : ("&")). 'vis='.$_GET[vis].'&manuf='.$_GET[manuf].'" class="an"><<</a>&nbsp;&nbsp;';
						  }
				}
	  
	for ($i = $start; $i <= $end; $i++) {
         if ($pages == $i) {
            $navbar .= '<span '.($i==1?'class="nomarg"':'').'>' . $i . '</span>';
         } elseif ($i<=$pages+2 and $i>=$pages-2) {
             $navbar .= '<a href="' . $url . $symbol . "page=" . $i . '&vis='.$_GET[vis].'&manuf='.$_GET[manuf].'">' . $i . '</a>';   
         }
         
         //} elseif ($i > 1) {
         //   $navbar .= '<a href="' . $url . $symbol . "page=" . $i . '">' . $i . '</a>';
         //} else {
         //   $navbar .= '<a href="' . $url . '">' . $i . '</a>';
         //}
      }
      //echo $pages;

 	  	if ($_COOKIE[ua]!=1){
                if ($pages < $allPages) {
					$navbar .= '<a href="' . $url . $symbol . "page=" . ($pages + 1) . '&vis='.$_GET[vis].'&manuf='.$_GET[manuf].'" class="an">>></a>&nbsp;&nbsp;';
					$navbar .= '<a href="' . $url . $symbol . "page=" . $allPages . '&vis='.$_GET[vis].'&manuf='.$_GET[manuf].'">Последняя страница</a>';
				}        
		}
			else{
					if ($pages < $allPages) {
						 $navbar .= '<a href="' . $url . $symbol . "page=" . ($pages + 1) . '&vis='.$_GET[vis].'&manuf='.$_GET[manuf].'" class="an">>></a>&nbsp;&nbsp;';
						 $navbar .= '<a href="' . $url . $symbol . "page=" . $allPages . '&vis='.$_GET[vis].'&manuf='.$_GET[manuf].'">Остання сторінка</a>';
					  }
				}

      $navbar .= "</div>";

      return $navbar;
   }

   protected function mergeUrlArray() {
      $url = $this->basePath;

      foreach ($this->url as $uri) {
         $url .= $uri . '/';
      }

      return $url;
   }


   public function finalise() {
      echo $url;
      $this->tpl->assign('WAY', $this->_way);
      $this->tpl->assign('BASE_PATH', $this->basePath);
      $this->tpl->parse('PAGE', 'page');
      $this->tpl->prnt();
   }

   public function error404() {
      $error = $this->db->fetchRow("SELECT `body`, `header`, `title`, `keywords`, `description` FROM `system_pages` WHERE `href` = '404' AND `language` = '" . $this->lang . "'");

      if (!$error) {
         throw new Exception('Unexpected Error');
      }

      $this->setMetaTags($error);
      $this->setWay($error['header']);

      $this->tpl->assign('CONTENT', stripslashes($error['body']));

      return true;
   }
   
      public function no_data() {
      $error = $this->db->fetchRow("SELECT `body`, `header`, `title`, `keywords`, `description` FROM `system_pages` WHERE `href` = 'no_data' AND `language` = '" . $this->lang . "'");

      if (!$error) {
         throw new Exception('Unexpected Error');
      }

      $this->setMetaTags($error);
      $this->setWay($error['header']);

      $this->tpl->assign('CONTENT', stripslashes($error['body']));

      return true;
   }

   protected function ru2Lat($str) {

      $rus = array('ё', 'ж', 'ц', 'ч', 'ш', 'щ', 'ю', 'я', 'Ё', 'Ж', 'Ц', 'Ч', 'Ш', 'Щ', 'Ю', 'Я', 'Ї', 'ї', 'Є', 'є', 'І', 'і', 'ь', 'Ь', 'Ъ', 'ъ');
      $lat = array('yo', 'zh', 'tc', 'ch', 'sh', 'sh', 'yu', 'ya', 'YO', 'ZH', 'TC', 'CH', 'SH', 'SH', 'YU', 'YA', 'YI', 'yi', 'E', 'e', 'I', 'i', '', '', '', '');
      $prototype = array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '-', '_', ' ', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', ':', '/', '.', '?', '&');

      /* if ($type == 'link') {
        array_push($prototype, ':', '/', '.', '?', '&');
        } */

      $str = str_replace($rus, $lat, $str);

      $str = strtr(iconv('utf-8', 'cp1251', $str), iconv('utf-8', 'cp1251', "АБВГДЕЗИЙКЛМНОПРСТУФХЪЫЬЭабвгдезийклмнопрстуфхыэ"), "ABVGDEZIJKLMNOPRSTUFH_I_Eabvgdezijklmnoprstufhie");

      $size = strlen($str);

      $temp = "";
      for ($i = 0; $i < $size; $i++) {
         if (in_array($str[$i], $prototype))
            $temp .= $str[$i];
      }

      $str = $temp;

      //$str = str_ireplace(' ', '-', $str);
      $str = trim($str);
      return (strtolower($str));
   }

   protected function getVar($key = null, $def = null) {
      if (isset($_POST[$key])) {
         return (isset($_POST[$key]) ? addslashes(trim($_POST[$key])) : $def);
      } elseif (isset($_GET[$key])) {
         return (isset($_GET[$key]) ? addslashes(trim($_GET[$key])) : $def);
      } elseif (isset($_FILES[$key])) {
         return (isset($_FILES[$key]) ? $_FILES[$key] : $def);
      } elseif (isset($_SERVER[$key])) {
         return (isset($_SERVER[$key]) ? addslashes($_SERVER[$key]) : $def);
      } else {

         return $def;
      }
   }

   protected function gp($arr, $name, $def=null) {
      if (isset($arr[$name])) {
         if (is_string($arr[$name])) {
            if (!get_magic_quotes_gpc()) {
               $arr[$name] = htmlspecialchars(addslashes($arr[$name]));
            }
         }
         return $arr[$name];
      } else {
         return $def;
      }
   }

   protected function gpm($arr, $name, $def=null) {
      if (isset($arr[$name])) {
         return $arr[$name];
      } else {
         return $def;
      }
   }

   protected function addErr($str = null) {
      if (null !== $str) {
         if ($this->_err != '') {
            $this->_err .= '<br />';
         }

         $this->_err .= $str;
      }
   }

   protected function viewErr() {
      if ($this->_err != '') {
	  
$ua=$this->getUa();
         $this->tpl->define_dynamic('_err', "$ua".'err.tpl');
         $this->tpl->define_dynamic('err', '_err');
         $this->tpl->assign('ERR', $this->_err);
         $this->tpl->parse('CONTENT', 'err');
         //return "<div style='color: red; font: Bold 11px Tahoma; padding: 0 0 35px 0;'>".$this->_err.'</div>';
      }

      return '';
   }

   protected function viewErrBasket() {
      if ($this->_err != '') {
	  
	  $ua=$this->getUa();
         $this->tpl->define_dynamic('_err', "$ua".'err.tpl');
         $this->tpl->define_dynamic('err', '_err');
         $this->tpl->assign('ERR', $this->_err);
         $this->tpl->parse('BASKET_FORM_ERR', 'err');
         //return "<div style='color: red; font: Bold 11px Tahoma; padding: 0 0 35px 0;'>".$this->_err.'</div>';
      }

      return '';
   }
   
   protected function viewMessage($message) {
   $ua=$this->getUa();
      $this->tpl->define_dynamic('_err', "$ua".'err.tpl');
      $this->tpl->define_dynamic('mess', '_err');
      $this->tpl->assign('ERR', $message);
      $this->tpl->parse('CONTENT', '.mess');
   }

   // Получение информации о товаре по ссылке или id
   protected function dataTreeManager($id, $options = array(), $items=array()) {

      if (!is_numeric($id)) {
         return false;
      }

      $id = (int) $id;

      if ($id <= 0) {
         return false;
      }

      $fields = (isset($options['fields']) ? $options['fields'] : '`id`, `href`, `name`, `name_ua`, `level` , `type`');
      $retValues = (isset($options['ret']) ? $options['ret'] : array('name', 'name_ua', 'href'));
      $subLevel = (isset($options['sub-level']) ? $options['level'] : array());
      $tableName = (isset($options['table']) ? $options['table'] : '`'.$this->regionData['kod'].'_catalog`');


      if (count($items) <= 0) {
         $items = $this->db->fetchAll("SELECT $fields FROM $tableName");
      }

      if (!$items) {
         return false;
      }

      $retArr = array();
      $counter = 0;
      $i = 0;
      $isRun = true;
      $linkArr = array();
      $level = 1;
      $nameArr = array();
      $argId = $id;

      while (true) {

         if (isset($items[$i]['id']) && $items[$i]['id'] == $id) {
            if ((is_array($retValues) && in_array('href', $retValues)) || $retValues == 'href' || $retValues == 'all') {
               if ($items[$i]['type'] != 'href') {

                  if (isset($options['level']['href'])) {
                     if ($options['level']['href'] == $level) {
                        $linkArr[] = $items[$i]['href'];
                     }
                  } else {
                     $linkArr[] = $items[$i]['href'];
                  }
               }
            }

            if ((is_array($retValues) && in_array('data', $retValues)) || $retValues == 'data' || $retValues == 'all') {

               if (isset($options['level']['data'])) {
                  if ($options['level']['data'] == $level) {
                     $retArr[] = $items[$i];
                  }
               } else {
                  $retArr[] = $items[$i];
               }
            }

		if ($_COOKIE[ua]!=1){
            if ((is_array($retValues) && in_array('name', $retValues)) || $retValues == 'name' || $retValues == 'all') {

               if (isset($options['level']['name'])) {
                  if ($options['level']['name'] == $level) {
                     $nameArr[] = $items[$i]['name'];
                  }
               } else {
                  $nameArr[] = $items[$i]['name'];
               }
            }
		}
		else{
		            if ((is_array($retValues) && in_array('name_ua', $retValues)) || $retValues == 'name_ua' || $retValues == 'all') {

               if (isset($options['level']['name_ua'])) {
                  if ($options['level']['name_ua'] == $level) {
                     $nameArr[] = $items[$i]['name_ua'];
                  }
               } else {
                  $nameArr[] = $items[$i]['name_ua'];
               }
            }
		
		}


            $id = $items[$i]['level'];


            if ($items[$i]['level'] == 0) {

               break;
            }

            $level++;
            $i = 0;
         } elseif (isset($items[$i]['id'])) {

            $i++;
         } else {
            break;
         }


         $counter++;
         if ($counter > 1000000) {
            break;
         }
      }

      if (count($nameArr) > 0) {

         $retArr['links'] = (count($linkArr > 0) ? implode('/', array_reverse($linkArr)) : '');
         $retArr['linksArr'] = array_reverse($linkArr);
         $retArr['names'] = array_reverse($nameArr);
         return $retArr;
      }

      return false;
   }

   protected function isBuyButtomType($goodsId) {
      $goodsSession = new Zend_Session_Namespace('goods');

      return (!isset($goodsSession->array[$goodsId]) || $goodsSession->array[$goodsId]['status'] == 'deleted');
   }

   public function getDb() {
      return $this->db;
   }

   protected function getCatalogList($fields='*') {
      return $this->db->fetchAll("SELECT $fields FROM `".$this->regionData['kod']."_catalog`");
      
   }

   /*Выводит список регионов*/
   
     
   private function listUserRegion(){
       
       $region = $this->db->fetchAll("SELECT `url`, `name`, `name_ua` FROM `regions` WHERE `language`='".$this->lang."' AND `visibility`='1' ORDER BY `name`");
       
						if ($_COOKIE[ua]!=1){
									   $link = '<a href="#" id="choose">'.$this->regionData['name'].'<!--Выбрать регион--></a>
									   <div class="region_list">
										<ul>';
										   foreach ($region as $key) {
											   $link .= '<li><a href="/'.$key['url'].'?auto=1">'.$key['name'].'</a></li>';
										   }
										   $link .= '</ul></div>'; 
										   $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'LIST_REGIONS' => $link));
				}
					else{
									   $link = '<a href="#" id="choose">'.$this->regionData['name_ua'].'<!--Выбрать регион--></a>
									   <div class="region_list">
										<ul>';
										   foreach ($region as $key) {
											   $link .= '<li><a href="/'.$key['url'].'?auto=1">'.$key['name_ua'].'</a></li>';
										   }
										   $link .= '</ul></div>'; 
										   $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'LIST_REGIONS' => $link));
						}

	   

   }
   /*
    * Последние новости на главной
    */
    protected function lastNewsToMain() {
        if (!isset($this->url[0]) || $this->url[0] != 'index') {
            return;
        }
        $sql= "SELECT * FROM `news` WHERE `visibility`='1' ORDER BY `date` DESC LIMIT 3";
        $items = $this->db->fetchAll($sql);
        if ($items) {
            foreach ($items as $items_news) {
            $this->tpl->assign(
                array(
                    'LAST_NEWS_INNER_DATE' => date('d.m.Y',$items_news['date']),
                    'LAST_NEWS_INNER_HEADER' => $items_news['header'],
                    'LAST_NEWS_INNER_DESC' => $items_news['preview'],
                    'LAST_NEWS_INNER_LINK' => '/news/'.$items_news['href'],
                    //'LAST_NEWS_INNER_CLASS' => ($z==3?'last':''),
                    'LAST_NEWS_INNER_PIC' => (!empty($items_news['pic'])?'<a href="/news/'.$items_news['href'].'"><img src="/img/news/'.$items_news['pic'].'" width="84" height="73" class="float_left" alt="" /></a>':'')
                    )
                );
                $this->tpl->parse('LAST_NEWS_INNER', '.last_news_inner');
            }
        }
    }
    private function getRegionData($regionUrl){
        $res = $this->db->fetchRow("SELECT * FROM `regions` WHERE `url`='$regionUrl'");
        if ($res==''){
            $res = $this->db->fetchRow("SELECT * FROM `regions` WHERE `kod`='04'");
        } 
        $this->tpl->assign(
                array(
                    'REGION_URL' => $res['url'].'/',
                    'REGION_NAME' => $res['name']
                    )
                );
        return $res;
    }
    
    private function loadLeftCatalogMenu() {
        $visibleQuery = '';
        if (!$this->_isAdmin()) {
            $visibleQuery = " AND `visibility` = '1'";
        }
        //ID Лекарственных препаратов
        $sectionID = $this->db->fetchRow("SELECT `id`,`href` FROM `".$this->regionData['kod']."_catalog` WHERE `name`='Лекарственные препараты'");

		if ($_COOKIE[ua]!=1){
						 $menu = $this->db->fetchAll("SELECT `id`, `href`, `name`, `type` FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '".$sectionID['id']."' $visibleQuery  AND   `language` = '" . $this->lang . "' ORDER BY `position` ,`name`");
						 
						         if ($menu) {
									foreach ($menu as $m) {
										$this->tpl->assign(
											array(
												'CATALOG_LEFT_MENU_HREF' => $m['type'] === 'link' ? $m['href'] : '/' . $this->regionData['url'] . '/catalog/'. $sectionID['href']. '/' . $m['href'],
												'CATALOG_LEFT_MENU_NAME' => stripslashes($m['name'])
											)
										);
										$this->tpl->parse('CATALOG_LEFT_MENU', '.catalog_left_menu');
									}
								} else {
									$this->tpl->parse('CATALOG_LEFT_MENU', 'null');
								}
						}
							else{
							$menu = $this->db->fetchAll("SELECT `id`, `href`, `name_ua`, `type` FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '".$sectionID['id']."' $visibleQuery  AND   `language` = '" . $this->lang . "' ORDER BY `position` ,`name_ua`");  

									if ($menu) {
										foreach ($menu as $m) {
											$this->tpl->assign(
												array(
													'CATALOG_LEFT_MENU_HREF' => $m['type'] === 'link' ? $m['href'] : '/' . $this->regionData['url'] . '/catalog/'. $sectionID['href']. '/' . $m['href'],
													'CATALOG_LEFT_MENU_NAME' => stripslashes($m['name_ua'])
												)
											);
											$this->tpl->parse('CATALOG_LEFT_MENU', '.catalog_left_menu');
										}
									} else {
										$this->tpl->parse('CATALOG_LEFT_MENU', 'null');
									}
							
								}
								

    }
    
    

/*
 * Лекарственные препараты
 */
    private function loadTOPCatalogMenuLP($query, $endTemp){

        $res = $this->db->fetchRow("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `name`='$query'");
        if (!empty($res['id'])) {
		
				if ($_COOKIE[ua]!=1){
				$resSection = $this->db->fetchAll("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `level`='".$res['id']."' ORDER BY `name`");
				}
				else{
					$resSection = $this->db->fetchAll("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `level`='".$res['id']."' ORDER BY `name_ua`");
					}
					
        } else {
            $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'CATALOG_TOP_MENU_'.$endTemp =>''));
            return true;
        }
        $countM = ceil((count($resSection))/3);
        $z=0;
        $y=0;
        $menu = '<ul><li><ul>';
        foreach ($resSection as $key) {
            $z++;
			
				if ($_COOKIE[ua]!=1){
				$menu .= '<li><a href="/'.$this->regionData['url'].'/catalog/'.$res['href'].'/'.$key['href'].'">'.$key['name'].'</a></li>';
			}
				else{
				$menu .= '<li><a href="/'.$this->regionData['url'].'/catalog/'.$res['href'].'/'.$key['href'].'">'.$key['name_ua'].'</a></li>';
					}

            
            if ($z==$countM){
                $y++;
                if ($y<3)
                $menu .= '</ul></li><li><ul>';
                $z=0;
                
            }
        }
        $menu .= '</ul></li></ul>';
        
        $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'CATALOG_TOP_MENU_'.$endTemp =>$menu));
    }
   
    
    private function basketStart() {
        $count=0;
        $summ=0;
        $goodsSession = new Zend_Session_Namespace('goods');
        //var_dump($goodsSession->array);
        if (!empty($goodsSession->array)) {
            foreach ($goodsSession->array as $index=>$basket) {   	
                if (isset($basket['status']) && $basket['status'] == 'active' && $basket['region']==$this->regionData['kod']) {
        	
        	if (is_numeric($basket['count']) && $basket['count'] > 0) {
        	
        		$summ +=  ( $basket['cost']  * $basket['count']);   
        		$count ++;        		
        		$count += ($basket['count'] - 1);
                        
        	} else {
        		
        		$goodsSession->array[$index]['status'] = 'deleted';
        	}
        	
    	}	
    	$isBasketEmpty = false;
    }
}

if (isset ($summ) and $summ > 0) {
	$goodsSession->array[$index]['summ']  = $summ;
}

	

if ($count <= 0) {
	if ($_COOKIE[ua]!=1){
	$basket = '<div class="busket">
	   <p class="tit"><a href="#" style="cursor:default;">Ваша Корзина</a></p>
	   <p><a href="#" style="cursor:default;">Товаров:</a><span><a href="#" style="cursor:default;">0 шт.</a></span></p> 
	   <p><a href="#" style="cursor:default;">На сумму:</a><span><a href="#" style="cursor:default;">0 грн.</a></span></p>
	   <p class="center"><a href="#" style="cursor:default;">Корзина пуста</a></p>
	  </div>';
		}
			else{
				$basket = '<div class="busket">
				<p class="tit"><a href="#" style="cursor:default;">Ваш Кошик</a></p>
				<p><a href="#" style="cursor:default;">Товарів:</a><span><a href="#" style="cursor:default;">0 шт.</a></span></p> 
				<p><a href="#" style="cursor:default;">На суму:</a><span><a href="#" style="cursor:default;">0 грн.</a></span></p>
				<p class="center"><a href="#" style="cursor:default;">Кошик пустий</a></p>
				</div>';
				}
  } else {
		if ($_COOKIE[ua]!=1){
		$basket = '<div class="busket busket_bg">
		   <p class="tit"><a href="/'.$this->regionData['url'].'/basket/">Ваша Корзина</a></p>
		   <p><a href="#">Товаров:</a><span><a href="/'.$this->regionData['url'].'/basket/" id="basket-count">'.$count.' шт.</a></span></p>
		   <p><a href="#">На сумму:</a><span><a href="/'.$this->regionData['url'].'/basket/" id="basket-summ">'.$summ.' грн.</a></span></p>
		   <p class="order"><a href="/'.$this->regionData['url'].'/basket/">Оформить заказ</a></p>
		  </div>';
	  }
		else {
			$basket = '<div class="busket busket_bg">
		   <p class="tit"><a href="/'.$this->regionData['url'].'/basket/">Ваш Кошик</a></p>
		   <p><a href="#">Товарів:</a><span><a href="/'.$this->regionData['url'].'/basket/" id="basket-count">'.$count.' шт.</a></span></p>
		   <p><a href="#">На суму:</a><span><a href="/'.$this->regionData['url'].'/basket/" id="basket-summ">'.$summ.' грн.</a></span></p>
		   <p class="order"><a href="/'.$this->regionData['url'].'/basket/">Ваше Замовлення</a></p>
		  </div>';
		
			}
}
$this->tpl->assign(
                    array(
                        'BASKET_START' => $basket
                    )
                );



    }
    protected function printListSearchABC(){
		if ($_COOKIE[ua]!=1){
			$abc = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Э', 'Ю', 'Я', '0-9', 'A-Z');
		}
		else{
			$abc = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Є', 'Ж', 'З', 'И', 'І', 'Ї', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ю', 'Я', '0-9', 'A-Z');
		}
	   $str = '';
        foreach ($abc as $value) {
            $str .= '<a href="/'.$this->regionData['url'].'/catalog/listABC/'.$value.'/">'.$value.'</a>';
        }
        $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'ABC' => $str));
    }
    protected function alertChangeRegion(){
	
	if ($_COOKIE[ua]!=1){
		$alert_change_region='<script>alert ("Вы сменили свой регион с '.$_SESSION['last_region']['name'].' на '.$this->regionData['name'].'. Наличие товара и цены могут измениться.");</script>';
	}
		else{
			$alert_change_region='<script>alert ("Ви змінили свій регіон з '.$_SESSION['last_region']['name'].' на '.$this->regionData['name'].'. Наявність товару и ціни можуть змінитися.");</script>';			
			}
	
        if (!empty ($_SESSION['last_region'])){
            if ($_SESSION['last_region']['kod'] != $this->regionData['kod']) {
                if (isset ($_GET['auto']) and $_GET['auto']!=1)
                    $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'ALERT_REGION' => $alert_change_region));
                $_SESSION['last_region'] = $this->regionData;
            }
        } else {
            $_SESSION['last_region'] = $this->regionData;
            $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'ALERT_REGION' => ''));
        }
        return true;
    }
    private function loadPoll() {
        $resPoll = $this->db->fetchRow("SELECT * FROM `poll` WHERE `active`=1");
        if ($resPoll) {
            $ans = '';
			if ($_COOKIE[ua]!=1){
				for ($i=1; $i<6; $i++){
					if ($resPoll['q'.$i]!='')
					$ans .= '<li>
					<div>
					<input class="ui-helper-hidden-accessible" value="'.$i.'" id="radio_'.$i.'" name="poll[]" type="radio">
					<label class="ui-radio" for="radio_'.$i.'">'.$resPoll['q'.$i].'</label>
					</div>
					</li>';
				}
			}
			else{
					for ($i=1; $i<6; $i++){
					if ($resPoll['q'.$i]!='')
					$ans .= '<li>
					<div>
					<input class="ui-helper-hidden-accessible" value="'.$i.'" id="radio_'.$i.'" name="poll[]" type="radio">
					<label class="ui-radio" for="radio_'.$i.'">'.$resPoll['q'.$i.'_ua'].'</label>
					</div>
					</li>';
				}
			}
            //if (isset ($_SESSION['pollTime']) and $_SESSION['pollTime']>(time()-86400)){
             if (isset ($_COOKIE['pollTime']) and $_COOKIE['pollTime']=='yes'){
                $res = $this->db->fetchRow("SELECT * FROM `poll` WHERE `active`=1");
				
				if ($_COOKIE[ua]!=1){
                $ans = '';
                $countQ=0;
                $summ = $res['a1']+$res['a2']+$res['a3']+$res['a4']+$res['a5'];
                for ($i=1;$i<6;$i++){
                    if ($res['a'.$i]!='') {
                        $countQ+=$res['a'.$i];
                    }
                }
                for ($i=1;$i<6;$i++){
                    if ($res['q'.$i]!='') {
                        $proc = 0;
                        if ($res['a'.$i]!=0 and $res['a'.$i]!=''){
                            $proc = ($res['a'.$i]/$countQ)*100;
                        }
                        $ans .='<li>'.$res['q'.$i].' <br> <span style="background-color:#3c7dc5; height:8px; margin-top:5px; width:'.round($proc).'px;">'.($res['a'.$i]>0?'&nbsp;':'').'</span>'.($res['a'.$i]>0?'&nbsp;&nbsp;':'').round($proc).'% ('.$res['a'.$i].')</li>';
                    }
                }

				
                $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'POLL' => '<div class="poll_tit">'.$res['name'].'</div>
				  <div class="poll_block">
				<form action="#" id="pollForm" method="post">
				<ul>
				'.$ans.'
					<li><br><b>Всего голосов: '.$countQ.'</b></li>
				</ul>
				</form>
				   <div class="clear"></div>
				   
				  </div><div class="bottom_corn"></div>'));
				}
			else{
			    $ans = '';
                $countQ=0;
                $summ = $res['a1']+$res['a2']+$res['a3']+$res['a4']+$res['a5'];
                for ($i=1;$i<6;$i++){
                    if ($res['a'.$i]!='') {
                        $countQ+=$res['a'.$i];
                    }
                }
                for ($i=1;$i<6;$i++){
                    if ($res['q'.$i.'_ua']!='') {
                        $proc = 0;
                        if ($res['a'.$i]!=0 and $res['a'.$i]!=''){
                            $proc = ($res['a'.$i]/$countQ)*100;
                        }
                        $ans .='<li>'.$res['q'.$i.'_ua'].' <br> <span style="background-color:#3c7dc5; height:8px; margin-top:5px; width:'.round($proc).'px;">'.($res['a'.$i]>0?'&nbsp;':'').'</span>'.($res['a'.$i]>0?'&nbsp;&nbsp;':'').round($proc).'% ('.$res['a'.$i].')</li>';
                    }
                }
				
                 $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'POLL' => '<div class="poll_tit">'.$res['name_ua'].'</div>
				  <div class="poll_block">
				<form action="#" id="pollForm" method="post">
				<ul>
				'.$ans.'
					<li><br><b>Всього голосів: '.$countQ.'</b></li>
				</ul>
				</form>
				   <div class="clear"></div>
				   
				  </div><div class="bottom_corn"></div>'));
				}

            
            } else {
            
			
			if ($_COOKIE[ua]!=1){
            $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'POLL' => '

				 <div class="poll_tit">'.$resPoll['name'].'</div>
				  <div class="poll_block">
				<form action="#" id="pollForm" method="post">
				<ul>
				'.$ans.'
				</ul>
				</form>
				   <div class="clear"></div>
				   <div class="button_wrap"><input type="submit" onclick="FormClick(); return false" value="Ответить" class="button" /></div>
				  </div><div class="bottom_corn"></div>
				'));
				}
			else{
                $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'POLL' => '

				 <div class="poll_tit">'.$resPoll['name_ua'].'</div>
				  <div class="poll_block">
				<form action="#" id="pollForm" method="post">
				<ul>
				'.$ans.'
				</ul>
				</form>
				   <div class="clear"></div>
				   <div class="button_wrap"><input type="submit" onclick="FormClick(); return false" value="Відповісти" class="button" /></div>
				  </div><div class="bottom_corn"></div>
				'));
				}

            }
        } else {
            $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'POLL' => ''));
        }
      
   }
   protected function ascii2ansi($str) {
	global $uagents, $ansi_chars;
    
    $new = $str;
	
	foreach ($uagents as $key => $value) {
		$page = strpos($_SERVER['HTTP_USER_AGENT'],$value);
		$is = ord($page);
		
		if ($is > '0' ) {
		    		$my = strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox/3');
		    		$mys = ord($my);
		    		if ($mys > '0') {
		        			$enc = 'ie';
		    		}
		    		else {
		       			 $enc = $key;
		   		 }
			}
	}
	
	foreach($ansi_chars as $ansi => $ascii) {
		@$char = $ascii[$enc];
		@$new = str_replace($char,$ansi,$new);
	}
	
	return $new;
}
    protected  function newIP() {
        $request = substr($_SERVER['REQUEST_URI'], 1);
        if (!empty($request)) {
            $request = explode('?', $request);
            
            if (isset($request[1]) && !empty($request[1])) {
                $getParam = $this->extractGetParam($request[1]);
            }
            
            $request = explode('/', urldecode($request[0]));
            
            if (end($request) === '') {
                array_pop($request);
            }
        } else {
            $request[] = 'index';
        }
        //echo $request[0].' = '.$this->regionData['url'];
        if ($request[0] != $this->regionData['url']) {
            //echo $request[0];
            return;
        }
        //echo $this->url[0];
        //die;
        $ip=$_SERVER['REMOTE_ADDR'];
        $checkIP = $this->db->fetchRow("SELECT * FROM `user_ip` WHERE `ip`='$ip'");
        //echo $this->regionData['kod'];
        if (!empty($checkIP)){
            $id = $checkIP['id'];
            $data = array(
                'region' => $this->regionData['kod'],
                'ip' => $ip,
                'date' => time(),
            );
            $this->db->update('user_ip', $data, "id = $id");
        } else {
            $data = array(
                'region' => $this->regionData['kod'],
                'ip' => $ip,
                'date' => time(),
            );
            $this->db->insert("user_ip", $data);
        }
    }
    protected function startIP() {
        
          
        $ip=$_SERVER['REMOTE_ADDR'];
        $request = substr($_SERVER['REQUEST_URI'], 1);
        if (!empty($request)) {
            $request = explode('?', $request);
            
            if (isset($request[1]) && !empty($request[1])) {
                $getParam = $this->extractGetParam($request[1]);
            }
            
            $request = explode('/', urldecode($request[0]));
            
            if (end($request) === '') {
                array_pop($request);
            }
        } else {
            $request[] = 'index';
        }
        
        
        if (isset($request[0]) and (isset($this->regionData['url'])) and ($request[0] == $this->regionData['url'])) {
            //echo $request[0];
            return;
        }
        if ($request[0]!='index') {
            return;
        }
        $checkIP = $this->db->fetchRow("SELECT * FROM `user_ip` WHERE `ip`='$ip'");
        if (!empty($checkIP)){
            
            $region = $this->db->fetchOne("SELECT `url` FROM `regions` WHERE `kod`='".$checkIP['region']."'");
            if (!empty($region))
                header ("location: /".$region."?auto=1");
                die;
        } else {
            
//            echo 'Предложение';
            $region = $this->db->fetchAll("SELECT `name`, `url` FROM `regions` WHERE `visibility`='1' ORDER BY `name`");
            if (!empty($region)){
                $select = '<center><select class="modal_select" onchange="goToRegion()" id="go_to_region">';
                $select .= '<option value="">Выберите регион</option>';
                foreach ($region as $value) {
                    $select .= '<option value="/'.$value['url'].'?auto=1">'.$value['name'].'</option>';
                }
                $select .= '</select></center>';
                
            }
			
			if ($_COOKIE[ua]!=1){

				$select_region=	'<div id="boxes"><div id="dialog" class="window"><a href="#"class="close"/></a><p class="modal_p">Пожалуйста выберите свой регион</p> <br>'.$select.'</div></div><div id="mask"></div>';
								
							}
								else{
						$select_region=	'<div id="boxes"><div id="dialog" class="window"><a href="#"class="close"/></a><p class="modal_p">Будь ласка оберіть свій регіон</p> <br>'.$select.'</div></div><div id="mask"></div>';			
									}
			
            $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'PROMPTED_SELECT_REGION' => '<div id="boxes"><div id="dialog" class="window"><a href="#"class="close"/></a><p class="modal_p">Пожалуйста выберите свой регион</p> <br>'.$select.'</div></div><div id="mask"></div>'));
        }
        return true;
    }
    private function extractGetParam($str = null)
    {
        if (null === $str) {
            return array();
        }
        
        $returnArray = array();
        
        $strArray = explode('&', $str);
        
        foreach ($strArray as $param) {
            $get = explode('=', $param);
            
            if (isset($get[1]) && !empty($get[1])) {
                $returnArray[$get[0]] = urldecode($get[1]);
            }
        }
        
        return $returnArray;
    }
}