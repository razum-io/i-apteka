<?php

class Init
{
    private $_config = null;
    
    private $_url = null;
    
    private $_basePath = null;
    
    private $_lang = null;
    
    private $_region = null;
    
    
    private $_getParam = null;
    
    private $_site_db = null;
    
    private $dbProfiler = null;
    
    public function __construct($config)
    {
        if ($config instanceof Zend_Config) {
            $this->_config = $config->toArray();
        } else {
            if (!is_array($config)) {
                $config = (array) $config;
            }
            
            $this->_config = $config;
        }
        
        $this->modRewrite();
        
        $runConfig = array(
                'url' => $this->_url,
                'basePath' => $this->_basePath,
                'lang' => $this->_lang,
                'getParam' => $this->_getParam,
                'region' => $this->_region,
        );
        
        Zend_Registry::set('run', $runConfig);
        
        $this->dispatch();
    }
    
    private function dispatch()
    {
        $dir = PATH . 'library/';       
        $fileName = $dir . 'Content' . '.php';
        $className = 'Content';
        $action = $this->_url[0];
        
        if (file_exists($dir . ucfirst(strtolower($this->_url[0])) . '.php')) {
            $fileName = $dir . ucfirst(strtolower($this->_url[0])) .'.php';
            $className = ucfirst(strtolower($this->_url[0]));
            
            $action = (isset($this->_url[1]) && !empty($this->_url[1])) ? $this->_url[1] : 'main';
        } else {
            if (!file_exists($fileName)) {
                throw new Exception('Base Class not found');
            }
        }
        
        require_once $fileName;
        
        $controller = new $className($this->_config);
        
        if (!$controller->factory()) {
            $this->redirect('404');
        }
        if ($action != 'index' && method_exists($controller, 'beforeAction')) { 
        		$controller->beforeAction(true);
        }
        	
        if (method_exists($controller, $action)) {
        	
            if (!$controller->$action()) {
                $this->redirect('404');
            } 
        } else {
            if (!$controller->main()) {
                $this->redirect('404');
            }
        }
        
        $controller->finalise();
        
        $this->dbProfiler = $controller->getProfiler();
        $this->_site_db = $controller->getDb();
    }
    
    private function modRewrite()
    {
        $request = substr($_SERVER['REQUEST_URI'], 1);
        $getParam = array();
        
        if (!empty($request)) {
            $request = explode('?', $request);
            
            if (isset($request[1]) && !empty($request[1])) {
                $getParam = $this->extractGetParam($request[1]);
            }
            
            $request = explode('/', urldecode($request[0]));
            
            if (end($request) === '') {
                array_pop($request);
            }
        } else {
            $request[] = 'index';
        }
        
        $request = $this->checkLang($request);
        $request = $this->checkRegion($request);

//print_r($request); die;
        //$request = $this->checkRegion($request);
        if (!$request){
            $request[0] = 'index';
        }
           
        if ($request[0] == '404') {
            $request[0] = 'error404';
        }
        
        $this->_url = $request;
        $this->_getParam = $getParam;
    }
    private function checkRegion($url = null){
        if (!$url){
            return '';
        } else {
            $this->connectionToDatabase();
            //$request[0]
            $region=$url[0];
            $res = $this->db->fetchOne("SELECT `id` FROM `regions` WHERE `url`='$region'");
            
            if (!empty($res)) {
                $this->_region = $region;
                array_shift($url); 
                return $url;
            }
            return $url;
        }
    }
    private function extractGetParam($str = null)
    {
        if (null === $str) {
            return array();
        }
        
        $returnArray = array();
        
        $strArray = explode('&', $str);
        
        foreach ($strArray as $param) {
            $get = explode('=', $param);
            
            if (isset($get[1]) && !empty($get[1])) {
                $returnArray[$get[0]] = urldecode($get[1]);
            }
        }
        
        return $returnArray;
    }
    
    private function checkLang($url = null)
    {
        if (null === $url) {
            return '';
        }
        
        $basePath = '/';
        $lang = $this->_config['language']['defaultLanguage'];
        
        if (array_key_exists($url[0], $this->_config['language']['allowLanguage'])) {
            $lang = $this->_config['language']['allowLanguage'][$lang];
            
            if ($lang != $this->_config['language']['defaultLanguage']) {
                $basePath .= $lang.'/';
            } else {
                $basePath .= ($this->_config['language']['useDefLangPath'] ? $lang.'/' : '');
            }
            
            $url = array_shift($url);
        }
        
        $this->_basePath = $basePath;
        $this->_lang = $lang;
        
        return $url;
    }

    private function redirect($url = null)
    {
        if (null === $url) {
            throw new Exception('Error redirect function!');
        }
        
        if (!headers_sent()) {
            header("location: " . $this->_basePath . $url);
        } else {
       
            throw new Exception('Unexpected Error');
        }
    }
    
    public function getProfiler() {
    	return $this->dbProfiler;
    }
    
    public function getDb()
    {
    	return $this->_site_db;
    }
    private function connectionToDatabase() {
      try {
         $database = Zend_Db::factory($this->_config['database']['adapter'], $this->_config['database']['params']);
         if (isset($this->isUseDbProfiler)){
            $this->dbProfiler = $database->getProfiler($this->isUseDbProfiler);
         }
         $database->getConnection();
      } catch (Zend_Db_Adapter_Exception $e) {
         throw new Exception('возможно, неправильные параметры соединения или СУРБД не запущена');
      } catch (Zend_Exception $e) {
         throw new Exception('возможно, попытка загрузки требуемого класса адаптера потерпела неудачу');
      }

      $this->db = $database;
   }
}