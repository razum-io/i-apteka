﻿<?php

require_once PATH . 'library/Abstract.php';

require_once PATH . 'library/Interface.php';


class Order extends Main_Abstract implements Main_Interface {
	
	public function factory() {
        return true;
    }      
        
    public function main() {      	
    	$this->showOrder();
        
    	//if ($this->isSetOrders()) {
    	//	$this->showOrderForm();
    	//}
    	return true;
    } 
   protected function showOrder() {
       $summ=0; $count=0;
       //$this->delGoodsRegion(); die;
       if (empty($_SESSION['userInfo']))
           return $this->error404();
		   
		   if ($_COOKIE[ua]!=1){
        $this->setMetaTags('Спасибо за заказ!');    	
        $this->setWay('Спасибо за заказ!');
		}
			else{
			$this->setMetaTags('Дякуємо за замовлення!');
			$this->setWay('Дякуємо за замовлення!');
				}

    	$fio    =   $_SESSION['userInfo']['fio'];//$this->getVar('fio', '');
        $adr    =   $_SESSION['userInfo']['adr'];//$this->getVar('adr', '');
        $email  =   $_SESSION['userInfo']['email'];//$this->getVar('email', '');
        $mphone =   $_SESSION['userInfo']['mphone'];//$this->getVar('mphone', '');
        $phone  =   $_SESSION['userInfo']['phone'];//$this->getVar('phone', '');
        $info   =   $_SESSION['userInfo']['info'];//$this->getVar('info', '');
        $tvivoza   =   $_SESSION['userInfo']['tvivoza'];//$this->getVar('info', '');
        
			$f = $this->db->fetchRow("SELECT * FROM `spravochnik` WHERE `id`='".$tvivoza."' ");
			$tvivoza_name=$f[tvivoz];
			
        unset ($_SESSION['userInfo']);
        $ins=1;
        
        if ($ins==1){
        /*
         * заносим данные о заказчике в базу
         */
        $orderDate = time();
        $data = array(
                'fio' => $fio,
                'adr' => $adr,
                'email' => $email,
                'mphone' => $mphone,
                'phone' => $phone,
                'info' => $info,
                'date' => $orderDate,
                'region' => $this->regionData['kod'],
                'tvivoz' => $tvivoza_name,
                'id_tvivoz' => $tvivoza,
				'summ' => "0"
            );
        $this->db->insert("orders", $data);
        
        
        
        $noOrder = $this->db->lastInsertId();
        $_SESSION['lastOrder']=$noOrder;
        
		$info = str_replace(array("\r","\n")," ",$info);
        $info = str_replace(array("  ")," ",$info);
		
		$adr = str_replace(array("\r","\n")," ",$adr);
        $adr = str_replace(array("  ")," ",$adr);
		
		
        $csvFileData = "".$noOrder.
                ";".$fio.
                ";".$email.
                ";".date('d.m.Y H:i:s', $orderDate).
                ";".$adr.
                ";".$phone.
                ";".$mphone.
                ";".$tvivoza_name.
                ";".$info.
                ";\r\n";
        //$csvFileData .= "Название ;Артикул ;Производитель ;Цена за еденицу ;Количество ;Сумма;;;\r\n";
        /*
         * Заносим данные о заказе
         */
        
        
        }
		
		$ua=$this->getUa();
		
    	$this->tpl->define_dynamic('_order_form', "$ua".'order.tpl');
    	$this->tpl->define_dynamic('order_form', '_order_form');
    	$this->tpl->define_dynamic('delivery_service_list', 'order_form');
    	$this->tpl->define_dynamic('order_goods_list', 'order_form');
        
        
        
        
        $deliveryType = $this->getVar('delivery_type', 'picup');

        
        
        $goodsSession = new Zend_Session_Namespace('goods');
        $np=0;
        $countStr=0;
        if (!empty($goodsSession->array)) {
            foreach ($goodsSession->array as $index=>$basket) {   	
                if (isset($basket['status']) && $basket['status'] == 'active' && $basket['region']==$this->regionData['kod']) {
                    if (is_numeric($basket['count']) && $basket['count'] > 0) {
                            $summ +=  ( $basket['cost']  * $basket['count']);   
                            $count ++;        		
                            $count += ($basket['count'] - 1);
                            //print_r ($basket);  echo '<br>';
                            $resInfo = $this->db->fetchRow("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `artikul`='".$basket['artikul']."'");
                            if ($ins==1) {
                                unset($data);
                                $orderCost = str_replace(',', '.', $basket['cost']);
                                $data = array(
                                    'order_id'      =>  $noOrder,
                                    'goods_artikul' =>  $basket['artikul'],
                                    'count'         =>  $basket['count'],
                                    'cost'          =>  $orderCost,
                                    'name'          =>  $basket['name'],
                                    'tvivoz'          =>  $tvivoza_name,
                                    'tvivoz_id'          =>  $tvivoza,
                                    'producer'      =>  $resInfo['producer']
                                );
                                $csvFileData .=$basket['name'] . 
                                        " ;" . $basket['artikul'] . 
                                        " ;" . $resInfo['producer'] . 
                                        " ;" . str_replace(',', '.', $basket['cost']) . 
                                        " ;" . $basket['count'] . 
                                        " ; " . ($orderCost * $basket['count']) . ";;;\r\n";
                                $countStr +=$basket['count'];
                                $this->db->insert("orders_goods", $data);
                            }
                            $np++;
                            $this->tpl->assign(array(
											'PROBA_CATALOG'     =>  (''),
                                'ORDER_GOODS_NP'        =>  $np,
    				'ORDER_GOODS_NAME'      =>  $basket['name'].'<a href="/'.$this->reionData['url'].'/order/delete/'.$basket['artikul'].'">&nbsp;</a>',
    				'ORDER_GOODS_PRODUCER'  =>  $resInfo['producer'],//'Натурфарм косметика',//$basket['name'],
                                'ORDER_GOODS_COST'      =>  str_replace('.', ',', $basket['cost']),
                                'ORDER_GOODS_COUNT'     =>  $basket['count'],
                                'ORDER_GOODS_COST_SUMM' =>  str_replace('.', ',', $basket['cost']*$basket['count']),
    			));
    			
    			$this->tpl->parse('ORDER_GOODS_LIST', '.order_goods_list');
                        
                        

                        
                    } else {
                        $goodsSession->array[$index]['status'] = 'deleted';
                    }
                }	
                $isBasketEmpty = false;
            }
        }
				//======================================	ДОБАВЛЕНИЕ СУММЫ ЗАКАЗА В ТАБЛИЦУ "orders"	=========================================================================
				include_once $_SERVER['DOCUMENT_ROOT'].'/g_config.php';
				mysql_connect(_HOSTBASE, _USERBASE, _PASSWORDBASE) or die("Не можем подключиться к серверу MySQL...");
				mysql_select_db(_DATABASE) or die("Не можем выбрать базу данных...");
				mysql_set_charset("utf8");
				mysql_query("UPDATE orders SET summ='$summ' where id='$noOrder'");
				//================================================================================================================================================================
		
        //foreach ($goodsSession->array as $index=>$basket) {   	
        
        //$nt = new NumToText();
//echo $nt->Convert(100) . '<BR />';
        //преобразование числа в текст
//require_once PATH . 'library/NumToText.php';

//        $nt = new NumToText();
        $this->tpl->assign(array(
						'PROBA_CATALOG'     =>  (''),
            'ORDER_GOODS_TOTAL'         =>  str_replace('.', ',', $summ),
            'ORDER_GOODS_NO_ZAKAZA'     =>  $noOrder,
            'ORDER_GOODS_DATA_ZAKAZA'   =>  date ('d.m.Y'),
            'ORDER_GOODS_FIO'           =>  $fio,
            'ORDER_GOODS_ADR'           =>  $adr,
            'ORDER_GOODS_EMAIL'         =>  $email,
            'ORDER_GOODS_MPHONE'        =>  $mphone,
            'ORDER_GOODS_PHONE'         =>  $phone,
            'ORDER_GOODS_INFO'          =>  $info,
            'ORDER_GOODS_TVIVOZA'          =>  $tvivoza_name,
            //'ORDER_GOODS_NUM_STR'       => substr($nt->Convert($countStr),0,-4)
    			));
        
        $filename = PATH . 'obmen/csv/' . $noOrder . '.csv';
        $handle = fopen($filename, 'w');
        //fwrite($handle, b"\xEF\xBB\xBF" ) ;
        fwrite($handle, $csvFileData);
        fclose($handle);

    	$deliveryServiceChecked = '';
    	$deliveryPicupChecked = 'checked';
    	
    	if ($deliveryType == 'delivery_service') {
    		$deliveryServiceChecked = 'checked';
    		$deliveryPicupChecked = '';
    	}
    	
    	$deliveryServiceList = $this->db->fetchAll("SELECT `name` FROM `delivery` WHERE `visibility` = 1 AND `language` = '$this->lang'");
    	
    	if ($deliveryServiceList) {
    		foreach ($deliveryServiceList as $deliveryServiceItem) {
    			$selected = '';
    			
    			if ($deliveryServiceItem['name'] == $this->getVar('delivery_service')) {
    				$selected = 'SELECTED';
    			}
    			
    			$this->tpl->assign(array(
								'PROBA_CATALOG'     =>  (''),
    				'ORDER_FORM_DELIVERY_SERVICE_VALUE'=>$deliveryServiceItem['name'],
    				'ORDER_FORM_DELIVERY_SERVICE_TEXT'=>$deliveryServiceItem['name'],
    				'ORDER_FORM_DELIVERY_SERVICE_SELECTED'=>$selected,
    			));
    			
    			$this->tpl->parse('DELIVERY_SERVICE_LIST', '.delivery_service_list');
    		}
    	} else {
    		$this->tpl->parse('DELIVERY_SERVICE_LIST', 'null');
    	}    	
    	    	
    	$this->tpl->assign(
    		array(
    			'ORDER_FORM_FIO'=>$this->getVar('fio', ''), 
    			'ORDER_FORM_EMAIL'=>$this->getVar('email', ''), 
    			'ORDER_FORM_CITY'=>$this->getVar('city', ''), 
    			'ORDER_FORM_MOBILE_PHONE'=>$this->getVar('modile_phone', ''), 
    			'ORDER_FORM_PHONE'=>$this->getVar('phone',''), 
    			'ORDER_FORM_STREET'=>$this->getVar('street',''), 
    			'ORDER_FORM_HOME'=>$this->getVar('home',''), 
    			'ORDER_FORM_APARTMENT'=>$this->getVar('apartment',''),
    			'ORDER_FORM_DELIVERY_SERVICE_CHECKED'=>$deliveryServiceChecked,
    			'ORDER_FORM_PICUP_CHECKED'=>$deliveryPicupChecked,
    			'ORDER_FORM_BODY'=>$this->getVar('body',''),
                    //'ORDER_GOODS_TVIVOZA'          =>  $tvivoza,
    		));
    	$this->tpl->parse('CONTENT', '.order_form');
        $this->sendOrder($noOrder);
        $this->delGoodsRegion();
   }
   
   
   private function delGoodsRegion(){
    $goodsSession = new Zend_Session_Namespace('goods');
       if (!empty($goodsSession->array)) {
            foreach ($goodsSession->array as $index=>$basket) {   	
                if (isset($basket['status']) && $basket['status'] == 'active' && $basket['region']==$this->regionData['kod']) {
                  unset ($goodsSession->array[$basket['id'].$this->regionData['kod']]);
                }	
            }
        }

		if ($_COOKIE[ua]!=1){
				$basket = '<div class="busket">
				   <p class="tit"><a href="#">Ваша Корзина</a></p>
				   <p><a href="#">Товаров:</a><span><a href="#">0 шт.</a></span></p> 
				   <p><a href="#">На сумму:</a><span><a href="#">0 грн.</a></span></p>
				   <p class="center"><a href="#">Корзина пуста</a></p>
				  </div>';
		}
			else{
						$basket = '<div class="busket">
						   <p class="tit"><a href="#">Ваш Кошик</a></p>
						   <p><a href="#">Товарів:</a><span><a href="#">0 шт.</a></span></p> 
						   <p><a href="#">На суму:</a><span><a href="#">0 грн.</a></span></p>
						   <p class="center"><a href="#">Кошик пустий</a></p>
						  </div>';
				}

$this->tpl->assign(
                    array(
                        'BASKET_START' => $basket
                    )
                );

   }
   
   public function printBill() {
       
      	  
             $ua=$this->getUa();
        //$this->tpl = new Templates('tpl/print/');
        $this->tpl->set_root('tpl/print/');
        $this->tpl->define_dynamic('page', "$ua"."bill.tpl");
        $this->tpl->define_dynamic('null', 'page');
        
        $this->tpl->define_dynamic('item_row', 'page');
        
        $totalSumm = 0;
        $order = $this->db->fetchRow("SELECT * FROM `orders` WHERE `id`='".$_SESSION['lastOrder']."'");
        $resInfoGoods = $this->db->fetchAll("SELECT * FROM `orders_goods` WHERE `order_id`='".$_SESSION['lastOrder']."'");
        $np=0;
        foreach ($resInfoGoods as $row) {
            $np++;
            $this->tpl->assign(
                array(
                    'PRINT_ORDER_GOODS_NO'        =>  $np,
                    'PRINT_ORDER_GOODS_NAME'        =>  $row['name'],
                    'PRINT_ORDER_GOODS_PRODUCER'    =>  $row['producer'],
                    'PRINT_ORDER_GOODS_COST'        =>  $row['cost'],
                    'PRINT_ORDER_GOODS_COUNT'       =>  $row['count'],
                    'PRINT_ORDER_GOODS_SUMM'        =>  $row['cost']*$row['count']
                )
            );
            
            $this->tpl->parse('ITEM_ROW', '.item_row');
            
            $totalSumm += $row['cost']*$row['count'];
        }
        
        $this->tpl->assign(
            array(
                'PRINT_ORDER_NUMBER' => $order['id'],
                'PRINT_ORDER_DATE' => $this->convertDate($order['date']),
                'PRINT_ORDER_ADR' => $order['adr'],
                'PRINT_ORDER_FIO' => $order['fio'],
                'PRINT_ORDER_PHONE' => $order['phone'],
                'PRINT_ORDER_MPHONE' => $order['mphone'],
                'PRINT_ORDER_EMAIL' => $order['email'],
                'PRINT_ORDER_INFO' => $order['info'],
                'PRINT_ORDER_TOTAL_SUMM' => $totalSumm,
                'PRINT_ORDER_TVIVOZA'          =>  $order['tvivoz'],
            )
        );
        
        return true;
   }   
   public function printOrder() {
       
      	  
             $ua=$this->getUa();
        //$this->tpl = new Templates('tpl/print/');
        $this->tpl->set_root('tpl/print/');
        $this->tpl->define_dynamic('page', "$ua"."order.tpl");
        $this->tpl->define_dynamic('null', 'page');
        
        $this->tpl->define_dynamic('item_row', 'page');
        
        $totalSumm = 0;
        $order = $this->db->fetchRow("SELECT * FROM `orders` WHERE `id`='".$_SESSION['lastOrder']."'");
        $resInfoGoods = $this->db->fetchAll("SELECT * FROM `orders_goods` WHERE `order_id`='".$_SESSION['lastOrder']."'");
        $np=0;
        foreach ($resInfoGoods as $row) {
            $np++;
            $this->tpl->assign(
                array(
                    'PRINT_ORDER_GOODS_NO'        =>  $np,
                    'PRINT_ORDER_GOODS_NAME'        =>  $row['name'],
                    'PRINT_ORDER_GOODS_PRODUCER'    =>  $row['producer'],
                    'PRINT_ORDER_GOODS_COST'        =>  $row['cost'],
                    'PRINT_ORDER_GOODS_COUNT'       =>  $row['count'],
                    'PRINT_ORDER_GOODS_SUMM'        =>  $row['cost']*$row['count']
                )
            );
            
            $this->tpl->parse('ITEM_ROW', '.item_row');
            
            $totalSumm += $row['cost']*$row['count'];
        }
        
        $this->tpl->assign(
            array(
                'PRINT_ORDER_NUMBER' => $order['id'],
                'PRINT_ORDER_DATE' => $this->convertDate($order['date']),
                'PRINT_ORDER_ADR' => $order['adr'],
                'PRINT_ORDER_FIO' => $order['fio'],
                'PRINT_ORDER_PHONE' => $order['phone'],
                'PRINT_ORDER_MPHONE' => $order['mphone'],
                'PRINT_ORDER_EMAIL' => $order['email'],
                'PRINT_ORDER_INFO' => $order['info'],
                'PRINT_ORDER_TOTAL_SUMM' => $totalSumm,
                'PRINT_ORDER_TVIVOZA'          =>  $order['tvivoz'],
            )
        );
        
        return true;
   }   
   
   
   
   
    protected function showOrderForm() {
    	
        
        /*
         * Выводим список купленых товаров
        */
        
        //var_dump($_SESSION);
        
		if ($_COOKIE[ua]!=1){
    	$this->setMetaTags('Оформить заказ');    	
        $this->setWay('Оформить заказ');
		}
			else{
			$this->setMetaTags('Оформити замовлення');    	
			$this->setWay('Оформити замовлення');
				}
        
        		
		$ua=$this->getUa();
    	
    	$this->tpl->define_dynamic('_order_form', "$ua".'order.tpl');
    	$this->tpl->define_dynamic('order_form', '_order_form');
    	$this->tpl->define_dynamic('delivery_service_list', 'order_form');
    	$this->tpl->define_dynamic('order_goods_list', 'order_form');
        
        
        
        
        $deliveryType = $this->getVar('delivery_type', 'picup');

        
       
        $goodsSession = new Zend_Session_Namespace('goods');
        if (!empty($goodsSession->array)) {
            foreach ($goodsSession->array as $index=>$basket) {   	
                if (isset($basket['status']) && $basket['status'] == 'active' && $basket['region']==$this->regionData['kod']) {
                    if (is_numeric($basket['count']) && $basket['count'] > 0) {
                            $summ +=  ( $basket['cost']  * $basket['count']);   
                            $count ++;        		
                            $count += ($basket['count'] - 1);
                            //print_r ($basket);  echo '<br>';
                            $summ +=$basket['cost']*$basket['count'];
                            $this->tpl->assign(array(
											'PROBA_CATALOG'     =>  (''),
    				'ORDER_GOODS_NAME'      =>  $basket['name'].'<a href="/'.$this->reionData['url'].'/order/delete/'.$basket['artikul'].'">123&nbsp;</a>',
    				'ORDER_GOODS_PRODUCER'  =>  'Натурфарм косметика',//$basket['name'],
                                'ORDER_GOODS_COST'      =>  str_replace('.', ',', $basket['cost']),
                                'ORDER_GOODS_COUNT'     =>  $basket['count'],
                                
    			));
    			
    			$this->tpl->parse('ORDER_GOODS_LIST', '.order_goods_list');
                        
                    } else {
                        $goodsSession->array[$index]['status'] = 'deleted';
                    }
                }	
                $isBasketEmpty = false;
            }
        }
        
        $this->tpl->assign(array(
						'PROBA_CATALOG'     =>  (''),
            'ORDER_GOODS_TOTAL' =>  str_replace('.', ',', $summ)
    			));
        
        
    	$deliveryServiceChecked = '';
    	$deliveryPicupChecked = 'checked';
    	
    	if ($deliveryType == 'delivery_service') {
    		$deliveryServiceChecked = 'checked';
    		$deliveryPicupChecked = '';
    	}
    	
    	$deliveryServiceList = $this->db->fetchAll("SELECT `name` FROM `delivery` WHERE `visibility` = 1 AND `language` = '$this->lang'");
    	
    	if ($deliveryServiceList) {
    		foreach ($deliveryServiceList as $deliveryServiceItem) {
    			$selected = '';
    			
    			if ($deliveryServiceItem['name'] == $this->getVar('delivery_service')) {
    				$selected = 'SELECTED';
    			}
    			
    			$this->tpl->assign(array(
								'PROBA_CATALOG'     =>  (''),
    				'ORDER_FORM_DELIVERY_SERVICE_VALUE'=>$deliveryServiceItem['name'],
    				'ORDER_FORM_DELIVERY_SERVICE_TEXT'=>$deliveryServiceItem['name'],
    				'ORDER_FORM_DELIVERY_SERVICE_SELECTED'=>$selected,
    			));
    			
    			$this->tpl->parse('DELIVERY_SERVICE_LIST', '.delivery_service_list');
    		}
    	} else {
    		$this->tpl->parse('DELIVERY_SERVICE_LIST', 'null');
    	}    	
    	    	
    	$this->tpl->assign(
    		array(
    			'ORDER_FORM_FIO'=>$this->getVar('fio', ''), 
    			'ORDER_FORM_EMAIL'=>$this->getVar('email', ''), 
    			'ORDER_FORM_CITY'=>$this->getVar('city', ''), 
    			'ORDER_FORM_MOBILE_PHONE'=>$this->getVar('modile_phone', ''), 
    			'ORDER_FORM_PHONE'=>$this->getVar('phone',''), 
    			'ORDER_FORM_STREET'=>$this->getVar('street',''), 
    			'ORDER_FORM_HOME'=>$this->getVar('home',''), 
    			'ORDER_FORM_APARTMENT'=>$this->getVar('apartment',''),
    			'ORDER_FORM_DELIVERY_SERVICE_CHECKED'=>$deliveryServiceChecked,
    			'ORDER_FORM_PICUP_CHECKED'=>$deliveryPicupChecked,
    			'ORDER_FORM_BODY'=>$this->getVar('body',''),
                        
    		));
    	$this->tpl->parse('CONTENT', '.order_form');
    }    
        
    public function add() {
    	
		if ($_COOKIE[ua]!=1){
    	$this->setMetaTags('Оформление заказа');    	
        $this->setWay('Оформление заказа');
		}
			else{
			$this->setMetaTags('Оформлення замовлення');    	
			$this->setWay('Оформлення замовлення');
				}
				

    	
    	if ($this->isSetOrders() && !empty($_POST)) { 
    		$goodsSession = new Zend_Session_Namespace('goods');
    		
    		$fio = $this->getVar('fio');
    		$email = $this->getVar('email');
    		$city = $this->getVar('city');
    		$modile_phone = $this->getVar('modile_phone');
    		$phone = $this->getVar('phone');
    		$delivery_type = $this->getVar('delivery_type');
    		$delivery_service = $this->getVar('delivery_service');    			
    		$street = $this->getVar('street');
    		$home = $this->getVar('home');
    		$apartment = $this->getVar('apartment');
    		$body = $this->getVar('body');
    		$date = date('Y-m-d H:i:s');
    		
    		
    		$validate = new Zend_Validate_EmailAddress();
    		if (!$fio) $this->addErr('Поле Ф.И.О не должно быть пустым');
		    if (!$validate->isValid($email)) $this->addErr('E-mail указан не правильно');
		    if (!$city) $this->addErr('Поле <b>Город</b> не должно быть пустым');
		    if (!$modile_phone) $this->addErr('Поле <b>Моб. телефон</b> не должно быть пустым');
		    //if (!$phone) $this->addErr('Поле <b>Тел. с кодом города</b> не должно быть пустым');
		
		    if ($delivery_type != 'pickup') {
		  	//  if (!$delivery_service) $this->addErr('Поле <b>Доставка</b> не должно быть пустым');
		    	if (!$street) $this->addErr('Поле <b>Улица</b> не должно быть пустым');
		    	if (!$home) $this->addErr('Поле <b>Номер дома</b> не должно быть пустым');
		    	if (!$apartment) $this->addErr('Поле <b>Квартира / Офис</b> не должно быть пустым');
		    }	
		  
		    if ($this->_err) {
		    	$this->viewErr();
		    	$this->showOrderForm();
		    	return true;
		    }
		    
    		
    		$data = array(
    			'fio'=>$fio,
    			'email'=>$email,
    			'city'=>$city,
    			'modile_phone'=>$modile_phone,
    			'phone'=>$phone,
    			'delivery_type'=>$delivery_type,
    			'delivery_service'=>$delivery_service,    			
    			'street'=>$street,
    			'home'=>$home,
    			'apartment'=>$apartment,
    			'date'=>$date,
    			'body'=>$body
    			);
    				
    		$this->db->insert('orders', $data);
    		
    		$orderId = $this->db->lastInsertId();
    		
    		foreach ($goodsSession->array as $item) { 
    			
    			if ($item['status'] != 'deleted') { 
    				$data = array(
    					'order_id'=>$orderId,
    					'name'=>$item['name'],
    					'goods_artikul' =>$item['artikul'],
    					'count'=>$item['count'],
    					'cost'=>$item['cost'],
    				);
    				
    				$this->db->insert('orders_goods', $data);
    			}
    		}
    	
    		
	    	
    		$goodsSession->array = array();
		
		$ua=$this->getUa();
		
    		$this->tpl->define_dynamic('_order_success', "$ua".'order.tpl');
    		$this->tpl->define_dynamic('order_success', '_order_success');
    		$this->tpl->parse('CONTENT', '.order_success');
    	}	
    	
    	if (isset($_SERVER['goods'])) {
    		unset($_SERVER['goods']);
    	}
    	$this->viewMessage("<meta http-equiv='refresh' content='2;URL=/'>");   	
    		
    	
    	return true;
    }    
    private function sendOrder($id){
        $bodyMail=''; $summAll=0;
    	$sql = "SELECT * FROM `orders` WHERE `id` = '$id'";
        $orderDetail = $this->db->fetchRow($sql);
        if ($id < 10) {
            $idItem = "0000$id";
        }

        if ($id >= 10 && $id < 100) {
            $idItem = "000$id";
        }

        if ($id >= 100 && $id < 1000) {
            $idItem = "00$id";
        }

        if ($id >= 1000 && $id < 10000) {
            $idItem = "0$id";
        }

        if ($id >= 10000) {
            $idItem = $id;
        }
        $idItem=$id;
	  if ($_COOKIE[ua]!=1){
        $bodyMail = '<table>
     <tr>
      <td>№</td>
      <td>'.$idItem.'</td>
     </tr>
     <tr>
      <td>Ф.И.О.:</td>
      <td>'.$orderDetail['fio'].'</td>
     </tr>
     <tr>
      <td>E-mail:</td>
      <td><a href="mailto:'.$orderDetail['email'].'">'.$orderDetail['email'].'</a></td>
     </tr>
     <tr>
      <td>Дата:</td>
      <td>'.date ('d.m.Y H:m:i',$orderDetail['date']).'</td>
     </tr>
     <tr>
      <td>Адрес:</td>
      <td>'.$orderDetail['adr'].'</td>
     </tr>
     <tr>
      <td>Телефон:</td>
      <td>'.$orderDetail['phone'].'</td>
     </tr>
     <tr>
      <td>Моб. телефон:</td>
      <td>'.$orderDetail['mphone'].'</td>
     </tr>
          <tr>
      <td>Точка самовывоза:</td>
      <td>'.$orderDetail['tvivoz'].'</td>
     </tr>
     
     <td>Дополнительно:</td>
      <td>'.$orderDetail['info'].'</td>
     </tr>
    </table>
    <table>
        <tr><td>Название&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Артикул&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Производитель&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Цена за еденицу&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Количество&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Сумма</td></tr>';
        }
	else{
	
	        $bodyMail = '<table>
     <tr>
      <td>№</td>
      <td>'.$idItem.'</td>
     </tr>
     <tr>
      <td>П.І.Б.:</td>
      <td>'.$orderDetail['fio'].'</td>
     </tr>
     <tr>
      <td>E-mail:</td>
      <td><a href="mailto:'.$orderDetail['email'].'">'.$orderDetail['email'].'</a></td>
     </tr>
     <tr>
      <td>Дата:</td>
      <td>'.date ('d.m.Y H:m:i',$orderDetail['date']).'</td>
     </tr>
     <tr>
      <td>Адреса:</td>
      <td>'.$orderDetail['adr'].'</td>
     </tr>
     <tr>
      <td>Телефон:</td>
      <td>'.$orderDetail['phone'].'</td>
     </tr>
     <tr>
      <td>Моб. телефон:</td>
      <td>'.$orderDetail['mphone'].'</td>
     </tr>
          <tr>
      <td>Точка самовивозу:</td>
      <td>'.$orderDetail['tvivoz'].'</td>
     </tr>
     
     <td>Додатково:</td>
      <td>'.$orderDetail['info'].'</td>
     </tr>
    </table>
    <table>
        <tr><td>Назва&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Артикул&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Виробник&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Ціна за одиницю&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Кількість&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Сума</td></tr>';
	
	}
        
                $sql = "SELECT * FROM `orders_goods` WHERE `order_id`='$id'";
      
        $orderList = $this->db->fetchAll($sql);
        $summ = 0;

        if ($orderList) {
            $this->tpl->parse('ORDER_DETAIL_LIST_EMPTY', 'null');
            foreach ($orderList as $orderItem) {

                if ($orderItem['cost'] > 0) {

                    $summItem = ($orderItem['cost'] * $orderItem['count']);
                    $summAll += $summItem;
                    
                    $bodyMail .='
     
        <tr>     
      <td>&nbsp;&nbsp;'.$orderItem['name'].'&nbsp;&nbsp;</td>
      <td>&nbsp;&nbsp;'.$orderItem['goods_artikul'].'&nbsp;&nbsp;</td>
      <td>&nbsp;&nbsp;'.$orderItem['producer'].'&nbsp;&nbsp;</td>
      <td>&nbsp;&nbsp;'.number_format($orderItem['cost'], 2, ',', " ").' грн.&nbsp;&nbsp;</td>
      <td>&nbsp;&nbsp;'.$orderItem['count'].' шт.&nbsp;&nbsp;</td>
      <td>&nbsp;&nbsp;'.number_format($summItem, 2, ',', " ").' грн.&nbsp;&nbsp;</td>
     </tr>';
                    $summ += $summItem;

                }
            }
        }
        $email = $orderDetail['email'];
        $bodyMail .='</table>';
        
    		$host = $_SERVER['HTTP_HOST'];
		if ($_COOKIE[ua]!=1){
			$subject = 'Заказ № '.$idItem.' с сайта: '.$host;
		}
		else{
		$subject = 'Заказ № '.$idItem.' з сайту: '.$host;
		}
    		$mail = new Zend_Mail('utf-8');
			$mail->setFrom('webmaster@'.$host, $host);
			$mail->addTo($email, '');						
			$mail->setSubject($subject);			    		
		
			$mail->setBodyHtml($bodyMail);		    	
			$mail->send();
		if ($_COOKIE[ua]!=1){
			$subject = 'Заказ № '.$idItem.' с сайта: '.$host;
		}
		else{
		$subject = 'Заказ № '.$idItem.' з сайту: '.$host;
		}
    		$mail = new Zend_Mail('utf-8');
			$mail->setFrom('webmaster@'.$host, $host);			
			$mail->addTo($this->settings['admin_email'], '');
			$mail->setSubject($subject);			    		
		
			$mail->setBodyHtml($bodyMail);		    	
			$mail->send();
                        
                        return true;

    }
    private function orderDetail($id) {
    	
    	if (!is_numeric($id)) {
    		$this->addErr("Описание заказа с номером $id не найдено");
    	}
    	
    	if ($this->_err) {
    		$this->viewErr();
    		return true;
    	}
    	
    	$sql = "SELECT * FROM `orders` WHERE `id` = '$id'";    	
    	$orderDetail = $this->db->fetchRow($sql);
    	
    	if (!$orderDetail) {
    		$this->addErr("Описание заказа с номером $id не найдено");
    		$this->viewErr();
    		return true;
    	}
    	    	$ua=$this->getUa();
    	$this->tpl->define_dynamic('_order_detail', "$ua".'orders.tpl');
    	$this->tpl->define_dynamic('order_detail', '_order_detail');
    	$this->tpl->define_dynamic('order_detail_list', 'order_detail');
    	$this->tpl->define_dynamic('order_detail_service', 'order_detail');
    	$this->tpl->define_dynamic('order_detail_list_empty', 'order_detail');
    	
    	$id = $orderDetail['id'];
    	$idItem = "00001";
    			
    	if ($id < 10) {
    		$idItem = "0000$id";
		}
    	
    	if ($id >= 10 && $id < 100) {
    		$idItem = "000$id";
    	}
    		
    	if ($id >= 100 && $id < 1000) {
    		$idItem = "00$id";
    	}
    		
    	if ($id >= 1000 && $id < 10000) {
    		$idItem = "0$id";
    	}
    		
    	if ($id >= 10000 ) {
    		$idItem = $id;
    	}
    	
    	$sql = "SELECT `catalog`.*, `orders_goods`.`count` FROM `catalog`, `orders_goods` WHERE  `orders_goods`.`order_id` = '$id' AND `catalog`.`artikul` = `orders_goods`.`goods_artikul`";
    	
    	$orderList = $this->db->fetchAll($sql);
    	$summ = 0;
    	
    	if ($orderList) { 
    		$this->tpl->parse('ORDER_DETAIL_LIST_EMPTY', 'null'); 
    		foreach ($orderList as $orderItem) {
    			
    			if ($orderItem['cost'] > 0) {
    	
    				$summItem = ($orderItem['cost'] * $orderItem['count']);
    				$this->tpl->assign(array(
									'PROBA_CATALOG'     =>  (''),
	    				'ORDER_DETAIL_LIST_NAME'=>$orderItem['name'],
    					
    					'ORDER_DETAIL_LIST_ARTIKUL'=>$orderItem['artikul'],
    					'ORDER_DETAIL_LIST_COST'=>number_format($orderItem['cost'], 2, ', ', ' '),
    					'ORDER_DETAIL_LIST_COUNT'=>$orderItem['count'],    					
    					'ORDER_DETAIL_LIST_SUMM'=>number_format($summItem, 2, ', ', ' ')
    				));
    				$summ += $summItem;
    				$this->tpl->parse('ORDER_DETAIL_LIST', '.order_detail_list');
    			} else {
    				$this->tpl->parse('ORDER_DETAIL_LIST', 'null');
    			}
    		}
    		
    		
    	} else {
    		$this->tpl->parse('ORDER_DETAIL_LIST', 'null');
    	}
    	
    	$deliveryType = array(
    		'delivery_service'=>'Служба доставки',
    		'pickup'=>'Самовывоз'
    	);
    	
    	if ($orderDetail['delivery_type'] == 'pickup') {
    		$this->tpl->parse('ORDER_DETAIL_SERVICE', 'null');
    	}
    	
    	
    	
    	$orderStatus = array(
			'no-paid'=>'Не оплачен',
			'paid'=>'Оплачен',
			'completed'=>'Выполнен',
			'booked'=>'Бронь',
			'advance'=>'Аванс'
		);
    	
		
		
    	$this->tpl->assign(array(
						'PROBA_CATALOG'     =>  (''),
    		'ORDER_DETAIL_ID'=>$id, 
    		'ORDER_DETAIL_ID1'=>$idItem, 
    		'ORDER_DETAIL_MAIL'=>$orderDetail['email'], 
    		'ORDER_DETAIL_FIO'=>$orderDetail['fio'], 
                'ORDER_DETAIL_FIO'=>$orderDetail['tvivoz'], 
    		'ORDER_DETAIL_DATE'=>$orderDetail['date'], 
    		'ORDER_DETAIL_CITY'=>$orderDetail['city'], 
    		'ORDER_DETAIL_PHONE'=>$orderDetail['phone'], 
    		'ORDER_DETAIL_DELIVERY_TYPE'=>$deliveryType[$orderDetail['delivery_type']], 
    		'ORDER_DETAIL_DELIVERY_SERVICE'=>$orderDetail['delivery_service'], 
    		'ORDER_DETAIL_MOBAIL_PHONE'=>$orderDetail['modile_phone'], 
    		'ADM_STATUS_NO'=>'',
    		'PAID_SELECTED'=>'',
    		'ADM_STATUS_NO_PAID_SELECTED'=>(isset($orderDetail['status']) && $orderDetail['status'] == 'no-paid' ? 'selected' : ''),
    		'ADM_STATUS_PAID_SELECTED'=>(isset($orderDetail['status']) && $orderDetail['status'] == 'paid' ? 'selected' : ''),
    		'ADM_STATUS_COMPLETED_SELECTED'=>(isset($orderDetail['status']) && $orderDetail['status'] == 'completed' ? 'selected' : ''),
    		'ADM_STATUS_BOOKED_SELECTED'=>(isset($orderDetail['status']) && $orderDetail['status'] == 'booked' ? 'selected' : ''),
    		'ADM_STATUS_ADVANCED_SELECTED'=>(isset($orderDetail['status']) && $orderDetail['status'] == 'advance' ? 'selected' : ''),
    		'ORDER_DETAIL_SUMM' =>number_format($summ, 2, ', ', ' '),
    		'ORDER_DETAIL_BODY' =>$orderDetail['body']
    	));
    	
    	$this->tpl->parse('CONTENT', '.order_detail');    	
    
    	return $this->tpl->prnt_to_var();
    	
    	
    }
    
    private function isSetOrders() {
    	$retVal = false;
    	$goodsSession = new Zend_Session_Namespace('goods');
    	if (isset($goodsSession->array)) {
    		if (count($goodsSession->array) > 0)	{
    			foreach ($goodsSession->array as $item) { 
    				if ($item['status'] != 'deleted') {
    					$retVal = true;
    					break;
    				}
    			}
    		}    		
    	}
    	
    	if (!$retVal) {
		
		$ua=$this->getUa();
    		$this->tpl->define_dynamic('_order_empty', "$ua".'order.tpl');
    		$this->tpl->define_dynamic('order_empty', '_order_empty');
    		$this->tpl->parse('CONTENT', '.order_empty');
    	}
    	return $retVal;
    }
}
