<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML><HEAD><TITLE>{TITLE}</TITLE>
<meta name="keywords" content="{KEYWORDS}">
<meta name="description" content="{DESCRIPTION}">
<META http-equiv=Content-Type content="text/html; charset=windows-1251">
<meta name="document-state" content="dynamic">
<meta name="revisit" content="7 days">
<meta name="revisit-after" content="7 days">
<META NAME="Resourse-type" CONTENT="document">
<META NAME="Robots" CONTENT="index,follow">
<META NAME="Rating" CONTENT="general">
<META NAME="Distribution" CONTENT="global">
<META NAME="Classification" CONTENT="">
<META NAME="Category" CONTENT="">
<meta http-equiv="Pragma" content="token">
<meta http-equiv="Cache-Control" content="token">
<META NAME="Copyright" CONTENT="2010 pixel.dp.ua">
<LINK href="/css/style.css" type="text/css" rel=stylesheet>
<link  rel="stylesheet" type="text/css" href="/css/jquery.lightbox-0.5.css" />
<!--[if IE]><link rel="stylesheet" href="/css/ie.css" type="text/css" /><![endif]-->
<!--[if lte IE 6]>  
<script defer type="text/javascript" src="/js/pngfix.js" mce_src="/js/pngfix.js"></script>
<![endif]-->
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jq-order-form.js"></script>
<SCRIPT type="text/javascript" src='/js/jquery.lightbox-0.5.min.js'></SCRIPT>
<SCRIPT type="text/javascript" src='/js/jq-light-box-init.js'></SCRIPT>

<!-- BDP: adminjslib -->
<script type="text/javascript" src="/js/swfobject.js"></script>
    <script type="text/javascript" src="/js/jquery.uploadify.v2.1.0.min.js"></script>

    	<script type="text/javascript">
    		jQuery(document).ready(function() {
    			jQuery("#uploadify").uploadify({
    				
    				'expressInstall': '/modules/uploadify/expressInstall.swf',
    				'uploader': '/modules/uploadify/uploadify.swf',
    				'script': '/modules/uploadify/uploadify.php',
    				'folder': '/upload',
    				'cancelImg': '/modules/uploadify/cancel.png',
    				
    				'queueID'        : 'fileQueue',
    				'fileDesc'       : 'jpg, gif, png',
    				'fileExt'        : '*.jpg;*.gif;*.png;*.jpeg',
    				'auto'           : false,
    				'multi'          : true,
    				'onComplete'   : function(event,queueID,fileObj,response,data) {
    									if (response != 'OK') {
    										jQuery('#response').append(response);
    									}
    								 },
    				onError: function (event, queueID, fileObj, errorObj) {
    							 err = '';
    					         if (errorObj.status == 404) {
    					            err += 'Could not find upload script.';
    					         } else if (errorObj.type === "HTTP") {
    					            err += 'error '+errorObj.type+": "+errorObj.status;
    					         } else if (errorObj.type ==="File Size") {
    					            err += fileObj.name+' '+errorObj.type+' Limit: '+Math.round(errorObj.sizeLimit/1024)+'KB';
    					         } else {
    					            err += 'error '+errorObj.type+": "+errorObj.text;
    					         }
    					      },
    				/*
    				*/
    			});
    		});
    	</script>


<!-- EDP: adminjslib -->
<script type="text/javascript" src="/js/jquery.maskedinput-1.2.2.min.js"></script>

<script type="text/javascript" src="/js/prototype.js"></script>
<script type="text/javascript" src="/js/scripts.js"></script>
<style>

</style>
</HEAD>
<body>

<div class="wraper">

<div class="header">
<table class="logo" >
<tr>
<td colspan="3" class="top">
 <div class="h-menu">  
  <table class="w100pr"> 
   <tr>
	<td class="first0"></td>
	 <!-- BDP: horisontal -->
	 <td class=""><div class="rel"><a href="{MENU_HREF}" title="{MENU_NAME}">{MENU_NAME}</a></div></td>	
	<!-- EDP: horisontal -->	
    <td class="last0"></td> 
   </tr>
  </table>  
  </div>
</td>
</tr>
<tr>

<td class="left" height="60" >
<a class="logo" href="/"  alt="{LOGOALT}" title="{LOGOALT}"><img src="/img/logo.jpg" width="152" height="35" alt="{LOGOALT}" title="{LOGOALT}" /><br />{SLG1}</a>
</td>
<td class="center" valign="middle" id="slg-2">
{SLG2}
</td>
<td class="right" valign="top">
  <div class="search_block">
  
  
  
   <form action="/search/" method="get">
  
    <p><img src="/img/input_left.gif" width="5" height="22" alt="" /><input value="����� �������" name="q" onclick="value=''" onfocus="if (this.value=='������� ������'){this.value=''}" onblur="if (this.value==''){this.value='������� ������'}" type="text" /><img src="/img/input_right.gif" width="5" height="22" alt="" /><input type="image" src="/img/search.gif" class="button"/></p>
  
    </form>
   	
</td>
</tr>
<tr>
<td class="left" height="55" valign="top">
  
  <ul class="pic_menu">
   <li class="active"><a class="home" href="/" title="{TITLEMAIN}">&nbsp;</a></li>
   <li><a href="/sitemap" title="{TITLEMAP}" class="sitemap">&nbsp;</a></li>
   <li><a href="#" onClick="add2Fav (this, 'http://{SERVER_NAME}/');" title="{TITLEFEED}" class="email">&nbsp;</a></li>
  
  </ul>
</td>
<td  class="center" valign="top">
 <p class="phone">{SLG2_PHONES}</p
</td>

<td class="right" valign="top">
  <div class="busket_block" id="busket-block"> 
  <script type="text/javascript">
  window.onload = function() {
    getBasket();	
  }
</script> 
  </div>
</td>

</tr>
</table>


</div>


 <!--<div class="top_banner"><a href="#"><img src="/img/pic.jpg" width="468" height="60" alt="" /></a></div>-->
 <div class="pager">{WAY}</div>
 <div class="columns_wrap">
 <div class="left_column">
  
 <!-- BDP: administration -->
 <div class="title-header">
 	<h2>�����������������</h2>
 </div>
 
 <ul class="left_menu">
 	<li><h3><a href='{BASE_PATH}admin/editpage/mainpage' title="������� ��������">������� ��������</a></h3></li>
 	<li><h3><a href='{BASE_PATH}admin/menu/horisontal' title="�������������� ����">�������������� ����</a></h3></li>
 	<li><h3><a href='{BASE_PATH}admin/menu/vertical' title="������������ ����">������������ ����</a></h3></li>
 	<li><h3><a href='{BASE_PATH}news' title="�������">�������</a></h3></li>
 	<li><h3><a href='{BASE_PATH}admin/orders/' title="���� �������">���� �������</a></h3></li>
        <li><h3><a href='{BASE_PATH}admin/deliveryservice/' title="�">������ ��������</a></h3></li>
 	<li><h3><a href='{BASE_PATH}admin/import/' title="������ �������">������ �������</a></h3></li>
 	<li><h3><a href='{BASE_PATH}admin/loadcatpics/' title="�������� �������� ��������">�������� �������� ��������</a></h3></li>
 	<li><h3><a href='{BASE_PATH}admin/settings' title="��������� �����">��������� �����</a></h3></li>
 	<li><h3><a href='{BASE_PATH}admin/lookups' title="������������� ����">������������� ����</a></h3></li>
 	<li><h3><a href='{BASE_PATH}admin/metatags' title="����-����">����-����</a></h3></li>
 	<li><h3><a href='{BASE_PATH}logout' title="�����">�����</a></h3></li>
 </ul>	
                        
  <br /> <br />
  
  <!-- EDP: administration -->
 <div class="title-header">
 <h2><a href="/catalog/">{CATTITLE}</a></h2>
 </div>
   
  
  <ul class="left_menu">
   
  <li><h3><a href="/catalog/novelty" title="�������">�������</a></h3></li>
 <li><h3><a href="/catalog/hits" title="����">����</a></h3></li>
   
  <!-- BDP: catalog_menu -->
                             
  	<!-- BDP: catalog_menu_complex -->
  		<li><h3><a href='/catalog{CATALOG_MENU_HREF}' title='{CATALOG_MENU_NAME}'>{CATALOG_MENU_NAME}</a></h3>    	
         	<ul>
        	<!-- BDP: catalog_menu_complex_sub -->
        		<li><a href="/catalog{CATALOG_MENU_HREF}"  title='{CATALOG_MENU_NAME}'>{CATALOG_MENU_NAME}</a></li>            	
           <!-- EDP: catalog_menu_complex_sub -->
            </ul>
       </li>
       <!-- EDP: catalog_menu_complex -->
       
       <!-- BDP: catalog_menu_single -->
       	<li><h3><a href='/catalog{CATALOG_MENU_HREF}' title='{CATALOG_MENU_NAME}'>{CATALOG_MENU_NAME}</a></h3></li>
       <!-- EDP: catalog_menu_single -->
   <!-- EDP: catalog_menu -->
   
   
  </ul>
  
  <div class="clear"></div>
  <p class="banner"><a href="#"><img src="/img/banner.gif" width="160" height="600" alt="" /></a></p>
 </div>
 <div class="right_column">
 
  <!-- BDP: p_header -->
     <h1 class="title_p">{HEADER}</h1>
 <!-- EDP: p_header -->
                    
 {CONTENT}
 



   </div>
 <div class="footer">
 <div class="footer-line"></div>
  <div class="left">
   <p>
   <div class="div-logo">   
   <img src="/img/logo-bot.gif" width="88" height="31" alt="" style="float: left; padding-right: 10px;" />
   
   </div>
   <div class="adress">{ADRES}</div></p>
  </div>
  <div class="right">
   <p>{SLG3}</p>
  </div>
  <div class="clear" style="border-bottom: 2px solid #c9c9c9; padding-top: 20px;"></div>
  <div class="left">
   <p class="cs"><a href="http://bingo.in.ua/" alt="Bingo! �������� ������" title="Bingo! �������� ������">�������� ������</a></p>
   <p><a href="http://bingo.in.ua/"><img src="/img/bingo-logo.gif" width="85" height="29" class="bingo"  alt="Bingo! �������� ������" title="Bingo! �������� ������" target="_blank"  /><a/></p>
  </div>
  <div class="right">
   <p class="partner">���� ������</p>   
   <ul>
   {PARTNERS}
    
   </ul>
  </div>
  <div class="clear" style="margin-bottom: 20px;"></div>
 </div>
</div>
</body>
</HTML>