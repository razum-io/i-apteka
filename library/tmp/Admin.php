<?php

require_once PATH . 'library/Abstract.php';

require_once PATH . 'library/Interface.php';

class Admin extends Main_Abstract implements Main_Interface {

   private $doImportArr = array();

   public function factory() {
      if (!$this->_isAdmin()) {
         return false;
      }

      $this->setWay('�����������������');

      $this->tpl->define_dynamic('edit', 'adm/edit.tpl');

      return true;
   }

   public function main() {
      return $this->error404();
   }

   public function addnews() {
      $this->setMetaTags('���������� �������');
      $this->setWay('���������� �������');

      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('mce', 'edit');
      $this->tpl->define_dynamic('pic', 'edit');
      $this->tpl->define_dynamic('adress', 'edit');
      $this->tpl->define_dynamic('meta', 'edit');
      $this->tpl->define_dynamic('visible', 'edit');
      $this->tpl->define_dynamic('news', 'edit');
      $this->tpl->define_dynamic('body', 'edit');
      $this->tpl->define_dynamic('end', 'edit');

      $href = $this->ru2Lat($this->getVar('adm_href', ''));
      //$href = str_replace(' ', '_', $this->getVar('adm_href', ''));
      $topnews = $this->getVar('topnews', 0);
      $date = $this->getVar('date', date('d.m.Y'));
      $preview = $this->getVar('preview', '');
      $header = $this->getVar('header', '');
      $title = $this->getVar('title', '');
      $keywords = $this->getVar('keywords', '');
      $description = $this->getVar('description', '');
      $visible = $this->getVar('visible', 1);
      $body = $this->getVar('body', '');

      $visible_s = '';
      if ($visible == 1) {
         $visible_s .= "<option value='1' selected>��</option>
            <option value='0'>���</option>";
      } else {
         $visible_s .= "<option value='1'>��</option>
            <option value='0' selected>���</option>";
      }

      $topnews_s = '';
      if ($topnews == 1) {
         $topnews_s .= "<option value='1' selected>��</option>
            <option value='0'>���</option>";
      } else {
         $topnews_s .= "<option value='1'>��</option>
            <option value='0' selected>���</option>";
      }

      if (!empty($_POST)) {
         $referer = $this->getVar('HTTP_REFERER', '');

         if (!$href) {
            $this->addErr('�� �������� �����');
         } else {
            $count = $this->db->fetchOne('SELECT COUNT(`id`) FROM `news` WHERE `href` = "' . $href . '" AND `language` = "' . $this->lang . '"');

            if ($count > 0) {
               $this->addErr('������� � ����� ������� ��� ����������');
            }
         }
      } else {
         $referer = $this->getVar('HTTP_REFERER', $this->basePath);
      }

      if (!empty($_POST) && !$this->_err) {
         $date = explode('.', $date);

         $date = mktime(date('H'), date('i'), date('s'), $date[1], $date[0], $date[2]);

         $data = array(
             'href' => $href,
             'top' => $topnews,
             'date' => $date,
             'preview' => stripslashes($preview),
             'header' => $header,
             'title' => $title,
             'keywords' => $keywords,
             'description' => $description,
             'visibility' => $visible,
             'body' => stripslashes($body),
             'language' => $this->lang
         );

         $id = $this->db->fetchOne("SELECT MAX(`id`) FROM `news`") + 1;

         if ($pic = $this->getVar('pic')) {
            if (null !== $pic && $pic['error'] == 0) {
               if (!$this->uploadCatPic($pic['tmp_name'], './img/news/' . $id . '-' . $pic['name'])) {
                  $this->addErr('�� ����� �������� �������� ��������� ������');
               }

               $data['pic'] = $id . '-' . $pic['name'];
            }
         }
      }

      if (!empty($_POST) && !$this->_err) {
         $this->db->insert("news", $data);

         $referer = (!empty($referer) && $referer != '{REFERER}') ? $referer : $this->basePath;
         $content = "������� ������� ���������<meta http-equiv='refresh' content='2;URL=$referer'>";

         $this->viewMessage($content);
      }

      if ($this->_err) {
         $this->viewErr();
      }

      if (empty($_POST) || $this->_err) {
         $this->tpl->assign(
                 array(
                     'ADM_HREF' => $href,
                     'ADM_TOPNEWS' => $topnews_s,
                     'ADM_DATE' => $this->convertDate($date),
                     'ADM_PREVIEW' => stripslashes($preview),
                     'ADM_BODY' => stripslashes($body),
                     'VISIBLE_S' => $visible_s,
                     'ADM_HEADER' => $header,
                     'ADM_TITLE' => $title,
                     'ADM_KEYWORDS' => $keywords,
                     'ADM_DESCRIPTION' => $description,
                     'REFERER' => $referer
                 )
         );

         $this->tpl->parse('CONTENT', '.start');
         $this->tpl->parse('CONTENT', '.mce');
         $this->tpl->parse('CONTENT', '.adress');
         $this->tpl->parse('CONTENT', '.pic');
         $this->tpl->parse('CONTENT', '.visible');
         $this->tpl->parse('CONTENT', '.news');
         $this->tpl->parse('CONTENT', '.meta');
         $this->tpl->parse('CONTENT', '.body');
         $this->tpl->parse('CONTENT', '.end');
      }

      return true;
   }

   public function editnews() {
      $id = end($this->url);

      if (!ctype_digit($id)) {
         return $this->error404();
      }

      $news = $this->db->fetchRow("SELECT * FROM `news` WHERE `id` = '$id'");

      if (!$news) {
         return $this->error404();
      }

      $this->setMetaTags('�������������� �������');
      $this->setWay('�������������� �������');

      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('mce', 'edit');
      $this->tpl->define_dynamic('pic', 'edit');
      $this->tpl->define_dynamic('adress', 'edit');
      $this->tpl->define_dynamic('meta', 'edit');
      $this->tpl->define_dynamic('visible', 'edit');
      $this->tpl->define_dynamic('news', 'edit');
      $this->tpl->define_dynamic('body', 'edit');
      $this->tpl->define_dynamic('end', 'edit');

      $href = $news['href'];
      $topnews = $news['top'];
      $date = $this->convertDate($news['date']);
      $preview = $news['preview'];
      $header = $news['header'];
      $title = $news['title'];
      $keywords = $news['keywords'];
      $description = $news['description'];
      $visible = $news['visibility'];
      $body = $news['body'];

      if (!empty($_POST)) {
         $referer = $this->getVar('HTTP_REFERER', '');

         //$href = str_replace(' ', '_', $this->getVar('adm_href', ''));
         $href = $this->ru2Lat($this->getVar('adm_href', ''));
         $topnews = $this->getVar('topnews', 0);
         $date = $this->getVar('date', date('d.m.Y'));
         $preview = $this->getVar('preview', '');
         $header = $this->getVar('header', '');
         $title = $this->getVar('title', '');
         $keywords = $this->getVar('keywords', '');
         $description = $this->getVar('description', '');
         $visible = $this->getVar('visible');

         $body = $this->getVar('body', '');

         if (!$href) {
            $this->addErr('�� �������� �����');
         } else {
            $count = $this->db->fetchOne("SELECT COUNT(`id`) FROM `news` WHERE `href` = '$href' AND `language` = '" . $this->lang . "' AND `id` <> '" . $id . "'");

            if ($count > 0) {
               $this->addErr('������� � ����� ������� ��� ����������');
            }
         }
      } else {
         $referer = $this->getVar('HTTP_REFERER', $this->basePath);
      }

      $visible_s = '';
      if ($visible == 1) {
         $visible_s .= "<option value='1' selected>��</option>
            <option value='0'>���</option>";
      } else {
         $visible_s .= "<option value='1'>��</option>
            <option value='0' selected>���</option>";
      }

      $topnews_s = '';
      if ($topnews == 1) {
         $topnews_s .= "<option value='1' selected>��</option>
            <option value='0'>���</option>";
      } else {
         $topnews_s .= "<option value='1'>��</option>
            <option value='0' selected>���</option>";
      }

      if (!empty($_POST) && !$this->_err) {
         $date = explode('.', $date);

         $date = mktime(date('H'), date('i'), date('s'), $date[1], $date[0], $date[2]);

         $data = array(
             'href' => $href,
             'top' => $topnews,
             'date' => $date,
             'preview' => stripslashes($preview),
             'header' => $header,
             'title' => $title,
             'keywords' => $keywords,
             'description' => $description,
             'visibility' => $visible,
             'body' => stripslashes($body)
         );

         if ($pic = $this->getVar('pic')) {
            if (null !== $pic && $pic['error'] == 0) {
               if (!$this->uploadCatPic($pic['tmp_name'], './img/news/' . $id . '-' . $pic['name'])) {
                  $this->addErr('�� ����� �������� �������� ��������� ������');
               }

               $data['pic'] = $id . '-' . $pic['name'];
            }
         }
      }

      if (!empty($_POST) && !$this->_err) {
         $n = $this->db->update('news', $data, "id = $id");

         $referer = (!empty($referer) && $referer != '{REFERER}') ? $referer : $this->basePath;
         $content = "������� ������� ���������������<meta http-equiv='refresh' content='2;URL=$referer'>";

         $this->viewMessage($content);
      }

      if ($this->_err) {
         $this->viewErr();
      }

      if (empty($_POST) || $this->_err) {
         $this->tpl->assign(
                 array(
                     'ADM_HREF' => $href,
                     'ADM_TOPNEWS' => $topnews_s,
                     'ADM_DATE' => $this->convertDate($date),
                     'ADM_PREVIEW' => stripslashes($preview),
                     'ADM_BODY' => stripslashes($body),
                     'VISIBLE_S' => $visible_s,
                     'ADM_HEADER' => $header,
                     'ADM_TITLE' => $title,
                     'ADM_KEYWORDS' => $keywords,
                     'ADM_DESCRIPTION' => $description,
                     'REFERER' => $referer
                 )
         );

         $this->tpl->parse('CONTENT', '.start');
         $this->tpl->parse('CONTENT', '.mce');
         $this->tpl->parse('CONTENT', '.adress');
         $this->tpl->parse('CONTENT', '.pic');
         $this->tpl->parse('CONTENT', '.visible');
         $this->tpl->parse('CONTENT', '.news');
         $this->tpl->parse('CONTENT', '.meta');
         $this->tpl->parse('CONTENT', '.body');
         $this->tpl->parse('CONTENT', '.end');
      }

      return true;
   }

   public function deletenews() {
      $id = end($this->url);

      if (!ctype_digit($id)) {
         return $this->error404();
      }

      $news = $this->db->fetchRow("SELECT * FROM `news` WHERE `id` = '$id'");

      if (!$news) {
         return $this->error404();
      }

      $this->setMetaTags('�������� �������');
      $this->setWay('�������� �������');

      if ($news['pic'] != '' && file_exists('./img/news/' . $news['pic'])) {
         @unlink('./img/news/' . $news['pic']);
      }

      $n = $this->db->delete('news', "id = $id");

      $referer = $this->getVar('HTTP_REFERER', $this->basePath);

      $content = "������� ������� �������<meta http-equiv='refresh' content='2;URL=$referer'>";
      $this->viewMessage($content);

      return true;
   }

   public function menu() {
      $type = end($this->url);

      switch ($type) {
         case 'horisontal' :
         case 'vertical' :
            break;
         default : return $this->error404();
            break;
      }

      $meta = ($type == 'horisontal' ? '�������������� ' : '������������ ') . '����';

      $this->setMetaTags($meta);
      $this->setWay($meta);

      $menus = $this->db->fetchAll("SELECT `id`, `header`, `href`, `type` FROM `page` WHERE `level` = '0' AND `menu` = '$type' ORDER BY `position`, `header`");

      $this->viewMessage($this->getAdminAdd('page', $type));

      if ($menus) {
         $this->tpl->define_dynamic('pages', 'pages.tpl');
         $this->tpl->define_dynamic('short_list', 'pages');

         foreach ($menus as $menu) {
            $this->tpl->assign(
                    array(
                        'PAGE_ADM' => $this->getAdminEdit('page', $menu['id']),
                        'PAGE_ADRESS' => $menu['type'] == 'link' ? $menu['href'] : $this->basePath . $menu['href'],
                        'PAGE_HEADER' => stripslashes($menu['header'])
                    )
            );

            $this->tpl->parse('CONTENT', '.short_list');
         }
      } else {
         $this->viewMessage('{EMPTY_SECTION}');
      }

      return true;
   }

   public function addlink() {
      return $this->addpages('link');
   }

   public function addpage() {
      return $this->addpages('page');
   }

   public function addsection() {
      return $this->addpages('section');
   }

   private function addpages($type = null) {
      if (null === $type) {
         return $this->error404();
      }

      $id = end($this->url);

      if ($id == 'horisontal' || $id == 'vertical') {
         $level = 0;
         $menu = $id;
      } elseif (ctype_digit($id) && $id > 0) {
         $level = $id;
         $menu = 'none';
      } else {
         return $this->error404();
      }

      switch ($type) {
         case 'section' : $meta = '���������� ������ �������';
            break;
         case 'page' : $meta = '���������� ����� ��������';
            break;
         case 'link' : $meta = '���������� ����� �����';
            break;
         default : return $this->error404();
            break;
      }

      $this->setMetaTags($meta);
      $this->setWay($meta);

      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('mce', 'edit');
      $this->tpl->define_dynamic('adress', 'edit');
      $this->tpl->define_dynamic('pos', 'edit');
      $this->tpl->define_dynamic('meta', 'edit');
      $this->tpl->define_dynamic('header', 'edit');
      $this->tpl->define_dynamic('visible', 'edit');
      $this->tpl->define_dynamic('top', 'edit');
      $this->tpl->define_dynamic('preview', 'edit');
      $this->tpl->define_dynamic('body', 'edit');
      $this->tpl->define_dynamic('end', 'edit');

      //$href = str_replace(' ', '_', $this->getVar('adm_href', ''));
      $href = $this->ru2Lat($this->getVar('adm_href', ''));
      $position = $this->getVar('position', 9999);
      $preview = $this->getVar('preview', '');
      $header = $this->getVar('header', '');
      $title = $this->getVar('title', '');
      $keywords = $this->getVar('keywords', '');
      $description = $this->getVar('description', '');
      $visible = $this->getVar('visible', 1);
      $top = $this->getVar('top', 0);
      $body = $this->getVar('body', '');

      $visible_s = '';
      if ($visible == 1) {
         $visible_s .= "<option value='1' selected>��</option>
            <option value='0'>���</option>";
      } else {
         $visible_s .= "<option value='1'>��</option>
            <option value='0' selected>���</option>";
      }

      $top_s = '';
      if ($top == 1) {
         $top_s .= "<option value='1' selected>��</option>
            <option value='0'>���</option>";
      } else {
         $top_s .= "<option value='1'>��</option>
            <option value='0' selected>���</option>";
      }

      if (!empty($_POST)) {
         $referer = $this->getVar('HTTP_REFERER', '');

         if (!$href) {
            $this->addErr('�� �������� �����');
         } else {
            $count = $this->db->fetchOne('SELECT COUNT(`id`) FROM `page` WHERE `href` = "' . $href . '" AND `language` = "' . $this->lang . '"');

            if ($count > 0) {
               $this->addErr('������� � ����� ������� ��� ����������');
            }
         }
      } else {
         $referer = $this->getVar('HTTP_REFERER', $this->basePath);
      }

      if (!empty($_POST) && !$this->_err) {
         $data = array(
             'href' => $href,
             'position' => $position,
             'preview' => stripslashes($preview),
             'header' => $header,
             'title' => $title,
             'keywords' => $keywords,
             'description' => $description,
             'visibility' => $visible,
             'body' => stripslashes($body),
             'level' => $level,
             'menu' => $menu,
             'top' => $top,
             'type' => $type,
             'language' => $this->lang
         );

         $this->db->insert("page", $data);

         $referer = (!empty($referer) && $referer != '{REFERER}') ? $referer : $this->basePath;
         $content = "������� ������� ��������<meta http-equiv='refresh' content='2;URL=$referer'>";

         $this->viewMessage($content);
      }

      if ($this->_err) {
         $this->viewErr();
      }

      if (empty($_POST) || $this->_err) {
         $this->tpl->assign(
                 array(
                     'ADM_HREF' => $href,
                     'ADM_POSITION' => $position,
                     'ADM_PREVIEW' => stripslashes($preview),
                     'ADM_BODY' => stripslashes($body),
                     'VISIBLE_S' => $visible_s,
                     'TOP_S' => $top_s,
                     'ADM_HEADER' => $header,
                     'ADM_TITLE' => $title,
                     'ADM_KEYWORDS' => $keywords,
                     'ADM_DESCRIPTION' => $description,
                     'REFERER' => $referer
                 )
         );

         $this->tpl->parse('CONTENT', '.start');
         $this->tpl->parse('CONTENT', '.mce');
         $this->tpl->parse('CONTENT', '.adress');
         if ($type == 'link')
            $this->tpl->parse('CONTENT', '.header');
         $this->tpl->parse('CONTENT', '.pos');
         $this->tpl->parse('CONTENT', '.visible');
         if ($type == 'page')
            $this->tpl->parse('CONTENT', '.top');
         if ($type != 'link')
            $this->tpl->parse('CONTENT', '.meta');
         if ($menu == 'none' && $type != 'link')
            $this->tpl->parse('CONTENT', '.preview');
         if ($type != 'link')
            $this->tpl->parse('CONTENT', '.body');
         $this->tpl->parse('CONTENT', '.end');
      }

      return true;
   }

   public function editpage() {
      $id = end($this->url);

      if ($id == 'mainpage') {
         $page = $this->db->fetchRow("SELECT * FROM `page` WHERE `language` = '" . $this->lang . "' AND `href` = '$id'");
         $type = 'mainpage';
      } elseif (ctype_digit($id) && $id > 0) {
         $page = $this->db->fetchRow("SELECT * FROM `page` WHERE `id` = '$id'");
         $type = $page['type'];
      } else {
         return $this->error404();
      }

      if (!$page) {
         return $this->error404();
      }

      $id = $page['id'];

      switch ($type) {
         case 'mainpage' : $meta = '�������������� ������� ��������';
            break;
         case 'section' : $meta = '�������������� �������';
            break;
         case 'page' : $meta = '�������������� ��������';
            break;
         case 'link' : $meta = '�������������� ������';
            break;
         default : return $this->error404();
            break;
      }

      $this->setMetaTags($meta);
      $this->setWay($meta);

      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('mce', 'edit');
      $this->tpl->define_dynamic('adress', 'edit');
      $this->tpl->define_dynamic('pos', 'edit');
      $this->tpl->define_dynamic('meta', 'edit');
      $this->tpl->define_dynamic('header', 'edit');
      $this->tpl->define_dynamic('visible', 'edit');
      $this->tpl->define_dynamic('top', 'edit');
      $this->tpl->define_dynamic('preview', 'edit');
      $this->tpl->define_dynamic('body', 'edit');
      $this->tpl->define_dynamic('end', 'edit');

      $href = $page['href'];
      $position = $page['position'];
      $preview = $page['preview'];
      $header = $page['header'];
      $title = $page['title'];
      $keywords = $page['keywords'];
      $description = $page['description'];
      $visible = $page['visibility'];
      $top = $page['top'];
      $body = $page['body'];

      if (!empty($_POST)) {
         if ($type == 'mainpage') {
            $href = 'mainpage';
            $visible = 1;
         } else {
            //$href = str_replace(' ', '_', $this->getVar('adm_href', ''));
            $href = $this->ru2Lat($this->getVar('adm_href', ''));
            $visible = $this->getVar('visible', 1);
         }

         $position = $this->getVar('position', 9999);
         $preview = $this->getVar('preview', '');
         $header = $this->getVar('header', '');
         $title = $this->getVar('title', '');
         $keywords = $this->getVar('keywords', '');
         $description = $this->getVar('description', '');
         $visible = $this->getVar('visible', 1);
         $top = $this->getVar('top', 0);
         $body = $this->getVar('body', '');

         $referer = $this->getVar('HTTP_REFERER', '');

         if (!$href) {
            $this->addErr('�� �������� �����');
         } else {
            $count = $this->db->fetchOne("SELECT COUNT(`id`) FROM `page` WHERE `href` = '$href' AND `language` = '" . $this->lang . "' AND `id` <> $id");

            if ($count > 0) {
               $this->addErr('������� � ����� ������� ��� ����������');
            }
         }
      } else {
         $referer = $this->getVar('HTTP_REFERER', $this->basePath);
      }

      $visible_s = '';
      if ($visible == 1) {
         $visible_s .= "<option value='1' selected>��</option>
            <option value='0'>���</option>";
      } else {
         $visible_s .= "<option value='1'>��</option>
            <option value='0' selected>���</option>";
      }

      $top_s = '';
      if ($top == 1) {
         $top_s .= "<option value='1' selected>��</option>
            <option value='0'>���</option>";
      } else {
         $top_s .= "<option value='1'>��</option>
            <option value='0' selected>���</option>";
      }

      if (!empty($_POST) && !$this->_err) {
         $data = array(
             'href' => $href,
             'position' => $position,
             'preview' => stripslashes($preview),
             'header' => $header,
             'title' => $title,
             'keywords' => $keywords,
             'description' => $description,
             'visibility' => $visible,
             'body' => stripslashes($body),
             'top' => $top
         );

         $n = $this->db->update('page', $data, "id = $id");

         $referer = (!empty($referer) && $referer != '{REFERER}') ? $referer : $this->basePath;
         $content = "������� ������� �������<meta http-equiv='refresh' content='2;URL=$referer'>";

         $this->viewMessage($content);
      }

      if ($this->_err) {
         $this->viewErr();
      }

      if (empty($_POST) || $this->_err) {
         $this->tpl->assign(
                 array(
                     'ADM_HREF' => $href,
                     'ADM_POSITION' => $position,
                     'ADM_PREVIEW' => $preview,
                     'ADM_BODY' => $body,
                     'VISIBLE_S' => $visible_s,
                     'TOP_S' => $top_s,
                     'ADM_HEADER' => $header,
                     'ADM_TITLE' => $title,
                     'ADM_KEYWORDS' => $keywords,
                     'ADM_DESCRIPTION' => $description,
                     'REFERER' => $referer
                 )
         );

         $this->tpl->parse('CONTENT', '.start');
         $this->tpl->parse('CONTENT', '.mce');
         if ($type != 'mainpage')
            $this->tpl->parse('CONTENT', '.adress');
         if ($type == 'link')
            $this->tpl->parse('CONTENT', '.header');
         if ($type != 'mainpage')
            $this->tpl->parse('CONTENT', '.pos');
         if ($type != 'mainpage')
            $this->tpl->parse('CONTENT', '.visible');
         if ($type == 'page')
            $this->tpl->parse('CONTENT', '.top');
         if ($type != 'link')
            $this->tpl->parse('CONTENT', '.meta');
         if ($type != 'mainpage' && $type != 'link')
            $this->tpl->parse('CONTENT', '.preview');
         if ($type != 'link')
            $this->tpl->parse('CONTENT', '.body');
         $this->tpl->parse('CONTENT', '.end');
      }

      return true;
   }

   public function editcatpage() {
      $id = end($this->url);

      if (ctype_digit($id) && $id > 0) {
         $page = $this->db->fetchRow("SELECT * FROM `catalog` WHERE `id` = '$id'");
      } else {
         return $this->error404();
      }

      if (!$page) {
         return $this->error404();
      }

      $this->setMetaTags('�������������� ������');
      $this->setWay('�������������� ������');

      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('mce', 'edit');
      $this->tpl->define_dynamic('adress', 'edit');
      $this->tpl->define_dynamic('pos', 'edit');
      $this->tpl->define_dynamic('meta', 'edit');
      $this->tpl->define_dynamic('header', 'edit');
      $this->tpl->define_dynamic('visible', 'edit');
      $this->tpl->define_dynamic('top', 'edit');
      $this->tpl->define_dynamic('preview', 'edit');
      $this->tpl->define_dynamic('body', 'edit');
      $this->tpl->define_dynamic('end', 'edit');

      $href = $page['href'];
      $position = $page['position'];
      $preview = $page['preview'];
      $header = $page['header'];
      $title = $page['title'];
      $keywords = $page['keywords'];
      $description = $page['description'];
      $visible = $page['visibility'];

      $body = $page['body'];

      if (!empty($_POST)) {
         //$href = str_replace(' ', '_', $this->getVar('adm_href', ''));
         $href = $this->ru2Lat($this->getVar('adm_href', ''));
         $visible = $this->getVar('visible', 1);


         $position = $this->getVar('position', 9999);
         $preview = $this->getVar('preview', '');
         $header = $this->getVar('header', '');
         $title = $this->getVar('title', '');
         $keywords = $this->getVar('keywords', '');
         $description = $this->getVar('description', '');
         $visible = $this->getVar('visible', 1);
         $top = $this->getVar('top', 0);
         $body = $this->getVar('body', '');

         $referer = $this->getVar('HTTP_REFERER', '');

         if (!$href) {
            $this->addErr('�� �������� �����');
         } else {
            $count = $this->db->fetchOne("SELECT COUNT(`id`) FROM `page` WHERE `href` = '$href' AND `language` = '" . $this->lang . "' AND `id` <> $id");

            if ($count > 0) {
               $this->addErr('������� � ����� ������� ��� ����������');
            }
         }
      } else {
         $referer = $this->getVar('HTTP_REFERER', $this->basePath);
      }


      if (!empty($_POST) && !$this->_err) {
         $data = array(
             'href' => $href,
             'position' => $position,
             'preview' => stripslashes($preview),
             'header' => $header,
             'title' => $title,
             'keywords' => $keywords,
             'description' => $description,
             'body' => stripslashes($body),
         );

         $n = $this->db->update('catalog', $data, "id = $id");

         $referer = (!empty($referer) && $referer != '{REFERER}') ? $referer : $this->basePath;
         $content = "������� ������� �������<meta http-equiv='refresh' content='2;URL=$referer'>";

         $this->viewMessage($content);
      }

      if ($this->_err) {
         $this->viewErr();
      }

      if (empty($_POST) || $this->_err) {
         $this->tpl->assign(
                 array(
                     'ADM_HREF' => $href,
                     'ADM_POSITION' => $position,
                     'ADM_PREVIEW' => $preview,
                     'ADM_BODY' => $body,
                     'ADM_HEADER' => $header,
                     'ADM_TITLE' => $title,
                     'ADM_KEYWORDS' => $keywords,
                     'ADM_DESCRIPTION' => $description,
                     'REFERER' => $referer
                 )
         );

         $this->tpl->parse('CONTENT', '.start');
         $this->tpl->parse('CONTENT', '.mce');
         $this->tpl->parse('CONTENT', '.adress');
         $this->tpl->parse('CONTENT', '.header');
         $this->tpl->parse('CONTENT', '.pos');

         $this->tpl->parse('CONTENT', '.meta');
         $this->tpl->parse('CONTENT', '.preview');
         $this->tpl->parse('CONTENT', '.body');
         $this->tpl->parse('CONTENT', '.end');
      }

      return true;
   }

   public function deletepage() {
      $id = end($this->url);

      if (!ctype_digit($id)) {
         return $this->error404();
      }

      $page = $this->db->fetchRow("SELECT * FROM `page` WHERE `id` = '$id'");

      if (!$page) {
         return $this->error404();
      }

      switch ($page['type']) {
         case 'section' : $meta = '�������� �������';
            break;
         case 'page' : $meta = '�������� ��������';
            break;
         case 'link' : $meta = '�������� ������';
            break;
         default : return $this->error404();
            break;
      }

      $this->setMetaTags($meta);
      $this->setWay($meta);

      $n = $this->db->delete('page', "id = $id");

      if ($page['type'] == 'section') {
         $this->deleteSubPages($page['id']);
      }

      $referer = $this->getVar('HTTP_REFERER', $this->basePath);

      $content = "�������(�) ������� ������(�)<meta http-equiv='refresh' content='2;URL=$referer'>";
      $this->viewMessage($content);

      return true;
   }

   private function deleteSubPages($id = null) {
      if (null === $id) {
         return $this->error404();
      }

      $pages = $this->db->fetchAll("SELECT `id`, `type` FROM `page` WHERE `level` = '$id'");

      $n = $this->db->delete('page', "level = $id");

      if ($pages) {
         foreach ($pages as $page) {
            if ($page['type'] == 'section') {
               $this->deleteSubPages($page['id']);
            }
         }
      }
   }

   public function lookups() {
      $this->setMetaTags('������������� ����');
      $this->setWay('������������� ����');

      if (!empty($_POST)) {
         foreach ($_POST as $key => $value) {

            $this->db->query("UPDATE `lookups` SET `value` = '" . addslashes($value) . "' WHERE `key` = '" . $key . "' AND `language` = '" . $this->lang . "'");
         }

         $content = "�������� ������� ���������<meta http-equiv='refresh' content='2;URL=" . $this->basePath . 'admin/lookups' . "'>";
         $this->viewMessage($content);

         return true;
      }

      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('lookups', 'edit');
      $this->tpl->define_dynamic('lookups_item', 'lookups');
      $this->tpl->define_dynamic('end', 'edit');

      foreach ($this->lookups as $lookups) {
         $this->tpl->assign(
                 array(
                     'LOOK_NAME' => $lookups['name'],
                     'LOOK_KEY' => $lookups['key'],
                     'LOOK_VALUE' => $lookups['value']
                 )
         );

         $this->tpl->parse('LOOKUPS_ITEM', '.lookups_item');
      }

      $this->tpl->parse('CONTENT', '.start');
      $this->tpl->parse('CONTENT', '.lookups');
      $this->tpl->parse('CONTENT', '.end');

      return true;
   }

   public function metatags() {
      $this->setMetaTags('����-����');
      $this->setWay('����-����');

      $meta = $this->db->fetchAll("SELECT `id`, `name`, `href` FROM `meta_tags` WHERE `language` = '" . $this->lang . "' ORDER BY `id`");

      $this->tpl->define_dynamic('pages', 'pages.tpl');
      $this->tpl->define_dynamic('short_list', 'pages');

      foreach ($meta as $item) {
         $this->tpl->assign(
                 array(
                     'PAGE_ADM' => '<a href="' . $this->basePath . 'admin/editmetatag/' . $item['id'] . '" title="�������������"><img src="/img/admin_icons/edit.png" width="12" height="12" alt="�������������" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                     'PAGE_ADRESS' => $this->basePath . $item['href'],
                     'PAGE_HEADER' => stripslashes($item['name'])
                 )
         );

         $this->tpl->parse('CONTENT', '.short_list');
      }

      return true;
   }

   public function editmetatag() {
      $id = end($this->url);

      if (!ctype_digit($id)) {
         return $this->error404();
      }

      $meta = $this->db->fetchRow("SELECT * FROM `meta_tags` WHERE `id` = $id");

      if (!$meta) {
         return $this->error404();
      }

      $this->setMetaTags('����-���� : ' . $meta['name']);
      $this->setWay('����-���� : ' . $meta['name']);

      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('mce', 'edit');
      $this->tpl->define_dynamic('meta', 'edit');
      $this->tpl->define_dynamic('body', 'edit');
      $this->tpl->define_dynamic('end', 'edit');

      $header = $meta['header'];
      $title = $meta['title'];
      $keywords = $meta['keywords'];
      $description = $meta['description'];
      $body = $meta['body'];

      if (!empty($_POST)) {
         $header = $this->getVar('header', '');
         $title = $this->getVar('title', '');
         $keywords = $this->getVar('keywords', '');
         $description = $this->getVar('description', '');
         $body = $this->getVar('body', '');
         $referer = $this->getVar('HTTP_REFERER', '');

         $data = array(
             'header' => $header,
             'title' => $title,
             'keywords' => $keywords,
             'description' => $description,
             'body' => $body
         );

         $this->db->update('meta_tags', $data, "id = $id");

         $referer = (!empty($referer) && $referer != '{REFERER}') ? $referer : $this->basePath;
         $content = "������� ������� �������<meta http-equiv='refresh' content='2;URL=$referer'>";

         $this->viewMessage($content);

         return true;
      } else {
         $referer = $this->getVar('HTTP_REFERER', $this->basePath);
         $this->tpl->assign(
                 array(
                     'ADM_BODY' => $body,
                     'ADM_HEADER' => $header,
                     'ADM_TITLE' => $title,
                     'ADM_KEYWORDS' => $keywords,
                     'ADM_DESCRIPTION' => $description,
                     'REFERER' => $referer
                 )
         );

         $this->tpl->parse('CONTENT', '.start');
         $this->tpl->parse('CONTENT', '.mce');
         $this->tpl->parse('CONTENT', '.meta');
         $this->tpl->parse('CONTENT', '.body');
         $this->tpl->parse('CONTENT', '.end');
      }

      return true;
   }

   public function settings() {
      $this->setMetaTags('��������� �����');
      $this->setWay('��������� �����');

      if (!empty($_POST)) {
         foreach ($_POST as $key => $value) {
            if (stristr($key, 'settings_')) {
               $key = str_ireplace('settings_', '', $key);
               if ($key != 'password') {
                  $this->db->query('UPDATE `settings` SET `value` = "' . $value . '" WHERE `key` = "' . $key . '"');
               } else {
                  if ($value != '') {
                     $pass = crypt($value, 'trabucco');
                     $this->db->query("UPDATE `users` SET `pass` = '" . $pass . "' WHERE `login` = 'admin'");
                  }
               }
            }
         }

         $content = "������ ��������<meta http-equiv='refresh' content='2;URL=" . $this->basePath . "admin/settings'>";
         $this->viewMessage($content);

         return true;
      }

      $settings = $this->db->fetchAll("SELECT * FROM `settings` ORDER BY `id`");

      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('settings', 'edit');
      $this->tpl->define_dynamic('settings_item', 'settings');
      $this->tpl->define_dynamic('end', 'edit');

      foreach ($settings as $sett) {
         $this->tpl->assign(
                 array(
                     'NAME' => $sett['name'],
                     'KEY' => $sett['key'],
                     'VALUE' => $sett['value']
                 )
         );

         $this->tpl->parse('SETTINGS_ITEM', '.settings_item');
      }

      $this->tpl->assign(
              array(
                  'NAME' => '������ ��������������',
                  'KEY' => 'password',
                  'VALUE' => ''
              )
      );

      $this->tpl->parse('SETTINGS_ITEM', '.settings_item');

      $this->tpl->parse('CONTENT', '.start');
      $this->tpl->parse('CONTENT', '.settings');
      $this->tpl->parse('CONTENT', '.end');

      return true;
   }

   // ����� �������� ������ ������ ������� ������������� � �����. ����� �������� � ���������� ������ ���������� ���������� ����������� � ����� ��������
   protected function doImport($level, $fields='*') {

      $items = $this->db->fetchAll("SELECT $fields FROM `catalog` WHERE `level` = '$level'");

      if ($items) {
         foreach ($items as $item) {
            $this->doImportArr[] = $item;
            $this->doImport($item['id'], $fields);
         }
      }
   }

   // ������ ��������    
   public function import() {
      $dLevel = $this->db->fetchOne("SELECT `id` FROM `catalog` WHERE `href` = 'raznoe'");

      if ($dLevel !== false) {
         $this->doImport($dLevel, 'preview, body, id, artikul');
      }

      $dLevel = $this->db->fetchOne("SELECT `id` FROM `catalog` WHERE `href` = 'mobilnie-telefoni'");

      if ($dLevel !== false) {
         $this->doImport($dLevel, 'preview, body, id, artikul');
      }


      //$fields = $this->db->fetchAll("DESC `catalog`");
      $index = 3;
//    	foreach ($fields as $field) {
//    		print "\$$field[Field] = (isset(\$exlItem[\$index]) ? trim(\$exlItem[\$index]) : '');<br> \$index++; <br>";
//    		$index++;
//    	}
//    	die;

      /*

        frame - ����
        size_in_folded - ������ � �������� ����
        size_in_factored_form - ������ � ���������� ����
        size_tops - ������ ����������
        name_of_additional_equipment - �������� ��������������� ������������
        amount_of_additional_equipment - ������ ��������������� ������������
        weight - ���
        load - ��������
        load_of_extra_equipment - �������� �� �������������� ������������
        packing_size ������ ��������
        packing_type
        completion - ������������
        scope - ����� ����������

        dignity - �����������
        features - �����������
        warranty - ��������

       */

      $this->setMetaTags('������ �������');
      $this->setWay('������ �������');

      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('import', 'edit');
      $this->tpl->define_dynamic('end', 'edit');


      if (!empty($_FILES)) {
         $file = ($_FILES['file']['size'] > 0) ? $_FILES['file'] : null;
         if ($file['type'] != "application/vnd.ms-excel" && $file['type'] != "application/octet-stream" && $file['type'] != "application/x-msexcel") {
            $this->_err .= "<br>������������ ������ ����� ��������";
         }
      }

      $arr_err = array();

      if (!empty($_FILES) && !$this->_err) {
         ini_set('max_execution_time', 120);
         require_once('library/xlsparser.php');
         $sheets = parse_excel($file['tmp_name']);
         $arr = $sheets[2];


         if (count($arr) > 0) {
            array_shift($arr);
            array_shift($arr);



            $this->db->query("TRUNCATE TABLE `catalog`");
            $sectionsArr = array();


            $pagesArr = array();
            $sectionArr = array();
            $subSectionArr = array();
            $sectionIndex = 0;
            $i = 0;

            $artikulsList = '';

            foreach ($arr as $exlItem) {

               $index = 0;
               $sectionName = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
//$sectionHref = '';

               $data = array();

               $data['name'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['proizvoditel'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['country'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['model'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['pic'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;

               $data['cost_old'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : ''); // ���� � ���������
               $index++;
               $data['cost'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['material'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['color'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['frame'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['is_new'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               if (strcasecmp($data['is_new'], '��') == 0) {
                  $data['is_new'] = 'yes';
               } else {
                  $data['is_new'] = 'no';
               }

               $index++;
               $data['action'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               if (strcasecmp($data['action'], '��') == 0) {
                  $data['action'] = 'yes';
               } else {
                  $data['action'] = 'no';
               }
               $index++;
               $data['hit'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               if (strcasecmp($data['hit'], '��') == 0) {
                  $data['hit'] = 'yes';
               } else {
                  $data['hit'] = 'no';
               }

               $index++;
               $data['size_in_folded_length'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['size_in_folded_width'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['size_in_folded_height'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['size_in_factored_form_length'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['size_in_factored_form_width'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['size_in_factored_form_height'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['size_tops_length'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['size_tops_width'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['size_tops_height'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['name_of_additional_equipment'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['amount_of_additional_equipment_length'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['amount_of_additional_equipment_width'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['amount_of_additional_equipment_height'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['weight'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['load'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['load_of_extra_equipment'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['packing_size_length'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['packing_size_width'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['packing_size_height'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['packing_type'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['completion'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['scope'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['dignity'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['features'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['warranty'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;

               $data['preview'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;
               $data['body'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');

               $index++;
               $data['availability'] = (isset($exlItem[$index]) ? trim($exlItem[$index]) : '');
               $index++;

               if (strcasecmp($data['availability'], '���') != 0) {
                  $data['availability'] = 'yes';
               } else {
                  $data['availability'] = 'no';
               }



               // �������� ��������
               $sectionHref = $this->ru2Lat($sectionName);
               $sectionHref = preg_replace('/\W+/i', '-', $sectionHref);

               if (!isset($sectionArr[$sectionHref])) {
                  $data1 = array('href' => $sectionHref, 'name' => $sectionName, 'level' => '0', 'type' => 'section', 'header' => $sectionName, 'title' => $sectionName, 'keywords' => $sectionName, 'description' => $sectionName, 'language' => $this->lang);

                  $fields1 = implode('`, `', array_keys($data1));
                  $values1 = implode("', '", array_values($data1));

                  $sql = "INSERT IGNORE INTO `catalog` (`$fields1`) VALUES ('$values1')";
                  $this->db->query($sql);
                  $sectionArr[$sectionHref] = $this->db->lastInsertId();
               }

               /*
                 // �������� �����������
                 if (isset($sectionArr[$sectionHref])) {
                 $subSectionHref = $this->ru2Lat($subSectionName);
                 $subSectionHref = preg_replace('/\W+/i', '-', $subSectionHref);


                 if (!isset($subSectionArr[$sectionHref][$subSectionHref])) {
                 $data = array( 'href'=>$subSectionHref, 'name'=>$subSectionName, 'level'=>$sectionArr[$sectionHref], 'type'=>'section', 'language'=>$this->lang);

                 $this->db->insert('catalog', $data);

                 $subSectionArr[$sectionHref][$subSectionHref] = $this->db->lastInsertId();

                 }
                 }
                */

               // �������� �������
               //if (isset($subSectionArr[$sectionHref][$subSectionHref])) {

               if ($sectionArr[$sectionHref]) {
                  //	var_dump($data);

                  $data['artikul'] = $this->ru2Lat($data['model']);
                  $artiku = trim($data['model']);
                  $artiku = str_replace('(', '', $artiku);
                  $artiku = str_replace(')', '', $artiku);

                  $data['artikul'] = strtolower(preg_replace('/\W+/i', '-', $artiku));
                  $artikulsList .= $data['artikul'] . '<br>';
                  $href = $this->ru2Lat($data['name'] . ' ' . $data['artikul']);
                  $href = preg_replace('/\W+/i', '-', $href);
                  $data['name'] .= ' ' . $data['model'];
                  $data['header'] = $data['name'];
                  $data['title'] = $data['name'];
                  $data['keywords'] = $data['name'];
                  $data['description'] = $data['name'];


                  $data['href'] = $href;
                  $data['level'] = $sectionArr[$sectionHref];
                  $data['type'] = 'page';

                  $fields = implode('`, `', array_keys($data));
                  $values = implode("', '", array_values($data));

                  $sql = "INSERT IGNORE INTO `catalog` (`$fields`) VALUES ('$values')";
                  $this->db->query($sql);
               }
            }
         }

         if (count($this->doImportArr) > 0) {
            foreach ($this->doImportArr as $val) {
               if (isset($val['id'])) {
                  unset($val['id']);
               }

               $dArtiku = false;

               if (isset($val['artikul'])) {
                  $dArtiku = $val['artikul'];
                  unset($val['artikul']);
               }

               $this->db->update('catalog', $val, "artikul = '$dArtiku'");
            }
         }

         if ($this->_err) {
            $this->viewErr();
         }
      }

      if (!empty($artikulsList)) {
         $this->viewMessage($artikulsList);
      }

      $this->tpl->parse('CONTENT', '.start');
      $this->tpl->parse('CONTENT', '.import');
      $this->tpl->parse('CONTENT', '.end');
      return true;
   }

   // ���� �������
   public function orders() {



      $this->setMetaTags('���� �������');
      $this->setWay('���� �������');

      $fields = $this->db->fetchAll('DESC `orders`');
      $orderBy = '`date`';
      $filterLink = '';
      $dateFilterLinkParams = '';



      if (isset($_GET['filter'])) {
         for ($i = 0; $i < count($fields); $i++) {
            if (isset($fields[$i]['Field']) && $fields[$i]['Field'] == $_GET['filter']) {
               $filterLink = "&filter=$_GET[filter]";
               if (isset($_GET['date1'])) {
                  $filterLink .= "&date1=$_GET[date1]";
                  $dateFilterLinkParams .= "&date1=$_GET[date1]";
               }
               if (isset($_GET['date2'])) {
                  $filterLink .= "&date2=$_GET[date2]";
                  $dateFilterLinkParams .= "&date2=$_GET[date2]";
               }
               $orderBy = "`$_GET[filter]`";
               break;
            }
         }
      }

      $printLink = '<a href="/admin/orders/?print' . $filterLink . '" >����� ...</a>';
      if (isset($_GET['print'])) {
         $printLink = '<a href="/admin/orders/" >������</a>  | <a href="#" id="a-print" >������</a>';
      }

      $sqlDate = "";
      $metaTexDays = "";



      if (isset($_GET['date1']) || isset($_GET['date2'])) {

         if (isset($_GET['filter']) && ($_GET['filter'] == 'is_send_email_vendor' || $_GET['filter'] == 'date')) {
            if (isset($_GET['date1'])) {

               $sqlDate .= " AND `o`.`$_GET[filter]` >= '$_GET[date1]' ";
               if ($_GET['filter'] == 'is_send_email_vendor') {
                  $sqlDate .= " AND `is_send_email_vendor` != 0 ";
               }
            }
            if (isset($_GET['date2'])) {
               $sqlDate .= " AND `o`.`$_GET[filter]` <= '$_GET[date2]' ";
               if ($_GET['filter'] == 'is_send_email_vendor') {
                  $sqlDate .= " AND `is_send_email_vendor` != 0 ";
               }
            }
         }
      } else {
         $sqlDate = " AND `date` >= " . strtotime("-31 day") . "";
      }

      $orderTypes = array(
          'retail' => '�������',
          'wholesale' => '���'
      );

      $orderStatus = array(
          'no-paid' => '�� �������',
          'paid' => '�������',
          'completed' => '��������',
          'booked' => '�����',
          'advance' => '�����'
      );

      $orderClass = array(
          'no-paid' => 'neoplachen',
          'paid' => 'oplachen',
          'completed' => 'done',
          'booked' => 'bron',
          'advance' => 'avans'
      );

      $sql = 'SELECT `o`.`status`,`o`.`city`, `o`.`type`, `o`.`cost1`, `o`.`cost2`,   `o`.`id`, `o`.`fio`, `o`.`email`, date_format(`o`.`date`, "%d/%m/%Y  %H:%i" ) AS `date`  , SUM(`g`.`cost`*`g`.`count`) as `summ`, `g`.`goods_artikul`  FROM `orders` as `o`, `orders_goods` as `g` WHERE `o`.`id` = `g`.`order_id` ' . $sqlDate . ' GROUP BY `o`.`date`  DESC';
      $ordersList = $this->db->fetchAll($sql);

      if ($ordersList) {
         $this->tpl->define_dynamic('_orders', 'adm/orders.tpl');
         $this->tpl->define_dynamic('orders', '_orders');
         $this->tpl->define_dynamic('orders_list', 'orders');

         foreach ($ordersList as $orderItem) {
            $id = $orderItem['id'];
            $idItem = "00001";

            if ($id < 10) {
               $idItem = "0000$id";
            }

            if ($id >= 10 && $id < 100) {
               $idItem = "000$id";
            }

            if ($id >= 100 && $id < 1000) {
               $idItem = "00$id";
            }

            if ($id >= 1000 && $id < 10000) {
               $idItem = "0$id";
            }

            if ($id >= 10000) {
               $idItem = $id;
            }

            $this->tpl->assign(array(
							'PROBA_CATALOG'     =>  (''),
                'ADM_ORDER_ID' => $id,
                'ADM_ORDER_ID1' => $idItem,
                'ADM_ORDER_FIO' => $orderItem['fio'],
                'ADM_ORDER_DATE' => $orderItem['date'],
                'ADM_ORDER_SUMM' => number_format($orderItem['summ'], 2, ', ', ' '),
                'ADM_ORDER_CITY' => $orderItem['city'],
                'ADM_ORDER_TYPE' => $orderTypes[$orderItem['type']],
                'ADM_ORDER_STATUS' => $orderStatus[$orderItem['status']],
                'ADM_ORDER_CLASS' => $orderClass[$orderItem['status']]
            ));
            $this->tpl->parse('ORDERS_LIST', '.orders_list');
         }

         $this->tpl->parse('CONTENT', '.orders');
      } else {
         $this->tpl->define_dynamic('_orders_empty', 'adm/orders.tpl');
         $this->tpl->define_dynamic('orders_empty', '_orders_empty');
         $this->tpl->parse('CONTENT', '.orders_empty');
      }
      return true;
   }

   public function orderupdatestatus() {
      

      $this->setMetaTags('��������� ������� ������');
      $this->setWay('��������� ������� ������');

      $id = end($this->url);

      if (!is_numeric($id)) {
         $this->addErr("�������� ������ � ������� $id �� �������");
      }

      if ($this->_err) {
         $this->viewErr();
         return true;
      }

      $sql = "SELECT * FROM `orders` WHERE `id` = '$id'";
      $orderDetail = $this->db->fetchRow($sql);

      if (!$orderDetail) {
         $this->addErr("�������� ������ � ������� $id �� �������");
         $this->viewErr();
         return true;
      }

      if (isset($_POST['status'])) {
         $sql = "UPDATE `orders` SET `status`='$_POST[status]' WHERE `orders`.`id`='$id'";
         $this->db->query($sql);
         $content = "������ ������ ������<meta http-equiv='refresh' content='2;URL=/admin/orders'>";
         $this->viewMessage($content);
      }



      return true;
   }

   // �������� �������� ��������

   public function loadcatpics() {
      $this->setMetaTags('�������� �������� ��������');
      $this->setWay('�������� �������� ��������');

      // $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('upload_catalog_pics', 'edit');
      //$this->tpl->define_dynamic('end', 'edit');

      if (!empty($_FILES) && !$this->_err) {
         foreach ($_FILES as $key => $value) {

            if (!empty($_FILES[$key]) && $_FILES[$key]['size'] > 0) {
               $_name = $_FILES[$key]['name']; //str_ireplace('.gif', '', str_ireplace('.png', '', str_ireplace('.jpg', '', str_ireplace('.jpeg', '', $_FILES[$key]['name']))));

               if (is_file(PATH . "img/catalog/small/$_name")) {
                  @unlink(PATH . "img/catalog/small/$_name");
               }

               if (is_file(PATH . "img/catalog/big/$_name")) {
                  @unlink(PATH . "img/catalog/big/$_name");
               }

               if (is_file(PATH . "img/catalog/rial/$_name")) {
                  @unlink(PATH . "img/catalog/rial/$_name");
               }
            }
         }
      }
      if (!empty($_FILES) && !$this->_err) {
         $content = "���������� ���������.<meta http-equiv='refresh' content='1;URL=/admin/loadcatpics'>";
         $this->viewMessage($content);
      }
      if ($this->_err) {
         $this->viewErr();
      }
      if (empty($_POST) || $this->_err) {
         //$this->tpl->parse('CONTENT', '.start');
         $this->tpl->parse('CONTENT', '.upload_catalog_pics');
         //$this->tpl->parse('CONTENT', '.end');
      }
      return true;
   }

   // ����� ���������    
   public function order_detail() {



      $this->setMetaTags('�������� ������:');
      $this->setWay('�������� ������');

      $id = end($this->url);

      if (!is_numeric($id)) {
         $this->addErr("�������� ������ � ������� $id �� �������");
      }

      if ($this->_err) {
         $this->viewErr();
         return true;
      }

      $sql = "SELECT * FROM `orders` WHERE `id` = '$id'";
      $orderDetail = $this->db->fetchRow($sql);



      if (!$orderDetail) {
         $this->addErr("�������� ������ � ������� $id �� �������");
         $this->viewErr();
         return true;
      }

      $this->tpl->define_dynamic('_order_detail', 'adm/orders.tpl');
      $this->tpl->define_dynamic('order_detail', '_order_detail');
      $this->tpl->define_dynamic('order_detail_list', 'order_detail');
      $this->tpl->define_dynamic('order_detail_service', 'order_detail');
      $this->tpl->define_dynamic('order_detail_list_empty', 'order_detail');

      $id = $orderDetail['id'];
      $idItem = "00001";

      if ($id < 10) {
         $idItem = "0000$id";
      }

      if ($id >= 10 && $id < 100) {
         $idItem = "000$id";
      }

      if ($id >= 100 && $id < 1000) {
         $idItem = "00$id";
      }

      if ($id >= 1000 && $id < 10000) {
         $idItem = "0$id";
      }

      if ($id >= 10000) {
         $idItem = $id;
      }
      $sql = "SELECT `catalog`.`name`,`catalog`.`artikul`, `orders_goods`.`count`, `orders_goods`.`name` as `name2`, `orders_goods`.`goods_artikul`, `orders_goods`.`cost` as `cost2` FROM  `orders_goods` LEFT JOIN `catalog` ON  `catalog`.`id` = `orders_goods`.`goods_id` AND `catalog`.`artikul` != ''  WHERE `orders_goods`.`order_id` = '$id'";

      $orderList = $this->db->fetchAll($sql);

      $summ = 0;

      if ($orderList) {
         $this->tpl->parse('ORDER_DETAIL_LIST_EMPTY', 'null');
         foreach ($orderList as $orderItem) {

            if ($orderItem['cost2'] > 0) {

               $summItem = ($orderItem['cost2'] * $orderItem['count']);
               $this->tpl->assign(array(
			   				'PROBA_CATALOG'     =>  (''),
                   'ORDER_DETAIL_LIST_NAME' => (empty($orderItem['name2']) ? $orderItem['name'] : $orderItem['name2']),
                   'ORDER_DETAIL_LIST_ARTIKUL' => (empty($orderItem['goods_artikul']) ? $orderItem['artikul'] : $orderItem['goods_artikul']),
                   'ORDER_DETAIL_LIST_COST' => number_format($orderItem['cost2'], 2, ',', " "),
                   'ORDER_DETAIL_LIST_COUNT' => $orderItem['count'],
                   'ORDER_DETAIL_LIST_SUMM' => number_format($summItem, 2, ',', " ")
               ));
               $summ += $summItem;
               $this->tpl->parse('ORDER_DETAIL_LIST', '.order_detail_list');
            } else {
               $this->tpl->parse('ORDER_DETAIL_LIST', 'null');
            }
         }
      } else {
         $this->tpl->parse('ORDER_DETAIL_LIST', 'null');
      }

      $deliveryType = array(
          'delivery_service' => '������ ��������',
          'home' => '� ����, � �����',
          'pickup' => '���������'
      );

      if ($orderDetail['delivery_type'] == 'pickup') {
         $this->tpl->parse('ORDER_DETAIL_SERVICE', 'null');
      }



      $orderStatus = array(
          'no-paid' => '�� �������',
          'paid' => '�������',
          'completed' => '��������',
          'booked' => '�����',
          'advance' => '�����'
      );

      $this->tpl->assign(array(
	  				'PROBA_CATALOG'     =>  (''),
          'ORDER_DETAIL_ID' => $id,
          'ORDER_DETAIL_ID1' => $idItem,
          'ORDER_DETAIL_MAIL' => $orderDetail['email'],
          'ORDER_DETAIL_FIO' => $orderDetail['fio'],
          'ORDER_DETAIL_DATE' => $orderDetail['date'],
          'ORDER_DETAIL_CITY' => $orderDetail['city'],
          'ORDER_DETAIL_PHONE' => (!empty($orderDetail['phone']) ? $orderDetail['phone'] : ''),
          'ORDER_DETAIL_STREET' => (!empty($orderDetail['street']) ? $orderDetail['street'] : '-'),
          'ORDER_DETAIL_HOME' => (!empty($orderDetail['home']) ? $orderDetail['home'] : '-'),
          'ORDER_DETAIL_APARTAMENT' => (!empty($orderDetail['apartment']) ? $orderDetail['apartment'] : '-'),
          'ORDER_DETAIL_DELIVERY_TYPE' => $deliveryType[$orderDetail['delivery_type']],
          'ORDER_DETAIL_DELIVERY_SERVICE' => (!empty($orderDetail['delivery_service']) ? $orderDetail['delivery_service'] : ''),
          'ORDER_DETAIL_DELIVERY_OFFICE' => (!empty($orderDetail['delivery_office']) ? $orderDetail['delivery_office'] : ''),
          'ORDER_DETAIL_MOBAIL_PHONE' => (!empty($orderDetail['modile_phone']) ? $orderDetail['modile_phone'] : ''),
          'ADM_STATUS_NO' => '',
          'PAID_SELECTED' => '',
          'ADM_STATUS_NO_PAID_SELECTED' => (isset($orderDetail['status']) && $orderDetail['status'] == 'no-paid' ? 'selected' : ''),
          'ADM_STATUS_PAID_SELECTED' => (isset($orderDetail['status']) && $orderDetail['status'] == 'paid' ? 'selected' : ''),
          'ADM_STATUS_COMPLETED_SELECTED' => (isset($orderDetail['status']) && $orderDetail['status'] == 'completed' ? 'selected' : ''),
          'ADM_STATUS_BOOKED_SELECTED' => (isset($orderDetail['status']) && $orderDetail['status'] == 'booked' ? 'selected' : ''),
          'ADM_STATUS_ADVANCED_SELECTED' => (isset($orderDetail['status']) && $orderDetail['status'] == 'advance' ? 'selected' : ''),
          'ORDER_DETAIL_SUMM' => number_format($summ, 2, ',', " "),
          'ORDER_DETAIL_BODY' => (!empty($orderDetail['body']) ? $orderDetail['body'] : '')
      ));

      $this->tpl->parse('CONTENT', '.order_detail');

      return true;
   }

   public function orderdelete() {
      $this->setMetaTags('�������� ������');
      $this->setWay('�������� ������');

      $id = end($this->url);

      if (!is_numeric($id)) {
         $this->addErr("�������� ������ � ������� $id �� �������");
      }

      if ($this->_err) {
         $this->viewErr();
         return true;
      }

      $sql = "SELECT * FROM `orders` WHERE `id` = '$id'";
      $orderDetail = $this->db->fetchRow($sql);

      if (!$orderDetail) {
         $this->addErr("�������� ������ � ������� $id �� �������");
         $this->viewErr();
         return true;
      }

      $sql = "DELETE FROM `orders` WHERE `orders`.`id`='$id'";
      $this->db->query($sql);

      $sql = "DELETE FROM `orders_goods` WHERE `orders_goods`.`order_id` = '$id'";
      $this->db->query($sql);

      $content = "����� ������<meta http-equiv='refresh' content='2;URL=/admin/orders'>";
      $this->viewMessage($content);

      return true;
   }

   public function addsectiontext() {

      $this->setMetaTags('�������� �������� �������');
      $this->setWay('�������� �������� �������');

      $id = end($this->url);
      $sectionId = '0';
      if (is_numeric($id)) {
         $sectionId = $id;
      } elseif ($id == 'novelty') { // ��� �������
         $sectionId = '-1';
      }

      if ($sectionId > '0') {
         $isSetSection = $this->db->fetchOne("SELECT `name` FROM `catalog` WHERE `id` = '$id'");
         if (!$isSetSection) {
            $this->_err = "������� <b>$isSetSection</b> �� �������";
         }
      }

      $isSetSectionText = $this->db->fetchOne("SELECT `id` FROM `catalog_sections_description` WHERE `section_id` = '$sectionId'");

      if ($isSetSectionText) {
         $this->_err = "�������� ��� ����� ������� ��� ����������";
      }

      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('name', 'edit');
      $this->tpl->define_dynamic('pos', 'edit');
      $this->tpl->define_dynamic('mce', 'edit');
      $this->tpl->define_dynamic('meta', 'edit');

      if (is_numeric($id)) {
         $this->tpl->define_dynamic('pic', 'edit');
      }

      $this->tpl->define_dynamic('body', 'edit');
      $this->tpl->define_dynamic('end', 'edit');

      //$sectionType = gpm($sectionVal, 'type', 'section');    
      $sectionS = '';
      $name = $this->getVar('name', '');
      $position = $this->getVar('position', 0);
      $header = $this->getVar('header', '');
      $title = $this->getVar('title', '');
      $keywords = $this->getVar('keywords', '');
      $description = $this->getVar('description', '');
      $body = $this->getVar('body', '');
      $pic = '';
      $artikul = $this->getVar('sections', '0');
      $referer = '';

      $referer = $this->getVar('HTTP_REFERER');

      if (isset($_FILES['pic']) && $_FILES['pic']['size'] > 0) {
         $pic = $sectionId . '-' . $_FILES['pic']['name'];
         $picPath = $_SERVER['DOCUMENT_ROOT'] . '/img/catalog/small/' . $pic;
         if (is_file($picPath)) {
            @chmod($picPath, 0666);
            @unlink($picPath);
         }
         $this->uploadCatPic($_FILES['pic']['tmp_name'], $picPath, 101, 99);
      }


      if (!empty($_POST) && !$this->_err) {

         $sql = "INSERT INTO `catalog_sections_description` VALUES (NULL, '$sectionId', '$name', '$position', '$pic', '$header', '$title', '$keywords', '$description', '$body', '$this->lang')";
         $this->db->query($sql);

         $content = "�������� ��������� <meta http-equiv='refresh' content='1;URL=" . $referer . "'>";
         $this->viewMessage($content);
      }



      if ($this->_err) {
         $this->viewErr();
         return true;
      }


      $this->tpl->assign(array(
	  				'PROBA_CATALOG'     =>  (''),
          'ADM_NAME' => $name,
          'ADM_HEADER' => $header,
          'ADM_TITLE' => $title,
          'ADM_KEYWORDS' => $keywords,
          'ADM_DESCRIPTION' => $description,
          'ADM_DEL_PICK' => '',
          'ADM_BODY' => $body,
          'ADM_POSITION' => $position,
          'REFERER' => $referer
      ));


      $this->tpl->parse('CONTENT', '.start');
      $this->tpl->parse('CONTENT', '.name');

      $this->tpl->parse('CONTENT', '.mce');
      $this->tpl->parse('CONTENT', '.meta');

      if (is_numeric($id) && $id > 0) {
         $this->tpl->parse('CONTENT', '.pic');
         // $this->tpl->parse('CONTENT', '.pos');
      }
      $this->tpl->parse('CONTENT', '.body');
      $this->tpl->parse('CONTENT', '.end');

      return true;
   }

   public function editsectiontext($retUrl='') {

      $this->setMetaTags('�������������� �������� �������');
      $this->setWay('�������������� �������� �������');

      $id = end($this->url);
      $sectionId = '0';
      if (is_numeric($id)) {
         $sectionId = $id;
      }

      if ($sectionId > '0') {
         $isSetSection = $this->db->fetchOne("SELECT `name` FROM `catalog` WHERE `id` = '$id'");
         if (!$isSetSection) {
            $this->_err = "������� <b>$isSetSection</b> �� �������";
         }
      }

      $sectionText = $this->db->fetchRow("SELECT * FROM `catalog_sections_description` WHERE `section_id` = '$sectionId'");

      if (!$sectionText) {
         $this->_err = "�������� ��� ����� ������� �� ����������";
      }

      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('name', 'edit');
      $this->tpl->define_dynamic('mce', 'edit');
      $this->tpl->define_dynamic('meta', 'edit');

      if (is_numeric($id)) {
         $this->tpl->define_dynamic('pos', 'edit');
         $this->tpl->define_dynamic('pic', 'edit');
      }

      $this->tpl->define_dynamic('body', 'edit');
      $this->tpl->define_dynamic('end', 'edit');

      //$sectionType = gpm($sectionVal, 'type', 'section');
      $sectionS = '';
      $name = $this->getVar('name', $sectionText['name']);
      $position = $this->getVar('position', $sectionText['position']);
      $header = $this->getVar('header', $sectionText['header']);
      $title = $this->getVar('title', $sectionText['title']);
      $keywords = $this->getVar('keywords', $sectionText['keywords']);
      $description = $this->getVar('description', $sectionText['description']);
      $body = $this->getVar('body', $sectionText['body']);
      $pic = '';
      $artikul = $this->getVar('sections', '0');
      $referer = '';

      $referer = $this->getVar('HTTP_REFERER');

      if (isset($_FILES['pic']) && $_FILES['pic']['size'] > 0) {
         $pic = $sectionId . '-' . $_FILES['pic']['name'];
         $picPath = $_SERVER['DOCUMENT_ROOT'] . '/img/catalog/small/' . $pic;
         if (is_file($picPath)) {
            @chmod($picPath, 0666);
            @unlink($picPath);
         }
         $this->uploadCatPic($_FILES['pic']['tmp_name'], $picPath, 101, 99);
         $pic = ", `pic`='$pic'";
      }

      if (!empty($_POST) && !$this->_err) {
         $sql = "UPDATE `catalog_sections_description` SET `name`='$name', `position`='$position', `header`='$header', `title`='$title', `keywords`='$keywords', `description`='$description', `body`='$body' $pic , `lang`='$this->lang' WHERE `id`='$sectionText[id]'";
         $this->db->query($sql);


         $content = "<b>�������� ���������</b> <meta http-equiv='refresh' content='1;URL=" . $referer . "'>";
         $this->viewMessage($content);
      }

      if ($this->_err) {
         $this->viewErr();
      }


      $this->tpl->assign(array(
	  				'PROBA_CATALOG'     =>  (''),
          'ADM_NAME' => $name,
          'ADM_HEADER' => $header,
          'ADM_TITLE' => $title,
          'ADM_KEYWORDS' => $title,
          'ADM_DESCRIPTION' => $description,
          'ADM_DEL_PICK' => '',
          'ADM_BODY' => $body,
          'ADM_POSITION' => $position,
          'REFERER' => $referer
      ));


      $this->tpl->parse('CONTENT', '.start');
      $this->tpl->parse('CONTENT', '.name');

      $this->tpl->parse('CONTENT', '.mce');
      $this->tpl->parse('CONTENT', '.meta');
      if (is_numeric($id) && $id > 0) {
         //	$this->tpl->parse('CONTENT', '.pos');
         $this->tpl->parse('CONTENT', '.pic');
      }
      $this->tpl->parse('CONTENT', '.body');
      $this->tpl->parse('CONTENT', '.end');

      return true;
   }

   public function deletesectiontext($retUrl='') {

      $this->setMetaTags('�������� �������� �������');
      $this->setWay('�������� �������� �������');

      $id = end($this->url);
      $sectionId = 0;
      if (is_numeric($id)) {
         $sectionId = $id;
      }

      $sectionText = $this->db->fetchRow("SELECT * FROM `catalog_sections_description` WHERE `section_id`='$sectionId'");

      if (!$sectionText) {
         $this->_err .= "�������� ��� ����� ������� �� �������";
         $this->viewErr();
      } else {
         $pic = $sectionText['pic'];
         $picPath = $_SERVER['DOCUMENT_ROOT'] . '/img/catalog/small/' . $pic;
         if (is_file($picPath)) {
            @chmod($picPath, 0666);
            @unlink($picPath);
         }
         $referer = $this->getVar('HTTP_REFERER');
         $sql = "DELETE FROM `catalog_sections_description` WHERE `id`='$sectionText[id]'";
         $this->db->query($sql);
         $content = "<b>�������� �������</b> <meta http-equiv='refresh' content='1;URL=" . $referer . "'>";
         $this->viewMessage($content);
      }
      return true;
   }

   public function testform() {
      $this->setMetaTags('Test Form');
      include PATH . 'library/FormManager.php';
      $options = array(
          'db' => $this->db,
          'tpl' => $this->tpl,
          'table' => 'page'
      );
      $form1 = new FormManager($options);
      $titlesArray = array(
          'brand' => '�����',
          'artikul' => '�������'
      );


      $valuesArray = array(
          'yes' => '��',
          'no' => '���',
          '0' => '��',
          '1' => '���',
          'page' => '��������',
          'section' => '������',
          'link' => '������'
      );

      $form1->add('fileldName')
              ->desc()
              ->hide('href')
              ->setTitle('��������', 'name')
              ->setTitle($titlesArray)
              ->setValueType($valuesArray, 'set')
              ->setType('mce', 'body')
              ->show();

      return true;
   }

   public function adddeliveryservice() {
      $this->setMetaTags("������ ��������");
      $this->setWay("������ ��������");
      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('mce', 'edit');
      $this->tpl->define_dynamic('end', 'edit');



      $this->tpl->parse('CONTENT', '.start');
      $this->tpl->parse('CONTENT', '.mce');
      $this->tpl->parse('CONTENT', '.meta');
      $this->tpl->parse('CONTENT', '.end');

      return true;
   }

   public function editdeliveryservice() {
      $this->setMetaTags("������ ��������");
      $this->setWay("������ ��������");
      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('mce', 'edit');
      $this->tpl->define_dynamic('end', 'edit');



      $this->tpl->parse('CONTENT', '.start');
      $this->tpl->parse('CONTENT', '.mce');
      $this->tpl->parse('CONTENT', '.meta');
      $this->tpl->parse('CONTENT', '.end');

      return true;
   }

   public function deliveryservice() {
      $this->setMetaTags("������ ��������");
      $this->setWay("������ ��������");

      $this->tpl->define_dynamic('_delivery', 'adm/delivery.tpl');
      $this->tpl->define_dynamic('delivery', '_delivery');
      $this->tpl->define_dynamic('delivery_item', 'delivery');

      $delivery = $this->db->fetchAll('SELECT * FROM `delivery`');
      $this->tpl->parse('DELIVERY_ITEM', 'null');

      if ($delivery) {
         $size = count($delivery);
         for ($i = 0; $i < $size; $i++) {
            $this->tpl->assign(
                    array(
                        'ID' => $delivery[$i]['id'],
                        'VISIBLE' => ($delivery[$i]['visibility'] == '1' ? '�������' : '�������'),
                        'NAME' => $delivery[$i]['name']
                    )
            );

            $this->tpl->parse('DELIVERY_ITEM', '.delivery_item');
         }
      }
      $this->tpl->parse('CONTENT', '.delivery');
      return true;
   }

   public function adddelivery() {

      $meta = '���������� ������ ��������';
      $this->setMetaTags($meta);
      $this->setWay($meta);
      //$idm = gp($this->w, 2);
      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('name', 'edit');
      $this->tpl->define_dynamic('visible', 'edit');
      $this->tpl->define_dynamic('end', 'edit');

      $name = $this->getVar('name');
      $visible = $this->getVar('visible', '1');

      $visible_s = '';
      if ($visible == 1) {
         $visible_s .= "<option value='1' selected>��</option>
            <option value='0'>���</option>";
      } else {
         $visible_s .= "<option value='1'>��</option>
            <option value='0' selected>���</option>";
      }

      if (!empty($_POST)) {
         $numRow = $this->db->fetchOne('SELECT count(`id`) FROM `delivery` WHERE `name` = "' . $name . '"');
         if ($numRow > 0) {
            $this->addErr('������� � ����� ������ ��� ����������!<br>');
         }
      }

      if (!empty($_POST) && !$this->_err) {

         $this->db->query('INSERT INTO `delivery` ( `name`, `visibility`)	VALUES ("' . $name . '", "' . $visible . '" )');
         $content = "������� ��������. <meta http-equiv='refresh' content='1;URL=/admin/deliveryservice'>";
         $this->viewMessage($content);
      }
      if ($this->_err) {
         $this->viewErr();
      }

      if (empty($name)) {
         $name = '';
      }

      if (empty($_POST) || $this->_err) {
         $this->tpl->assign(
                 array(
                     'VISIBLE_S' => $visible_s,
                     'ADM_NAME' => $name,
                 )
         );



         $this->tpl->parse('CONTENT', '.start');
         $this->tpl->parse('CONTENT', '.name');
         $this->tpl->parse('CONTENT', '.visible');
         $this->tpl->parse('CONTENT', '.end');
      }
      return true;
   }

   public function editdelivery() {
      $id = end($this->url);

      if (!ctype_digit($id)) {
         return false;
      }
      $meta = '�������������� ������ ��������';
      $this->setMetaTags($meta);
      $this->setWay($meta);

      $this->tpl->define_dynamic('start', 'edit');
      $this->tpl->define_dynamic('name', 'edit');
      $this->tpl->define_dynamic('visible', 'edit');
      $this->tpl->define_dynamic('end', 'edit');

      $name = $this->getVar('name');
      $visible = $this->getVar('visible', '1');
      $visible_s = '';


      $deliveryValues = $this->db->fetchAll('SELECT * FROM `delivery` WHERE `id` = "' . $id . '"');
      if (count($deliveryValues) <= 0) {
         $this->addErr('�������� � ����� id �� ����������!<br>');
      }

      if (!empty($_POST) && !$this->_err) {

         $this->db->query('UPDATE `delivery` SET  `name` = "' . $name . '", `visibility` =  "' . $visible . '" WHERE `id` = "' . $id . '"');
         $content = "������ �������� . <meta http-equiv='refresh' content='1;URL=/admin/deliveryservice'>";
         $this->viewMessage($content);
      }

      if ($this->_err) {
         $this->viewErr();
      }

      if ((empty($_POST) || $this->_err) && isset($deliveryValues[0])) {

         if ($deliveryValues[0]['visibility'] == '1') {
            $visible_s .= "<option value='1' selected>��</option>
            		<option value='0'>���</option>";
         } else {
            $visible_s .= "<option value='1'>��</option>
            	<option value='0' selected>���</option>";
         }
         $this->tpl->assign(
                 array(
                     'VISIBLE_S' => $visible_s,
                     'ADM_NAME' => $deliveryValues[0]['name'],
                 )
         );

         $this->tpl->parse('CONTENT', '.start');
         $this->tpl->parse('CONTENT', '.name');
         $this->tpl->parse('CONTENT', '.visible');
         $this->tpl->parse('CONTENT', '.end');
      }
      return true;
   }

   public function deletedelivery() {
      $meta = '�������� ������ ��������';
      $this->setMetaTags($meta);
      $this->setWay($meta);

      if (!isset($this->url[2]) || !is_numeric($this->url[2]))
         return false;
      $id = $this->url[2];
      $length = $this->db->fetchRow('SELECT * FROM `delivery` WHERE `id` = ' . $id);

      if (count($length) <= 0) {
         $this->addErr('�������� � ����� id �� ����������!<br>');
      }

      if ($this->_err) {
         $this->viewErr();
      } else {
         $query = 'DELETE FROM `delivery` WHERE `id` = ' . $id;
         $this->db->query($query);
         $content = "������� �����.<meta http-equiv='refresh' content='1;URL=/admin/deliveryservice'>";
         $this->viewMessage($content);
      }

      return true;
   }

   private function uploadCatPic($from, $to, $maxwidth = 107, $maxheight = 80, $quality = 80) {
      ini_set('max_execution_time', '120');

      // ������ �� Null-���� ���������� PHP
      $from = preg_replace('/\0/uis', '', $from);
      $to = preg_replace('/\0/uis', '', $to);

      // ���������� �� �����������
      $imageinfo = @getimagesize($from);
      // ���� �������� ���������� �� ������� - ������
      if (!$imageinfo) {
         $this->addErr('>������ ��������� ���������� �� �����������');
         return false;
      }
      // �������� ��������� �����������
      $width = $imageinfo[0];  // ������
      $height = $imageinfo[1]; // ������
      $format = $imageinfo[2]; // ID ������� (�����)
      $mime = $imageinfo['mime']; // mime-���
      // ���������� ������ � ������ �����������
      switch ($format) {
         case 2: $img = imagecreatefromjpeg($from);
            break; // jpg
         case 3: $img = imagecreatefrompng($from);
            break; // png
         case 1: $img = imagecreatefromgif($from);
            break; // gif
         default: $this->addErr('�������� ��� ������������ ������ ������������ �����');
            return false;
            break;
      }
      // ���� ������� ����������� �� ������� - ������
      if (!$img) {
         $this->addErr('������ �������� �����������');
         return false;
      }

      // ������ ������� �����������
      $newwidth = $width;
      $newheight = $height;
      // ��������� ���������� ��������
      if ($maxwidth == $maxheight) {
         // ������� �������� ������ �� X � �� Y
         if ($width > $maxwidth && $height > $maxheight) {
            // ��������� �������� ���������
            if ($width == $height) {
               $newwidth = $maxwidth;
               $newheight = $maxheight;
            }
            // ������ ������
            elseif ($width > $height) {
               $newwidth = $maxwidth;
               $newheight = intval(((float) $newwidth / (float) $width) * $height);
            }
            // ������ ������
            else {
               $newheight = $maxheight;
               $newwidth = intval(((float) $newheight / (float) $height) * $width);
            }
         }
         // ������� �������� ������ ������ �� X
         elseif ($width > $maxwidth) {
            $newwidth = $maxwidth;
            $newheight = intval(((float) $newwidth / (float) $width) * $height);
         }
         // ������� �������� ������ ������ �� Y
         elseif ($height > $maxheight) {
            $newheight = $maxheight;
            $newwidth = intval(((float) $newheight / (float) $height) * $width);
         }
         // � ��������� ������� ������ ������ �� ����
         else {
            $newwidth = $width;
            $newheight = $height;
         }
      }
      // ��������� �������������� ��������
      elseif ($maxwidth > $maxheight) {
         // ������� �������� ������ �� X � �� Y
         if ($width > $maxwidth && $height > $maxheight) {
            // ������ ������
            if ($width > $height) {
               $newwidth = $maxwidth;
               $newheight = intval(((float) $newwidth / (float) $width) * $height);

               if ($newheight > $maxheight) {
                  $newheight = $maxheight;
                  $newwidth = intval(((float) $newheight / (float) $height) * $width);
               }
            }
            // ������ ������ ��� ����� ������
            else {
               $newheight = $maxheight;
               $newwidth = intval(((float) $newheight / (float) $height) * $width);
            }
         }
         // ������� �������� ������ ������ �� X
         elseif ($width > $maxwidth) {
            $newwidth = $maxwidth;
            $newheight = intval(((float) $newwidth / (float) $width) * $height);
         }
         // ������� �������� ������ ������ �� Y
         elseif ($height > $maxheight) {
            $newheight = $maxheight;
            $newwidth = intval(((float) $newheight / (float) $height) * $width);
         }
         // � ��������� ������� ������ ������ �� ����
         else {
            //echo '1';
            $newwidth = $width;
            $newheight = $height;
         }
      }
      // ��������� ������������ ��������
      elseif ($maxwidth < $maxheight) {
         // ������� �������� ������ �� X � �� Y
         if ($width > $maxwidth && $height > $maxheight) {
            // ������ ������ ��� ����� ������
            if ($width >= $height) {
               $newwidth = $maxwidth;
               $newheight = intval(((float) $newwidth / (float) $width) * $height);
            }
            // ������ ������
            else {
               $newheight = $maxheight;
               $newwidth = intval(((float) $newheight / (float) $height) * $width);
            }
         }
         // ������� �������� ������ ������ �� X
         elseif ($width > $maxwidth) {
            $newwidth = $maxwidth;
            $newheight = intval(((float) $newwidth / (float) $width) * $height);
         }
         // ������� �������� ������ ������ �� Y
         elseif ($height > $maxheight) {
            $newheight = $maxheight;
            $newwidth = intval(((float) $newheight / (float) $height) * $width);
         }
         // � ��������� ������� ������ ������ �� ����
         else {
            $newwidth = $width;
            $newheight = $height;
         }
      }

      // ���� ��������� ��� ��������� ����������� �� ���� - ������ �������� �
      /* if ($newwidth == $width && $newheight == $height && $quality == 80) {
        echo '123';
        if (copy($from, $to)) return true;
        else {
        $this->_err .= '<br />������ ����������� �����!';
        return false;
        }
        } */

      // ������ ����� �����������
      //$new = imagecreatetruecolor($newwidth, $newheight);
      $new = imagecreatetruecolor($maxwidth, $maxheight);
      $black = imagecolorallocate($new, 0, 0, 0);
      $white = imagecolorallocate($new, 255, 255, 255);
      // �������� ������ � ����� � ������ ����� ��������
      imagefilledrectangle($new, 0, 0, $maxwidth - 1, $maxheight - 1, $white);
      //imagecolortransparent($new, $white);
      $center_w = round(($maxwidth - $newwidth) / 2);
      $center_w = ($center_w < 0) ? 0 : $center_w;
      $center_h = round(($maxheight - $newheight) / 2);
      $center_h = ($center_h < 0) ? 0 : $center_h;
      imagecopyresampled($new, $img, $center_w, $center_h, 0, 0, $newwidth, $newheight, $width, $height);
      // ������ ���� � ����� ������������
      switch ($format) {
         case 2: // jpg
            if ($quality < 0)
               $quality = 0;
            if ($quality > 100)
               $quality = 100;
            imagejpeg($new, $to, $quality);
            break;
         case 3: // png
            $quality = intval($quality * 9 / 100);
            if ($quality < 0)
               $quality = 0;
            if ($quality > 9)
               $quality = 9;
            imagepng($new, $to, $quality);
            break;
         case 1: // gif
            imagegif($new, $to);
            break;
      }

      @chmod($to, 0644);

      return true;
   }

   protected function dataTreeManager($id, $options = array()) {



      $fields = (isset($options['fields']) ? $options['fields'] : '*');
      $retValues = (isset($options['ret']) ? $options['ret'] : 'all');
      $subLevel = (isset($options['sub-level']) ? $options['level'] : array());
      $tableName = (isset($options['table']) ? $options['table'] : '`catalog`');



      $items = $this->db->fetchAll("SELECT $fields FROM $tableName");
      if (!$items) {
         return false;
      }

      $retArr = array();
      $counter = 0;
      $i = 0;
      $isRun = true;
      $linkArr = array();
      $level = 1;
      $nameArr = array();

      while (true) {

         if (isset($items[$i]['id']) && $items[$i]['id'] == $id) {

            $id = $items[$i]['level'];

            if ((is_array($retValues) && in_array('href', $retValues)) || $retValues == 'href' || $retValues == 'all') {
               if ($items[$i]['type'] != 'href') {

                  if (isset($options['level']['href'])) {
                     if ($options['level']['href'] == $level) {
                        $linkArr[] = $items[$i]['href'];
                     }
                  } else {
                     $linkArr[] = $items[$i]['href'];
                  }
               }
            }

            if ((is_array($retValues) && in_array('data', $retValues)) || $retValues == 'data' || $retValues == 'all') {

               if (isset($options['level']['data'])) {
                  if ($options['level']['data'] == $level) {
                     $retArr[] = $items[$i];
                  }
               } else {
                  $retArr[] = $items[$i];
               }
            }

            if ((is_array($retValues) && in_array('name', $retValues)) || $retValues == 'name' || $retValues == 'all') {

               if (isset($options['level']['name'])) {
                  if ($options['level']['name'] == $level) {
                     $nameArr[] = $items[$i]['name'];
                  }
               } else {
                  $nameArr[] = $items[$i]['name'];
               }
            }

            if ($items[$i]['level'] == 0) {
               break;
            }

            $level++;
            $i = 0;
         } else {

            $i++;
         }


         $counter++;
         if ($counter > 100000000) {
            break;
         }
      }


      $retArr['links'] = (count($linkArr > 0) ? implode('/', array_reverse($linkArr)) : '');
      $retArr['names'] = (count($nameArr > 0) ? implode('', array_reverse($nameArr)) : '');


      return (count($retArr) == 0 ? false : $retArr);
   }

}