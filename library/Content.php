<?php

require_once PATH . 'library/Abstract.php';

require_once PATH . 'library/Interface.php';

class Content extends Main_Abstract implements Main_Interface {

   public function factory() {
      return true;
   }

   public function main() {
       
      $href = end($this->url);

      $page = $this->db->fetchRow("SELECT * FROM `page` WHERE `language` = '" . $this->lang . "' AND `href` = '" . $href . "'");

      if (!$page) {
         return $this->error404();
      }

      $this->generateWayFromUri();

	  if ($_COOKIE[ua]!=1){
			$this->setWay($page['header'], $page['type'] == 'section' ? $this->mergeUrlArray() : null);
		}
		else{
			$this->setWay($page['header_ua'], $page['type'] == 'section' ? $this->mergeUrlArray() : null);
		}
      $page['header'] = $this->getAdminEdit('page', $page['id']) . $page['header'];

      $this->setMetaTags($page);

	  if ($_COOKIE[ua]!=1){
      $this->tpl->assign(array('CONTENT' => stripslashes($page['body'])));
		}
		else{
		$this->tpl->assign(array('CONTENT' => stripslashes($page['body_ua'])));
		}
      if ($page['type'] == 'section') {
         $this->viewMessage($this->getAdminAdd('page', $page['id']));
      }

      if ($page['type'] == 'section') {
         $this->loadSectionPage((int) $page['id']);
      }

      return true;
   }

   private function loadSectionPage($id = null) {
      if (null === $id) {
         return true;
      }

      $url = $this->mergeUrlArray();

      $start = 0;
      $navbar = $navTop = $navBot = '';
      $page = 1;

      $num_pages = $this->settings['num_page_items'];

      $count = $this->db->fetchOne("SELECT COUNT(`id`) FROM `page` WHERE `language` = '" . $this->lang . "' AND `level` = '" . $id . "'");

      if ($count > 0) {
         if ($count > $num_pages) {
            if (isset($this->getParam['page'])) {
               $page = (int) $this->getParam['page'];
               $start = $num_pages * $page - $num_pages;

               if ($start > $count) {
                  $start = 0;
               }
            }

            $navbar = $this->loadPaginator((int) ceil($count / $num_pages), (int) $page, $url);

            if ($navbar) {
               $navTop = '<div class="pager_right">' . $navbar . '</div>';
               $navBot = $navTop;
            }
         }

		 $ua=$this->getUa();
         $this->tpl->define_dynamic('_section', "$ua".'pages.tpl');
         $this->tpl->define_dynamic('section', '_section');
         $this->tpl->define_dynamic('section_row', 'section');

		 if ($_COOKIE[ua]!=1){
         $pages = $this->db->fetchAll("SELECT * FROM `page` WHERE `language` = '" . $this->lang . "' AND `level` = '" . $id . "' ORDER BY `position`, `header` LIMIT $start, $num_pages");
		}
			else{
				$pages = $this->db->fetchAll("SELECT * FROM `page` WHERE `language` = '" . $this->lang . "' AND `level` = '" . $id . "' ORDER BY `position`, `header` LIMIT $start, $num_pages");
				}

         foreach ($pages as $page) {
            $link = $url . $page['href'];
            $pic = '';
            if ($page['pic'] != '' && file_exists('./img/pages/' . $page['pic'])) {
               $pic = '<a href="' . $link . '" title="' . $page['header'] . '"><img src="/img/pages/' . $page['pic'] . '" alt="' . $page['header'] . '" class="left" width="84" height="73" align="left" style="margin-right: 10px;"></a>';
            }
			if ($_COOKIE[ua]!=1){
            $this->tpl->assign(
                    array(
                        'PAGE_ADM' => $this->getAdminEdit('page', $page['id']),
                        'PAGE_ADRESS' => $link,
                        'PAGE_HEADER' => stripslashes($page['header']),
                        'PAGE_PREVIEW' => stripslashes($page['preview']),
                        'PIC'=>$pic
                    )
            );

            $this->tpl->parse('SECTION_ROW', '.section_row');
			}
			else{
            $this->tpl->assign(
                    array(
                        'PAGE_ADM' => $this->getAdminEdit('page', $page['id']),
                        'PAGE_ADRESS' => $link,
                        'PAGE_HEADER' => stripslashes($page['header_ua']),
                        'PAGE_PREVIEW' => stripslashes($page['preview_ua']),
                        'PIC'=>$pic
                    )
            );

            $this->tpl->parse('SECTION_ROW', '.section_row');
			}
         }

         $this->tpl->assign(
                 array(
                     'PAGES_TOP' => $navTop,
                     'PAGES_BOTTOM' => $navBot
                 )
         );

         $this->tpl->parse('CONTENT', '.section');
      }
   }

   public function index() {
      $this->loadBanners(true);
	  
	  if ($_COOKIE[ua]!=1){
 $index = $this->db->fetchRow("SELECT `title`, `keywords`, `description`, `header`, `body` FROM `page` WHERE `href` = 'mainpage' AND `language` = '" . $this->lang . "'");
		}
			else{
    $index = $this->db->fetchRow("SELECT `title_ua`, `keywords_ua`, `description_ua`, `header_ua`, `body_ua` FROM `page` WHERE `href` = 'mainpage' AND `language` = '" . $this->lang . "'");               
				}
	  
      

      if (!$index) {
         return $this->error404();
      }

      $this->setMetaTags($index);
		
		if ($_COOKIE[ua]!=1){
			$this->setWay($index['header']);

		}
		else{
			$this->setWay($index['header_ua']);
		}
      
	  $this->tpl->parse('P_HEADER', 'null');

	  $ua=$this->getUa();
      $this->tpl->define_dynamic('main', "$ua".'main.tpl');
      $this->tpl->define_dynamic('main_news', 'main');
      $this->tpl->define_dynamic('main_index', 'main');

      $this->tpl->define_dynamic('main_new_goods', 'main');
      $this->tpl->define_dynamic('main_hit_goods', 'main');
      $this->tpl->define_dynamic('main_actions_goods', 'main');


      //$this->indexNewGoods();
      //$this->indexHitsGoods();
      //$this->indexActionsGoods();

      //$this->topNews();
      $this->indexNews();

      //$this->indexArtikle();


		  if ($_COOKIE[ua]!=1){
				  $this->tpl->assign(
						  array(
							  'INDEX_HEADER' => stripslashes($index['header']),
							  'INDEX_BODY' => stripslashes($index['body']),
						  )
				  );
		  }
		  else{
		  
			  $this->tpl->assign(
					  array(
						  'INDEX_HEADER' => stripslashes($index['header_ua']),
						  'INDEX_BODY' => stripslashes($index['body_ua']),
					  )
			  );
		  
		  }
	  
      $this->tpl->parse('CONTENT', '.main_index');

      return true;
   }

   protected function indexNewGoods() {

      $sql = "SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `type` = 'page' AND `status`='new' ORDER BY RAND() LIMIT 3";

      $items = $this->db->fetchAll($sql);
      $itemsLength = count($items);
      $catItem1 = '';
      $catItem2 = '';
      $catItem3 = '';
      $catItem4 = '';
      $catItem5 = '';
      $catalogItem = '';

      $i = 0;
      $f = 0;
      $s = 0;
      if ($itemsLength > 0) {

         while (true) {
            if (isset($items[$i])) {

               $itemUrl = '';
               $groupName = '-';
               $groupNameUrl = '#';

               if (($parenSectionInfo = $this->dataTreeManager($items[$i]['id']))) {
                  $itemUrl = '/catalog/' . $parenSectionInfo['links'];
                  $groupNameIndex = (count($parenSectionInfo['names']) - 3);

                  if (isset($parenSectionInfo['names'][$groupNameIndex])) {

                     $groupNameUrl = '/catalog';

                     for ($f1 = 0; $f1 <= $groupNameIndex; ++$f1) {
                        $groupNameUrl .= '/' . $parenSectionInfo['linksArr'][$f1];
                     }

                     $groupName = $parenSectionInfo['names'][$groupNameIndex];
                  }
               }


               $link = (($items[$i]['type'] != 'link') ? ('/catalog/') : ('')) . $parenSectionInfo['links'];
               $name = $items[$i]['name'];

               if (!empty($types)) {
                  $types = "$types ";
               }



               $liLastClass = '';

               if ($f == 2) {
                  $liLastClass = 'last';
               }

               $imgSrc = 'nophoto_s.jpg';
               $imgAlt = 'Нет фото';
               $imgTitle = $imgAlt;

               if (is_file($_SERVER['DOCUMENT_ROOT'] . '/img/catalog/small/' . $items[$i]['pic'])) {
                  $imgSrc = $items[$i]['pic'];
                  $imgAlt = $items[$i]['pic_alt'];
                  $imgTitle = $items[$i]['pic_alt'];

                  if (empty($imgAlt)) {
                     $imgAlt = $items[$i]['name'];
                     $imgTitle = $items[$i]['name'];
                  }
               } elseif (is_file($_SERVER['DOCUMENT_ROOT'] . '/img/catalog/small/' . $items[$i]['artikul'] . '.jpg')) {
                  $imgSrc = $items[$i]['artikul'] . '.jpg';
                  $imgAlt = $items[$i]['pic_alt'];
                  $imgTitle = $items[$i]['pic_title'];

                  if (empty($imgAlt)) {
                     $imgAlt = $items[$i]['name'];
                  }

                  if (empty($imgTitle)) {
                     $imgTitle = $items[$i]['name'];
                  }
               }

               $imgType = '';


               if (isset($items[$i]['status']) && $items[$i]['status'] == 'new') {
                  $imgType = '<img src="/img/new.png" class="png" width="173" height="37" alt="" />';
               }

               $preview = $items[$i]['preview'];

               if (empty($preview)) {
                  $preview = '&nbsp;';
               }

               $catItem1 .= '<li><div class="img">' . $imgType . '<a href="' . $itemUrl . '"><img src="/img/catalog/small/' . $imgSrc . '" width="200" height="180" alt="' . $imgAlt . '" title="' . $imgTitle . '"/></a></div><p><span class="left"></span><span class="right"><a href="' . $itemUrl . '" title="' . $items[$i]['name'] . '">' . $items[$i]['name'] . '</a></span></p></li> ';
               $catItem2 .= '<li><p><span class="left">Группа:</span><span class="right"><a class="black" href="' . $groupNameUrl . '">' . $groupName . '</a></span></p></li>';
               $catItem3 .= '<li><p class="desc">' . stripslashes($preview) . '</p></li>';
               $catItem4 .= '<li>';
               $catItem5 .= '<li>';
               if ($this->getCatOption('isStore')) {

                  if (isset($items[$i]['cost_old'])) {
                     if ($items[$i]['cost_old'] > $items[$i]['cost']) {
                        $econom = ($items[$i]['cost_old'] - $items[$i]['cost']);
                        if ($econom > 0) {
                           $catItem4 .= '
     									<p><span>Розничная цена:</span><span class="price">' . number_format($items[$i]['cost_old'], 2, ',', " ") . ' грн.</span></p>
     									<p><span>Экономия:</span><span class="eco">' . number_format($econom, 2, ',', " ") . ' грн.</span></p>';
                        }
                     }
                  }

                  $buyButton = '<div ><div style="float: left;"> <input type="text"  class="goods-length" value="1" id="goods_' . $items[$i]['id'] . '" /> шт. </div> <div style="text-align: right;"> <a href="#" class="buy" onclick="addToBasket(\'goods_' . $items[$i]['id'] . '\', $(\'goods_' . $items[$i]['id'] . '\').value);"> Заказать</a></div></div>';

                  if (!$this->isBuyButtomType($items[$i]['id'])) {
                     $buyButton = '<p class="right"><img src="/img/ok.gif" width="13" height="12" alt="" /><a href="/order">Оформить заказ</a></p>';
                  }
                  if (empty($items[$i]['cost'])) {
                     $items[$i]['cost'] = 0;
                  }
                  $catItem5.='<p><span>Наша цена:</span><span class="newprice">' . number_format($items[$i]['cost'], 2, ',', " ") . ' грн.</span></p><span id="by_button_goods_' . $items[$i]['id'] . '">' . $buyButton . '</span>';
               }

               $catItem4 .= '</li>';
               $catItem5 .= '</li>';
            } else {
               $liLastClass = '';

               if ($f == 2) {
                  $liLastClass = '';
               }

               //	$catItem1 .= '<li class="li-name-empty'.$liLastClass.'"></li>';
               //	$catItem2 .= '<li class="li-name-empty'.$liLastClass.'"></li>';
               //	$catItem3 .= '<li class="li-name-empty'.$liLastClass.'"></li>';
               //	$catItem4 .= '<li class="li-name-empty'.$liLastClass.'"><div class="g_img"><img src="/img/block_product.jpg" width="186" height="186" /></li>';	
            }

            $f++;
            $i++;

            if ($f == 3) {
               $f = 0;
               $s+=3;


               $catalogItem .='<ul>' . $catItem1 . '</ul>' . '<div class="clear"></div>';
               $catalogItem .='<ul>' . $catItem2 . '</ul>' . '<div class="clear"></div>';
               $catalogItem .='<ul>' . $catItem3 . '</ul>' . '<div class="clear"></div>';
               $catalogItem .='<ul>' . $catItem4 . '</ul>' . '<div class="clear"></div>';
               $catalogItem .='<ul>' . $catItem5 . '</ul>' . '<div class="clear" style="margin-bottom: 35px;"></div>';
               $catItem1 = '';
               $catItem2 = '';
               $catItem3 = '';
               $catItem4 = '';
               $catItem5 = '';


               //block_product.jpg
            }

            if ($s >= $itemsLength) {
               $catItem1 = '';
               $catItem2 = '';
               $catItem3 = '';
               $catItem4 = '';
               $catItem5 = '';
               break;
            }
         }
         $this->tpl->assign(array('NEW_CATALOG_ITEMS' => $catalogItem));


         $this->tpl->parse('CONTENT', '.main_new_goods');
      }
   }

   protected function indexHitsGoods() {
      $sql = "SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `type` = 'page' AND `status`='hit' ORDER BY RAND() LIMIT 3";

      $items = $this->db->fetchAll($sql);
      $itemsLength = count($items);


      $catItem1 = '';
      $catItem2 = '';
      $catItem3 = '';
      $catItem4 = '';
      $catItem5 = '';
      $catalogItem = '';

      $i = 0;
      $f = 0;
      $s = 0;
      if ($itemsLength > 0) {

         while (true) {
           
            if (isset($items[$i])) {

               $itemUrl = '';
               $groupName = '-';
               $groupNameUrl = '#';

               if (($parenSectionInfo = $this->dataTreeManager($items[$i]['id']))) {
                  $itemUrl = '/catalog/' . $parenSectionInfo['links'];
                  $groupNameIndex = (count($parenSectionInfo['names']) - 3);

                  if (isset($parenSectionInfo['names'][$groupNameIndex])) {
                     $groupNameUrl = '/catalog';

                     for ($f1 = 0; $f1 <= $groupNameIndex; ++$f1) {
                        $groupNameUrl .= '/' . $parenSectionInfo['linksArr'][$f1];
                     }
                     $groupName = $parenSectionInfo['names'][$groupNameIndex];
                  }
               }


               $link = (($items[$i]['type'] != 'link') ? ('/catalog/') : ('')) . $parenSectionInfo['links'];
               //$types = adminCatalogEdit($items[$i]['id']);
               $name = $items[$i]['name'];
               //$url = '/catalog/'.$parenSectionInfo[0]['href'];

               if (!empty($types)) {
                  $types = "$types ";
               }


               $liLastClass = '';

               if ($f == 2) {
                  $liLastClass = 'last';
               }


               $imgSrc = 'nophoto_s.jpg';
               $imgAlt = 'Нет фото';
               $imgTitle = $imgAlt;

               if (is_file($_SERVER['DOCUMENT_ROOT'] . '/img/catalog/small/' . $items[$i]['pic'])) {
                  $imgSrc = $items[$i]['pic'];
                  $imgAlt = $items[$i]['pic_alt'];
                  $imgTitle = $items[$i]['pic_alt'];

                  if (empty($imgAlt)) {
                     $imgAlt = $items[$i]['name'];
                     $imgTitle = $items[$i]['name'];
                  }
               } elseif (is_file($_SERVER['DOCUMENT_ROOT'] . '/img/catalog/small/' . $items[$i]['artikul'] . '.jpg')) {
                  $imgSrc = $items[$i]['artikul'] . '.jpg';
                  $imgAlt = $items[$i]['pic_alt'];
                  $imgTitle = $items[$i]['pic_title'];

                  if (empty($imgAlt)) {
                     $imgAlt = $items[$i]['name'];
                  }

                  if (empty($imgTitle)) {
                     $imgTitle = $items[$i]['name'];
                  }
               }

               $imgType = '';

               if (isset($items[$i]['status']) && $items[$i]['status'] == 'hit') {
                  $imgType = '<img src="/img/hit.png" class="png" width="173" height="37" alt="" />';
               }


               $preview = $items[$i]['preview'];

               if (empty($preview)) {
                  $preview = '&nbsp;';
               }

               $catItem1 .= '<li><div class="img">' . $imgType . '<a href="' . $itemUrl . '"><img src="/img/catalog/small/' . $imgSrc . '" width="200" height="180" alt="' . $imgAlt . '" title="' . $imgTitle . '"/></a></div><p><span class="left"></span><span class="right"><a href="' . $itemUrl . '" title="' . $items[$i]['name'] . '">' . $items[$i]['name'] . '</a></span></p></li> ';
               $catItem2 .= '<li><p><span class="left">Группа:</span><span class="right"><a class="black" href="' . $groupNameUrl . '">' . $groupName . '</a></span></p></li>';
               $catItem3 .= '<li><p class="desc">' . stripslashes($preview) . '</p></li>';
               $catItem4 .= '<li>';
               $catItem5 .= '<li>';

               if ($this->getCatOption('isStore')) {

                  if (isset($items[$i]['cost_old'])) {
                     if ($items[$i]['cost_old'] > $items[$i]['cost']) {
                        $econom = ($items[$i]['cost_old'] - $items[$i]['cost']);
                        if ($econom > 0) {
                           $catItem4 .= '
     									<p><span>Розничная цена:</span><span class="price">' . number_format($items[$i]['cost_old'], 2, ',', " ") . ' грн.</span></p>
     									<p><span>Экономия:</span><span class="eco">' . number_format($econom, 2, ',', " ") . ' грн.</span></p>';
                        }
                     }
                  }

                  $buyButton = '<div ><div style="float: left;"> <input type="text"  class="goods-length" value="1" id="goods_' . $items[$i]['id'] . '" /> шт. </div> <div style="text-align: right;"> <a href="#" class="buy" onclick="addToBasket(\'goods_' . $items[$i]['id'] . '\', $(\'goods_' . $items[$i]['id'] . '\').value);"> Заказать</a></div></div>';

                  if (!$this->isBuyButtomType($items[$i]['id'])) {
                     $buyButton = '<p class="right"><img src="/img/ok.gif" width="13" height="12" alt="" /><a href="/order">Оформить заказ</a></p>';
                  }
                  if (empty($items[$i]['cost'])) {
                     $items[$i]['cost'] = 0;
                  }
                  $catItem5.='<p><span>Наша цена:</span><span class="newprice">' . number_format($items[$i]['cost'], 2, ',', " ") . ' грн.</span></p><span id="by_button_goods_' . $items[$i]['id'] . '">' . $buyButton . '</span>';
               }
               $catItem4 .= '</li>';
               $catItem5 .= '</li>';
            } else {
               $liLastClass = '';

               if ($f == 2) {
                  $liLastClass = '';
               }

               //	$catItem1 .= '<li class="li-name-empty'.$liLastClass.'"></li>';
               //	$catItem2 .= '<li class="li-name-empty'.$liLastClass.'"></li>';
               //	$catItem3 .= '<li class="li-name-empty'.$liLastClass.'"></li>';
               //	$catItem4 .= '<li class="li-name-empty'.$liLastClass.'"><div class="g_img"><img src="/img/block_product.jpg" width="186" height="186" /></li>';	
            }

            $f++;
            $i++;

            if ($f == 3) {
               $f = 0;
               $s+=3;


               $catalogItem .='<ul>' . $catItem1 . '</ul>' . '<div class="clear"></div>';
               $catalogItem .='<ul>' . $catItem2 . '</ul>' . '<div class="clear"></div>';
               $catalogItem .='<ul>' . $catItem3 . '</ul>' . '<div class="clear"></div>';
               $catalogItem .='<ul>' . $catItem4 . '</ul>' . '<div class="clear"></div>';
               $catalogItem .='<ul>' . $catItem5 . '</ul>' . '<div class="clear" style="margin-bottom: 35px;"></div>';
               $catItem1 = '';
               $catItem2 = '';
               $catItem3 = '';
               $catItem4 = '';
               $catItem5 = '';


               //block_product.jpg
            }

            if ($s >= $itemsLength) {
               $catItem1 = '';
               $catItem2 = '';
               $catItem3 = '';
               $catItem4 = '';
               $catItem5 = '';
               break;
            }
         }
         $this->tpl->assign(array('HIT_CATALOG_ITEMS' => $catalogItem));

         $this->tpl->parse('CONTENT', '.main_hit_goods');
      }
   }

   protected function indexActionsGoods() {

      $sql = "SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `type` = 'page' AND `status`='action' ORDER BY RAND() LIMIT 3";

      $items = $this->db->fetchAll($sql);
      $itemsLength = count($items);


      $catItem1 = '';
      $catItem2 = '';
      $catItem3 = '';
      $catItem4 = '';
      $catItem5 = '';
      $catalogItem = '';

      $i = 0;
      $f = 0;
      $s = 0;
      if ($itemsLength > 0) {

         while (true) {
           
            if (isset($items[$i])) {

               $itemUrl = '';
               $groupName = '-';
               $groupNameUrl = '#';

               if (($parenSectionInfo = $this->dataTreeManager($items[$i]['id']))) {
                  $itemUrl = '/catalog/' . $parenSectionInfo['links'];
                  $groupNameIndex = (count($parenSectionInfo['names']) - 3);

                  if (isset($parenSectionInfo['names'][$groupNameIndex])) {
                     $groupNameUrl = '/catalog';

                     for ($f1 = 0; $f1 <= $groupNameIndex; ++$f1) {
                        $groupNameUrl .= '/' . $parenSectionInfo['linksArr'][$f1];
                     }
                     $groupName = $parenSectionInfo['names'][$groupNameIndex];
                  }
               }


               $link = (($items[$i]['type'] != 'link') ? ('/catalog/') : ('')) . $parenSectionInfo['links'];
               //$types = adminCatalogEdit($items[$i]['id']);
               $name = $items[$i]['name'];
               //$url = '/catalog/'.$parenSectionInfo[0]['href'];

               if (!empty($types)) {
                  $types = "$types ";
               }


               $liLastClass = '';

               if ($f == 2) {
                  $liLastClass = 'last';
               }


               $imgSrc = 'nophoto_s.jpg';
               $imgAlt = 'Нет фото';
               $imgTitle = $imgAlt;

               if (is_file($_SERVER['DOCUMENT_ROOT'] . '/img/catalog/small/' . $items[$i]['pic'])) {
                  $imgSrc = $items[$i]['pic'];
                  $imgAlt = $items[$i]['pic_alt'];
                  $imgTitle = $items[$i]['pic_alt'];

                  if (empty($imgAlt)) {
                     $imgAlt = $items[$i]['name'];
                     $imgTitle = $items[$i]['name'];
                  }
               } elseif (is_file($_SERVER['DOCUMENT_ROOT'] . '/img/catalog/small/' . $items[$i]['artikul'] . '.jpg')) {
                  $imgSrc = $items[$i]['artikul'] . '.jpg';
                  $imgAlt = $items[$i]['pic_alt'];
                  $imgTitle = $items[$i]['pic_title'];

                  if (empty($imgAlt)) {
                     $imgAlt = $items[$i]['name'];
                  }

                  if (empty($imgTitle)) {
                     $imgTitle = $items[$i]['name'];
                  }
               }

               $imgType = '';

               if (isset($items[$i]['status']) && $items[$i]['status'] == 'action') {
                  $imgType = '<img src="/img/akcia.png" class="png" width="173" height="37" alt="" />';
               }



               $preview = $items[$i]['preview'];

               if (empty($preview)) {
                  $preview = '&nbsp;';
               }

               $catItem1 .= '<li><div class="img">' . $imgType . '<a href="' . $itemUrl . '"><img src="/img/catalog/small/' . $imgSrc . '" width="200" height="180" alt="' . $imgAlt . '" title="' . $imgTitle . '"/></a></div><p><span class="left"></span><span class="right"><a href="' . $itemUrl . '" title="' . $items[$i]['name'] . '">' . $items[$i]['name'] . '</a></span></p></li> ';
               $catItem2 .= '<li><p><span class="left">Группа:</span><span class="right"><a class="black" href="' . $groupNameUrl . '">' . $groupName . '</a></span></p></li>';
               $catItem3 .= '<li><p class="desc">' . stripslashes($preview) . '</p></li>';
               $catItem4 .= '<li>';
               $catItem5 .= '<li>';

               if ($this->getCatOption('isStore')) {

                  if (isset($items[$i]['cost_old'])) {
                     if ($items[$i]['cost_old'] > $items[$i]['cost']) {
                        $econom = ($items[$i]['cost_old'] - $items[$i]['cost']);
                        if ($econom > 0) {
                           $catItem4 .= '
     									<p><span>Розничная цена:</span><span class="price">' . number_format($items[$i]['cost_old'], 2, ',', " ") . ' грн.</span></p>
     									<p><span>Экономия:</span><span class="eco">' . number_format($econom, 2, ',', " ") . ' грн.</span></p>';
                        }
                     }
                  }


                  $buyButton = '<div><div style="float: left;"> <input type="text" class="goods-length" value="1" id="goods_' . $items[$i]['id'] . '" /> шт. </div> <div style="text-align: right;"> <a href="#" class="buy" onclick="addToBasket(\'goods_' . $items[$i]['id'] . '\', $(\'goods_' . $items[$i]['id'] . '\').value);"> Заказать</a></div></div>';

                  if (!$this->isBuyButtomType($items[$i]['id'])) {
                     $buyButton = '<p class="right"><img src="/img/ok.gif" width="13" height="12" alt="" /><a href="/order">Оформить заказ</a></p>';
                  }
                  if (empty($items[$i]['cost'])) {
                     $items[$i]['cost'] = 0;
                  }
                  $catItem5.='<p><span>Наша цена:</span><span class="newprice">' . number_format($items[$i]['cost'], 2, ',', " ") . ' грн.</span></p><span id="by_button_goods_' . $items[$i]['id'] . '">' . $buyButton . '</span>';
               }

               $catItem4 .= "</li>";
               $catItem5 .= "</li>";
            } else {
               $liLastClass = '';

               if ($f == 2) {
                  $liLastClass = '';
               }
            }

            $f++;
            $i++;

            if ($f == 3) {
               $f = 0;
               $s+=3;


               $catalogItem .='<ul>' . $catItem1 . '</ul>' . '<div class="clear"></div>';
               $catalogItem .='<ul>' . $catItem2 . '</ul>' . '<div class="clear"></div>';
               $catalogItem .='<ul>' . $catItem3 . '</ul>' . '<div class="clear"></div>';
               $catalogItem .='<ul>' . $catItem4 . '</ul>' . '<div class="clear"></div>';
               $catalogItem .='<ul>' . $catItem5 . '</ul>' . '<div class="clear" style="margin-bottom: 35px;"></div>';
               $catItem1 = '';
               $catItem2 = '';
               $catItem3 = '';
               $catItem4 = '';
               $catItem5 = '';


               //block_product.jpg
            }

            if ($s >= $itemsLength) {
               $catItem1 = '';
               $catItem2 = '';
               $catItem3 = '';
               $catItem4 = '';
               $catItem5 = '';
               break;
            }
         }

         $this->tpl->assign(array('ACT_CATALOG_ITEMS' => $catalogItem));


         $this->tpl->parse('CONTENT', '.main_actions_goods');
      }
   }

   private function indexNews() {
      $news = $this->db->fetchAll("SELECT * FROM `news` WHERE `visibility` = '1' ORDER BY `date` DESC, `header` LIMIT 0, " . $this->settings['num_lastnews']);

      if ($news) {
	  $ua=$this->getUa();
         $this->tpl->define_dynamic('_news', "$ua".'news.tpl');
         $this->tpl->define_dynamic('news', '_news');
         $this->tpl->define_dynamic('news_item', 'news');

         foreach ($news as $item) {
            $pic = '';
            if ($item['pic'] != '' && file_exists('./img/news/' . $item['pic'])) {
               $pic = '<a href="/news/' . $item['href'] . '" title="' . $item['header'] . '"><img src="/img/news/' . $item['pic'] . '" alt="' . $item['header'] . '" class="left" width="84" height="73" align="left" style="margin-right: 10px;"></a>';
            }

           if ($_COOKIE[ua]!=1){
            $this->tpl->assign(
                    array(
                        'ADM_EDIT' => $index ? '' : $this->getAdminEdit('news', $item['id']),
                        'DATE' => $this->convertDate($item['date']),
                        'NEWS_ADRESS' => $item['href'],
                        'NEWS_HEADER' => stripslashes($item['header']),
                        'PIC' => $pic,
                        'NEWS_PREVIEW' => stripslashes($item['preview'])
                    )
            );
		}
		else{
            $this->tpl->assign(
                    array(
                        'ADM_EDIT' => $index ? '' : $this->getAdminEdit('news', $item['id']),
                        'DATE' => $this->convertDate($item['date']),
                        'NEWS_ADRESS' => $item['href'],
                        'NEWS_HEADER' => stripslashes($item['header_ua']),
                        'PIC' => $pic,
                        'NEWS_PREVIEW' => stripslashes($item['preview_ua'])
                    )
            );
		}

            $this->tpl->parse('NEWS_ITEM', '.news_item');
         }

         $this->tpl->assign(
                 array(
                     'PAGES_TOP' => '',
                     'PAGES_BOTTOM' => ''
                 )
         );

         $this->tpl->parse('CONTENT', '.main_news');
         //$this->tpl->parse('NEWS_INDEX_SEP', '.news_index_sep');

         $this->tpl->parse('CONTENT', '.news');
      }
   }

   private function indexArtikle() {
      $num_pages = $this->settings['num_page_items'];

	  $ua=$this->getUa();
      $this->tpl->define_dynamic('_section', "$ua".'pages.tpl');
      $this->tpl->define_dynamic('section', '_section');
      $this->tpl->define_dynamic('section_row', 'section');

      $pages = $this->db->fetchAll("SELECT * FROM `page` WHERE `top` = '1' AND `type` = 'page' ORDER BY RAND() LIMIT 0, " . $num_pages);

      if (sizeof($pages) > 0) {
	  
	  if ($_COOKIE[ua]!=1){
         $groups = $this->db->fetchAll("
                SELECT `id`, `header`, `href`, `level`
                FROM `page`
                WHERE `type` = 'section' AND `language` = '" . $this->lang . "'
            ");
		}
			else{
			 $groups = $this->db->fetchAll("
					SELECT `id`, `header`, `href`, `level`
					FROM `page`
					WHERE `type` = 'section' AND `language` = 'ua'
				");
				}
				


         foreach ($pages as $item) {
		 if ($_COOKIE[ua]!=1){
            $this->tpl->assign(
                    array(
                        'PAGE_ADM' => '',
                        'PAGE_ADRESS' => $this->getHref($groups, $page['level']) . $page['href'],
                        'PAGE_HEADER' => stripslashes($page['header']),
                        'PAGE_PREVIEW' => stripslashes($page['preview'])
                    )
            );

            $this->tpl->parse('SECTION_ROW', '.section_row');
		  }
		  else
		  {
            $this->tpl->assign(
                    array(
                        'PAGE_ADM' => '',
                        'PAGE_ADRESS' => $this->getHref($groups, $page['level']) . $page['href'],
                        'PAGE_HEADER' => stripslashes($page['header_ua']),
                        'PAGE_PREVIEW' => stripslashes($page['preview_ua'])
                    )
            );

            $this->tpl->parse('SECTION_ROW', '.section_row');
		  }
        }

         $this->tpl->parse('CONTENT', '.section');
      }
   }

   private function topNews() {
      $news = $this->db->fetchAll("SELECT * FROM `news` WHERE `top` = '1' ORDER BY `date` DESC");

	  $ua=$this->getUa();
      $this->tpl->define_dynamic('_news', "$ua".'news.tpl');
      $this->tpl->define_dynamic('news_top', '_news');
      $this->tpl->define_dynamic('news_top_item', 'news_top');

      if (sizeof($news) > 0) {
         foreach ($news as $item) {
            $pic = '';
            if ($item['pic'] != '' && file_exists('./img/news/' . $item['pic'])) {
               $pic = '<div class="nz_i"><a href="/news/' . $item['href'] . '" title="' . $item['header'] . '"><img src="/img/news/' . $item['pic'] . '" width="143px" height="107px" alt="' . $item['header'] . '" /></a></div>';
            }

            $this->tpl->assign(
                    array(
                        'NEWS_ADRESS' => $item['href'],
                        'NEWS_HEADER' => stripslashes($item['header']),
                        'PIC' => $pic,
                        'NEWS_PREVIEW' => stripslashes($item['preview'])
                    )
            );

            $this->tpl->parse('NEWS_TOP_ITEM', '.news_top_item');
         }

         $this->tpl->parse('CONTENT', '.news_top');
      }
   }

   public function news($index = false) {
   
      if ($_COOKIE[ua]!=1){
       $this->setMetaTags('Новости');    	
       $this->setWay('Новости');
		}
			else{
                $this->setMetaTags('Новини');    	
				$this->setWay('Новини');
				}
				
   $ua=$this->getUa();
      $this->tpl->define_dynamic('_news', "$ua".'news.tpl');
      $this->tpl->define_dynamic('news', '_news');
      $this->tpl->define_dynamic('news_item', 'news');
      $this->tpl->define_dynamic('news_detail', '_news');

      if (!isset($this->url[1])) {
         $start = 0;
         $navbar = $navTop = $navBot = '';
         $page = 1;

         if ($index) {
            $num_news = $this->settings['num_lastnews'];
         } else {
            $num_news = $this->settings['num_news'];

            $count = $this->db->fetchOne("SELECT COUNT(`id`) FROM `news` WHERE `language` = '" . $this->lang . "'");

            if ($count > 0) {
               if ($count > $num_news) {
                  if (isset($this->getParam['page'])) {
                     $page = (int) $this->getParam['page'];
                     $start = $num_news * $page - $num_news;

                     if ($start > $count) {
                        $start = 0;
                     }
                  }

                  $navbar = $this->loadPaginator((int) ceil($count / $num_news), (int) $page, $this->basePath . 'news/');

                  if ($navbar) {
                     $navTop = '<div class="pager_right">' . $navbar . '</div>';
                     $navBot = $navTop;
                  }
               }
            } else {
               $this->tpl->assign('CONTENT', ($index ? '' : $this->getAdminAdd('news')) . '{EMPTY_SECTION}');
               return true;
            }
         }

         $news = $this->db->fetchAll("SELECT * FROM `news` WHERE `language` = '" . $this->lang . "'" . ($this->_isAdmin() ? "" : " AND `visibility` = '1'") . "" . ($index ? ' AND `top` = "0"' : '') . " ORDER BY `date` DESC LIMIT " . $start . ", " . $this->settings['num_news']);
if ($news){
         foreach ($news as $item) {
            $pic = '';
            if ($item['pic'] != '' && file_exists('./img/news/' . $item['pic'])) {
               $pic = '<a href="/news/' . $item['href'] . '" title="' . $item['header'] . '"><img src="/img/news/' . $item['pic'] . '" alt="' . $item['header'] . '" alt="' . $item['title'] . '"  width="84" height="73" align="left" style="margin-right: 10px;" /></a>';
            }

		if ($_COOKIE[ua]!=1){
            $this->tpl->assign(
                    array(
                        'ADM_EDIT' => $index ? '' : $this->getAdminEdit('news', $item['id']),
                        'DATE' => $this->convertDate($item['date']),
                        'NEWS_ADRESS' => $item['href'],
                        'NEWS_HEADER' => stripslashes($item['header']),
                        'PIC' => $pic,
                        'NEWS_PREVIEW' => stripslashes($item['preview'])
                    )
            );
		}
		else{
            $this->tpl->assign(
                    array(
                        'ADM_EDIT' => $index ? '' : $this->getAdminEdit('news', $item['id']),
                        'DATE' => $this->convertDate($item['date']),
                        'NEWS_ADRESS' => $item['href'],
                        'NEWS_HEADER' => stripslashes($item['header_ua']),
                        'PIC' => $pic,
                        'NEWS_PREVIEW' => stripslashes($item['preview_ua'])
                    )
            );
		}

            $this->tpl->parse('NEWS_ITEM', '.news_item');
         }
}
         $header = '';
         if ($index) {
            $header = $this->db->fetchOne('SELECT `header` FROM `meta_tags` WHERE `href` = "news" AND `language` = "' . $this->lang . '"');
         }

         $this->tpl->assign(
                 array(
                     'PAGES_TOP' => $navTop,
                     'PAGES_BOTTOM' => $navBot,
                     'NEWS_HEADER' => $header,
                     'CONTENT' => $index ? '' : $this->getAdminAdd('news')
                 )
         );

         if (@$count > 0) {
            if ($index) {
               $this->tpl->parse('CONTENT', '.main_news');
            }
if ($news){
            $this->tpl->parse('CONTENT', '.news');
}
         }
      } elseif (!isset($this->url[2])) {
         $href = end($this->url);

         $news = $this->db->fetchRow("SELECT * FROM `news` WHERE `language` = '" . $this->lang . "' AND `href` = '$href'");

         if (!$news) {
            return $this->error404();
         }

         $date = $this->convertDate($news['date']);

         $this->setWay($date . ' / ' . $news['header']);

         $header = $news['header'];
         $news['header'] = $this->getAdminEdit('news', $news['id']) . $news['header'];

         $this->setMetaTags($news);
         $pic = '';

         if (is_file(PATH . 'img/news/' . $news['pic'])) {
            $pic = '<img src="/img/news/' . $news['pic'] . '" width="137" height="102" alt="' . $header . '" title="' . $header . '" style="float: left; margin-right: 10px;" />';
         }
        $this->tpl->parse('P_HEADER', 'null');
        		 
		if ($_COOKIE[ua]!=1){
            $this->tpl->assign(
                 array(
                     'NEWS_HEADER' => stripslashes($news['header']),
                     'NEWS_BODY' => stripslashes($news['body']),
                     'NEWS_DATE' => $date,
                     'NEWS_TITLE' => $header,
                     'NEWS_PREVEW' => stripslashes($news['preview']),
                     'PIC' => $pic
                 )
         );
		}
		else{
            $this->tpl->assign(
                 array(
                     'NEWS_HEADER' => stripslashes($news['header_ua']),
                     'NEWS_BODY' => stripslashes($news['body_ua']),
                     'NEWS_DATE' => $date,
                     'NEWS_TITLE' => $header,
                     'NEWS_PREVEW' => stripslashes($news['preview_ua']),
                     'PIC' => $pic
                 )
         );
		}

         $this->tpl->parse('CONTENT', 'news_detail');
      } else {
         return $this->error404();
      }

      return true;
   }

   public function feedback() {
   
      if ($_COOKIE[ua]!=1){
       $this->setMetaTags('Обратная связь');    	
       $this->setWay('Обратная связь');
		}
			else{
                $this->setMetaTags('Зворотний зв’язок');    	
				$this->setWay('Зворотний зв’язок');
				}
   
   $ua=$this->getUa();
      $this->tpl->define_dynamic('feedback', "$ua".'feedback.tpl');

      $admin_email = $this->settings['admin_email'];

      $fio = $this->getVar('fio', '');
      $email = $this->getVar('email', '');
      $body = $this->getVar('body', '');
      $captchaId = $this->getVar('captcha_id', '');
      $captchaInput = $this->getVar('captcha_input', '');

      if (!empty($_POST)) {
         if (!$fio)
            $this->addErr('{EMPTY_FIO}');

         $validate = new Zend_Validate_EmailAddress();
         if (!$validate->isValid($email))
            $this->addErr('{WRONG_EMAIL}');

         if (!$body)
            $this->addErr('{EMPTY_TEXT}');

         $captchaSession = new Zend_Session_Namespace('Zend_Form_Captcha_' . $captchaId);
         $captchaIterator = $captchaSession->getIterator();
         @$captchaWord = $captchaIterator['word'];

		 
		 if ($_COOKIE[ua]!=1){
				if (!$captchaWord)
				$this->addErr('Проверочный код устарел. Пожалуйста введите код заново');
				else {
				if (!$captchaInput)
				   $this->addErr('Вы не ввели проверочный код');
				else {
				   if ($captchaInput != $captchaWord)
					  $this->addErr('Ошибка. Введите проверочнй код повторно');
				}
			 }
		}
			else{
                   if (!$captchaWord)
					$this->addErr('Код перевірки застарів. Будь ласка введіть код заново');
					else {
					if (!$captchaInput)
					$this->addErr('Ви не ввели перевірочний код');
					else {
					if ($captchaInput != $captchaWord)
                  $this->addErr('Помилка. Введіть код перевірки повторно');
						}
					 }
				}
         
      }

      if (!empty($_POST) && !$this->_err) {
         $body = "Ф.И.О.: $fio <br>E-Mail: $email <br>Сообщение:<br>$body";
         $subject = "Сообщение с сайта http://" . $_SERVER['HTTP_HOST'] . '/';

         $mail = new Zend_Mail('utf8');
         $mail->setBodyHtml($body);
         $mail->setFrom('webmaster@' . $_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST']);
         $mail->addTo($admin_email, 'Administrator');
         $mail->setSubject($subject);
         $mail->send();

         $this->tpl->assign('CONTENT', '{MESSAGE_SENT}');
      }

      if ($this->_err) {
         $this->viewErr();
      }

      if (empty($_POST) || $this->_err) {
         $captcha = new Zend_Captcha_Png(array(
                     'name' => 'cptch',
                     'wordLen' => 6,
                     'timeout' => 1800,
                 ));
         $captcha->setFont('./Zend/Captcha/Fonts/ANTIQUA.TTF');
         $captcha->setStartImage('./img/captcha.png');
         $id = $captcha->generate();

         $this->tpl->assign(
                 array(
                     'FIO' => $fio,
                     'EMAIL' => $email,
                     'BODY' => $body,
                     'CAPTCHA_ID' => $id
                 )
         );
         $this->tpl->parse('CONTENT', '.feedback');
      }

      return true;
   }

   public function kontakti() {
   
		   if ($_COOKIE[ua]!=1){
      $this->setMetaTags("Контакты и отзывы");
      $this->setWay("Контакты и отзывы");
				}
					else{
		$this->setMetaTags("Контакти та відгуки");
		$this->setWay("Контакти та відгуки");
							}


	  $ua=$this->getUa();
      $this->tpl->define_dynamic('feedback', "$ua".'feedback.tpl');

      $admin_email = $this->settings['admin_email'];

      $fio = $this->getVar('fio', '');
      $email = $this->getVar('email', '');
      $phone = $this->getVar('phone', '');
      $body = $this->getVar('body', '');
      $captchaId = $this->getVar('captcha_id', '');
      $captchaInput = $this->getVar('captcha_input', '');

      if (!empty($_POST)) {
         if (!$fio)
            $this->addErr('{EMPTY_FIO}');
        if (!$phone)
            $this->addErr('{EMPTY_PHONE}');
         $validate = new Zend_Validate_EmailAddress();
         if (!$validate->isValid($email))
            $this->addErr('{WRONG_EMAIL}');

         if (!$body)
            $this->addErr('{EMPTY_TEXT}');

         $captchaSession = new Zend_Session_Namespace('Zend_Form_Captcha_' . $captchaId);
         $captchaIterator = $captchaSession->getIterator();
         @$captchaWord = $captchaIterator['word'];

				 if ($_COOKIE[ua]!=1){
						 if (!$captchaWord)
							$this->addErr('Проверочный код устарел. Пожалуйста введите код заново');
						 else {
							if (!$captchaInput)
							   $this->addErr('Вы не ввели проверочный код');
								else {
								   if ($captchaInput != $captchaWord)
									  $this->addErr('Ошибка. Введите проверочнй код повторно');
									}
								}
				}
					else{
									if (!$captchaWord)
									$this->addErr('Код перевірки застарів. Будь ласка введіть код заново');
								 else {
										if (!$captchaInput)
										   $this->addErr('Ви не ввели перевірочний код');
										else {
										   if ($captchaInput != $captchaWord)
											  $this->addErr('Помилка. Введіть код перевірки повторно');
											}
									}
						}
		 
      }

      if (!empty($_POST) && !$this->_err) {
         $body = "Ф.И.О.: $fio <br>E-Mail: $email <br>Телефон: $phone <br>Сообщение:<br>$body";
         $subject = "Сообщение с сайта http://" . $_SERVER['HTTP_HOST'] . '/';
         $mail = new Zend_Mail('utf-8');
         //$mail = new Zend_Mail('utf8');
         $mail->setBodyHtml($body);
         $mail->setFrom('webmaster@' . $_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST']);
         $mail->addTo($admin_email, 'Administrator');
         $mail->setSubject($subject);
         $mail->send();

         $this->tpl->assign('CONTENT', '{MESSAGE_SENT}');
      }

      if ($this->_err) {
         $this->viewErr();
      }
	  
	$kontakti = $this->db->fetchRow("SELECT `body` FROM `page` WHERE `href` = 'kontakti'");
	
	 include_once $_SERVER['DOCUMENT_ROOT'].'/g_config.php';
	 mysql_connect(_HOSTBASE, _USERBASE, _PASSWORDBASE) or die("Не можем подключиться к серверу MySQL...");
	 mysql_select_db(_DATABASE) or die("Не можем выбрать базу данных...");
	 mysql_set_charset(utf8);
	 
	 $prewie="";
	$q = mysql_query("SELECT preview FROM `page` WHERE `language` = '" . $this->lang . "' AND `level` = '375' ORDER BY `position`, `header` LIMIT 30");
	while($r=mysql_fetch_assoc($q)){
	$prewie=$prewie."$r[preview] <br /><br />";
	}
	mysql_close();
			$this->tpl->assign(array('KONTAKTI_BODY' => ($kontakti ) ? $kontakti['body'] : ''
		));

		$this->tpl->assign(array(
		'PAGE_PREVIEW' => $prewie
		));
     
		

     //kontakti

      if (empty($_POST) || $this->_err) {
         $captcha = new Zend_Captcha_Png(array(
                     'name' => 'cptch',
                     'wordLen' => 6,
                     'timeout' => 1800,
                 ));
         $captcha->setFont('./Zend/Captcha/Fonts/ANTIQUA.TTF');
         $captcha->setStartImage('./img/captcha.png');
         $id = $captcha->generate();

         $this->tpl->assign(
                 array(
                     'FIO' => $fio,
                     'EMAIL' => $email,
                     'PHONE' => $phone,
                     'BODY' => $body,
                     'CAPTCHA_ID' => $id
                 )
         );
         $this->tpl->parse('CONTENT', '.feedback');
      }


      return true;
   }

   public function sitemap() {
      $elems_h = $this->db->fetchAll('SELECT * FROM `page` WHERE `level` = "0" AND `menu` = "horisontal" AND `visibility` = "1" AND `language` = "' . $this->lang . '" ORDER BY `position`, `header`');

      $elems_v = $this->db->fetchAll('SELECT * FROM `page` WHERE `level` = "0" AND `menu` = "vertical" AND `visibility` = "1" AND `language` = "' . $this->lang . '" ORDER BY `position`, `header`');

      $map = '<ul>';
      $map .= '<li><a href="' . $this->basePath . '" title="{FIRST_WAY}">{FIRST_WAY}</a><ul>';

      $isCatalog = false;

      foreach ($elems_h as $elem) {
         $map .= "<li><a href='" . (($elem['type'] != 'link') ? $this->basePath : '') . $elem['href'] . "' title='" . $elem['header'] . "'>" . $elem['header'] . "</a>";

         if ($elem['href'] == '/catalog') {
            $isCatalog = true;
            $catalog = $this->db->fetchAll("SELECT `name`, `href`, `id` FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '0' ");
            if ($catalog) {
               $map .= '<ul>';
               foreach ($catalog as $cat) {
                  $map .= "<li><a href='" . $this->basePath . 'catalog/' . $cat['href'] . "' title='" . $cat['name'] . "'>" . $cat['name'] . "</a>";
                  $catalog1 = $this->db->fetchAll("SELECT `name`, `href` FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '$cat[id]' ");
                  if ($catalog1) {
                     $map .= '<ul>';
                     foreach ($catalog1 as $cat1) {
                        $map .= "<li><a href='" . $this->basePath . 'catalog/' . $cat['href'] . '/' . $cat1['href'] . "' title='" . $cat1['name'] . "'>" . $cat1['name'] . "</a></li>";
                     }
                     $map .= '</ul>';
                  }
                  $map .= "</li>";
               }
               $map .= '</ul>';
            }
         }

         if ($elem['type'] == 'section') {
            $sub = $this->db->fetchAll("SELECT * FROM `page` WHERE `level` = '" . $elem['id'] . "'");

            if ($sub) {
               $map .= "<ul>";

               foreach ($sub as $s) {

                  $url = ($s['type'] == 'link') ? $s['href'] : $this->basePath . $elem['href'] . '/' . $s['href'];
                  $map .= '<li><a href="' . $url . (($s['type'] == 'section') ? ('/') : ('')) . '" title="' . $s['header'] . '">' . $s['header'] . '</a></li>';
               }
               $map .= "</ul></li>";
            }
         }
         $map .= "</li>";
      }

      foreach ($elems_v as $elem) {
         $map .= "<li><a href='" . (($elem['type'] != 'link') ? $this->basePath : '') . $elem['href'] . "' title='" . $elem['header'] . "'>" . $elem['header'] . "</a>";

         if ($elem['type'] == 'section') {
            $sub = $this->db->fetchAll("SELECT * FROM `page` WHERE `level` = '" . $elem['id'] . "'");

            if ($sub) {
               $map .= "<ul>";

               foreach ($sub as $s) {

                  if (isset($s['href']) && isset($elem['href']) && isset($url)) {
                     $map .= '<li><a href="' . $url . (($s['type'] == 'section') ? ('/') : ('')) . '" title="' . $s['header'] . '">' . $s['header'] . '</a></li>';
                  }
               }
               $map .= "</ul></li>";
            }
         }
         $map .= "</li>";
      }

      $map .= "</ul></li></ul>";

      if (!$isCatalog) {
         $map .= "<ul><li><a href='/catalog' title='{CATTITLE}'>{CATTITLE}</a>";
         $catalog = $this->db->fetchAll("SELECT `name`, `href`, `id` FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '0' ");
         if ($catalog) {
            $map .= '<ul>';
            foreach ($catalog as $cat) {
               $map .= "<li><a href='" . $this->basePath . 'catalog/' . $cat['href'] . "' title='" . $cat['name'] . "'>" . $cat['name'] . "</a>";
               $catalog1 = $this->db->fetchAll("SELECT `href`, `name` FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '$cat[id]' ");
               if ($catalog1) {
                  $map .= '<ul>';
                  foreach ($catalog1 as $cat1) {
                     $map .= "<li><a href='" . $this->basePath . 'catalog/' . $cat['href'] . '/' . $cat1['href'] . "' title='" . $cat1['name'] . "'>" . $cat1['name'] . "</a></li>";
                  }
                  $map .= '</ul>';
               }
               $map .= "</li>";
            }
            $map .= '</ul>';
         }
      }


      $this->tpl->assign(array('CONTENT' => $map));

      return true;
   }

   public function search() { 
   
	   if ($_COOKIE[ua]!=1){
       $this->setMetaTags('Поиск по сайту');    	
       $this->setWay('Поиск по сайту');
		}
			else{
                $this->setMetaTags('Пошук по сайту');    	
				$this->setWay('Пошук по сайту');
				}
				
      $query = $this->getVar('term1');
      //echo $query;
      //echo '</br>'.urldecode($query);
      //if (!preg_match("/%/i", $query)) {
      //$query = $this->ascii2ansi($query);
      //echo '</br>'.$query;
$catalog_count = NULL;      
$pages_count = NULL;
$news_count = NULL;      
    //$query = iconv ('windows-1251', 'utf-8', $query);
//}
      //Это было взято с гавнокода, но оно реально работает )))
if(!iconv("UTF-8","UTF-8",$query)==$query)
    $query = iconv ('windows-1251', 'utf-8', $query);

  

$searchCount = 1;

		if ($_COOKIE[ua]!=1){
				  if ($query == 'Поиск товаров') {
					 $query = false;
				  }

				  if (!$query) {
					 $this->addErr('Не задана поисковая фраза');
				  } else {
					 $query = strip_tags(htmlspecialchars(trim($query)));

					 if (strlen($query) < 3) {
						$this->addErr('Поискова фраза не может быть короче 3-х символов');
					 }
				  }
				}
					else{
						 if ($query == 'Пошук товарів') {
						$query = false;
						}

						if (!$query) {
						 $this->addErr('Не заданий пошуковий вираз');
						} 
							else {
							 $query = strip_tags(htmlspecialchars(trim($query)));

							 if (strlen($query) < 3) {
							$this->addErr('Пошуковий вираз не може бути коротшим 3-х символів');
									}
								}
						}


	  
	  
      if ($this->_err) {
         $this->viewErr();
         return true;
      }

      //$pages_count = $this->getCountSearch($query, 'page');
      //$news_count = $this->getCountSearch($query, 'news');
      $catalog_count = $this->getCountSearch($query, $this->regionData['kod'].'_catalog');

      if ($pages_count === 'die' || $news_count === 'die' || $catalog_count === 'die') {
         return $this->error404();
      }
     
	 $ua=$this->getUa();
      $this->tpl->define_dynamic('_search', "$ua".'search.tpl');
      $this->tpl->define_dynamic('search', '_search');
      $this->tpl->define_dynamic('search_row', 'search');
     
      

      if (($pages_count + $news_count + $catalog_count) > 0) {
         if ($pages_count > 0) {
            $groups = $this->db->fetchAll("
                    SELECT `id`, `header`, `href`, `level`
                    FROM `page`
                    WHERE `type` = 'section' AND `language` = 'ua'
                ");

            $pages = $this->getResultSearch($query, 'page');

            if ($pages === 'die') {
               return $this->error404();
            }

            foreach ($pages as $page) {

               $this->tpl->assign(
                       array(
                           'SEARCH_COUNTER' => $searchCount,
                           'SEARCH_HREF' => $page['href'] != 'mainpage' ? $this->getHref($groups, $page['level']) . $page['href'] : $this->basePath,
                           'SEARCH_NAME' => stripslashes($page['header']),
                           'SEARCH_PREVIEW' => $this->getPreview($page['preview'], $query)
                       )
               );
               $searchCount++;
               $this->tpl->parse('SEARCH_ROW', '.search_row');
            }
         }

         if ($news_count > 0) {
            $news = $this->getResultSearch($query, 'news');

            if ($news === 'die') {
               return $this->error404();
            }
            
            

            foreach ($news as $new) {
               $this->tpl->assign(
                       array(
                           'SEARCH_COUNTER' => $searchCount,
                           'SEARCH_HREF' => '/news/' . $new['href'],
                           'SEARCH_NAME' => stripslashes($new['header']),
                           'SEARCH_PREVIEW' => $this->getPreview($new['preview'], $query)
                       )
               );

               $this->tpl->parse('SEARCH_ROW', '.search_row');
            }
            $searchCount++;
         }
         
         
         

         if ($catalog_count > 0) {
            $catalog = $this->getResultSearch($query, $this->regionData['kod'].'_catalog'); 

            if ($catalog === 'die') {
               return $this->error404();
            }

            foreach ($catalog as $cat) {

               $link = '';

               if (($dataTree = $this->dataTreeManager($cat['id']))) {
                  $dataTree = $dataTree['links'];
               }
if (!isset($cat['header'])) $cat['header'] = NULL;
               $catName = stripslashes($cat['header']);
               $catName = str_replace('  ', ' ', $catName);
			
			if($_COOKIE["ua"]!=1){
				   if (empty($catName) || $catName == ' ') {
					  $catName = stripslashes($cat['name']);
				   }
               }
			   else{
				   if (empty($catName) || $catName == ' ') {
					  $catName = stripslashes($cat['name_ua']);
				   }
			   }
               $catName = $this->getPreview($catName);
               if (!isset($cat['preview'])) { $cat['preview'] = NULL; }
               $this->tpl->assign(
                       array(
                           'SEARCH_COUNTER' => $searchCount,
                           'SEARCH_HREF' => '/'.$this->regionData['url'].'/catalog/' . $dataTree,
                           'SEARCH_NAME' => $catName,
                           'SEARCH_PREVIEW' => $this->getPreview($cat['preview'], $query)
                       )
               );
               $searchCount++;
               $this->tpl->parse('SEARCH_ROW', '.search_row');
            }
         }

         $this->tpl->assign(
                 array(
                     'SEARCH_QUERY' => $query,
                     'SEARCH_COUNT' => $pages_count + $news_count + $catalog_count
                 )
         );

         $this->tpl->parse('CONTENT', '.search');
      } else {
         $this->tpl->parse('SEARCH_ROW', 'null');

         $this->tpl->assign(
                 array(
                     'SEARCH_QUERY' => $query,
                     'SEARCH_COUNT' => $pages_count + $news_count
                 )
         );

         $this->tpl->parse('CONTENT', '.search');
      }



      return true;
   }

   public function enter() {
   
   if ($_COOKIE[ua]!=1){
       $this->setMetaTags('Авторизация');    	
       $this->setWay('Авторизация');
		}
			else{
                $this->setMetaTags('Авторизація');    	
				$this->setWay('Авторизація');
				}
   
   $ua=$this->getUa();
      $this->tpl->define_dynamic('_enter', "$ua".'users.tpl');
      $this->tpl->define_dynamic('enter', '_enter');

      $email = $this->getVar('login');
      $password = $this->getVar('password');

      if (!empty($_POST)) {
         if (null === $email) {
            $this->addErr('Введите Логин');
            $this->_err = 'Введите Логин<br>';
         }

         if (null === $password) {
            $this->addErr('Введите Пароль');
            $this->_err = 'Введите Пароль<br>';
         }
         
      }
      if ((empty($email) OR  null === $email)  AND isset($_POST['go_admin']))
          $this->addErr('Введите Логин!!!');
      if ((empty($password) OR null === $password) AND isset($_POST['go_admin'])) 
            $this->addErr('Введите Пароль!!!');

        //echo $this->_err; die;
        
      if (!empty($_POST) && empty($this->_err)) {
          
         $authAdapter = new Zend_Auth_Adapter_DbTable($this->db);
         $authAdapter->setTableName('users');
         $authAdapter->setIdentityColumn('login');
         $authAdapter->setCredentialColumn('pass');

         $authAdapter->setIdentity($email);
         $authAdapter->setCredential(crypt($password, 'tEXFVrqY'));

         $auth = Zend_Auth::getInstance();
         $result = $auth->authenticate($authAdapter);

         if ($result->isValid()) {
            $data = $authAdapter->getResultRowObject(null, 'pass');
            $auth->getStorage()->write($data);

            $this->viewMessage('Здравствуйте, ' . Zend_Auth::getInstance()->getIdentity()->fio . '!<meta http-equiv="refresh" content="1;URL=' . $this->basePath . '">');
         } else {
            $this->addErr('Логин или Пароль введен неверно');
         }
      }

      if ($this->_err) {
         $this->viewErr();
         
      }

      if (empty($_POST) || $this->_err) {
          
         $this->tpl->parse('CONTENT', '.enter');
      }

      return true;
   }

   public function logout() {
   
      if ($_COOKIE[ua]!=1){
       $this->setMetaTags('Выход');    	
       $this->setWay('Выход');
		}
			else{
                $this->setMetaTags('Вихід');    	
				$this->setWay('Вихід');
				}
	  
	  if ($_COOKIE[ua]!=1){
		  $this->tpl->assign(
				  array(
					  'TITLE' => 'Завершение сеанса пользователя',
					  'KEYWORDS' => '',
					  'DESCRIPTION' => '',
					  'HEADER' => 'Завершение сеанса пользователя'
				  )
		  );
	  }
			else{
            $this->tpl->assign(
              array(
                  'TITLE' => 'Завершення сеансу користувача',
                  'KEYWORDS' => '',
                  'DESCRIPTION' => '',
                  'HEADER' => 'Завершення сеансу користувача'
              )
      );
				}

      Zend_Auth::getInstance()->clearIdentity();
      $this->viewMessage('<meta http-equiv="refresh" content="1;URL=' . $this->basePath . '">');

      return true;
   }

   private function getCountSearch($query = null, $table = null) {
      if (null === $query || null === $table) {
         return 'die';
      }
//$query =  iconv("windows-1251", "UTF-8", $query);
//echo $query;
      $select = $this->db->select();
      $select->from(
              array($table), array('c' => 'COUNT(`id`)')
      );

      if ($table == 'page') {
         $select->where("type <> 'link'");
      }

      $select->where("id LIKE ('%$query%')");
      
      //$select->orWhere("preview LIKE ('%$query%')");
      //$select->orWhere("body LIKE ('%$query%')");
      if ($table == $this->regionData['kod'].'_catalog') {
	  
	  if($_COOKIE["ua"]!=1){
         $select->orWhere("`name` LIKE ('%$query%')");   
			}
			else{
			$select->orWhere("`name_ua` LIKE ('%$query%')"); 
			}
         //$select->where("type <> 'link'");
         $select->where("type <> 'section'");       
         //$select->orWhere("artikul LIKE ('%$query%')");       
         //$select->orWhere("header LIKE ('%$query%')");       
         //$select->orWhere("title LIKE ('%$query%')");       
      }
//      echo $select;
      $stmt = $this->db->query($select);
      $result = $stmt->fetchAll();

      return $result[0]['c'];
   }

   private function getResultSearch($query = null, $table = null) {     
      if (null === $query || null === $table) {
         return 'die';
      }

      $select = $this->db->select();
      $select->from(
              array($table)
      );

      if ($table == 'page') {
         $select->where("type <> 'link'");
      }
      
      
$select->where("id LIKE ('%$query%')");
      //$select->where("header LIKE ('%$query%')");
      if ($table == $this->regionData['kod'].'_catalog') {
	  if($_COOKIE["ua"]!=1){
         $select->orWhere("name LIKE ('%$query%')");
		 }
		 else{
		 $select->orWhere("name_ua LIKE ('%$query%')");
		 }
         $select->orWhere("artikul LIKE('%$query%')");
      }
      //$select->orWhere("preview LIKE ('%$query%')");
      //$select->orWhere("body LIKE ('%$query%')");
      
      $stmt = $this->db->query($select);
      
      $result = $stmt->fetchAll();      

      return $result;
   }

   private function getHref($groups = null, $level = null) {
      if (null === $groups || null === $level) {
         return '';
      }

      $url = '/';

      while ($level != 0) {
         foreach ($groups as $group) {
            if ($group['id'] == $level) {
               $url = '/' . $group['href'] . $url;
               $level = $group['level'];
            }
         }
      }

      return $this->basePath . substr($url, 1);
   }

   private function getPreview($preview = null, $query = null) {   
      $ret = str_ireplace($query, '<span class="light">' . $query . '</span>', $preview);
      return $ret;
   }

}