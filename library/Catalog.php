<?php

require_once PATH . 'library/Abstract.php';

require_once PATH . 'library/Interface.php';

class Catalog extends Main_Abstract implements Main_Interface {    
	
	private $activePage = false;
	private $activeHeader = false;	
	private $regionKod = '';
	private $catalogOptions = array();
	
    public function factory() {
        return true;
    }      
        
    /*
    		Функкция main
    	В этой функции происходит загрузка метатегов, определение активного раздела, вывод разделов, подразделов, списка товаров
    	в зависимости от выбранного url		
    	
    */   
    
    public function main() {    
	
	$_SESSION['region_url']=$_SERVER[REQUEST_URI];
	//echo $_SESSION['region_url'];
		
    	//die;
        $pageLevel = NULL;
    	$this->catalogOptions = $this->getConfig('catalog');
    	 		
  		if (count($this->url) > 0) {  			
  			$level = 0;  			
  			
  			$url = end($this->url);  			 			
  			  			
  			// Если url заканчивается словом catalog - это корневой уровень каралога.
  			// Если нет - загружаем активную страницу
  			//die;
  			if ($url == 'catalog') {
  				
                                //var_dump($regionData);die;
  				// Для корня каталога ищется метатеги в таблице page если есть ссылка в горизонтальном меню
  				
  				$this->activeHeader = $this->db->fetchRow("SELECT * FROM `meta_tags` WHERE `href` = 'catalog'" ); 								

  				if ($this->activeHeader) {  					
  					
  					$this->setMetaTags($this->activeHeader);
  				} else {  					
  					$meta['title'] = 'Каталог';
  					$meta['keywords'] = $meta['title'];  					
  					$meta['description'] = $meta['title'];
  					$meta['header'] = $meta['title'].' '.$this->getAdminAdd('section_meta_tags', 0);
  					$meta['title'] = $meta['title'];  					
  					$this->setMetaTags($meta);
 				}
  				
  				$metaBody = '';
  				
  				if (isset($this->activeHeader['body'])) {
  					$metaBody = $this->activeHeader['body'];
  				}
  				
  				$this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'CATALOG_SECTION_BODY'=>$metaBody));
  				
                                
                                
  			} elseif($this->loadActivePage($url))  {  				  			
  				if ($this->activePage) { 
  					
  					$level = $this->activePage['id'];
  				}
  			} else { 
				return $this->no_data();
  			} 		
  			
                       
                        //$this->loadSubSectionGoods($pageLevel);
                        
                        $this->loadPagesItems($pageLevel);	// Список страниц	  					
//                        

                        
			//$this->setMetaTags($this->activePage);
			
			if ((isset($this->activePage['type']) && $this->activePage['type'] == 'section') || $level == 0) {  							
  				if (is_numeric(($pageLevel = $this->loadSectionsItems($level)))) { // Загрузка разделов. Если в разделе обнаружатся страницы - выводим список страниц
  					//$this->loadPagesItems($pageLevel);	// Список страниц	  					
                                        $this->loadSubSectionGoods($pageLevel);
  				} 
  				
  			} elseif ($this->activePage['type'] == 'page') { // Подробная информация						  				        		
  				$this->loadDetail();
  			}
  			
  			return true;	
  		} 
  		
  		return false;
    	
    } 
    
    // loadActivePage() - Определяет активную страницу по url.
        
    protected function loadActivePage($href) {  

 if (count($this->url)>2 && $this->url[2]==$href){
		return ($this->activePage = $this->db->fetchRow("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `href` = '$href' AND level!='0'")); 
				}
					else{
				    	return ($this->activePage = $this->db->fetchRow("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `href` = '$href'")); 	
					}
    }

    // loadSectionsItems() - Выводит список разделов и подразделов. Если в списке разделов окажутся страницы - вернет уровень.  
    
    protected function loadSectionsItems($id) {    	
    	if (!isset($this->activePage['header'])){ $this->activePage['header']=''; }	
    	// Постраничный навигатор
    	if (!isset($this->activePage['name'])) $this->activePage['name']='';
    	$start = 0;
        $navbar = $navTop = $navBot = '';
        $pagePagen = 1;    	
        $navTop = '';
        $navBot = '';
        $pagenHref = 'catalog';
        
        
        $catalogPageLength =  $this->settings['num_catalog_section'];
        
        $visibleQuery = '';
        
        if (!$this->_isAdmin()) {
        	$visibleQuery = " AND `visibility` = '1'";
        }
        //var_dump($this->activePage); die;
        $getMeta = $this->db->fetchRow("SELECT * FROM `section_info` WHERE `name`='".$this->activePage['name']."'");
        
        $meta['title']=(!empty($getMeta['title'])?$getMeta['title']:$this->activePage['name']);
        $meta['keywords']=(!empty($getMeta['keywords'])?$getMeta['keywords']:$this->activePage['name']);
        $meta['description']=(!empty($getMeta['description'])?$getMeta['description']:$this->activePage['name']);
        $meta['header']=$this->activePage['name'];
        $this->setMetaTags($meta);
        
        $count = $this->db->fetchOne("SELECT COUNT(`id`) FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '$id' $visibleQuery");
        
        if (($dataTree = $this->dataTreeManager($id, array('fields'=>'`id`, `href`, `name`, `name_ua`, `level`, `type`')))) {  
                       
            $slg2 = '';
            
            $slg2 = '';
        
		if ($_COOKIE[ua]!=1){
            if (isset($dataTree[0]['name'])) {                
                $slg2 .=  $dataTree[0]['name'];
                
                if (isset($dataTree[1]['name'])) {
                   $slg2 .= ' '.$dataTree[1]['name'] ;                    
                }
                
                $slg2 .= ' {TOP_LINE_TEXT1} ' .$dataTree[0]['name']. ' {TOP_LINE_TEXT2} ';
            }
		}
        else{
            if (isset($dataTree[0]['name_ua'])) {                
                $slg2 .=  $dataTree[0]['name_ua'];
                
                if (isset($dataTree[1]['name_ua'])) {
                   $slg2 .= ' '.$dataTree[1]['name_ua'] ;                    
                }
                
                $slg2 .= ' {TOP_LINE_TEXT1} ' .$dataTree[0]['name_ua']. ' {TOP_LINE_TEXT2} ';
            }
		}			
            if (!empty($slg2)) {
                $this->tpl->assign('SLG2', $slg2);
                //$this->setMetaTags($slg2);
            }
           
            $pagenHref .= '/'. $dataTree['links'];
           	
           	// Загрузка паффиндера
           	
           	$dataTreeLength = count($dataTree['names']);
           //	$this->setWay('Каталог', '/catalog/');
           	if ($dataTreeLength > 0) {       
           		//var_dump($dataTree);    	
           		for ($i = 0; $i < $dataTreeLength; $i++) {
           			$url = '/catalog';
           			if ($i != ($dataTreeLength -1)) {           				
           				$url .= '/'. $dataTree['linksArr'][$i];
           			} else {
           				$url = null;
           			}
           			
           			$this->setWay($dataTree['names'][$i], $url);
           		}
           	}
        }	
        
        
        
        if ($count > 0) {
        	if ($count > $catalogPageLength) {
            	if (isset($this->getParam['page'])) {
                	$pagePagen = (int) $this->getParam['page'];
                    $start = $catalogPageLength * $pagePagen - $catalogPageLength;
                            
                    if ($start > $count) {
                    	$start = 0;
                    }
                }
                
                $pagenParams = '';
                
                if (!empty($brandName)) { 
                	$pagenParams = "?brand=$brandName";	
                }
                
                $navbar = $this->loadPaginator((int) ceil($count/$catalogPageLength), (int) $pagePagen, $this->basePath.$pagenHref.$pagenParams );
                   
                if ($navbar) {
                	$navTop = '<div class="pager2 pager2_center">' . $navbar . '</div>';
                    $navBot = $navTop ;
                }
           }
        } 
        
       

    	$sql = "SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '$id' $visibleQuery ORDER BY `position` ,`name` LIMIT ".$start.", ".$this->settings['num_catalog_section'];    		
    	
        $sectionItems = $this->db->fetchAll($sql);
            
    	if (isset($sectionItems[0]['type']) && $sectionItems[0]['type'] == 'page') {    		
			return $sectionItems[0]['level'];
		}
    		
    		$ua=$this->getUa();
			
		$this->tpl->define_dynamic('_catalog_section_list_body', "$ua". 'catalog.tpl');
		$this->tpl->define_dynamic('catalog_section_list_body', '_catalog_section_list_body');
		$this->tpl->define_dynamic('catalog_section_list_empty', 'catalog_section_list_body');
		$this->tpl->define_dynamic('catalog_section_list', 'catalog_section_list_body');
		$this->tpl->define_dynamic('catalog_section_list_image', 'catalog_section_list_body');
		
                $this->tpl->define_dynamic('catalog_section_list1', 'catalog_section_list_body');
		$this->tpl->define_dynamic('catalog_section_list_image1', 'catalog_section_list_body');
		
		
		
		if (count($sectionItems) > 0) {
			
			if ($this->activePage) {
			
				$this->activePage['header'] .= '<p> '. $this->getAdminAdd('catsection', $this->activePage['id']).'</p>';
				//$this->setMetaTags($this->activePage);
			}	
						
			$this->tpl->parse('CATALOG_SECTION_LIST_EMPTY', 'null');
                        $this->tpl->parse('CATALOG_SECTION_LIST_EMPTY1', 'null');
			$counSectionItes = count($sectionItems);
                        $sectionItemsI = 0;
			foreach ($sectionItems as $sectionItem) {					
				$sectionItemsI++;	
				$url = '/'. $pagenHref . '/'.$sectionItem['href'];
				
			if (isset($sectionItem['body'])) {	
			    $body = stripslashes($sectionItem['body']);
                        } else { $body = ''; }
                
                //$name = (!empty($sectionItem['header1']) ? $sectionItem['header1'] : $sectionItem['name']);
                  
                
                            
                $name = $sectionItem['name'];
                $name_ua = $sectionItem['name_ua'];
                $sectionInfo = $this->db->fetchRow("SELECT * FROM `section_info` WHERE `name`='$name'");
                //Добавим url региона в начало url ссылки
                $url = '/' . $this->regionData['url'] . $url;
				
					if ($_COOKIE[ua]!=1){
				$this->tpl->assign(array(
								'PROBA_CATALOG'     =>  (''),
					'CATALOG_SECTION_LIST_ADM'=>'',//$this->getAdminEdit('catsection', $sectionItem['id']),
					'CATALOG_SECTION_LIST_NAME'=>$name,
                                        'CATALOG_SECTION_LIST_PIC'=>(!empty($sectionInfo['pic']) ? '<img src="/img/catalog/section/small/'.$sectionItem['pic'].'" width="84" height="73" alt="'.$sectionItem['pic_alt'].'" title="'.$sectionItem['pic_title'].'" class="float_left" />' : ''),
                                        'CATALOG_SECTION_LIST_HREF'=>$url,
                                        'CATALOG_SECTION_LIST_PREVIEW'=>(!empty($sectionInfo['short_text']) ? $sectionInfo['short_text'] : '')
				));
				
                                $this->tpl->assign(array(
												'PROBA_CATALOG'     =>  (''),
					'CATALOG_SECTION_LIST_ADM1'=>$this->getAdminEdit('catsection', $sectionItem['id']),
					'CATALOG_SECTION_LIST_NAME1'=>$name,
                                        'CATALOG_SECTION_LIST_NAME11'=>($counSectionItes != $sectionItemsI ? '&nbsp;<font style="color:#000;">|</font>' : ''),
    	            'CATALOG_SECTION_LIST_PIC1'=>(isset($sectionItem['pic']) ? $sectionItem['pic'] : ''),
                    'CATALOG_SECTION_LIST_PIC_ALT1'=>(!empty($sectionItem['pic_alt']) ? $sectionItem['pic_alt'] :  $name),
                    'CATALOG_SECTION_LIST_PIC_TITLE1'=>(!empty($sectionItem['pic_title']) ? $sectionItem['pic_title'] :  $name),
                    'CATALOG_SECTION_LIST_HREF1'=>$url,
					'CATALOG_SECTION_LIST_PREVIEW1'=>(!empty($sectionItem['preview']) ? $sectionItem['preview'] : '')
						
				));
            }
				else{
						$this->tpl->assign(array(
										'PROBA_CATALOG'     =>  (''),
							'CATALOG_SECTION_LIST_ADM'=>'',//$this->getAdminEdit('catsection', $sectionItem['id']),
							'CATALOG_SECTION_LIST_NAME'=>$name_ua,
												'CATALOG_SECTION_LIST_PIC'=>(!empty($sectionInfo['pic']) ? '<img src="/img/catalog/section/small/'.$sectionItem['pic'].'" width="84" height="73" alt="'.$sectionItem['pic_alt'].'" title="'.$sectionItem['pic_title'].'" class="float_left" />' : ''),
												'CATALOG_SECTION_LIST_HREF'=>$url,
												'CATALOG_SECTION_LIST_PREVIEW'=>(!empty($sectionInfo['short_text']) ? $sectionInfo['short_text'] : '')
						));
						
										$this->tpl->assign(array(
														'PROBA_CATALOG'     =>  (''),
							'CATALOG_SECTION_LIST_ADM1'=>$this->getAdminEdit('catsection', $sectionItem['id']),
							'CATALOG_SECTION_LIST_NAME1'=>$name_ua,
												'CATALOG_SECTION_LIST_NAME11'=>($counSectionItes != $sectionItemsI ? '&nbsp;<font style="color:#000;">|</font>' : ''),
							'CATALOG_SECTION_LIST_PIC1'=>(isset($sectionItem['pic']) ? $sectionItem['pic'] : ''),
							'CATALOG_SECTION_LIST_PIC_ALT1'=>(!empty($sectionItem['pic_alt']) ? $sectionItem['pic_alt'] :  $name_ua),
							'CATALOG_SECTION_LIST_PIC_TITLE1'=>(!empty($sectionItem['pic_title']) ? $sectionItem['pic_title'] :  $name_ua),
							'CATALOG_SECTION_LIST_HREF1'=>$url,
							'CATALOG_SECTION_LIST_PREVIEW1'=>(!empty($sectionItem['preview']) ? $sectionItem['preview'] : '')
								
						));
					}			
                                
				if (isset($sectionItem['pic']) && is_file($_SERVER['DOCUMENT_ROOT'].'/img/catalog/small/'.$sectionItem['pic'])) {
                       $this->tpl->parse('CATALOG_SECTION_LIST_IMAGE', 'catalog_section_list_image');
                       $this->tpl->parse('CATALOG_SECTION_LIST_IMAGE1', 'catalog_section_list_image1');
                } else {
                   	$this->tpl->parse('CATALOG_SECTION_LIST_IMAGE', 'null');
                        $this->tpl->parse('CATALOG_SECTION_LIST_IMAGE1', 'null');
                   	$pic = '';
                }
				$this->tpl->parse('CATALOG_SECTION_LIST', '.catalog_section_list');
                                $this->tpl->parse('CATALOG_SECTION_LIST1', '.catalog_section_list1');
			}
			$this->tpl->assign(array( 'PROBA_CATALOG'     =>  (''), 'TOP_NAV_BAR'=>$navTop, 'BOTT_NAV_BAR'=>$navBot));			
		} else { 
			
                        if (!$this->activePage) {
                            $this->activeHeader['header'] .= '<p> '. $this->getAdminAdd('catsection', 0).'</p>';
                            //$this->setMetaTags($this->activeHeader);
                        } else {
                            $this->activePage['header'] .= '<p> '. $this->getAdminAdd('catsection', $this->activePage['id']).$this->getAdminAdd('catpage', $this->activePage['id']).'</p>';
                            //$this->setMetaTags($this->activePage);
                        }
			
			$this->tpl->parse('CATALOG_SECTION_LIST', 'null');
                        $this->tpl->parse('CATALOG_SECTION_LIST1', 'null');
			$this->tpl->parse('CATALOG_SECTION_LIST_BODY', '.catalog_section_list_empty');
			$this->tpl->assign(array( 'PROBA_CATALOG'     =>  (''), 'TOP_NAV_BAR'=>'', 'BOTT_NAV_BAR'=>''));			
		}
		
		
		$this->tpl->parse('CONTENT', '.catalog_section_list_body');		
    	
    	return true;
    }
    //Список товаров подраздела
    protected function loadPagesItems ($level) {
    	
    	if (!is_numeric($level)) {
            return false;
    	}
    	
    	if ($level <= 0) {
            return false;
    	}    	
    	
    	$this->activePage['header'] .= '<p> '. $this->getAdminAdd('catpage', $this->activePage['id']).'</p>';
	$this->setMetaTags($this->activePage);
    	
		$ua=$this->getUa();
		
    	$this->tpl->define_dynamic('_catalog', "$ua". 'catalog.tpl');
    	$this->tpl->define_dynamic('catalog', '_catalog');
    	
    	$this->tpl->define_dynamic('catalog_items_body', 'catalog');
    	$this->tpl->define_dynamic('catalog_brands_items', 'catalog');
    	$this->tpl->define_dynamic('catalog_items', 'catalog_items_body');
    	
    	$this->tpl->define_dynamic('catalog_items_empty', 'catalog');
        
        // Бренды
        
        $in = "AND `level` = '".$this->activePage['id']."'";
        /*
        if (($levels = $this->db->fetchAll("SELECT `id` FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '".$this->activePage['level']."'"))) {
            $counter = 0;
            $in = '';
            foreach ($levels as $lev) {
                $in .= ($counter > 0 ? ', ': '')."'$lev[id]'";
                $counter ++;
            }
            $in = " AND `level` IN ($in)";
        }*/
                  
    	//$brandsList = $this->db->fetchAll("SELECT DISTINCT `proizvoditel` FROM `".$this->regionData['kod']."_catalog`  WHERE `type` = 'page' $in ");
    	$brandName = '';
    		
    	
    	if (count($brandsList) > 0) {
            
            if (isset($_GET['brand']) && !empty($_GET['brand']) && is_string($_GET['brand'])) {
                $brandName = $_GET['brand'];
                $brandName = urldecode($brandName);
                $brandName = mysql_escape_string($brandName);
            	$brandName= preg_replace('/insert|delete|drop|truncate|select|union|update|\/+|\\|\;/i', '', $brandName);    			
            }
            
            $brandUrlParam = '?';
            if (isset($_GET['page']) && is_numeric($_GET['page'])) {                
    		$brandUrlParam = "?page=$_GET[page]&";
            }
    		
            $brand = '<span class="plsh"><div><a href="'.$brandUrlParam.'brand=все">Все</a></div></span>';
            if ($brandName == 'все' || empty($brandName)) {
    		$brandName = '';
    		$brand = "<span class='plsh'><div><div class='left'></div><div class='center'>Все</div><div class='right'></div> </div></span>";
            }
    				
            $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'CATALOG_BRAND'=>$brand));
            $this->tpl->parse('CATALOG_BRANDS_ITEMS', '.catalog_brands_items'); 				
    		
            foreach ($brandsList as $brandsItem) {
                if (!empty($brandsItem['proizvoditel'])) { 
    				
                    $brand = '<a href="'.$brandUrlParam.'brand='.$brandsItem['proizvoditel'].'">'.$brandsItem['proizvoditel'].'</a>';
                    if ($brandsItem['proizvoditel'] == $brandName) {
                        $brand = "<span class='plsh'><div><div class='left'></div><div class='center'>$brandName</div><div class='right'></div> </div> </span>";
                    } else {
                        $brand = "<span class='plsh'><div>$brand </div></span>";
                    }
                    $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'CATALOG_BRAND'=>$brand));  
                    $this->tpl->parse('CATALOG_BRANDS_ITEMS', '.catalog_brands_items'); 				
    		}
            }
    	}
    	
    	$brandSql = '';
    	if (!empty($brandName)) {
    		$brandSql = " AND `proizvoditel` = '$brandName'";
    	}
    	// Постраничная навигация
    	
    	$visibleQuery = '';
        
        if (!$this->_isAdmin()) {
            $visibleQuery = " AND `visibility` = '1'";
        }
    	
    	$start = 0;
        $navbar = $navTop = $navBot = '';
        $pagePagen = 1;
    
        $catalogPageLength = $this->settings['num_catalog'];
        
        $count = $this->db->fetchOne("SELECT COUNT(`id`) FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '$level' $visibleQuery AND `type` = 'page' $brandSql");
        
        
        if ($count > 0) {
            if ($count > $catalogPageLength) {
            	if (isset($this->getParam['page'])) {
                    $pagePagen = (int) $this->getParam['page'];
                    $start = $catalogPageLength * $pagePagen - $catalogPageLength;
                            
                    if ($start > $count) {
                    	$start = 0;
                    }
                }
                
                $pagenParams = '';
                
                if (!empty($brandName)) { 
                	$pagenParams = "?brand=$brandName";	
                }
                
                $pagenHref = 'catalog';
                if (isset($this->activePage['href1'])) {
                	$pagenHref .= '/'. $this->activePage['href1'];
                }
                $pagenHref .= '/'. $this->activePage['href'];
                
                $navbar = $this->loadPaginator((int) ceil($count/$catalogPageLength), (int) $pagePagen, $this->basePath.$pagenHref.$pagenParams );
                        
                if ($navbar) {
                	$navTop = '<div class="pager_right">' . $navbar . '</div>';
                    $navBot = $navTop ;
                }
           }
        } else {
        	//$this->tpl->assign('CONTENT', ($index ? '' : $this->getAdminAdd('news')).'{EMPTY_SECTION}');
        //  return true;
        }
        
    
        $sql = "SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '$level' $visibleQuery AND `type` = 'page'  $brandSql ORDER BY `position`, `name` LIMIT ".$start.", ".$catalogPageLength;   
     	
    	$items = $this->db->fetchAll($sql);     	
    	$itemsLength = count($items);
    	
    	if ($itemsLength > 0) {
    		$this->tpl->parse('CATALOG_ITEMS_EMPTY', 'null');
    	}
    	
    	$catItem1 = '';
        $catItem2 = '';
        $catItem3 = '';
        $catItem4 = '';
        $catItem5 = '';
        $catalogItem = '';
        			
        $i = 0;
        $f = 0;
        $s = 0;
		
        if ($itemsLength > 0) {
			
            while (true) { 
    			        				
                if (isset($items[$i])) {
                    $itemUrl = '';	
                    $groupName = '-';
                    $groupNameUrl = '#';
   				
                    if (($parenSectionInfo = $this->dataTreeManager($items[$i]['id']))) {
        		$itemUrl = $parenSectionInfo['links'];	
        		$groupNameIndex = (count($parenSectionInfo['names']) - 3);
        			
        		if (isset($parenSectionInfo['names'][$groupNameIndex])) {        				
                            $groupNameUrl = '/catalog';
        				
                            for ($f1 = 0; $f1 <= $groupNameIndex; ++$f1) {
        			$groupNameUrl .= '/'. $parenSectionInfo['linksArr'][$f1];
                            }	
                            $groupName = $parenSectionInfo['names'][$groupNameIndex];
        		}
                    }
        																
        																
                    $link = (($items[$i]['type'] != 'link')?('/catalog'):('')).$parenSectionInfo['links'];                            
                    //$types = adminCatalogEdit($items[$i]['id']);
                    $name = $items[$i]['name'];                                               
                    //$url = '/catalog/'.$parenSectionInfo[0]['href'];
    						
                    if (!empty($types)) {
    			$types = "$types ";
                    }
    						
                    
                    $liLastClass = '';
    						
                    if ($f == 2) {
    			$liLastClass = 'last';
                    }
    			
    			
                    $imgSrc = 'nophoto_s.jpg';    			
                    $imgAlt = 'Нет фото';
                    $imgTitle = $imgAlt;
    			
                    if (is_file($_SERVER['DOCUMENT_ROOT'].'/img/catalog/small/'.$items[$i]['pic'])) {
    			$imgSrc = $items[$i]['pic'];
    			$imgAlt = $items[$i]['pic_alt'];
    			$imgTitle = $items[$i]['pic_title'];
    		
  			if (empty($imgAlt)) {
                            $imgAlt = $items[$i]['name'];    						
    			}
    					
    			if (empty($imgTitle)) {    						
                            $imgTitle = $items[$i]['name'];
    			}
                    } elseif (is_file($_SERVER['DOCUMENT_ROOT'].'/img/catalog/small/'.$items[$i]['artikul'].'.jpg')) {
    			$imgSrc = $items[$i]['artikul'].'.jpg';
    			$imgAlt = $items[$i]['pic_alt'];
    			$imgTitle = $items[$i]['pic_title'];
    		
  			if (empty($imgAlt)) {
                            $imgAlt = $items[$i]['name'];    						
    			}
    					
    			if (empty($imgTitle)) {    						
                            $imgTitle = $items[$i]['name'];
    			}
                        
                    }
    			
                    $imgType = '';
    				
                    if (isset($items[$i]['status']) && $items[$i]['status'] == 'hit') {
    			$imgType = '<img src="/img/hit.png" class="png" width="173" height="37" alt="" />';
                    }
    			
                    if (isset($items[$i]['status']) && $items[$i]['status'] == 'action') {
    			$imgType = '<img src="/img/akcia.png" class="png" width="173" height="37" alt="" />';
                    }	
    			
                    if (isset($items[$i]['status']) && $items[$i]['status'] == 'new') {
    			$imgType = '<img src="/img/new.png" class="png" width="173" height="37" alt="" />';
                    }    			    			  
    				
                    $adminButton = $this->getAdminEdit('catpage', $items[$i]['id']);
                    
    				
                    if (empty($items[$i]['preview'])) {
    			$items[$i]['preview'] = '&nbsp;';
                    }
    					
                    $catItem1 .= '<li><div class="img">'.$imgType.'<a href="/catalog/'.$itemUrl.'"><img src="/img/catalog/small/'.$imgSrc .'" width="200" height="180" alt="'.$imgAlt.'" title="'.$imgTitle.'"/></a></div><p><span class="left"></span><span class="right"><a href="/catalog/'.$itemUrl.'" title="'.$items[$i]['name'].'">'.$items[$i]['name'].'</a></span>'.$adminButton.'</p></li> ';
                    $catItem2 .= '<li class="li-group"><p><span class="left">Группа:</span><span class="right"><a class="black" href="'.$groupNameUrl.'">'.$groupName.'</a></span></p></li>';
                    $catItem3 .= '<li><p class="desc">'.stripslashes($items[$i]['preview']).'</p></li>';    			
                    $catItem4 .= '<li>';
                    $catItem5 .= '<li>';

                    if ($this->getCatOption('isStore')) {
    			if (isset($items[$i]['cost_old']) ) {
                            if ($items[$i]['cost_old'] > $items[$i]['cost']) {
    				$econom = ($items[$i]['cost_old'] - $items[$i]['cost']);
                                    if ($econom > 0) {
    					$catItem4 .= '<p><span>Розничная цена:</span><span class="price">'. number_format($items[$i]['cost_old'], 2, ',', " ").' грн.</span></p>
     								<p><span>Экономия:</span><span class="eco">'.number_format($econom, 2, ',', " ").' грн.</span></p>';
                                    }
    				}
                            }		
    			
    			//$buyButton = '<p class="right"><input type="button" onclick="addToBasket(\'goods_'.$items[$i]['id'].'\', 1);" value="Заказать" class="button" /></p>';
    			$buyButton = '<div><div style="float: left;"> <input type="text" class="goods-length" value="1" id="goods_'.$items[$i]['id'].'" /> шт. </div> <div style="text-align: right;"> <a href="#" class="buy" onclick="addToBasket(\'goods_'.$items[$i]['id'].'\', $(\'goods_'.$items[$i]['id'].'\').value);"> Заказать</a></div></div>';
	    			
    			if (!$this->isBuyButtomType($items[$i]['id'])) {
                            $buyButton = '<p class="right"><img src="/img/ok.gif" width="13" height="12" alt="" /><a href="/order">Оформить заказ</a></p>';
    			}
                        
                        if (empty($items[$i]['cost'])) {
                            $items[$i]['cost'] = '0';
                        }
     			
                        $catItem5.='<p><span>Наша цена:</span><span class="newprice">'. number_format($items[$i]['cost'], 2, ',', " ").' грн.</span></p><span id="by_button_goods_'.$items[$i]['id'].'">'.$buyButton.'</span>';    				
                    }
                    $catItem4 .= "&nbsp;</li>";
                    $catItem5 .= "&nbsp;</li>";
    						
                } else {
                    $liLastClass = '';
    						
                    if ($f == 2) {
                        $liLastClass = '';
                    }
                }
        
                $f++;
                $i++;					    					
    			        
                if ($f == 3 ) {
                    $f = 0; 
                    $s+=3;
                    $catalogItem .='<ul>'.$catItem1.'</ul>'.'<div class="clear"></div>' ;
                    $catalogItem .='<ul>'.$catItem2.'</ul>'.'<div class="clear"></div>' ;
                    $catalogItem .='<ul>'.$catItem3.'</ul>'.'<div class="clear"></div>' ; 
                    $catalogItem .='<ul>'.$catItem4.'</ul>'.'<div class="clear"></div>' ; 
                    $catalogItem .='<ul>'.$catItem5.'</ul>'.'<div class="clear" style="margin-bottom: 25px;"></div>' ; 
                    $catItem1 = '';
                    $catItem2 = '';
                    $catItem3 = ''; 
                    $catItem4 = ''; 
                    $catItem5 = ''; 

                }
    			        
                if ($s >= $itemsLength) {
                    $catItem1 = '';
                    $catItem2 = '';
                    $catItem3 = ''; 
                    $catItem4 = ''; 
                    $catItem5 = ''; 
                    break;
                } 
            }
        }
		
        $this->tpl->assign(array(
						'PROBA_CATALOG'     =>  (''),
   				'CATALOG_ITEM'=>$catalogItem,
    				'TOP_NAV_BAR' => $navTop,
    				'BOTT_NAV_BAR' => $navBot 
    				
   			));
        $this->tpl->parse('CONTENT', '.catalog');
    }
    //protected function loadPagesItems ($level) {
    protected function loadSubSectionGoods ($level) 
    
        {
    	$brandSql = NULL;
    	if (!is_numeric($level)) {
            return false;
    	}
    	
    	if ($level <= 0) {
            return false;
    	}    	
    	if (!isset($this->activePage['id']))
                $this->activePage['id']=NULL;
        if (!isset($this->activePage['header']))
            $this->activePage['header']=NULL;
    	$this->activePage['header'] .= '<p> '. $this->getAdminAdd('catpage', $this->activePage['id']).'</p>';
	$this->setMetaTags($this->activePage);
    	
        $getMeta = $this->db->fetchRow("SELECT * FROM `section_info` WHERE `name`='".$this->activePage['name']."'");
        
        $meta['title']=(!empty($getMeta['title'])?$getMeta['title']:$this->activePage['name']);
        $meta['keywords']=(!empty($getMeta['keywords'])?$getMeta['keywords']:$this->activePage['name']);
        $meta['description']=(!empty($getMeta['description'])?$getMeta['description']:$this->activePage['name']);
        $meta['header']=$this->activePage['name'];
        $this->setMetaTags($meta);
        
        $ua=$this->getUa();
		
    	$this->tpl->define_dynamic('_catalog', "$ua". 'catalog.tpl');
    	$this->tpl->define_dynamic('catalog', '_catalog');
    	
    	$this->tpl->define_dynamic('catalog_items_body', 'catalog');
    	$this->tpl->define_dynamic('catalog_list_items', 'catalog');
    	$this->tpl->define_dynamic('catalog_items', 'catalog_items_body');
    	
    	$this->tpl->define_dynamic('cataog_items_empty', 'catalog');
        
       	
    	
            
            //$this->tpl->parse('CATALOG_LIST_ITEMS', '.catalog_list_items'); 				
    		
            
    	
    	// Постраничная навигация
    	
    	$visibleQuery = '';
        
        if (!$this->_isAdmin()) {
            $visibleQuery = " AND `visibility` = '1'";
        }
    	
    	$start = 0;
        $navbar = $navTop = $navBot = '';
        $pagePagen = 1;
    
        //$catalogPageLength = ($this->settings['num_catalog']>1?($this->settings['num_catalog'])-1:1);
        $catalogPageLength = ($this->settings['num_catalog']);
        
        $vis = $this->getVar('vis', 0);
        
        if (isset($vis)){
            
            if ((int)$vis>0){
                //$_SESSION['vis']=($vis>1?$vis-1:1);
                $_SESSION['vis']=$vis;
            }
        }
        $navTop = '<div class="pager2"><p>'.$this->printNav().'</p></div>';
        $navBot = $navTop ;
               
        $catalogPageLength = ((!empty($_SESSION['vis']) and $_SESSION['vis']>0) ? $_SESSION['vis'] : $this->settings['num_catalog']);
        //echo $catalogPageLength; die;
        $count = $this->db->fetchOne("SELECT COUNT(`id`) FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '$level' $visibleQuery AND `type` = 'page' $brandSql");
        
        $resBaseUrl = $this->db->fetchRow("SELECT `href`, `level` FROM `".$this->regionData['kod']."_catalog` WHERE `id` = '$level'");
        $baseUrl = $resBaseUrl['href'];
        $resBaseUrl = $this->db->fetchOne("SELECT `href` FROM `".$this->regionData['kod']."_catalog` WHERE `id` = '".$resBaseUrl['level']."'");
        $baseUrl = '/'.$this->regionData['url'].'/catalog/'.$resBaseUrl.'/'.$baseUrl.'/';
        //echo $baseUrl2;
        if ($count > 0) {
            if ($count > $catalogPageLength) {
            	if (isset($this->getParam['page'])) {
                    $pagePagen = (int) $this->getParam['page'];
                    $start = $catalogPageLength * $pagePagen - $catalogPageLength;
                            
                    if ($start > $count) {
                    	$start = 0;
                    }
                }
                
                $pagenParams = '';
                
                if (!empty($brandName)) { 
                	$pagenParams = "?brand=$brandName";	
                }
                
                $pagenHref = 'catalog';
                if (isset($this->activePage['href1'])) {
                	$pagenHref .= '/'. $this->activePage['href1'];
                }
                $pagenHref .= '/'. $this->activePage['href'];
                
        
                $navbar = $this->loadPaginator((int) ceil($count/$catalogPageLength), (int) $pagePagen, $this->basePath.$pagenHref.$pagenParams );
                       
                if ($navbar) {
                	$navTop = '<div class="pager2">' . $navbar . '<p>'.$this->printNav().'</p></div>';
                    $navBot = $navTop ;
                }
           }
        } else {
        	//$this->tpl->assign('CONTENT', ($index ? '' : $this->getAdminAdd('news')).'{EMPTY_SECTION}');
        //  return true;
        }
		
		//значение фильтра производителя
			if((int)$_GET[manuf]!=0){
				$query = "SELECT name FROM `produsers` WHERE `id` = '$_GET[manuf]'";
				$row = $this->db->fetchAll($query); 
				foreach ($row as $v) {
					$where_manuf="AND `producer`= '".$v[name]."'";
				}
				
			}
			
		if($_SESSION['manuf']!=0 || $_SESSION['manuf']!=""){
				$query = "SELECT name FROM `produsers` WHERE `id` = '$_GET[manuf]'";
				$row = $this->db->fetchAll($query); 
				foreach ($row as $v) {
				$where_manuf="AND `producer`= '".$v[name]."'";
				}
				
			}
			//=============================================================
			
			
				//значение фильтра цены
				if (!$_SESSION[maxCost]){$_SESSION[maxCost]=$_POST[maxCost];}
				if (!$_SESSION[minCost]){$_SESSION[minCost]=$_POST[minCost];}
				if ($_POST[maxCost]){$_SESSION[maxCost]=$_POST[maxCost];}
				if ($_POST[minCost]){$_SESSION[minCost]=$_POST[minCost];}
				
				//значение фильтра цены если нет одного из параметров либо есть оба
					if((int)$_SESSION[maxCost]!=0 && (int)$_SESSION[minCost]!=0){
					$where_cost="AND `cost`>= '".$_SESSION[minCost]."' AND `cost`<='".$_SESSION[maxCost]."'";
					}
					
							elseif((int)$_SESSION[minCost]!=0 && (int)$_SESSION[maxCost]==0){
								$where_cost="AND `cost`>= '".$_SESSION[minCost]."'";
							}
							
					elseif((int)$_SESSION[maxCost]!=0 && (int)$_SESSION[minCost]==0){
						$where_cost="AND `cost`<='".$_SESSION[maxCost]."'";
					}
				//===============================================================================

				
        $sql = "SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `level` = '$level' $where_manuf $where_cost $visibleQuery AND `type` = 'page'  $brandSql ORDER BY `position`, `name` LIMIT ".$start.", ".$catalogPageLength;   
     	
    	$items = $this->db->fetchAll($sql);     	
    	$itemsLength = count($items);
    	
    	if ($itemsLength <= 0) {
            
    		$this->tpl->parse('CATALOG_ITEMS_EMPTY', 'null');
			
			$this->tpl->assign(array(
								'PROBA_CATALOG'     =>  (''),
                    'CATALOG_ART' => '---',
                    'CATALOG_PRODUCER'=>'---',
                    'CATALOG_PRICE'=>'---',
                    'CATALOG_INPUT'=> '---',
					'CATALOG_BUY'=>'---',
                    
));  
    	}

		
        if ($itemsLength > 0) {
			
		if ($_COOKIE[ua]!=1){
            foreach ($items as $value) {
                
                $this->tpl->assign(array(
					'PROBA_CATALOG'     =>  (''),
                    'CATALOG_ART' => $value['artikul'],
                    'CATALOG_PRODUCER'=>'<a href="'.$baseUrl.$value['href'].'">'.$value['name'].'</a><br />'.$value['producer'],
                    'CATALOG_PRICE'=>number_format($value['cost'], 2, ',', ' '),
                    'CATALOG_INPUT'=> '<input onkeypress=" if (event.keyCode==\'13\') { addToBasket( \''.$this->regionData['kod'].'\', \'goods_'.$value['id'].'\', $(\'goods_'.$value['id'].'\').value); }" type="text" id="goods_'.$value['id'].'" value="1" />',
                    
				));  
				
				$zak=$this->getZak();

				$this->tpl->assign(array(
                    'CATALOG_BUY'=>'<a href="#" class="buy"  onclick="addToBasket( \''.$this->regionData['kod'].'\', \'goods_'.$value['id'].'\', $(\'goods_'.$value['id'].'\').value);">'.$zak.'</a>',
				));  
				
                $this->tpl->parse('CATALOG_LIST_ITEMS', '.catalog_list_items'); 				
                $this->tpl->parse('CATALOG_ITEMS_EMPTY', 'null');
            }        
		}
		else{
		            foreach ($items as $value) {
                
								if($value['name_ua']==""){$value['name_ua']=$value['name'];}
				
                $this->tpl->assign(array(
					'PROBA_CATALOG'     =>  (''),
                    'CATALOG_ART' => $value['artikul'],
                    'CATALOG_PRODUCER'=>'<a href="'.$baseUrl.$value['href'].'">'.$value['name_ua'].'</a><br />'.$value['producer'],
                    'CATALOG_PRICE'=>number_format($value['cost'], 2, ',', ' '),
                    'CATALOG_INPUT'=> '<input onkeypress=" if (event.keyCode==\'13\') { addToBasket_ua( \''.$this->regionData['kod'].'\', \'goods_'.$value['id'].'\', $(\'goods_'.$value['id'].'\').value); }" type="text" id="goods_'.$value['id'].'" value="1" />',
                    
					));  
				
				$zak=$this->getZak();

				$this->tpl->assign(array(
                    'CATALOG_BUY'=>'<a href="#" class="buy"  onclick="addToBasket_ua( \''.$this->regionData['kod'].'\', \'goods_'.$value['id'].'\', $(\'goods_'.$value['id'].'\').value);">'.$zak.'</a>',
					));  
				
                $this->tpl->parse('CATALOG_LIST_ITEMS', '.catalog_list_items'); 				
                $this->tpl->parse('CATALOG_ITEMS_EMPTY', 'null');
            } 
		
		}
            
     }
		
        $this->tpl->assign(array(
				'PROBA_CATALOG'     =>  (''),
    				'TOP_NAV_BAR' => $navTop,
    				'BOTT_NAV_BAR' => $navBot 
    				
   			));
        $this->tpl->parse('CONTENT', '.catalog');
		
    }

    protected function printNav(){
        if (!isset($_SESSION['vis'])) {
            $_SESSION['vis']=30;
        }
		
		if (!isset($_SESSION['manuf'])) {
            $_SESSION['manuf']=0;
        }
		

		

			
			if($_GET[sbros]>0){
			$_POST[minCost]=$valueMin=$_SESSION[minCost]=$_SESSION[maxCost]=$valueMax=$_POST[maxCost]=0;
			unset($_POST[minCost], $valueMin, $_SESSION[minCost], $_SESSION[maxCost], $valueMax, $_POST[maxCost]);
			$activ_form="http://$_SERVER[HTTP_HOST]$_SERVER[REDIRECT_URL]";
			}
			else{
			$activ_form="$_SERVER[REQUEST_URI]";
			}
		
								if($_POST[minCost]){$valueMin=$_POST[minCost];} else {$valueMin=$_SESSION[minCost];}
						if($_POST[maxCost]){$valueMax=$_POST[maxCost];} else {$valueMax=$_SESSION[maxCost];}
						
		if ($_COOKIE[ua]!=1){
				if($_GET[manuf]!="" || $_GET[page]!="" || $_GET[vis]!="" || $_SESSION[minCost]!="" || $_SESSION[maxCost]!="" || $_POST[minCost]!="" || $_POST[maxCost]!=""){$silka_sbrosa="<a href=\"http://$_SERVER[HTTP_HOST]$_SERVER[REDIRECT_URL]?sbros=1\">сбросить фильтры</a>";}
			//находим level дочерних записей в зависимости от адреса
			$ttt=$this->url[2];
				if(count($this->url)<3){$ttt=$this->url[1];}
			$q1 = $this->db->fetchAll("SELECT `id` FROM `".$this->regionData['kod']."_catalog` WHERE `href`='".$ttt."'");
			 foreach ($q1 as $key) { }


				
					$manuf="<br /><br />Производитель: <select style=\"width:200px\">";
					 $manuf=$manuf.'<option '.($_SESSION['manuf'] == $ke[id] ?  ' selected ':'').' onclick="location.href = \''.$_SERVER['REDIRECT_URL'].'?page='.$_GET[page].'&vis='.$_SESSION['vis'].'&manuf='.$ke[id].'\'">Все</option>';
						//узнаем список производителей
						$q = $this->db->fetchAll("SELECT DISTINCT `producer` FROM `".$this->regionData['kod']."_catalog` WHERE `type` = 'page' AND `level`='$key[id]' AND `id`!='3' order  by producer ASC");
					   array_unshift($q,' ');
					   $q[0]=array('producer'=>'');
					  // print_r($q);
					   //пока есть имя производителя 
							   foreach ($q as $key=>$val) {
							
							    //узнаем его Ид и запихиваем в гет параметр селекта
								$q2 = $this->db->fetchAll("SELECT * FROM `produsers` WHERE `name` = '$val[producer]'");
								foreach ($q2 as $ke) { }
									if((int)$ke[id]!=0){
									   $manuf=$manuf.'<option '.($_SESSION['manuf'] == $ke[id] ?  ' selected ':'').' onclick="location.href = \''.$_SERVER['REDIRECT_URL'].'?page='.$_GET[page].'&vis='.$_SESSION['vis'].'&manuf='.$ke[id].'\'">'.$val[producer].'</option>';
								   }
							   }
							   
						$manuf=$manuf.'</select>';
						$_SESSION['manuf']=(int)$_GET[manuf];
						
				return ''.$manuf.'

				
				
				Показать по: <select style="width:60px;">
				<option '.($_SESSION['vis'] == 10 ?  ' selected ':'').' onclick="location.href = \''.$_SERVER['REDIRECT_URL'].'?page='.$_GET[page].'&vis=10&manuf='.$_SESSION['manuf'].'\'">10</option>
				<option '.($_SESSION['vis'] == 30 ?  ' selected ':'').' onclick="location.href = \''.$_SERVER['REDIRECT_URL'].'?page='.$_GET[page].'&vis=30&manuf='.$_SESSION['manuf'].'\'">30</option>
				<option '.($_SESSION['vis'] == 50 ?  ' selected ':'').' onclick="location.href = \''.$_SERVER['REDIRECT_URL'].'?page='.$_GET[page].'&vis=50&manuf='.$_SESSION['manuf'].'\'">50</option>
				<option '.($_SESSION['vis'] == 100 ? ' selected ':'').' onclick="location.href = \''.$_SERVER['REDIRECT_URL'].'?page='.$_GET[page].'&vis=100&manuf='.$_SESSION['manuf'].'\'">100</option>
				</select>
				<br /><br />
				
				
				<div class="forma_ceni">
				Цена: 
				<form action="'.$activ_form.'" method="post">
								<input type="text" name="minCost" size="5" value="'.$_SESSION[minCost].'" style="width:40px"/> -
								<input type="text" name="maxCost" size="5" value="'.$_SESSION[maxCost].'" style="width:40px"/>
                <input type="submit" value="Искать" style="width:60px">
						</form>
						<br />
						'.$silka_sbrosa.'
						</div>
						';  
		}
			else{
					if($_GET[manuf]!="" || $_GET[page]!="" || $_GET[vis]!="" || $_SESSION[minCost]!="" || $_SESSION[maxCost]!="" || $_POST[minCost]!="" || $_POST[maxCost]!=""){$silka_sbrosa="<a href=\"http://$_SERVER[HTTP_HOST]$_SERVER[REDIRECT_URL]?sbros=1\">зкинути фільтри</a>";}
					//находим level дочерних записей в зависимости от адреса
					$ttt=$this->url[2];
						if(count($this->url)<3){$ttt=$this->url[1];}
					$q1 = $this->db->fetchAll("SELECT `id` FROM `".$this->regionData['kod']."_catalog` WHERE `href`='".$ttt."'");
					foreach ($q1 as $key) {	}
									
						$manuf="<br /><br />Виробник: <select style=\"width:200px\">";
							$manuf=$manuf.'<option '.($_SESSION['manuf'] == $ke[id] ?  ' selected ':'').' onclick="location.href = \''.$_SERVER['REDIRECT_URL'].'?page='.$_GET[page].'&vis='.$_SESSION['vis'].'&manuf='.$ke[id].'\'">Всі</option>';
						//узнаем список производителей
						$q = $this->db->fetchAll("SELECT DISTINCT `producer` FROM `".$this->regionData['kod']."_catalog` WHERE `type` = 'page' AND `level`='$key[id]' AND `id`!='3' order  by producer ASC");
					   array_unshift($q,' ');
					    $q[0]=array('producer'=>'Всі');
								//пока есть имя производителя 
							   foreach ($q as $key=>$val) {
							   
							   //узнаем его Ид и запихиваем в гет параметр селекта
							  $q2 = $this->db->fetchAll("SELECT * FROM `produsers` WHERE `name` = '$val[producer]'");
							   foreach ($q2 as $ke) { }
								   if((int)$ke[id]!=0){
									   $manuf=$manuf.'<option '.($_SESSION['manuf'] == $ke[id] ?  ' selected ':'').' onclick="location.href = \''.$_SERVER['REDIRECT_URL'].'?page='.$_GET[page].'&vis='.$_SESSION['vis'].'&manuf='.$ke[id].'\'">'.$val[producer].'</option>';
									}
							   }
							   
						$manuf=$manuf.'</select>';
						$_SESSION['manuf']=(int)$_GET[manuf];
						
						if($_POST[minCost]){$valueMin=$_POST[minCost];} else {$valueMin=$_SESSION[minCost];}
						if($_POST[maxCost]){$valueMax=$_POST[maxCost];} else {$valueMax=$_SESSION[maxCost];}
			
				return ''.$manuf.'
						
			&nbsp;
			
				Показати по: <select style="width:60px;">
				<option '.($_SESSION['vis'] == 10 ?  ' selected ':'').' onclick="location.href = \''.$_SERVER['REDIRECT_URL'].'?page='.$_GET[page].'&vis=10&manuf='.$_SESSION['manuf'].'\'">10</option>
				<option '.($_SESSION['vis'] == 30 ?  ' selected ':'').' onclick="location.href = \''.$_SERVER['REDIRECT_URL'].'?page='.$_GET[page].'&vis=30&manuf='.$_SESSION['manuf'].'\'">30</option>
				<option '.($_SESSION['vis'] == 50 ?  ' selected ':'').' onclick="location.href = \''.$_SERVER['REDIRECT_URL'].'?page='.$_GET[page].'&vis=50&manuf='.$_SESSION['manuf'].'\'">50</option>
				<option '.($_SESSION['vis'] == 100 ? ' selected ':'').' onclick="location.href = \''.$_SERVER['REDIRECT_URL'].'?page='.$_GET[page].'&vis=100&manuf='.$_SESSION['manuf'].'\'">100</option>
							</select>
							<br /><br />
							
					<div class="forma_ceni">
					
				Ціна: 
				<form style="clear:both" action="'.$activ_form.'" method="post">
								<input type="text" name="minCost" size="5" value="'.$valueMin.'" style="width:40px" /> -
								<input type="text" name="maxCost" size="5" value="'.$valueMax.'" style="width:40px"/>
                <input type="submit" value="Пошук" style="width:60px" />
						</form>
						<br /><br />
						'.$silka_sbrosa.'
						</div>
						';
						
				}

    }
    protected function loadSectionCatalog($id=0) {
        //Выводим все разделы
        $res = $this->db->fetchAll("SELECT * FROM `".$this->regionData['kod']."_sections` WHERE `pid`='$id'");
        foreach ($res as $key) {
            //echo $key['name'].$key['href'];
        }
    	return true;
    }
    // Выводит новинки, акции, хиты
    protected function drowGoodsList2Status ($header, $status) {
    	
    	$this->setMetaTags($header);    	
    	$this->setWay($header);    	    	
    	
		$ua=$this->getUa();
		
    	$this->tpl->define_dynamic('_catalog', "$ua". 'catalog.tpl');
    	$this->tpl->define_dynamic('catalog', '_catalog');
    	
    	$this->tpl->define_dynamic('catalog_items_body', 'catalog');
    	$this->tpl->define_dynamic('catalog_brands_items', 'catalog');
    	$this->tpl->define_dynamic('catalog_items', 'catalog_items_body');
    	
    	$this->tpl->define_dynamic('cataog_items_empty', 'catalog');
        
        // Бренды
                
    	$brandsList = $this->db->fetchAll("SELECT DISTINCT `proizvoditel` FROM `".$this->regionData['kod']."_catalog` WHERE `type` = 'page' AND `status` = '$status' ");
    	$brandName = '';
    	
    	if (count($brandsList) > 0) {
            if (isset($_GET['brand']) && !empty($_GET['brand']) && is_string($_GET['brand'])) {
    		$brandName= preg_replace('/insert|delete|drop|truncate|select|union|update|\/+|\\|\;/i', '', $_GET['brand']);    			
            }
            
            $brandUrlParam = '?';
            if (isset($_GET['page'])) {
    		$brandUrlParam = "?page=$_GET[page]&";
            }
    		
            $brand = '<a href="'.$brandUrlParam.'brand=все">Все</a>';
            if ($brandName == 'все' || empty($brandName)) {
    		$brandName = '';
    		$brand = "<span>Все</span>";
            }
    				
            $this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'CATALOG_BRAND'=>$brand));
            $this->tpl->parse('CATALOG_BRANDS_ITEMS', '.catalog_brands_items'); 				
    		
            foreach ($brandsList as $brandsItem) {
                if (!empty($brandsItem['proizvoditel'])) { 
                    $brand = '<a href="'.$brandUrlParam.'brand='.$brandsItem['proizvoditel'].'">'.$brandsItem['proizvoditel'].'</a>';
    			if ($brandsItem['proizvoditel'] == $brandName) {
                            $brand = "<span>$brandName</span>";
    			}
    			$this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'CATALOG_BRAND'=>$brand));  
    			$this->tpl->parse('CATALOG_BRANDS_ITEMS', '.catalog_brands_items'); 				
                    }
    		}
            } 
    	
            $brandSql = '';
            
            if (!empty($brandName)) {
    		$brandSql = " AND `proizvoditel` = '$brandName'";
            }

            // Постраничная навигация
    	
            $start = 0;
            $navbar = $navTop = $navBot = '';
            $pagePagen = 1;
    
            $catalogPageLength = $this->settings['num_catalog'];
            $count = $this->db->fetchOne("SELECT COUNT(`id`) FROM `".$this->regionData['kod']."_catalog` WHERE `type` = 'page' $brandSql  AND `status` = '$status'");
        
            if ($count > 0) {
        	if ($count > $catalogPageLength) {
                    if (isset($this->getParam['page'])) {
                	$pagePagen = (int) $this->getParam['page'];
                        $start = $catalogPageLength * $pagePagen - $catalogPageLength;
                            
                        if ($start > $count) {
                            $start = 0;
                        }
                    }
                
                    $pagenParams = '';
                
                    if (!empty($brandName)) { 
                	$pagenParams = "?brand=$brandName";	
                    }
                
                    $pagenHref = 'catalog';
                    
                    if (isset($this->activePage['href1'])) {
                	$pagenHref .= '/'. $this->activePage['href1'];
                    }
                    
                    $pagenHref .= '/'. $this->activePage['href'];
                    $navbar = $this->loadPaginator((int) ceil($count/$catalogPageLength), (int) $pagePagen, $this->basePath.$pagenHref.$pagenParams );
                        
                    if ($navbar) {
                        
                	$navTop = '<div class="pager_right">' . $navbar . '</div>';
                        $navBot = $navTop ;
                    }
                }
            } else {
        	//$this->tpl->assign('CONTENT', ($index ? '' : $this->getAdminAdd('news')).'{EMPTY_SECTION}');
            //  return true;
            }
    
            $sql = "SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `type` = 'page' $brandSql  AND `status` = '$status' ORDER BY `position`, `name` LIMIT ".$start.", ".$this->settings['num_catalog'];   
            $items = $this->db->fetchAll($sql);     	
            $itemsLength = count($items);
    	
            if ($itemsLength > 0) {
    		$this->tpl->parse('CATALOG_ITEMS_EMPTY', 'null');
            }
    	
            $catItem1 = '';
            $catItem2 = '';
            $catItem3 = '';
            $catItem4 = '';
            $catItem5 = '';
            $catalogItem = '';
        			
            $i = 0;
            $f = 0;
            $s = 0;
        	
            		
            if ($itemsLength > 0) {
    		while (true) { 
                    if (isset($items[$i])) {
        		$itemUrl = '';	
   			$groupName = '-';
   			$groupNameUrl = '#';
                        
        		if (($parenSectionInfo = $this->dataTreeManager($items[$i]['id']))) {
                            $itemUrl = $parenSectionInfo['links'];	
                            $groupNameIndex = (count($parenSectionInfo['names']) - 3);
        			
                            if (isset($parenSectionInfo['names'][$groupNameIndex])) {        				
        			$groupNameUrl = '';
        			
        			for ($f1 = 0; $f1 <= $groupNameIndex; ++$f1) {
                                    $groupNameUrl .= '/'. $parenSectionInfo['linksArr'][$f1];
        			}	
        		
                                $groupName = $parenSectionInfo['names'][$groupNameIndex];
                            }
        		}
        																
        																
        		$link = (($items[$i]['type'] != 'link')?('/catalog'):('')).$parenSectionInfo['links'];                                            	
                	$name = $items[$i]['name'];                                               
    						
    			if (!empty($types)) {
                            $types = "$types ";
    			}
    						
    			
    						
    			$liLastClass = '';
    						
    			if ($f == 2) {
                            $liLastClass = 'last';
    			}
    			
    			$imgSrc = 'nophoto_s.jpg';    			
    			$imgAlt = 'Нет фото';
    			$imgTitle = $imgAlt;
    			
    			if (is_file($_SERVER['DOCUMENT_ROOT'].'/img/catalog/small/'.$items[$i]['pic'])) {
                            $imgSrc = $items[$i]['pic'];
                            $imgAlt = $items[$i]['pic_alt'];
                            $imgTitle = $items[$i]['pic_alt'];
    		
                            if (empty($imgAlt)) {
    				$imgAlt = $items[$i]['name'];
    				$imgTitle = $items[$i]['name'];
                            }
    			}elseif (is_file($_SERVER['DOCUMENT_ROOT'].'/img/catalog/small/'.$items[$i]['artikul'].'.jpg')) {
    			$imgSrc = $items[$i]['artikul'].'.jpg';
    			$imgAlt = $items[$i]['pic_alt'];
    			$imgTitle = $items[$i]['pic_title'];
    		
  			if (empty($imgAlt)) {
                            $imgAlt = $items[$i]['name'];    						
    			}
    					
    			if (empty($imgTitle)) {    						
                            $imgTitle = $items[$i]['name'];
    			}
                        
                    }
    			
                        $adminButton = $this->getAdminEdit('catpage', $items[$i]['id']);
    			$imgType = '';
                       	
    			if (empty($items[$i]['preview'])) {
                            $items[$i]['preview'] = '&nbsp;';
    			}
    				
    			$statusImgNames = array(
    					'new'=>'new.png',
    					'action'=>'akcia.png',
    					'hit'=>'hit.png',
    			);
    			
    			if (isset($items[$i]['status']) && $items[$i]['status'] == $status && isset($statusImgNames[$status])) {
                            $imgType = '<img src="/img/'.$statusImgNames[$status].'" class="png" width="173" height="37" alt="" />';
    			}    			    			  
    						
    			$catItem1 .= '<li><div class="img">'.$imgType.'<a href="/catalog/'.$itemUrl.'"><img src="/img/catalog/small/'.$imgSrc .'" width="200" height="180" alt="'.$imgAlt.'" title="'.$imgTitle.'"/></a></div><p><span class="left"></span><span class="right"><a href="/catalog/'.$itemUrl.'" title="'.$items[$i]['name'].'">'.$items[$i]['name'].'</a></span>'.$adminButton.'</p></li> ';
  	  		$catItem2 .= '<li class="li-group"><p><span class="left">Группа:</span><span class="right"><a class="black" href="/catalog'.$groupNameUrl.'">'.$groupName.'</a></span></p></li>';
  	  		$catItem3 .= '<li><p class="desc">'.stripslashes($items[$i]['preview']).'</p></li>';    			
    			$catItem4 .= '<li>';
    			$catItem5 .= '<li>';

                            if ($this->getCatOption('isStore')) {
                                if (isset($items[$i]['cost_old']) ) {
                                    if ($items[$i]['cost_old'] > $items[$i]['cost']) {
                                        $econom = ($items[$i]['cost_old'] - $items[$i]['cost']);
    					if ($econom > 0) {
                                            $catItem4 .= '<p><span>Розничная цена:</span><span class="price">'. number_format($items[$i]['cost_old'], 2, ',', " ").' грн.</span></p>
     								<p><span>Экономия:</span><span class="eco">'.number_format($econom, 2, ',', " ").' грн.</span></p>';
    					}
                                    }
    				}		
    			
    				//$buyButton = '<p class="right"><input type="button" onclick="addToBasket(\'goods_'.$items[$i]['id'].'\', 1);" value="Заказать" class="button" /></p>';
    				$buyButton = '<div><div style="float: left;"> <input type="text" class="goods-length" value="1" id="goods_'.$items[$i]['id'].'" /> шт. </div> <div style="text-align: right;"> <a href="#" class="buy" onclick="addToBasket(\'goods_'.$items[$i]['id'].'\', $(\'goods_'.$items[$i]['id'].'\').value);"> Заказать</a></div></div>';
	    			
    				if (!$this->isBuyButtomType($items[$i]['id'])) {
                                    $buyButton = '<p class="right"><img src="/img/ok.gif" width="13" height="12" alt="" /><a href="/order">Оформить заказ</a></p>';
    				}
                                if (empty($items[$i]['cost'])) {
                                    $items[$i]['cost'] = '0';
                                }
     				$catItem5.='<p><span>Наша цена:</span><span class="newprice">'. number_format($items[$i]['cost'], 2, ',', " ").' грн.</span></p><span id="by_button_goods_'.$items[$i]['id'].'">'.$buyButton.'</span>';    				
                            }	
                            
                            $catItem4 .= '&nbsp;</li>';
                            $catItem5 .= '&nbsp;</li>';
    						
        		} else {
                            $liLastClass = '';    						
                            if ($f == 2) {
    				$liLastClass = '';
                            }
        		}
        				
    			$f++;
    			$i++;					    					
    			        
	        	if ($f == 3 ) {
                            $f = 0; 
                            $s+=3;
                            $catalogItem .='<ul>'.$catItem1.'</ul>'.'<div class="clear"></div>' ;
                            $catalogItem .='<ul>'.$catItem2.'</ul>'.'<div class="clear"></div>' ;
                            $catalogItem .='<ul>'.$catItem3.'</ul>'.'<div class="clear"></div>' ; 
                            $catalogItem .='<ul>'.$catItem4.'</ul>'.'<div class="clear"></div>' ; 
                            $catalogItem .='<ul>'.$catItem5.'</ul>'.'<div class="clear" style="margin-bottom: 25px;"></div>' ; 
                            $catItem1 = '';
                            $catItem2 = '';
                            $catItem3 = ''; 
                            $catItem4 = ''; 
                            $catItem5 = ''; 
                        }
    			        
                        if ($s >= $itemsLength) {
                            $catItem1 = '';
                            $catItem2 = '';
                            $catItem3 = ''; 
                            $catItem4 = ''; 
                            $catItem5 = ''; 
                            break;
        		} 
   		}
            } else {
			
            }
		
            $this->tpl->assign(array(
							'PROBA_CATALOG'     =>  (''),
    				'CATALOG_ITEM'=>$catalogItem,
    				'TOP_NAV_BAR' => $navTop,
    				'BOTT_NAV_BAR' => $navBot 
    				
    			));
   	$this->tpl->parse('CONTENT', '.catalog');
    	
    	return true;
    }
    public function novelty() {
	
    	return $this->drowGoodsList2Status ('Новинки', 'new');    	
    }
    public function hits() {
	if ($_COOKIE[ua]!=1){
               	return $this->drowGoodsList2Status ('Хиты', 'hit');         
		}
			else{
                        	return $this->drowGoodsList2Status ('Хіти', 'hit'); 
				}
   	    	
    }
    public function actions() {
	
	if ($_COOKIE[ua]!=1){
                  	return $this->drowGoodsList2Status ('Акции', 'action');      
		}
			else{
                      	return $this->drowGoodsList2Status ('Акції', 'action');   
				}
   	    	    	
    }
    
    // Рекомендуемые товары    
    protected function featuredProducts($level, $where='') {
    
        $sql = "SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `type` = 'page' $where ORDER BY `position`, `name` ";   
    	$items = $this->db->fetchAll($sql);     	
    	$itemsLength = count($items);
    		
    	$catItem1 = '';
        $catItem2 = '';
        $catItem3 = '';
        $catItem4 = '';
        $catItem5 = '';
        $catalogItem = '';
        			
        $i = 0;
        $f = 0;
        $s = 0;
	
        if ($itemsLength > 0) {
            while (true) { 
                if (isset($items[$i])) {
                    $itemUrl = '';	
                    $groupName = '-';
                    $groupNameUrl = '#';
   				
                    if (($parenSectionInfo = $this->dataTreeManager($items[$i]['id']))) {
                        $itemUrl = $parenSectionInfo['links'];	
        		$groupNameIndex = (count($parenSectionInfo['names']) - 2);
        			
        		if (isset($parenSectionInfo['names'][$groupNameIndex])) {        				
                            $groupNameUrl = '';
        				
                            for ($f1 = 0; $f1 <= $groupNameIndex; ++$f1) {
        			$groupNameUrl .= '/'. $parenSectionInfo['linksArr'][$f1];
                            }	
                            
                            $groupName = $parenSectionInfo['names'][$groupNameIndex];
        		}
                    }
        																
                    $link = (($items[$i]['type'] != 'link')?('/catalog/'):('')).$parenSectionInfo['links'];                                                
                    $name = $items[$i]['name'];                                               
    					
                    if (!empty($types)) {
    			$types = "$types ";
                    }
                   
    						
                    $liLastClass = '';
    						
                    if ($f == 2) {
    			$liLastClass = 'last';
                    }
                    
                    $imgSrc = 'nophoto_s.jpg';    			
                    $imgAlt = 'Нет фото';
                    $imgTitle = $imgAlt;
    			
                    if (is_file($_SERVER['DOCUMENT_ROOT'].'/img/catalog/small/'.$items[$i]['pic'])) {
    			$imgSrc = $items[$i]['pic'];
    			$imgAlt = $items[$i]['pic_alt'];
    			$imgTitle = $items[$i]['pic_title'];
    		
    			if (empty($imgAlt)) {
                            $imgAlt = $items[$i]['name'];    					
    			}
    					
    			if (empty($imgTitle)) {
                            $imgTitle = $items[$i]['name'];    					
    			}
                    } elseif (is_file($_SERVER['DOCUMENT_ROOT'].'/img/catalog/small/'.$items[$i]['artikul'].'.jpg')) {
    			$imgSrc = $items[$i]['artikul'].'.jpg';
    			$imgAlt = $items[$i]['pic_alt'];
    			$imgTitle = $items[$i]['pic_title'];
    		
  			if (empty($imgAlt)) {
                            $imgAlt = $items[$i]['name'];    						
    			}
    					
    			if (empty($imgTitle)) {    						
                            $imgTitle = $items[$i]['name'];
    			}
                        
                    }
    			
                    $imgType = '';
    		
                    if (isset($items[$i]['status']) && $items[$i]['status'] == 'hit') {
    			$imgType = '<img src="/img/hit.png" class="png" width="173" height="37" alt="" />';
                    }
    			
                    if (isset($items[$i]['status']) && $items[$i]['status'] == 'action') {
    			$imgType = '<img src="/img/akcia.png" class="png" width="173" height="37" alt="" />';
                    }	
    			
                    if (isset($items[$i]['status']) && $items[$i]['status'] == 'new') {
    			$imgType = '<img src="/img/new.png" class="png" width="173" height="37" alt="" />';
                    }    		    			  
    					
                  		
                    if (empty($items[$i]['preview'])) {
    			$items[$i]['preview'] = '&nbsp;';
                    }
    					
                    $catItem1 .= '<li><div class="img">'.$imgType.'<a href="'.$itemUrl.'"><img src="/img/catalog/small/'.$imgSrc .'" width="200" height="180" alt="'.$imgAlt.'" title="'.$imgTitle.'"/></a></div><p><span class="left"></span><span class="right"><a href="'.$itemUrl.'" title="'.$items[$i]['name'].'">'.$items[$i]['name'].'</a></span></p></li> ';
					
					if ($_COOKIE[ua]!=1){
                                    $catItem2 .= '<li><p><span class="left">Группа:</span><span class="right"><a class="black" href="/catalog'.$groupNameUrl.'">'.$groupName.'</a></span></p></li>';
						}
							else{
									 $catItem2 .= '<li><p><span class="left">Група:</span><span class="right"><a class="black" href="/catalog'.$groupNameUrl.'">'.$groupName.'</a></span></p></li>';
								}

                    $catItem3 .= '<li><p class="desc">'.stripslashes($items[$i]['preview']).'</p></li>';    			
                    $catItem4 .= '<li>';
                    $catItem5 .= '<li>';
    				
                    if ($this->getCatOption('isStore')) {
    			if (isset($items[$i]['cost_old']) ) { 
                            if ($items[$i]['cost_old'] > $items[$i]['cost']) {
    				$econom = ($items[$i]['cost_old'] - $items[$i]['cost']);
    				if ($econom > 0) {
					
								if ($_COOKIE[ua]!=1){
												$catItem4 .= '<p><span>Розничная цена:</span><span class="price">'. number_format($items[$i]['cost_old'], 2, ',', " ").' грн.</span></p>
												<p><span>Экономия:</span><span class="eco">'.number_format($econom, 2, ',', " ").' грн.</span></p>';
									}
										else{
											$catItem4 .= '<p><span>Роздрібна ціна:</span><span class="price">'. number_format($items[$i]['cost_old'], 2, ',', " ").' грн.</span></p>
												<p><span>Економія:</span><span class="eco">'.number_format($econom, 2, ',', " ").' грн.</span></p>';
										}					
						}
                    }
    			}		
    			
    			//$buyButton = '<p class="right"><input type="button" onclick="addToBasket(\'goods_'.$items[$i]['id'].'\', 1);" value="Заказать" class="button" /></p>';
	    		$buyButton = '';
    			if (!$this->isBuyButtomType($items[$i]['id'])) {
				
						if ($_COOKIE[ua]!=1){
							 $buyButton = '<p class="right"><img src="/img/ok.gif" width="13" height="12" alt="" /><a href="/order">Оформить заказ</a></p>';
						}
							else{
								 $buyButton = '<p class="right"><img src="/img/ok.gif" width="13" height="12" alt="" /><a href="/order">Оформити Замовлення</a></p>';	
								}
                           
    			}
                        
                        if (empty($items[$i]['cost'])) {
                            $items[$i]['cost'] = 0;
                        }
    			
     			$catItem5.='<p><span>Наша цена:</span><span class="newprice">'. number_format($items[$i]['cost'], 2, ',', " ").' грн.</span></p><span id="by_button_goods_'.$items[$i]['id'].'">'.$buyButton.'</span>';    				
                    }	
                    $catItem4 .= '&nbsp;</li>';
                    $catItem5 .= '&nbsp;</li>';
    						
        	} else {
                    $liLastClass = '';
                    if ($f == 2) {
    			$liLastClass = '';
                    }
        					
                    //$catItem1 .= '<li class="li-name-empty'.$liLastClass.'"></li>';
                    //$catItem2 .= '<li class="li-name-empty'.$liLastClass.'"></li>';
                    //$catItem3 .= '<li class="li-name-empty'.$liLastClass.'"></li>';
                    //$catItem4 .= '<li class="li-name-empty'.$liLastClass.'"><div class="g_img"><img src="/img/block_product.jpg" width="186" height="186" /></li>';	
        	}
        				
    		$f++;
    		$i++;					    					
    			        
	        if ($f == 3 ) {
                    $f = 0; 
                    $s+=3;
    			        	
                    $catalogItem .='<ul>'.$catItem1.'</ul>'.'<div class="clear"></div>' ;
                    $catalogItem .='<ul>'.$catItem2.'</ul>'.'<div class="clear"></div>' ;
                    $catalogItem .='<ul>'.$catItem3.'</ul>'.'<div class="clear"></div>' ; 
                    $catalogItem .='<ul>'.$catItem4.'</ul>'.'<div class="clear"></div>' ; 
                    $catalogItem .='<ul>'.$catItem5.'</ul>'.'<div class="clear"></div>' ; 
                    $catItem1 = '';
                    $catItem2 = '';
                    $catItem3 = ''; 
                    $catItem4 = '';   
                    $catItem5 = '';   
    	     	}
    			        
    	     	if ($s >= $itemsLength) {
                    $catItem1 = '';
                    $catItem2 = '';
                    $catItem3 = ''; 
                    $catItem4 = ''; 
                    $catItem5 = ''; 
                    break;
        	} 
            }
	} else { 
            $this->tpl->parse('CATALOG_ITEMS', 'null');  
	}
		
    	$this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'FEATURED_CATALOG_ITEM'=>$catalogItem));    	
    	return ($itemsLength > 0);
    }
    
    // Комплект        
    protected function usedComplete($level, $where='') {
    
        $sql = "SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `type` = 'page' $where ORDER BY `position`, `name` ";   
    	$items = $this->db->fetchAll($sql);     	
    	$itemsLength = count($items);
    		
    	$catItem1 = '';
        $catItem2 = '';
        $catItem3 = '';
        $catItem4 = '';
        $catItem5 = '';
        $catalogItem = '';
        			
        $i = 0;
        $f = 0;
        $s = 0;
	
        if ($itemsLength > 0) {
            while (true) { 
                if (isset($items[$i])) {
                    $itemUrl = '';	
                    $groupName = '-';
                    $groupNameUrl = '#';
   				
                    if (($parenSectionInfo = $this->dataTreeManager($items[$i]['id']))) {
                        $itemUrl = $parenSectionInfo['links'];	
        		$groupNameIndex = (count($parenSectionInfo['names']) - 2);
        			
        		if (isset($parenSectionInfo['names'][$groupNameIndex])) {        				
                            $groupNameUrl = '';
        				
                            for ($f1 = 0; $f1 <= $groupNameIndex; ++$f1) {
        			$groupNameUrl .= '/'. $parenSectionInfo['linksArr'][$f1];
                            }	
                            
                            $groupName = $parenSectionInfo['names'][$groupNameIndex];
        		}
                    }
        																
                    $link = (($items[$i]['type'] != 'link')?('/catalog/'):('')).$parenSectionInfo['links'];                                                
                    $name = $items[$i]['name'];                                               
    					
                    if (!empty($types)) {
    			$types = "$types ";
                    }
                    $pic = '/img/nophoto_s.jpg';
    					
                    if (is_file($_SERVER['DOCUMENT_ROOT'].'/img/catalog/small/'.$items[$i]['pic']) ) {
    			$pic = '/img/catalog/small/'.$items[$i]['pic'];
                    }
    						
                    $liLastClass = '';
    						
                    if ($f == 2) {
    			$liLastClass = 'last';
                    }
                    
                    $imgSrc = 'nophoto_s.jpg';    			
                    $imgAlt = 'Нет фото';
                    $imgTitle = $imgAlt;
    			
                    if (is_file($_SERVER['DOCUMENT_ROOT'].'/img/catalog/small/'.$items[$i]['pic'])) {
    			$imgSrc = $items[$i]['pic'];
    			$imgAlt = $items[$i]['pic_alt'];
    			$imgTitle = $items[$i]['pic_title'];
    		
    			if (empty($imgAlt)) {
                            $imgAlt = $items[$i]['name'];    					
    			}
    					
    			if (empty($imgTitle)) {
                            $imgTitle = $items[$i]['name'];    					
    			}
                    }
    			
                    $imgType = '';
    		
                    if (isset($items[$i]['status']) && $items[$i]['status'] == 'hit') {
    			$imgType = '<img src="/img/hit.png" class="png" width="173" height="37" alt="" />';
                    }
    			
                    if (isset($items[$i]['status']) && $items[$i]['status'] == 'action') {
    			$imgType = '<img src="/img/akcia.png" class="png" width="173" height="37" alt="" />';
                    }	
    			
                    if (isset($items[$i]['status']) && $items[$i]['status'] == 'new') {
    			$imgType = '<img src="/img/new.png" class="png" width="173" height="37" alt="" />';
                    }    		    			  
    					
            			
                    if (empty($items[$i]['preview'])) {
    			$items[$i]['preview']= '&nbsp;';
                    }
    					
                    $catItem1 .= '<li><div class="img">'.$imgType.'<a href="'.$itemUrl.'"><img src="/img/catalog/small/'.$imgSrc .'" width="200" height="180" alt="'.$imgAlt.'" title="'.$imgTitle.'"/></a></div><p><span class="left"></span><span class="right"><a href="'.$itemUrl.'" title="'.$items[$i]['name'].'">'.$items[$i]['name'].'</a></span></p></li> ';
                    $catItem2 .= '<li><p><span class="left">Группа:</span><span class="right"><a class="black" href="/catalog'.$groupNameUrl.'">'.$groupName.'</a></span></p></li>';
                    $catItem3 .= '<li><p class="desc">'.stripcslashes($items[$i]['preview']).'</p></li>';    			
                    $catItem4 .= '<li>';
                    $catItem5 .= '<li>';
    				
                    if ($this->getCatOption('isStore')) {
    			if (isset($items[$i]['cost_old']) ) { 
                            if ($items[$i]['cost_old'] > $items[$i]['cost']) {
    				$econom = ($items[$i]['cost_old'] - $items[$i]['cost']);
    				if ($econom > 0) {
                                    $catItem4 .= '<p><span>Розничная цена:</span><span class="price">'. number_format($items[$i]['cost_old'], 2, ',', " ").' грн.</span></p>
     									<p><span>Экономия:</span><span class="eco">'.number_format($econom, 2, ',', " ").' грн.</span></p>';
    				}
                            }
    			}		
    			
    			//$buyButton = '<p class="right"><input type="button" onclick="addToBasket(\'goods_'.$items[$i]['id'].'\', 1);" value="Заказать" class="button" /></p>';
	    		$buyButton = '';
    			if (!$this->isBuyButtomType($items[$i]['id'])) {
                            $buyButton = '<p class="right"><img src="/img/ok.gif" width="13" height="12" alt="" /><a href="/order">Оформить заказ</a></p>';
    			}
                        
                        if (empty($items[$i]['cost'])) {
                            $items[$i]['cost'] = 0;
                        }
    			
     			$catItem5.='<p><span>Наша цена:</span><span class="newprice">'. number_format($items[$i]['cost'], 2, ',', " ").' грн.</span></p><span id="by_button_goods_'.$items[$i]['id'].'">'.$buyButton.'</span>';    				
                    }	
                    $catItem4 .= '&nbsp;</li>';
                    $catItem5 .= '&nbsp;</li>';
    						
        	} else {
                    $liLastClass = '';
                    if ($f == 2) {
    			$liLastClass = '';
                    }
        					
                    //$catItem1 .= '<li class="li-name-empty'.$liLastClass.'"></li>';
                    //$catItem2 .= '<li class="li-name-empty'.$liLastClass.'"></li>';
                    //$catItem3 .= '<li class="li-name-empty'.$liLastClass.'"></li>';
                    //$catItem4 .= '<li class="li-name-empty'.$liLastClass.'"><div class="g_img"><img src="/img/block_product.jpg" width="186" height="186" /></li>';	
        	}
        				
    		$f++;
    		$i++;					    					
    			        
	        if ($f == 3 ) {
                    $f = 0; 
                    $s+=3;
    			        	
                    $catalogItem .='<ul>'.$catItem1.'</ul>'.'<div class="clear"></div>' ;
                    $catalogItem .='<ul>'.$catItem2.'</ul>'.'<div class="clear"></div>' ;
                    $catalogItem .='<ul>'.$catItem3.'</ul>'.'<div class="clear"></div>' ; 
                    $catalogItem .='<ul>'.$catItem4.'</ul>'.'<div class="clear"></div>' ; 
                    $catalogItem .='<ul>'.$catItem5.'</ul>'.'<div class="clear"></div>' ; 
                    $catItem1 = '';
                    $catItem2 = '';
                    $catItem3 = ''; 
                    $catItem4 = '';   
                    $catItem5 = '';   
    	     	}
    			        
    	     	if ($s >= $itemsLength) {
                    $catItem1 = '';
                    $catItem2 = '';
                    $catItem3 = ''; 
                    $catItem4 = ''; 
                    $catItem5 = ''; 
                    break;
        	} 
            }
	} else { 
            
	}
		
    	$this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'USED_COMPLATE_CATALOG_ITEM'=>$catalogItem));    	
    	return ($itemsLength > 0);
    }
	
    // Прорисовывет картинки товаров с других ракурсов
    protected function drowForeshortening($goodsArtikul) {
    	
    	$items = $this->db->fetchAll("SELECT * FROM `catalog_gallery` WHERE `goods_artikul` = '$goodsArtikul'  AND `gallery_type` = 'foreshortening'");
    	if ($items) {
    		
    		foreach ($items as $item) { 
    			$this->tpl->assign(array(
								'PROBA_CATALOG'     =>  (''),
    				'F_ID'=>$item['id'], 
    				'F_SRC'=>$item['pic'], 
    				'F_ALT'=>$item['alt'],
    				'F_TITLE'=>$item['title']
    			));
				$this->tpl->parse('CATALOG_FORESHORTENING_ITEMS', '.catalog_foreshortening_items');
    		}
    		$this->tpl->parse('CATALOG_FORESHORTENING_LIST', '.catalog_foreshortening_list');
    	}
    }
    
     // Прорисовывет картинки из минигалереи
    protected function drowMiniGallery($goodsArtikul) {
    	
    	$items = $this->db->fetchAll("SELECT * FROM `catalog_gallery` WHERE `goods_artikul` = '$goodsArtikul' AND `gallery_type` = 'gallery'");
    	if ($items) {
    		
    		foreach ($items as $item) { 
    			$this->tpl->assign(array(
								'PROBA_CATALOG'     =>  (''),
    				'G_ID'=>$item['id'], 
    				'G_SRC'=>$item['pic'], 
    				'G_ALT'=>$item['alt'],
    				'G_TITLE'=>$item['title']
    			));
				$this->tpl->parse('CATALOG_GALLERY_ITEMS', '.catalog_gallery_items');
    		}
    		$this->tpl->parse('CATALOG_GALLERY_LIST', '.catalog_gallery_list');
    	}
    }
    
    protected function loadDetail() {
    	
		$ua=$this->getUa();
		
    	$this->tpl->define_dynamic('_catalog_detail_body', "$ua". 'catalog.tpl');
    	$this->tpl->define_dynamic('catalog_detail_body', '_catalog_detail_body');    	
        
        //var_dump($this->activePage);
        
        $getInstruction = $this->db->fetchRow("SELECT * FROM `instructions` WHERE `artikul`='".$this->activePage['artikul']."'");
        $meta['title']=(!empty($getInstruction['title'])?$getInstruction['title']:$this->activePage['name']);
        $meta['keywords']=(!empty($getInstruction['keywords'])?$getInstruction['keywords']:$this->activePage['name']);
        $meta['description']=(!empty($getInstruction['description'])?$getInstruction['description']:$this->activePage['name']);
        $meta['header']=$this->activePage['name'];
        $this->setMetaTags($meta);
        
        $goodsInfo = $this->db->fetchRow("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `artikul`='".$this->activePage['artikul']."'");
        //Узнаем цену товара
        $getGoodsCost = $this->db->fetchOne("SELECT `cost` 
            FROM `".$this->regionData['kod']."_catalog` 
            WHERE `artikul`='".$this->activePage['artikul']."'");
        
        //Картинка для товара
        $imgPathSmall = 'img/catalog/small/'.$this->activePage['artikul'].'.jpg';
        $imgPathBig = 'img/catalog/big/'.$this->activePage['artikul'].'.jpg';
        $goodsImgSmall = '';
        $goodsImgBig = '';
        if (file_exists(PATH.$imgPathSmall)){
            $goodsImgSmall = '<a href="/'.$imgPathBig.'" rel="lightbox[photo-item1]"><img src="/'.$imgPathSmall.'" width="295" height="295" alt="" /></a>';
        }
		else{
			$goodsImgSmall = '<img src="/img/catalog/zaglushka.png" width="295" height="295" alt="" />';
		}
        if (file_exists(PATH.$imgPathBig)){
            $goodsImgBig = $imgPathBig;
        }

  
        if (empty($getInstruction['body'])){
            $standart_text = $this->db->fetchOne ("SELECT `text` FROM `standart_text` WHERE `id`=1");
            $standart_text = str_replace('{preparation}', $this->activePage['name'] , $standart_text);
        } else {
            $standart_text = '';
        }		
		
			if ($_COOKIE[ua]!=1){
						$this->tpl->assign(array(
            'PROBA_CATALOG'     =>  ('<!-- АВТОКОМЛИТТЕР 	1 ЧАСТЬ	--><script src="/js/jquery.min.js" type="text/javascript"></script>
										<script>var $j = jQuery.noConflict();</script><!-- /АВТОКОМЛИТТЕР -->'),
            'GOODS_INSTRUCTION'     =>  (!empty($getInstruction['body']) ? $getInstruction['body'] : 'Инструкция не найдена!<br>'),
            'GOODS_LIKE'            =>  '<a href="#"><img src="/img/icons.gif" width="16" height="20" alt="" /></a> <a href="#"><img src="/img/icons2.gif" width="16" height="20" alt="" /></a> <a href="#"><img src="/img/icons3.gif" width="16" height="20" alt="" /></a> <a href="#"><img src="/img/icons4.gif" width="16" height="20" alt="" /></a> <a href="#"><img src="/img/f.gif" width="74" height="20" alt="" /></a> <a href="#"><img src="/img/g.gif" width="49" height="20" alt="" /></a><br /><a href="#"><img src="/img/vk.gif" width="140" height="22" alt="" /></a>',
            'GOODS_IN_STORE'        =>  '<strong>Есть в наличии</strong>',
            'GOODS_BY_PHONE'        =>  'Заказать по телефону: <span>(056) 355-55-45</span>',
            'GOODS_COST'            =>  (!empty($getGoodsCost) ? str_replace('.', ',', $getGoodsCost) . ' грн.' : ''),
            'GOODS_PRODUCER'        =>  $goodsInfo['producer'],//'Натурфарм косметика',
            'GOODS_TEXT'            =>  (!empty ($getInstruction['text'])?$getInstruction['text']:''),//'<p>Источник K+ и Mg2+, регулирует метаболические процессы, способствует восстановлению электролитного баланса, оказывает антиаритмическое действие. </p>',
            'GOODS_IMG_SMALL'       =>  $goodsImgSmall,
            'GOODS_IMG_BIG'         =>  (!empty($goodsImgBig) ? '<span class="zoom"><a href="/' . $goodsImgBig . '">Увеличить</a></span>' : ''),
            'GOODS_INPUT_DATA'      =>  'id="goods_'.$this->activePage['id'].'"',
            'GOODS_BUY_DATA'        =>  'onclick="addToBasket( \''.$this->regionData['kod'].'\', \'goods_'.$this->activePage['id'].'\', $(\'goods_'.$this->activePage['id'].'\').value);"',
            'GOODS_BUY_ENTER'       =>  'onkeypress=" if (event.keyCode==\'13\') { addToBasket( \''.$this->regionData['kod'].'\', \'goods_'.$this->activePage['id'].'\', $(\'goods_'.$this->activePage['id'].'\').value); }"',
            'GOODS_STANDART_TEXT'   =>  $standart_text
             )); 	   
					}
						else{
								$this->tpl->assign(array(
            'PROBA_CATALOG'     =>  ('<!-- АВТОКОМЛИТТЕР 	1 ЧАСТЬ	--><script src="/js/jquery.min.js" type="text/javascript"></script>
										<script>var $j = jQuery.noConflict();</script><!-- /АВТОКОМЛИТТЕР -->'),
            'GOODS_INSTRUCTION'     =>  (!empty($getInstruction['body']) ? $getInstruction['body'] : 'Инструкція не знайдена!<br>'),
            'GOODS_LIKE'            =>  '<a href="#"><img src="/img/icons.gif" width="16" height="20" alt="" /></a> <a href="#"><img src="/img/icons2.gif" width="16" height="20" alt="" /></a> <a href="#"><img src="/img/icons3.gif" width="16" height="20" alt="" /></a> <a href="#"><img src="/img/icons4.gif" width="16" height="20" alt="" /></a> <a href="#"><img src="/img/f.gif" width="74" height="20" alt="" /></a> <a href="#"><img src="/img/g.gif" width="49" height="20" alt="" /></a><br /><a href="#"><img src="/img/vk.gif" width="140" height="22" alt="" /></a>',
            'GOODS_IN_STORE'        =>  '<strong>Є в наявності</strong>',
            'GOODS_BY_PHONE'        =>  'Замовити по телефону: <span>(056) 355-55-45</span>',
            'GOODS_COST'            =>  (!empty($getGoodsCost) ? str_replace('.', ',', $getGoodsCost) . ' грн.' : ''),
            'GOODS_PRODUCER'        =>  $goodsInfo['producer'],//'Натурфарм косметика',
            'GOODS_TEXT'            =>  (!empty ($getInstruction['text'])?$getInstruction['text']:''),//'<p>Источник K+ и Mg2+, регулирует метаболические процессы, способствует восстановлению электролитного баланса, оказывает антиаритмическое действие. </p>',
            'GOODS_IMG_SMALL'       =>  $goodsImgSmall,
            'GOODS_IMG_BIG'         =>  (!empty($goodsImgBig) ? '<span class="zoom"><a href="/' . $goodsImgBig . '">Збільшити</a></span>' : ''),
            'GOODS_INPUT_DATA'      =>  'id="goods_'.$this->activePage['id'].'"',
            'GOODS_BUY_DATA'        =>  'onclick="addToBasket_ua( \''.$this->regionData['kod'].'\', \'goods_'.$this->activePage['id'].'\', $(\'goods_'.$this->activePage['id'].'\').value);"',
            'GOODS_BUY_ENTER'       =>  'onkeypress=" if (event.keyCode==\'13\') { addToBasket_ua( \''.$this->regionData['kod'].'\', \'goods_'.$this->activePage['id'].'\', $(\'goods_'.$this->activePage['id'].'\').value); }"',
            'GOODS_STANDART_TEXT'   =>  'Забронювати '.$this->activePage['name_ua'].' можна за вигідною ціною у нашій аптеці',
			'HEADER_OF_PAGE' => $this->activePage['name_ua']
             )); 
							}
    	  
    
        $this->tpl->parse('CONTENT', '.catalog_detail_body');
        
        
    	return true;
    
    }
    
    protected function drowComments($goodsArtlkul) {
                
        $this->tpl->parse('COMMENTS_LIST', 'null');
                
        $comments = $this->db->fetchAll("SELECT `id`, `fio`, `period_of_operation`, `dignity`, `shortcomings`, `recommendations`, `conclusion`, `points`, `tip_helpful`, DATE_FORMAT(`date`, '%d/%m/%Y') as `date` FROM `comments` WHERE `goods_artikul` = '$goodsArtlkul' ".(!$this->_isAdmin() ? " AND `visible` = '1' " : ''));
        
        if ($comments) {
            $counter = 0;
            
            if (count($comments) > 0) {            
                foreach ($comments as $comm) { 
                    $this->tpl->assign(
                        array('COMMENT_LIST_DATE'=>$comm['date'], 
                            'COMMENT_LIST_ADMIN'=>'<p class="right">'. $this->getAdminEdit('comments', $comm['id']).'</p>',
                            'COMMENT_LIST_CLASS'=>($counter == 0 ? 'class="first"' : ''),
                            'COMMENT_LIST_FIO'=>$comm['fio'],
                            'COMMENT_LIST_PERIOD_OF_OPERATION'=>$comm['period_of_operation'],
                            'COMMENT_LIST_DIGNITY'=>$comm['dignity'],
                            'COMMENT_LIST_SHORTCOMMINGS'=>$comm['shortcomings'], 
                            'COMMENT_LIST_RECOMENDATIONS'=>$comm['recommendations'], 
                            'COMMENT_LIST_CONCLUSION'=>$comm['conclusion'])
                        );
                    $this->tpl->parse('COMMENTS_LIST', '.comments_list');
                    $counter++;
                } 
            } else {
                
            }
        }
        
	 $captcha = new Zend_Captcha_Png(array(
             'name' => 'cptch',
             'wordLen' => 6,
             'timeout' => 1800,
         ));
        
        $fio = mysql_escape_string($this->getVar('fio', ''));
        $period_of_operation = mysql_escape_string($this->getVar('period_of_operation', ''));
        $dignity = mysql_escape_string($this->getVar('dignity', ''));
        $shortcomings = mysql_escape_string($this->getVar('shortcomings', ''));
        $recommendations = mysql_escape_string($this->getVar('recommendations', ''));
        $conclusion = mysql_escape_string($this->getVar('conclusion', ''));
        $captchaId = $this->getVar('captcha_id', '');
        $captchaInput = $this->getVar('captcha_input', '');
        
        if (!empty($_POST)) {
            $captchaSession = new Zend_Session_Namespace('Zend_Form_Captcha_'  . $captchaId);
            $captchaIterator = $captchaSession->getIterator();
        
            @$captchaWord = $captchaIterator['word'];
            
			
			if ($_COOKIE[ua]!=1){

			if (empty($fio)) {
                $this->addErr('Поле "Имя и фамилия" не должно быть пустым');
            }
            
            $validate = new Zend_Validate_EmailAddress();
	    
            
            if (empty($period_of_operation)) {
                $this->addErr('Поле "Период эксплуатации" не должно быть пустым');
            }
            
            if (empty($dignity)) {
                $this->addErr('Поле "Достоинства" не должно быть пустым');
            }
            
            if (empty($shortcomings)) {
                $this->addErr('Поле "Недостатки" не должно быть пустым');
            }
            
            if (empty($recommendations)) {
                $this->addErr('Поле "Рекомендации" не должно быть пустым');
            }
            
            if (empty($conclusion)) {
                $this->addErr('Поле "Вывод" не должно быть пустым');
            }
            
            
            if (!$captchaWord) {
                $this->addErr('Проверочный код устарел. Пожалуйста введите код заново');
            } else {
                if (!$captchaInput) {
                    $this->addErr('Вы не ввели проверочный код');
                } else {
                    if ($captchaInput != $captchaWord) $this->addErr('Ошибка. Введите проверочнй код повторно');
                }
            }
		}
			else{
                   if (empty($fio)) {
                $this->addErr('Поле "Им’я и Прізвище" повинно бути заповненим');
            }
            
            $validate = new Zend_Validate_EmailAddress();
	    
            
            if (empty($period_of_operation)) {
                $this->addErr('Поле "Період эксплуатації" повинно бути заповненим');
            }
            
            if (empty($dignity)) {
                $this->addErr('Поле "Переваги" повинно бути заповненим');
            }
            
            if (empty($shortcomings)) {
                $this->addErr('Поле "Недоліки" повинно бути заповненим');
            }
            
            if (empty($recommendations)) {
                $this->addErr('Поле "Рекомендації" повинно бути заповненим');
            }
            
            if (empty($conclusion)) {
                $this->addErr('Поле "Вивод" повинно бути заповненим');
            }
            
            
            if (!$captchaWord) {
                $this->addErr('Код перевірки застарів. Будь ласка введіть код заново');
            } else {
                if (!$captchaInput) {
                    $this->addErr('Ви не ввели перевірочний код');
                } else {
                    if ($captchaInput != $captchaWord) $this->addErr('Помилка. Введіть код перевірки повторно');
                }
            }
				}
                
        }
	
        
        $captcha->setFont('./Zend/Captcha/Fonts/ANTIQUA.TTF');
        $captcha->setStartImage('./img/captcha.png');
        $id = $captcha->generate();
        
        if (!empty($_POST) && $this->_err) {
            $this->viewErr();
        } 
        
        if (!empty($_POST) && !$this->_err) {
            $data = array(
                'date'=>date('Y-m-d'),
                'goods_artikul'=>$goodsArtlkul,
                'fio'=>$fio,
                'period_of_operation'=>$period_of_operation,
                'dignity'=>$dignity,
                'shortcomings'=>$shortcomings,
                'recommendations'=>$recommendations,
                'conclusion'=>$conclusion,
            );
           $this->db->insert('comments', $data); 
           $referrer = "";
           if (isset($_SERVER['REQUEST_URI'])) {
               $referrer = $_SERVER['REQUEST_URI'];
           }
		   
		   if ($_COOKIE[ua]!=1){
           $content = "<h2>Ваш отзыв добавлен</h2> <meta http-equiv='refresh' content='3;URL=$referrer' />";
		}
			else{
                $content = "<h2>Ваш відгук додано</h2> <meta http-equiv='refresh' content='3;URL=$referrer' />";
				}
  
           $this->viewMessage($content);
           $this->tpl->parse('COMMENTS', 'null');
        }
        
        if (!empty($_POST) && !$this->_err) {
            
            $fio = '';
            $dignity = '';
            $period_of_operation = '';
            $shortcomings = '';
            $recommendations = '';
            $conclusion = '';
        }
        
        $this->tpl->assign(array(
						'PROBA_CATALOG'     =>  (''),
                'CAPTCHA_ID'=> $id,
                'COMMENT_FIO'=>$fio,
                'COMMENT_PERIOD_OF_OPERATION'=>$period_of_operation,
                'COMMENT_DIGNITY'=>$dignity,
                'COMMENT_SHORTCOMMINGS'=>$shortcomings,
                'COMMENT_RECOMENDATIONS'=>$recommendations,
                'COMMENT_CONCLUSION'=>$conclusion
            ));
        
        
        // $this->tpl->parse('CATALOG_DETAIL_BODY', '.comments');
    }
    
    
    /*
     * Поиск по первой букве
     */
    public function listABC(){
        
        $start = 0;
        $navbar = $navTop = $navBot = '';
        $pagePagen = 1;
        $pagenHref = NULL;
        $navTop = '';
        $navBot = '';
        $listABCPageLength =  100;//$this->settings['num_catalog'];
        $url = end($this->url); 
        $visibleQuery = '';
        if (!$this->_isAdmin()) {
            $visibleQuery = " AND `visibility` = '1'";
        }
        $url = htmlspecialchars($url);
        
					if ($_COOKIE[ua]!=1){
						if ($url=='0-9'){
							$like = "WHERE `name` LIKE '0%' OR `name` LIKE '1%' OR `name` LIKE '2%' OR `name` LIKE '3%' OR `name` LIKE '4%' OR `name` LIKE '5%' OR `name` LIKE '6%' OR `name` LIKE '7%' OR `name` LIKE '8%' OR `name` LIKE '9%'";
						} elseif ($url=='A-Z'){
							$like = " WHERE ";
							for($i = "A"; $i <= "Z"; $i++) {
								$like .= " `name` LIKE '$i%' OR ";
							}
							$like .= "`name` LIKE 'Z%'";
				//$like = "WHERE `name` LIKE 'A%' OR `name` LIKE 'B%' OR `name` LIKE '2%' OR `name` LIKE '3%' OR `name` LIKE '4%' OR `name` LIKE '5%' OR `name` LIKE '6%' OR `name` LIKE '7%' OR `name` LIKE '8%' OR `name` LIKE '9%'";
						} else {
							$url = substr($url, 0, 2);
							$like = "WHERE `name` LIKE '".$url."%'";
						}
					}
					else{
						if ($url=='0-9'){
							$like = "WHERE `name_ua` LIKE '0%' OR `name_ua` LIKE '1%' OR `name_ua` LIKE '2%' OR `name_ua` LIKE '3%' OR `name_ua` LIKE '4%' OR `name_ua` LIKE '5%' OR `name_ua` LIKE '6%' OR `name_ua` LIKE '7%' OR `name_ua` LIKE '8%' OR `name_ua` LIKE '9%'";
						} elseif ($url=='A-Z'){
							$like = " WHERE ";
							for($i = "A"; $i <= "Z"; $i++) {
								$like .= " `name_ua` LIKE '$i%' OR ";
							}
							$like .= "`name_ua` LIKE 'Z%'";
				//$like = "WHERE `name` LIKE 'A%' OR `name` LIKE 'B%' OR `name` LIKE '2%' OR `name` LIKE '3%' OR `name` LIKE '4%' OR `name` LIKE '5%' OR `name` LIKE '6%' OR `name` LIKE '7%' OR `name` LIKE '8%' OR `name` LIKE '9%'";
						} else {
							$url = substr($url, 0, 2);
							$like = "WHERE `name_ua` LIKE '".$url."%' AND `type`!='section'";
						}
					}
        
        $count = $this->db->fetchOne("SELECT COUNT(`id`) FROM `".$this->regionData['kod']."_catalog` $like $visibleQuery");
        if ($count > 0) {
        	if ($count > $listABCPageLength) {
            	if (isset($this->getParam['page'])) {
                	$pagePagen = (int) $this->getParam['page'];
                    $start = $listABCPageLength * $pagePagen - $listABCPageLength;
                    if ($start > $count) {
                    	$start = 0;
                    }
                }
                $pagenParams = '';
                
                $navbar = $this->loadPaginator((int) ceil($count/$listABCPageLength), (int) $pagePagen, '/'.$this->regionData['url'].'/catalog/listABC/'.$url.'/'.$pagenHref.$pagenParams );
                if ($navbar) {
                	$navTop = '<div class="pager2 pager2_center">' . $navbar . '</div>';
                    $navBot = $navTop ;
                }
           }
        } 
    	
       $ua=$this->getUa(); 
        
		if ($_COOKIE[ua]!=1){
                    $this->setMetaTags("Поиск: $url");
					$this->setWay("Поиск: $url");
		}
			else{
                $this->setMetaTags("Пошук: $url");
				$this->setWay("Пошук: $url");
				}
				

        $this->tpl->define_dynamic('_listabc', "$ua".'listABC.tpl');
        $this->tpl->define_dynamic('listabc', '_listabc');
        $this->tpl->define_dynamic('listabc_list', 'listabc');
        $this->tpl->define_dynamic('listabc_header', 'listabc');

        
        //var_dump ($this->url);

     	if ($_COOKIE[ua]!=1){
			$ABCList = $this->db->fetchAll('SELECT * FROM `'.$this->regionData['kod'].'_catalog` '.$like.'  ORDER BY `name` LIMIT '.$start.', '.$listABCPageLength);
		
				if ($ABCList) {
					$this->tpl->parse('LISTABC_EMPTY', 'null');

					foreach ($ABCList as $ABC) {

		 

						$this->tpl->assign(array(
						'PROBA_CATALOG'     =>  (''),
							'LISTABC_ID'            =>  $ABC['id'],
							'LISTABC_ART'           =>  $ABC['artikul'],
							'LISTABC_NAME'          =>  $ABC['name'],
							'LISTABC_PRODUCER'      =>  $ABC['producer'],
							'LISTABC_REGION_KOD'    =>  $this->regionData['kod'],
							'LISTABC_COST'          =>  number_format($ABC['cost'], 2, ',', ' '),
							'LISTABC_URL'           =>  $this->urlAbc($ABC['id']),
						));

						$this->tpl->parse('LISTABC_LIST', '.listabc_list');
					}
				} else {
					$this->tpl->parse('LISTABC_LIST', 'null');
					$this->tpl->parse('LISTABC_HEADER', 'null');
					
				}
		
		}
		else{
			$ABCList = $this->db->fetchAll('SELECT * FROM `'.$this->regionData['kod'].'_catalog` '.$like.'  ORDER BY `name_ua` LIMIT '.$start.', '.$listABCPageLength);
		
		if ($ABCList) {
					$this->tpl->parse('LISTABC_EMPTY', 'null');

					foreach ($ABCList as $ABC) {

		 

						$this->tpl->assign(array(
						'PROBA_CATALOG'     =>  (''),
							'LISTABC_ID'            =>  $ABC['id'],
							'LISTABC_ART'           =>  $ABC['artikul'],
							'LISTABC_NAME'          =>  $ABC['name_ua'],
							'LISTABC_PRODUCER'      =>  $ABC['producer'],
							'LISTABC_REGION_KOD'    =>  $this->regionData['kod'],
							'LISTABC_COST'          =>  number_format($ABC['cost'], 2, ',', ' '),
							'LISTABC_URL'           =>  $this->urlAbc($ABC['id']),
						));

						$this->tpl->parse('LISTABC_LIST', '.listabc_list');
					}
				} else {
					$this->tpl->parse('LISTABC_LIST', 'null');
					$this->tpl->parse('LISTABC_HEADER', 'null');
					
				}
		}
		

$this->tpl->assign(array('PROBA_CATALOG'     =>  (''), 'TOP_NAV_BAR'=>$navTop, 'BOTT_NAV_BAR'=>$navBot));			
        $this->tpl->parse('CONTENT', 'listabc');

        return true;
    }
    protected function urlAbc($id){
        $res2 = NULL;
        $res3 = NULL;
        $res = $this->db->fetchRow("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `id`='$id'");
        if ($res['level']!=0){
            $res2 = $this->db->fetchRow("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `id`='".$res['level']."'");
        }
        if ($res2['level']!=0){
            $res3 = $this->db->fetchRow("SELECT * FROM `".$this->regionData['kod']."_catalog` WHERE `id`='".$res2['level']."'");
        }
        return '/'.$this->regionData['url'].'/catalog/'.$res3['href'].'/'.$res2['href'].'/'.$res['href'].'/';
    }
}