<?php
// Buyme script 2013 by Nazar Tokar
// www.dedushka.org * www.nazartokar.com * nazartokar@gmail.com

header("Content-Type: text/html; charset=utf-8"); //charset

//адрес почты для отправки уведомления
$to = "zakaz@ikwell.dp.ua"; //несколько ящиков могут перечисляться через запятую
$from = "service@".($_SERVER["HTTP_HOST"]); //адрес, от которого придёт уведомление, можно не трогать

// данные для отправки смс

$id = "";
$key = "";
$sms_login = "";
$sms_pass = "";
$frm = "callme"; // добавьте новую подпись в смс-шлюзе и дождитесь апрува
$num = ""; // ваш номер в формате без + (f.e. 380501112233 или 79218886622)
$prv = "sms.ru"; // на выбор: sms.ru, infosmska.ru, bytehand.com, sms-sending.ru, smsaero.ru

function uc($s){
	$s = urlencode($s);
	return $s;
}

function gF($s){ // no shit
	$s = substr((htmlspecialchars($_GET[$s])), 0, 500);
	if (strlen($s)>1) return $s;
}

switch (gF("lng")) { //languages
	case "ru": $ln = 0; break;
	case "ua": $ln = 1; break;
	case "en": $ln = 2; break;
	case "":   $ln = 0; break;
}

function sendSMS($to, $msg){
	global $id;
	global $key;
	global $from;
	global $frm;
	global $num;
	global $prv;
	global $sms_login;
	global $sms_pass;
	
	$u['sms.ru'] = "sms.ru/sms/send?api_id=".uc($key)."&to=".uc($num)."&text=".uc($msg);
	$u['bytehand.com'] = "bytehand.com:3800/send?id=".uc($id)."&key=".uc($key)."&to=".uc($num)."&partner=callme&from=".uc($frm)."&text=".uc($msg);
	$u['sms-sending.ru'] = "lcab.sms-sending.ru/lcabApi/sendSms.php?login=".uc($sms_login)."&password=".uc($sms_pass)."&txt=".uc($msg)."&to=".uc($num);
	$u['infosmska.ru'] = "api.infosmska.ru/interfaces/SendMessages.ashx?login=".uc($sms_login)."&pwd=".uc($sms_pass)."&sender=SMS&phones=".uc($num)."&message=".uc($msg);
	$u['smsaero.ru'] = "gate.smsaero.ru/send/?user=".uc($sms_login)."&password=".md5(uc($sms_pass))."&to=".uc($num)."&text=".uc($msg)."&from=".uc($frm);
	
	$r = file_get_contents("http://".$u[$prv]);	
	//echo "<br>".$u[$prv]."<br>\n\n".$r;
}

$l[0]["sent"] = "Заказ только что был отправлен";
$l[1]["sent"] = "Замовлення тільки що було відправлено";
$l[2]["sent"] = "Your offer has been just receipt";

$l[0]["err"] = "Пожалуйста, заполните все поля";
$l[1]["err"] = "Будь ласка, заповніть всі поля";
$l[2]["err"] = "Please fill in all the fields";

$l[0]["ok"] = "Спасибо, заказ принят. Ждите звонка";
$l[1]["ok"] = "Дякуємо, замовлення прийнято. Чекайте на дзвінок";
$l[2]["ok"] = "Thanks, your offer has been confirmed. Wait for our call";

$l[0]["title"] = "Купить в один клик: новый заказ";
$l[1]["title"] = "Купить в один клик: нове замовлення";
$l[2]["title"] = "Купить в один клик: new offer";

$l[0]["name"] = "Имя";
$l[1]["name"] = "Ім'я";
$l[2]["name"] = "Name";

$l[0]["contact"] = "Контакт";
$l[1]["contact"] = "Контакт";
$l[2]["contact"] = "Contact";

$l[0]["comment"] = "Точка самовивоза";
$l[1]["comment"] = "Точка самовивозу";
$l[2]["comment"] = "Point of export";

$l[0]["footer"] = "<a href='http://dedushka.org/tag/buyme/'>Следите</a> за обновлениями скрипта.<br>Спасибо за использование BuyMe.";
$l[1]["footer"] = "<a href='http://dedushka.org/tag/buyme/'>Слідкуйте</a> за оновленнями скрипта.<br>Дякую за користування BuyMe.";
$l[2]["footer"] = "<a href='http://dedushka.org/tag/buyme/'>Follow</a> the script renewals.<br>Thanks for using BuyMe.";

function getOptions($o){ // get product options
	$captions = $_GET["captions"];
	$options = $_GET["options"];
	$i = 0;	
	if ($o == 1) {
		foreach ($options as $value) {
			if($value != 0) {
				$opts .= "<b>".$captions[$i]."</b>:<br>".$value."<br><br>";
			}
			$i++;
		}
	} else {
		foreach ($options as $value) {
			if($value != 0) {
				$opts .= $captions[$i]."(".$value.") ";
			}
			$i++;
		}		
	}
	return $opts;
}

// translit by programmerz.ru
function translit($str){
	$tr = array("А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D","Е"=>"E","Ё"=>"E","Ж"=>"J","З"=>"Z","И"=>"I","Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N","О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"C","Ч"=>"CH","Ш"=>"SH","Щ"=>"SC","Ъ"=>"","Ы"=>"Y","Ь"=>"","Э"=>"E","Ю"=>"U","Я"=>"YA","а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"j","з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l","м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r","с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h","ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y","ы"=>"y","ь"=>"","э"=>"e","ю"=>"u","я"=>"ya");
	return strtr($str,$tr);
}

header("Content-Type: text/html; charset=utf-8"); //charset

// далее можно не трогать

$time = time(); // время отправки
$interval = $time - gF("time");
if ($interval < 10) { // если прошло менее (сек)
	$result = "err";
	$cls = "b1c_err";
	$time = "";
	$message = $l[$ln]["sent"];
} else {

if (gF("cnt")){ // data to send
	$ip = $_SERVER['REMOTE_ADDR']; 
	$prd = gF("prd");
	$contact = gF("cnt");
	$name = gF("nam");
	$comment = gF("cmt");
	$url = gF("url");

	$title = $l[$ln]["title"];
	$mess = "<p>Заказ на ".$prd."</p>
	<p><b>".$l[$ln]["contact"]."</b><br>".$contact."</p>
	<p><b>".$l[$ln]["name"]."</b><br>".$name."</p>
	<p>".(getOptions(1))."</p>
	<p><b>".$l[$ln]["comment"]."</b><br>".$comment."</p>";

	$mess = $mess."<p><b>URL</b><br>".$url."</p>
	<p><b>ip</b><br>".$ip."<hr>".$l[$ln]["footer"];
	
	$headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
	$headers .= "From: BuyMe 1.2.0 <".$from.">\r\n"; 

$msg_sms  = translit($prd."*".(getOptions(0))."*".$name."(".$contact.")");
$msg_sms .= substr(translit($comment), 0, (160-strlen($msg_sms)));

@mail($to, $title, $mess, $headers);
	$result = "ok";
	$cls = "b1c_ok";
	$message = $l[$ln]["ok"]; //сообщение об отправке
	if (($id!="")||($key!="")||($sms_login!="")) { 
		@sendSMS($num, $msg_sms); 
	}
} else {
	$result = "err";
	$cls = "b1c_err";
	$time = "";
	$message = $l[$ln]["err"];
}
}
?>{
"result": "<? echo $result; ?>",
"cls": "<? echo $cls; ?>",
"time": "<? echo $time; ?>",
"message": "<? echo $message; ?>"
}