/**
* jQuery.Storage: Dave Schindler, 2010
* jQuery.Buyme: Nazar Tokar, 2013
**/

if(typeof b1c_lang  == "undefined") { var b1c_lang  = "ru"; } // language

(function(jQuery) {
	var isLS=typeof window.localStorage!=='undefined';
	function wls(n,v){var c;if(typeof n==="string"&&typeof v==="string"){localStorage[n]=v;return true;}else if(typeof n==="object"&&typeof v==="undefined"){for(c in n){if(n.hasOwnProperty(c)){localStorage[c]=n[c];}}return true;}return false;}
	function wc(n,v){var dt,e,c;dt=new Date();dt.setTime(dt.getTime()+31536000000);e="; expires="+dt.toGMTString();if(typeof n==="string"&&typeof v==="string"){document.cookie=n+"="+v+e+"; path=/";return true;}else if(typeof n==="object"&&typeof v==="undefined"){for(c in n) {if(n.hasOwnProperty(c)){document.cookie=c+"="+n[c]+e+"; path=/";}}return true;}return false;}
	function rls(n){return localStorage[n];}
	function rc(n){var nn, ca, i, c;nn=n+"=";ca=document.cookie.split(';');for(i=0;i<ca.length;i++){c=ca[i];while(c.charAt(0)===' '){c=c.substring(1,c.length);}if(c.indexOf(nn)===0){return c.substring(nn.length,c.length);}}return null;}
	function dls(n){return delete localStorage[n];}
	function dc(n){return wc(n,"",-1);}
	jQuery.extend({Storage: {
	set: isLS ? wls : wc,
	get: isLS ? rls : rc,
	remove: isLS ? dls :dc
	}
	});
})(jQuery);

(function buyMe(){

var bn, bg;
var bc = []; // captions array
var bo = []; // options array

function anim(o,i,t){
	jQuery(o).animate({ opacity: i }, t);
} // opacity animate

function dl(f,t){
	var t = t * 1000;
	setTimeout(function(){
		eval(f+"()");
	}, t); 
} // delay

function clearForm(){ 
	jQuery("#b1c_nme").val(jQuery.Storage.get('b1c_nme'));
	jQuery("#b1c_cnt").val(jQuery.Storage.get('b1c_cnt'));
} 

function showForm(){
	jQuery(".b1c_form").fadeToggle("fast");
	jQuery(".b1c_back").fadeToggle("fast");
	jQuery(".b1c_back").height(jQuery(document).height());
	jQuery(".b1c_result").html("");
	clearForm();
} 

function hideForm(){
	jQuery(".b1c_form").fadeOut("fast");
	jQuery(".b1c_back").fadeOut("fast");
	jQuery(".b1c_result").html("");
	clear();
} 

function sendForm() {
	jQuery.Storage.set("b1c_nme", jQuery("#b1c_nme").val());
	jQuery.Storage.set("b1c_cnt", jQuery("#b1c_cnt").val());

	var cnt = jQuery.Storage.get('b1c_sent'); // last sent time
	if (!cnt) { 
		cnt = 0; 
	}

	jQuery.getJSON("/buyme/index.php", {
		contentType: "text/html; charset=utf-8",
		prd: bn,
		nam: jQuery("#b1c_nme").val(),
		cnt: jQuery("#b1c_cnt").val(),
		cmt: jQuery("#b1c_msg").val(),
		url: location.href,
		lng: b1c_lang,
		time: cnt, 
		'options[]':  bo,
		'captions[]': bc
	}, function(data) {
		jQuery(".b1c_result").html("<div class='" + data.cls + "'>" + data.message +"</div>");
		if (data.result == "ok") {
			jQuery.Storage.set("b1c_sent", data.time);
			jQuery.Storage.set("b1c_nme", jQuery("#b1c_nme").val());
			jQuery.Storage.set("b1c_cnt", jQuery("#b1c_cnt").val());
			dl('hideForm',7);
			dl('clearForm',10);
		}
	});
}

jQuery.get("/buyme/lang/" + b1c_lang + ".php", function(data) {
	jQuery("body").append(data);
	jQuery(".b1c_send").css("display", "none");
}); // insert form

jQuery(document).on("click", "#b1c", function(){
	jQuery(".b1c_send").fadeIn("fast");
	sendForm();
	return false;
}); //send data

jQuery(document).on("click", ".b1c", function(){
	bg = jQuery(this).closest(".bm_good");
	bn = jQuery(bg).find(".b1c_name").html();

	bc = [0];
	bo = [0]; 

	//============= КОСТЫЛЬ ТОЛЬКО ДЛЯ САЙТА АПТЕК	<START>
	
	nnn=bn.indexOf('</hc>');
	nnn=nnn + 5;
	
	bn=bn.substr(nnn);
	//alert(bn);
	
	//=============  /КОСТЫЛЬ ТОЛЬКО ДЛЯ САЙТА АПТЕК	<END>
																	
	if (jQuery(bg).find(".b1c_caption").length){
		jQuery(bg).find(".b1c_caption").each(function() {
			bc.push(jQuery(this).html());
		});
	}

	if (jQuery(bg).find(".b1c_option").length){
		jQuery(bg).find(".b1c_option").each(function() {
			bo.push(jQuery(this).find(':selected').text());
		});
	}

	jQuery("#b1c_caption").html(bn);
	showForm();
	return false; 
});

jQuery(document).on("mouseover", ".b1c_close", function(){
	anim(this, 0.8, 120);
}).on("mouseleave", ".b1c_close", function(){
	anim(this, 1, 100);
}).on("click", ".b1c_close", function(){
	showForm();
	return false;
}); // close button

jQuery(document).on("click", ".b1c_back", function(){
	hideForm();
});

jQuery(document).keyup(function(e) {
	if ( (jQuery(".b1c_form").is(":visible")) && (e.keyCode == 27) ) {
		hideForm();
	}
}); // esc

})();