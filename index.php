<?php

//print crypt('adminapteka', 'tEXFVrqY'); die;
//print crypt('developer', ''); die;
date_default_timezone_set('Europe/Kiev');
function microtime_float()
{
   list($usec, $sec) = explode(" ", microtime());
   return ((float)$usec + (float)$sec);
}

//ЯЗЫК

setcookie("reg", "$_SERVER[REQUEST_URI]");

if ($_GET[lang_cook]==1){setcookie("ua", "1");	header("location: /");}
if (isset($_GET[lang_cook]) && $_GET[lang_cook]==0 && $_COOKIE[ua]==1){setcookie("ua", '1', time() - 3600);	header("location: /");}
//-------------

$time_start = microtime_float();
ini_set('display_errors', 'off');

define("PATH", $_SERVER['DOCUMENT_ROOT']."/");

set_include_path(implode(PATH_SEPARATOR, array(
            PATH . 'excel/',
            get_include_path(),
        )));


require_once PATH . 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader(true);
$config = array();

require_once PATH . 'config/config.php';

$config = new Zend_Config($config, true);

/*try {
    $database = Zend_Db::factory($config->database);
    $database->getConnection();
} catch (Zend_Db_Adapter_Exception $e) {
    // возможно, неправильные параметры соединения или СУРБД не запущена
} catch (Zend_Exception $e) {
    // возможно, попытка загрузки требуемого класса адаптера потерпела неудачу
}*/

Zend_Session::start();
require_once PATH . 'library/Init.php';

$init = new Init($config);


$time_end = microtime_float();
$time = $time_end - $time_start;
$profiler = $init->getProfiler();

echo "<!--Time of Scripting: ".(round($time, 5))." seconds -->";
echo "<!--Time of Query length: ".$profiler->getTotalNumQueries()." Time: ".$profiler->getTotalElapsedSecs()."  seconds -->";
//var_dump($profiler->getQueryProfiles());


$profiler->clear();

