<?php

ini_set('display_errors', 'on');

define("PATH", $_SERVER['DOCUMENT_ROOT']."/");

require_once PATH . 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader(true);

Zend_Session::start();

$goodsSession = new Zend_Session_Namespace('goods');

require_once PATH . 'library/Templates.php';

$templates = new Templates('tpl/');

$discount = 0;
$isAction = false;


if (isset($_POST['item_id']) && isset($_POST['item_count']) ) {
	
	require_once PATH . 'config/config.php';
	$config = new Zend_Config($config, true);

	
	
    $id = htmlspecialchars(addslashes($_POST['item_id']));
    $count = htmlspecialchars(addslashes($_POST['item_count']));
    $regionKod = htmlspecialchars(addslashes($_POST['region_kod']));
    if (empty($regionKod)){
        $regionKod='04';
    }
    
    if (!is_numeric($count) || $count <= 0) {
    	$count = -1;
    }
    
    $id = explode('_', $id);
    
    if (sizeof($id) != 2) {
        return;
    }
    
    if ($id[0] !== 'goods') {
        return;
    }
    
    $id = (int) $id[1];
    
    if ($id && $count) {
        
    	
		try {
			$database = Zend_Db::factory($config->database);
    		$database->getConnection();
		} catch (Zend_Db_Adapter_Exception $e) {
			throw new Exception('возможно, неправильные параметры соединения или СУРБД не запущена');
		} catch (Zend_Exception $e) {
    		throw new Exception('возможно, попытка загрузки требуемого класса адаптера потерпела неудачу');
		}
    	
		$zendSessionNameSpace = new Zend_Session_Namespace('Zend_Auth');		
    	
        $item = $database->fetchRow("SELECT * FROM `".$regionKod."_catalog` WHERE `id` = '$id'");
       $regionData = $database->fetchRow("SELECT * FROM `regions` WHERE `kod` = '$regionKod'");
        if ($item) {
        	//$isAction = ($item['action'] == '1');
        	$cost = (float)$item['cost'];
    		$cost1 = $cost;    		
        	
    	
        
            $goodsSession->array[$id.$regionKod] = array(
                'id' => $id,
                'href' => $item['href'],
                'count' => ($count == -1 ? 1 : $count),
                'artikul' => $item['artikul'],
                'name' => $item['name'],
                'cost' => number_format($cost, 2, '.', ''),
                'status'=>'active',
                'region'=>$regionKod,
                //'rialCost' =>$item['cost'],
                //'discountCost'=>number_format($cost, 2, '.', ''),
                //'producer' => $item['producer'],              
                //'weight' => $item['weight'],
                //'unit' => $item['unit'],
                //'group' => $item['group'],
              //  'action' => $item['action']
            );
           
        }
    }	
}


//$templates->define_dynamic('cart', 'cart.tpl');


$summ = 0;
$count = 0;

$isBasketEmpty = true; // Если в корзине нет даже удаленных товаров. 

if (!empty($goodsSession->array)) {
    foreach ($goodsSession->array as $index=>$basket) {   	
        if (isset($basket['status']) && $basket['status'] == 'active' && $basket['region']==$regionKod) {
        	
        	if (is_numeric($basket['count']) && $basket['count'] > 0) {
        	
        		$summ +=  ( $basket['cost']  * $basket['count']);   
        		$count ++;        		
        		$count += ($basket['count'] - 1);
        	} else {
        		
        		$goodsSession->array[$index]['status'] = 'deleted';
        	}
        	
    	}	
    	$isBasketEmpty = false;
    }
}

if ($summ > 0) {
	$goodsSession->array[$index]['summ']  = $summ;
}

	
		if ($_COOKIE[ua]!=1){
		
				if ($count <= 0) {

					$templates->define_dynamic('cart', 'cart_empty.tpl');
					$templates->assign(array('BASKET_URL' => ($isBasketEmpty) ? '<span class="basket-empty-text">Корзина</span>' : '<a href="/basket">Корзина</a>' ));
				} else {
					$templates->define_dynamic('cart', 'cart.tpl');
				}
				
		}
		else{

		if ($count <= 0) {

					$templates->define_dynamic('cart', 'ua_cart_empty.tpl');
					$templates->assign(array('BASKET_URL' => ($isBasketEmpty) ? '<span class="basket-empty-text">Корзина</span>' : '<a href="/basket">Корзина</a>' ));
				} else {
					$templates->define_dynamic('cart', 'ua_cart.tpl');
				}
		
		}

//if ($discount && $discount > 0 ) {
//	$discount = ($summ / 100) * $discount;
//	
//	if (!$isAction) {
//		$summ -= $discount;			
//	}
//	
//	print "$summ<br>";
//}



if (isset($_POST['ret_type']) && $_POST['ret_type'] == 'json') {
	print json_encode($goodsSession->array);
	die;
}
$templates->assign(array('BASKET_COUNT' => $count, 'BASKET_SUMM' => number_format($summ, 2, ',', ' '), 'BASKET_URL' => '/'.$regionData['url'].'/basket/'));


$templates->parse('CART', 'cart');
$templates->prnt();
