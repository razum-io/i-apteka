jQuery(function() {
    jQuery('#choose').toggle(function () {
		jQuery('body').click(function(){
			jQuery('.region_list').slideUp('medium');		
		});
		jQuery('.region_list').slideToggle('slow');
    },
	function () {
		jQuery('body').click(function(){
			jQuery('.region_list').slideUp('medium');		
		});
		jQuery('.region_list').slideToggle('slow');
    }
    );
	var myEmail = jQuery('#email');
	myEmail.focus(function() { if (jQuery(this).val() == 'Поиск препарата по названию') {jQuery(this).val('');} });
	myEmail.blur(function() { if (jQuery(this).val() == '') {jQuery(this).val('Поиск препарата по названию');} });
	jQuery(".top_banner_list li:last").addClass("last");
	jQuery(".main_menu li:first").addClass("first");
	jQuery(".main_menu li").prepend("<span>&nbsp;</span>");
	jQuery(".main_menu li::nth-child(2n)").addClass("link2");
	jQuery(".main_menu li::nth-child(3n)").addClass("link3");
	jQuery(".main_menu li::nth-child(4n)").addClass("link4");
	jQuery(".main_menu li::nth-child(5n)").addClass("link5");
	jQuery(".main_menu li::nth-child(6n)").addClass("link6");
	jQuery(".main_menu li:last").addClass("last");
	jQuery(".main_menu li li:first").removeClass("first");
	jQuery(".main_menu li li::nth-child(2n)").removeClass("link2");
	jQuery(".main_menu li li::nth-child(3n)").removeClass("link3");
	jQuery(".main_menu li li::nth-child(4n)").removeClass("link4");
	jQuery(".main_menu li li::nth-child(5n)").removeClass("link5");
	jQuery(".main_menu li li::nth-child(6n)").removeClass("link6");
	jQuery(".main_menu li li:last").removeClass("last");
	jQuery(".left_column input.button, .right_column .type_elem input.button").hover(function() {jQuery(this).addClass('button-hover');},function() {jQuery(this).removeClass('button-hover');});
	jQuery(".left_column input.button, .right_column .type_elem input.button").focus(function() {jQuery(this).addClass('button-focus');}).click(function() {jQuery(this).addClass('button-focus');});
	jQuery(".left_column input.button, .right_column .type_elem input.button").blur(function() {jQuery(this).removeClass('button-focus');});
	jQuery(".goods_list li:nth-child(4n)").addClass("last");
	jQuery('.goods_list').append('<div class="nobord">&nbsp;</div>');
	jQuery(".product_table tr").find('td:first').addClass("first");
	jQuery(".product_table tr").find('th:first').addClass("first");
	jQuery(".product_table tr").find('th:nth-child(3n)').addClass("center");
	jQuery(".product_table tr").find('td:nth-child(3n)').addClass("center");
	jQuery(".product_table tr").find('td:nth-child(4n)').addClass("inp");
	jQuery(".product_table tr").find('td:nth-child(5n)').addClass("last");
	jQuery(".basket_table tr td:nth-child(3n), .basket_table tr td:nth-child(5n), .basket_table tr th:nth-child(3n), .basket_table tr th:nth-child(5n)").addClass("right");
	jQuery(".basket_table tr td:nth-child(2n)").addClass("now");
	jQuery(".basket_table tr").find('td:last').addClass("last");
	jQuery(".basket_table tr th:last").addClass("last");
	jQuery(".basket_table2 tr td:nth-child(4n), .basket_table2 tr td:nth-child(6n), .basket_table2 tr th:nth-child(4n), .basket_table2 tr th:nth-child(6n)").addClass("right");
	jQuery(".basket_table2 tr td:nth-child(3n)").addClass("now");
	jQuery(".basket_table2 tr td:nth-child(5n)").addClass("center");	
	if (jQuery.browser.msie && jQuery.browser.version == 7) {
	    jQuery('.header .search_block input').css({'line-height':'25px'});
    	jQuery('.header .search_block input.button').css({'line-height':'18px'});
    	jQuery('.right_column .type_elem input').css({'height':'20px','padding-top':'5px'});
    	jQuery('.right_column .type_elem input.button').css({'height':'34px','padding-top':'2px'});
    	jQuery('.right_column .type_elem .goods_list ul li .buy input, .right_column .type_elem .product_table table td input, .right_column .type_elem .basket_table table td input, .right_column .type_elem .basket_table2 table td input').css({'height':'16px','padding-top':'2px'});
    	jQuery('.right_column .type_elem .product_full .desc .price p input').css({'height':'18px','padding-top':'2px'});
	}
}); 
   