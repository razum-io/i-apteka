jQuery(function() {
	// Use this example, or...
	jQuery('a[rel*=lightbox]').lightBox({
		overlayBgColor: '#878787',
		overlayOpacity: 0.6,
		imageLoading: '/img/jquery-lightbox/lightbox-ico-loading.gif',
		imageBtnClose: '/img//jquery-lightbox/lightbox-btn-close.gif',
		imageBtnPrev: '/img/jquery-lightbox/prevlabel.gif',
		imageBtnNext: '/img/jquery-lightbox/nextlabel.gif',
		containerResizeSpeed: 350,
		txtImage: 'Imagem',
		txtOf: 'de'

	});
	
	jQuery('a[rel*=lbox]').lightBox({
		overlayBgColor: '#878787',
		overlayOpacity: 0.6,
		imageLoading: '/img/jquery-lightbox/lightbox-ico-loading.gif',
		imageBtnClose: '/img//jquery-lightbox/lightbox-btn-close.gif',
		imageBtnPrev: '/img/jquery-lightbox/lightbox-btn-prev.gif',
		imageBtnNext: '/img/jquery-lightbox/lightbox-btn-next.gif',
		containerResizeSpeed: 350,
		txtImage: 'Imagem',
		txtOf: 'de'

	});
	

	// Select all links that contains lightbox in the attribute rel
	
	
});