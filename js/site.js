jQuery(document).ready(function(){
    jQuery("tr").hover(function(){
        jQuery(this).addClass("hovered");
        jQuery(this).prev("tr").addClass("pre_hovered");
    }, function(){
        jQuery(this).removeClass("hovered");
        jQuery(this).prev("tr").removeClass("pre_hovered");
    })
    jQuery(".goods_img li").hover(function(){
        var ind = jQuery(this).index();
        jQuery(this).parent().next("ul").find("li").eq(ind).addClass("hovered_good");
    }, function(){
        var ind = jQuery(this).index();
        jQuery(this).parent().next("ul").find("li").eq(ind).removeClass("hovered_good");
    })
    jQuery(".put_to_basket").hover(function(){
        var ind = jQuery(this).parent().index();
        var i_ul = jQuery(this).parent().parent().prev().prev().prev("ul");
        jQuery(i_ul).find("li").eq(ind).addClass("hovered_good");
        jQuery(i_ul).prev("ul").find("li").eq(ind).addClass("h_good");
    }, function(){
        var ind = jQuery(this).parent().index();
        var i_ul = jQuery(this).parent().parent().prev().prev().prev("ul");
        jQuery(i_ul).find("li").eq(ind).removeClass("hovered_good");
        jQuery(i_ul).prev("ul").find("li").eq(ind).removeClass("h_good");
    })
    jQuery(".goods_title li a").hover(function(){
        var ind = jQuery(this).parent().index();
        jQuery(this).parent().parent().prev("ul").find("li").eq(ind).addClass("h_good");
    }, function(){
        var ind = jQuery(this).parent().index();
        jQuery(this).parent().parent().prev("ul").find("li").eq(ind).removeClass("h_good");
    })
    jQuery("input:radio").checkBox();
    jQuery(".sub_button").wrap('<span class="s_but_holder"><span class="pos">');
    jQuery(".sub_button").hover(function(){
        jQuery(this).parent().parent().addClass("s_but_hover");
    }, function(){
        jQuery(this).parent().parent().removeClass("s_but_hover");
        jQuery(this).parent().parent().removeClass("s_but_active");
    })
    jQuery(".sub_button").mousedown(function(){
        jQuery(this).parent().parent().addClass("s_but_active");
        return false;
    })
    jQuery(".sub_button").mouseup(function(){
        jQuery(this).parent().parent().removeClass("s_but_active");
        return false;
    })
    jQuery("select").selectBox();
    jQuery("#region_list_control a").click(function(){
        return false;
    })
    jQuery("#region_list_control").click(function(){
        if(jQuery(this).hasClass("opened"))
        {
            jQuery("#region_list").css("display","none");
            jQuery(this).removeClass("opened");
        }
        else
        {
            jQuery("#region_list").css("display","block");
            jQuery(this).addClass("opened");
        }
    });
    jQuery(document).click(function(e){
        if(jQuery(e.target).attr("id")!="region_list_control")
        {
            jQuery("#region_list").css("display","none");
            jQuery("#region_list_control").removeClass("opened");
        }
    })
    jQuery(".put_to_basket").each(function(){
        jQuery(this).html('<span class="but_l"></span>'+jQuery(this).text()+'<span class="but_r"></span>');
    })
    jQuery(".button").each(function(){
        jQuery(this).html('<span class="but_l"></span><span class="b_text">'+jQuery(this).text()+'</span><span class="but_r"></span>');
    })
    jQuery(".clearable").focus(function(){
        if(jQuery(this).val()==jQuery(this).attr("placeholder"))
        {
            jQuery(this).val('');
        }
    })
    jQuery(".clearable").blur(function(){
        if(jQuery(this).val()=='')
        {
            jQuery(this).val(jQuery(this).attr("placeholder"));
        }
    })
})