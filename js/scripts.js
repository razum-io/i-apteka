var getBasket = function() {
		
    var url = '/basket.php';
    var pars = '';
    
    var myAjax = new Ajax.Updater('busket-block', url, {method: 'post', parameters: pars, onFailure: function(){}});
}

function reportError(request)
    {
        alert('Sorry. There was an error.');
    }

var addToBasket = function(region_kod, id, count, className) {

	
	
    if (count < 1) {
        alert('Количество товара должно быть целым числом и больше нуля!');
        return false;
    }
    
    var url = '/basket.php';
    var pars = 'item_id='+id+'&item_count='+count+'&region_kod='+region_kod;//
    //var pars = 'item_id=7&item_count='+count;
    
    var myAjax = new Ajax.Updater('busket-block',
     url, {method: 'post', parameters: pars, onSuccess: function () {         	
     	alert('Товар добавлен в корзину');
     	$('by_button_'+id).update('<p class="right"><img src="/img/ok.gif" width="13" height="12" alt="" /><a href="/order">Оформить заказ</a></p>');     	
     	
     } });
    
    
    return false;
}


var addToBasket_ua = function(region_kod, id, count, className) {

	
	
    if (count < 1) {
        alert('Кількість товару повинно бути цілим числом більшим від нуля!');
        return false;
    }
    
    var url = '/basket.php';
    var pars = 'item_id='+id+'&item_count='+count+'&region_kod='+region_kod;//
    //var pars = 'item_id=7&item_count='+count;
    
    var myAjax = new Ajax.Updater('busket-block',
     url, {method: 'post', parameters: pars, onSuccess: function () {         	
     	alert('Товар додано до кошика');
     	$('by_button_'+id).update('<p class="right"><img src="/img/ok.gif" width="13" height="12" alt="" /><a href="/order">Оформити замовлення</a></p>');     	
     	
     } });
    
	
    
    return false;
}


String.prototype.numberFormat=function(length, strRepl){
	if (!isNaN(this)) {
		var ret = this;
		var retStr = '';
		
		retStr = '';
		var retArr = ret.split('.');
		ret = retArr[0]+strRepl;
			
			var retStr = '';
			var slider = ' ';
			
			for (var i = 0, f = 1; i < retArr[0].length; i++, f++) {
				
				retStr += retArr[0].charAt(i);
				
				
				if (retArr[0].length == 4 && i == 0) {					
					retStr += slider;
				
				}
				if (retArr[0].length == 5 && i == 1) {
					retStr += slider;				
				}
				if (retArr[0].length == 6 && i == 2) {
					retStr += slider;				
				}
				if (retArr[0].length == 7 && (i == 0 || i == 3)) {
					retStr += slider;				
				}
				if (retArr[0].length == 8 && (i == 1 || i == 4)) {
					retStr += slider;				
				}
				if (retArr[0].length == 9 && (i == 2 || i == 5)) {
					retStr += slider;				
				}
			}
			
		    ret = retStr;
			
			var retStr = '';			
		    
			if (retArr.length == 1) {
				for (var i = 0; i < length; i++) {
					retStr += '0';
				}
			} else if (retArr.length == 2) {
				for (var i = 0; i < length; i++) {				
					if (retArr[1][i] != undefined) {
						retStr += retArr[1][i];
					}	else {
						retStr += '0';
						
					}
				}
			}
			
			ret += strRepl+retStr;			
	}
	
	return ret;
};



var addToBasket2 = function(region_kod, id, countObj, className,event) {

	
	var keyCode = event.which;
    if (keyCode == undefined) {
          keyCode = event.keyCode;
    }
	
    var count = countObj.value;
    
    if (count < 1) {
       // alert('Количество товара должно быть целым числом и больше нуля!');
       // return false;
    }
    
    var url = '/basket.php';    
	var pars = 'region_kod='+region_kod+'&item_id='+id+'&item_count='+count+'&ret_type=json'; 

	if ( (keyCode >=48 && keyCode <= 58) || (keyCode >=96 && keyCode <= 105) || keyCode == 8) {
	
		
	var myAjax = new Ajax.Request(url, {method:'post',parameters: pars,	onSuccess: function(transport){ 
		
      			var response = transport.responseText.evalJSON() ;
      			var summ = 0;
      			var count = 0;
      			
      			var resp = transport.responseText;
      			
      			
      			
      			for (res in response) {      				
      				
      				if (response[res].cost != undefined) {   
      					
      					var countStr = new String(response[res].count);
						
      					if (countStr.length >= 6) {
      						countObj.value = countStr.substr(0, 6);
      						alert("Не более 6 цифр");
      						return false;
      						
      					}
      					     					
      					if (response[res].status != 'deleted') {
      						var cost = parseFloat((response[res].cost * response[res].count));
      						summ += cost;
      						count += parseInt(response[res].count);
                                                //alert (res);
      						$('span_goods_'+res).update(new String(cost).numberFormat(2, ','));
                                                
      					}      					
      				
      				}
      				
      			}
                        
                        
      			
      			$('basket-count').update(count+" шт.");
      			summ = new String(summ).numberFormat(2, ',');
      			
      			$('basket-summ').update(summ+" грн."); 
      			$('all-summ').update( summ+" грн.");
                        
     			
    	}});
  	 
	}
   
    
    return false;
}

function add2Fav (x, text){
    if (document.all  && !window.opera) {
     if (typeof window.external == "object") {
        window.external.AddFavorite (document.location, document.title);
       return true;
    }
    else return false;

    } else{
         x.href=text;
        
        x.rel = "sidebar";
       return true;
   }
}



(function( $ ){
	
	//// ---> Проверка на существование элемента на странице
	jQuery.fn.exists = function() {
	   return jQuery(this).length;
	}
	
	//	Phone Mask
	$(function() {
		
    if(!is_mobile()){
    
      if($('#user_phone').exists()){
        
        $('#user_phone').each(function(){
          $(this).mask("+38(999) 999-99-99");
        });
        $('#user_phone')
          .addClass('rfield')
          .removeAttr('required')
          .removeAttr('pattern')
          .removeAttr('title')
          .attr({'placeholder':'+38(___) ___ __ __'});
      }
      
      if($('.phone_form').exists()){
        
        var form = $('.phone_form'),
          btn = form.find('.btn_submit');
        
        form.find('.rfield').addClass('empty_field');
      
        setInterval(function(){
        
          if($('#user_phone').exists()){
            var pmc = $('#user_phone');
            if ( (pmc.val().indexOf("_") != -1) || pmc.val() == '' ) {
              pmc.addClass('empty_field');
            } else {
                pmc.removeClass('empty_field');
            }
          }
          
          var sizeEmpty = form.find('.empty_field').size();
          
          if(sizeEmpty > 0){
            if(btn.hasClass('disabled')){
              return false
            } else {
              btn.addClass('disabled')
            }
          } else {
            btn.removeClass('disabled')
          }
          
        },200);

        btn.click(function(){
          if($(this).hasClass('disabled')){
            return false
          } else {
            form.submit();
          }
        });
        
      }
    }

	});

})( jQuery );