<?php

class page{
	
	private $db = null;
	
	private $tpl = null;
	
	public $cfg = null;
	
	protected $_err = null;
	
	private $getParam = null;
	
	private $userData = array();
	
	public $metatitle = '';
	
	public $metakeywords = '';
	
	public $metadescription = '';
	
	public $way = '';
	
	public $header = '';
	
	private $lookups = array();
	
	public $w = null;
	
	protected $page = '';
	
	private $admin = '';
	
	public $lang = 'ru';
	
	public $langUrl = '';
	
	public $hpic=null;
	
	private $groups = null;
	
	public function __construct($db, $tpl, $cfg){
		$this->db = $db;
		$this->tpl = $tpl;
		$this->cfg = $cfg;
		
		$this->getSettings();
		$this->getAdres();
		$this->drawLangPanel();
		$this->getLookUps();
		
		if(isset($_SESSION['userData'])){
			$this->userData = $_SESSION['userData'];
		}
		
		$this->tpl->define_dynamic('page', 'design.tpl');
		
		if(isset($this->userData['privilege']) && ($this->userData['privilege'] == 'admin' || $this->userData['privilege'] == 'master')){
			$this->loadAdminMenu();
			$this->setWay(' &raquo; ����� ������');
		}
		else{
			$this->tpl->assign(array('ADM_PANEL' => ''));
		}
		
		$this->load_meta_tags();
		
		$this->tpl->define_dynamic('null', 'page');
		
		$this->tpl->define_dynamic('menu', 'page');
		$this->tpl->define_dynamic('menu_complex', 'menu');
		$this->tpl->define_dynamic('menu_complex_sub', 'menu_complex');
		$this->tpl->define_dynamic('menu_single', 'menu');
		
		$this->tpl->define_dynamic('menu_catalog', 'page');
        $this->tpl->define_dynamic('menu_catalog_single', 'menu_catalog');
        $this->tpl->define_dynamic('menu_catalog_complex', 'menu_catalog');
        $this->tpl->define_dynamic('menu_catalog_complex_sub', 'menu_complex');
		
		$this->tpl->define_dynamic('p_header', 'page');
		
		$this->drawTopMenu();
		$this->drawCatalogMenu();
		//$this->drawLeftMenu('_v');
		//$this->loadCollage();
	}
	
	private function loadAdminMenu(){
	    //print_r(auth::getPrivilege());
	    
		$adminMenu = '
<TABLE cellPadding="0" align="center" width="100%">
<tr>
<td width="100%">
<div style="float: left">�����������:
<select name="adminForm" class="frm_text2" onChange="location.href=this.value;">
<option value="">---------------</option>
<option value="{LANGURL}/admin/editpages/{MAIN_ID}">������� ��������</option>
<option value="{LANGURL}/admin/menu/horizontal">�������������� ����</option>
<option value="{LANGURL}/catalog/">�������</option>
<option value="{LANGURL}/news/">�������</option>
<!--<option value="{LANGURL}/admin/import/">������ ��������</option>-->
<option value="{LANGURL}/admin/updateprice/">������ ���</option>
<option value="{LANGURL}/admin/orders">���� �������</option>
<option value="{LANGURL}/admin/phrases">������������� ����</option>
<option value="{LANGURL}/admin/settings">���������</option>
<option value="{LANGURL}/admin/meta_tags">���� ����</option>
<option value="/admin/profile/">�������� ������ ������</option>
<option value="/admin/delivery">������ ��������</option>
</select>
</div>
<div style="float: right"><a href="/logout">�����</a></div>
</td>
</tr>
</table>
		';
		
		$this->tpl->assign(array('ADM_PANEL' => $adminMenu));
	}
	
	public function getAdres(){
		$str = $this->ascii2ansi($_SERVER['REQUEST_URI']);
		$str = substr($str, 1);
		
		$str = explode('?', $str);
		
		(isset($str[1])) ? $this->getParam = $str[1] : '';
		
		$this->w = explode('/', $str[0]);
		
		if(in_array($this->w[0], $this->cfg['settings']['langs'])){
			$this->lang = $this->w[0];
			array_shift($this->w);
		}
		
		if(sizeof($this->w) == 0){
			$this->w[0] = 'index';
		}
		elseif(sizeof($this->w) == 1){
			if($this->w[0] == 'index.php' || $this->w[0] == '') $this->w[0] = 'index';
			elseif($this->w[0] == '404') $this->w[0] = 'err_404';
		}
		else{
			if($this->w[sizeof($this->w) - 1] == ''){
				unset($this->w[sizeof($this->w) - 1]);
			}
			
			$size = sizeof($this->w) - 1;
			
			if(eregi("page=", $this->w[$size])){
				$page = explode('=', $this->w[$size]);
				if($page[1]) $this->page = $page[1];
				
				unset($this->w[$size]);
			}
		}
		
		return true;
	}
	
	private function ascii2ansi($str) {
		$new = $str;
		
		foreach ($this->cfg['uagents'] as $key => $value) {
		
			$page = strpos($_SERVER['HTTP_USER_AGENT'], $value);
			$is = ord($page);
			if ($is > '0' ) $enc = $key;
		}
		
		foreach($this->cfg['ansi_chars'] as $ansi => $ascii) {
			@$char = $ascii[$enc];
			@$new = str_replace($char, $ansi,$new);
		}
		
		return $new;
	}
	
	private function drawTopMenu($index = ''){
		$index_upper = strtoupper($index);
		
		//$query = "SELECT `id`, `name`, `link`, `type`, `menu`, `slide`, `position`, `level`, `visibility` FROM `page` WHERE `level` = 0 AND `menu` = 'horizontal' AND `visibility` = '1' AND `lang` = '".$this->lang."' ORDER BY `position`, `name`";
		//$this->db->setQuery($query);
		//$top = $this->db->loadAssocList();
		
		$top = $this->db->getAllRecords("SELECT `id`, `name`, `link`, `type`, `menu`, `slide`, `position`, `level`, `visibility` FROM `page` WHERE `level` = 0 AND `menu` = 'horizontal' AND `visibility` = '1' AND `lang` = '".$this->lang."' ORDER BY `position`, `name`");
		
		$size = sizeof($top);
		
		if($size > 0){
			$sub = "";
			for($i=0; $i<$size; $i++){
				if($top[$i]['type'] == "section" && $top[$i]['slide'] == 1)
					if($sub == "") $sub .= $top[$i]['id'];
					else $sub .= ", ".$top[$i]['id'];
			}
			
			//$query = "SELECT `id`, `visibility`, `name`, `link`, `level`, `type` FROM `page` WHERE `level` IN (".$sub.") AND `visibility` = '1' ORDER BY `type`, `position`, `name`";
			//$this->db->setQuery($query);
			//$submenu = $this->db->loadAssocList();
			if($sub != ''){
			    $submenu = $this->db->getAllRecords("SELECT `id`, `visibility`, `name`, `link`, `level`, `type` FROM `page` WHERE `level` IN (".$sub.") AND `visibility` = '1' ORDER BY `type`, `position`, `name`");
			}
			
			for($i=0; $i<$size; $i++){
				$separator = (($i+1) < $size) ? '<td class="tm_sep"><img src="/{TPLPATH}img/tm_sep.gif" width="2px" height="2px" alt="" /></td>' : '';
				if($top[$i]['type'] == "section" && $top[$i]['slide'] == 1 && $sub != ''){
					$sub = null;
					$sub = $this->getSubMenu($submenu, $top[$i]['id']);
					if(sizeof($sub) > 0){
						$size_sub = sizeof($sub);
						$this->tpl->parse('MENU'.$index_upper.'_COMPLEX_SUB', 'null');
						
						for($j=0; $j<$size_sub; $j++){
							$separator_sub = ($j+1 < $size_sub) ? '<div class="tm_sub_sep"><img src="/{TPLPATH}img/z.gif" width="1px" height="1px" alt="" /></div>' : '';
							if($sub[$j]['type'] != "link"){
								$link = '/'.$top[$i]['link'].'/'.$sub[$j]['link'].(($sub[$j]['type'] != 'section')?(''):('/'));
							}
							else{
								$link = $sub[$j]['link'];
							}
							
							$this->tpl-> assign(
		                        array(
			                        'MENU_ID' => $sub[$j]['id'],
			                        'MENU_HREF' => $link,
			                        'MENU_NAME' => $sub[$j]['name'],
			                        'MENU_TITLE' => $sub[$j]['name'], // TITLE
			                        'SUBMENU_SEPARATOR' => $separator_sub
		                        )
	                        );
	                        $this->tpl->parse('MENU'.$index_upper.'_COMPLEX_SUB', '.menu'.$index.'_complex_sub');
						}
						
						$this->tpl->assign(
			                array(
				                'MENU_ID' => $top[$i]['id'],
				                'MENU_NAME' => $top[$i]['name'],
				                'MENU_HREF' => '/'.$top[$i]['link'].'/',
				                'SEPARATOR' => $separator
			                )
		                );
		                $this->tpl->parse('MENU'.$index_upper.'_SINGLE', 'null');
		                $this->tpl->parse('MENU'.$index_upper.'_COMPLEX', 'menu'.$index.'_complex');
					}
					else{
						$this->tpl->assign(
			                array(
				                'MENU_ID' => $top[$i]['id'],
				                'MENU_HREF' => '/'.$top[$i]['link'],
				                'MENU_NAME' => $top[$i]['name'],
				                'MENU_TITLE' => $top[$i]['name'],
				                'SEPARATOR' => $separator
				                )
			                );
		                $this->tpl->parse('MENU'.$index_upper.'_SINGLE', 'menu'.$index.'_single');
		                $this->tpl->parse('MENU'.$index_upper.'_COMPLEX', 'null');
					}
				}
				else{
					$link = ($top[$i]['type'] == 'link') ? $top[$i]['link'] : '/'.$top[$i]['link'].(($top[$i]['type'] == 'section')?('/'):(''));
					
					$this->tpl->assign(
		                array(
			                'MENU_ID' => $top[$i]['id'],
			                'MENU_HREF' => $link,
			                'MENU_NAME' => $top[$i]['name'],
			                'MENU_TITLE' => $top[$i]['name'],
			                'SEPARATOR' => $separator
			                )
		                );
	                $this->tpl->parse('MENU'.$index_upper.'_SINGLE', 'menu'.$index.'_single');
	                $this->tpl->parse('MENU'.$index_upper.'_COMPLEX', 'null');
				}
				
				$this->tpl->parse('MENU'.$index_upper, '.menu'.$index);
			}
		}
		else{
			$this->tpl->parse('MENU'.$index_upper.'', 'null');
		}
		
		
		return true;
	}
	
	private function drawLeftMenu($index = '_v'){
		$index_upper = strtoupper($index);
		
		$query = "SELECT `id`, `name`, `link`, `type`, `menu`, `slide`, `position`, `level`, `visibility` FROM `page` WHERE `level` = 0 AND `menu` = 'vertical' AND `visibility` = '1' AND `lang` = '".$this->lang."' ORDER BY `position`, `name`";
		$this->db->setQuery($query);
		$left = $this->db->loadAssocList();
		
		$size = sizeof($left);
		
		if($size > 0){
			for($i=0; $i<$size; $i++){
				$link = ($left[$i]['type'] == 'link') ? $left[$i]['link'] : $this->langUrl.'/'.$left[$i]['link'].(($left[$i]['type'] == 'section')?('/'):(''));
						
				$this->tpl->assign(
					array(
						'MENU_HREF' => $link,
						'MENU_NAME' => $left[$i]['name'],
						'MENU_TITLE' => $left[$i]['name']
					)
				);
				
				
				
                $this->tpl->parse('MENU'.$index_upper.'_SINGLE', 'menu'.$index.'_single');
                $this->tpl->parse('MENU'.$index_upper.'', '.menu'.$index);
			}
		}
		else{
			$this->tpl->parse('MENU'.$index_upper.'', 'null');
		}
	}
	
	private function drawCatalogMenu(){
	    //$query = "SELECT `id`, `name`, `link`, `type`, `slide`, `position`, `level`, `visibility` FROM `catalog` WHERE `level` = '0' AND `visibility` = '1' ORDER BY `position`, `name`";
		//$this->db->setQuery($query);
		//$left = $this->db->loadAssocList();
		
		$left = $this->db->getAllRecords("SELECT `id`, `name`, `link`, `type`, `slide`, `position`, `level`, `visibility` FROM `catalog` WHERE `level` = '0' AND `visibility` = '1' ORDER BY `position`, `name`");
		
		$size = sizeof($left);
		
		if($size > 0){
		    $sub = "";
			for($i=0; $i<$size; $i++){
				if($left[$i]['type'] == "section" && $left[$i]['slide'] == 1)
					if($sub == "") $sub .= $left[$i]['id'];
					else $sub .= ", ".$left[$i]['id'];
			}
			
			//$query = "SELECT `id`, `visibility`, `name`, `link`, `level`, `type` FROM `page` WHERE `level` IN (".$sub.") AND `visibility` = '1' ORDER BY `type`, `position`, `name`";
			//$this->db->setQuery($query);
			//$submenu = $this->db->loadAssocList();
			if($sub != ''){
			    $submenu = $this->db->getAllRecords("SELECT `id`, `visibility`, `name`, `link`, `level`, `type` FROM `catalog` WHERE `level` IN (".$sub.") AND `visibility` = '1' ORDER BY `type`, `position`, `name`");
			}
		    //echo "SELECT `id`, `visibility`, `name`, `link`, `level`, `type` FROM `page` WHERE `level` IN (".$sub.") AND `visibility` = '1' ORDER BY `type`, `position`, `name`";
			//echo $left[0]['type'].'<br />'.$left[0]['slide'].'<br />'.$sub;
			//print_r($submenu);
			for($i=0; $i<$size; $i++){
			    if($left[$i]['type'] == 'section' && $left[$i]['slide'] == 1 && $sub != ''){
			        $subs = $this->getSubMenu($submenu, $left[$i]['id']);
			        //print_r($subs);
			        $size_sub = sizeof($subs);
			        
			        $this->tpl->parse('MENU_CATALOG_COMPLEX_SUB', 'null');
											
			        if($size_sub > 0) {
                        for($j=0; $j<$size_sub; $j++){
							if($subs[$j]['type'] != "link"){
								$link = '/'.$left[$i]['link'].'/'.$subs[$j]['link'].(($subs[$j]['type'] == 'section')?('/'):(''));
							}
							else{
								$link = $subs[$j]['link'];
							}
							
							$this->tpl-> assign(
		                        array(
			                        'MENU_ID' => $subs[$j]['id'],
			                        'MENU_HREF' => '/catalog'.$link,
			                        'MENU_NAME' => stripslashes($subs[$j]['name']),
			                        'MENU_TITLE' => stripslashes($subs[$j]['name'])
		                        )
	                        );
	                        $this->tpl->parse('MENU_CATALOG_COMPLEX_SUB', '.menu_catalog_complex_sub');
						}
			        }
			        
			        $link = ($left[$i]['type'] == 'link') ? $left[$i]['link'] : $left[$i]['link'].(($left[$i]['type'] == 'section')?('/'):(''));
						
					$this->tpl->assign(
						array(
							'MENU_ID' => $left[$i]['id'],
							'MENU_HREF' => '/catalog/'.$link,
							'MENU_NAME' => stripslashes($left[$i]['name']),
							'MENU_TITLE' => stripslashes($left[$i]['name'])
						)
					);
	                
	                $this->tpl->parse('MENU_CATALOG_SINGLE', 'null');
                	$this->tpl->parse('MENU_CATALOG_COMPLEX', 'menu_catalog_complex');
                	
			    }
			    else{
			        $link = ($left[$i]['type'] == 'link') ? $left[$i]['link'] : '/'.$left[$i]['link'].(($left[$i]['type'] == 'g')?('/'):(''));
						
    				$this->tpl->assign(
    					array(
    						'MENU_HREF' => '/catalog'.$link,
    						'MENU_NAME' => stripslashes($left[$i]['name']),
    						'MENU_TITLE' => stripslashes($left[$i]['name'])
    					)
    				);
    				
                    $this->tpl->parse('MENU_CATALOG_SINGLE', 'menu_catalog_single');
                    $this->tpl->parse('MENU_CATALOG_COMPLEX', 'null');
                    
			    }
			    $this->tpl->parse('MENU_CATALOG', '.menu_catalog');
			}
		}
		else{
			$this->tpl->parse('MENU_CATALOG', 'null');
		}
	}
	

	
	private function drawLangPanel(){
		$panel = '';
		$separator = ' | ';
		
		if($this->lang == 'ru'){
			$panel .= '���';
			$langUrl = '';
			$id = 1;
		}
		else{
			$panel .= '<a href="/">���</a>';
		}
		
		$panel .= $separator;
		
		if($this->lang == 'ua'){
			$langUrl = '/ua';
			$id = 2;
			$panel .= '���';
		}
		else{
			$panel .= '<a href="/ua/">���</a>';
		}
		
		$this->langUrl = $langUrl;
		
		$this->tpl->assign(array('LANGPANEL' => $panel, 'LANGURL' => $langUrl, 'MAIN_ID' => $id));
	}
	
	private function getSubMenu($menu, $id){
		$num = sizeof($menu);
		
		$sub = array();
		
		for($i=0; $i<$num; $i++){
			if($menu[$i]['level'] == $id){
				$sub[] = $menu[$i];
			}
		}
		
		return $sub;
	}
	
	public function index(){
		$this->tpl->parse('P_HEADER', 'null');
		
		$this->tpl->define_dynamic('index', 'index.tpl');
		$this->tpl->define_dynamic('i_hits', 'index');
		$this->tpl->define_dynamic('i_news', 'index');		
		$this->tpl->define_dynamic('i_artikle', 'index');
		$this->tpl->define_dynamic('i_items', 'index');	
		$this->tpl->define_dynamic('index_header_txt', 'index');
		
		$page = $this->db->getRow('SELECT * FROM `page` WHERE `id` = "1"');
		
		if($this->db->getNumRows() != 1){
		    er_404();
		    exit;
		}
		
		$this->topNews();
		//$this->hits();
		$this->news(true);
		$this->items(true);
		//$this->artikles();
		
		$this->tpl->assign(
			array(
				'I_HEADER' => $page['metah1'],
				'I_BODY' => $page['description'],
			)
		);
		$this->tpl->parse('CONTENT', '.index_header_txt');
		
		//$this->setWay($page['name']);
		$this->SetMeta($page['metatitle'], $page['metakeywords'], $page['metadescription'], '', ' &raquo; '.$page['name']);
		
		return true;
	}
	
	private function hits() {
		$url = $this->extractPageAdres();
	    $hits = $this->db->getAllRecords('SELECT * FROM `catalog` WHERE `hit` = "1" ORDER BY RAND() LIMIT 0, '.$this->cfg['settings']['num_hits']);
	    //$hits = $this->db->getAllRecords('SELECT * FROM `catalog` WHERE `hit` = "1" ORDER BY RAND() LIMIT 0, 3');

	    $size = sizeof($hits);

	    if ($size > 0) {
	        $this->tpl->define_dynamic('_hits', 'hits.tpl');
	        $this->tpl->define_dynamic('hits', '_hits');
	        $this->tpl->define_dynamic('hits_item', 'hits');
	        $this->tpl->define_dynamic('hits_item_null', 'hits');

	        $i=0;

	        $groups = $this->db->getAllRecords('SELECT * FROM `catalog` WHERE `type` = "section"');

	        foreach ($hits as $hit) {
	            $this->tpl->parse('HITS_ITEM_NULL', 'null');

	            if ($i > 0) {
	                if ($i%2 != 0) {
	                    $sep = '</div><div class="c_sep_h"><div><img src="/img/z.gif" alt="" height="1px" width="1px"></div></div><div id="lst">';
	                }
	                else {
	                    $sep = '<div class="c_sep_v"><img src="/img/z.gif" alt="" height="230px" width="1px"></div>';
	                }
	            }
	            else {
	                $sep = '<div class="c_sep_v"><img src="/img/z.gif" alt="" height="230px" width="1px"></div>';
	            }

	            $pic = '/img/no_pic.jpg';
	            if ($hit['pic'] != '' && file_exists('./img/catalog/'.$hit['pic'])) {
	                $pic = '/img/catalog/'.$hit['pic'];
	            }
	            $link = '/catalog/'.$hit['link'];
	           // $types = adminCatalogEdit($hit['id']);
    			if (empty($hit['cost'])) {
        		   $hit['cost'] = (float) 0;
        		}

        		$coef = getCoefficient($hit['cost'], $this->cfg['settings']['coefficient']);
        		$prev = stripslashes($hit['preview']);

	            $this->tpl->assign(
                            array(
                                'HIT_TYPE' => '',
                                'HIT_ADRESS' =>$link,
                                'HIT_HEADER' => stripslashes($hit['name']),
                                'HIT_ARTIKUL' => $hit['artikul'],
                                'HIT_PROIZVODITEL' => stripslashes($hit['proizvoditel']),
                                'HIT_COST1' => number_format($hit['cost'], 2, ',', " "),
                                'HIT_COST' => number_format($coef, 2, ',', " "),
                                'ECONOM' =>number_format(($hit['cost'] - $coef), 2, ',', " ") ,
                                'HIT_PIC' => ($hit['pic'] == '') ? '/img/no_pic.jpg' : '/img/catalog/'.$hit['pic'],
                                'HIT_PREVIEW' => $prev,
                                'ID' => $hit['id'],
                                'SEPARATOR' => ''//'<div class="c_sep_v"><img src="/img/z.gif" width="1px" height="320px" alt="" /></div>'
                            )
    			        );


	            $this->tpl->parse('HITS_ITEM', '.hits_item');

	            $i++;
	        }

	        if ($i%2 != 0) {
	            $this->tpl->parse('HITS_ITEM_NULL', '.hits_item_null');
	        }

	        /*$sizeof = ($size%2 != 0) ? (($size+1)/2) : $size/2;

	        for ($i=0; $i<$sizeof; $i++) {
			    $link = $url.$section[$i*2]['link'].(($section[$i*2]['type'] == 'section')?('/'):(''));

                $types = adminCatalogEdit($section[$i*2]['id']);

		        $this->tpl->assign(
                    array(
                        'PAGE_TYPE' => $types,
                        'PAGE_ADRESS' => $link,
                        'PAGE_HEADER' => stripslashes($section[$i*2]['name']),
                        'PAGE_ARTIKUL' => $section[$i*2]['artikul'],
                        'PAGE_PROIZVODITEL' => stripslashes($section[$i*2]['proizvoditel']),
                        'PAGE_COST' => number_format($section[$i*2]['cost'], 2, ',', "'"),
                        'PAGE_PIC' => ($section[$i*2]['pic'] == '') ? '/img/no_pic.jpg' : '/img/catalog/'.$section[$i*2]['pic'],
                        'PAGE_PREVIEW' => stripslashes($section[$i*2]['preview']),
                        'ID' => $section[$i*2]['id'],
                        'SEPARATOR' => '<div class="c_sep_v"><img src="/img/z.gif" width="1px" height="230px" alt="" /></div>'
                    )
		        );

		        $this->tpl->parse('SECTION_SUB_ROW', '.section_sub_row');
		        //$this->tpl->parse('SECTION_SUB_EROW', 'null');

			    if (isset($section[$i*2+1]['name'])) {
			        $link = $url.$section[$i*2+1]['link'].(($section[$i*2+1]['type'] == 'section')?('/'):(''));

                    $types = adminCatalogEdit($section[$i*2+1]['id']);

			        $this->tpl->assign(
                        array(
                            'PAGE_TYPE' => $types,
                            'PAGE_ADRESS' => $link,
                            'PAGE_HEADER' => stripslashes($section[$i*2+1]['name']),
                            'PAGE_ARTIKUL' => $section[$i*2+1]['artikul'],
                            'PAGE_PROIZVODITEL' => stripslashes($section[$i*2+1]['proizvoditel']),
                            'PAGE_COST' => number_format($section[$i*2+1]['cost'], 2, ',', "'"),
                            'PAGE_PIC' => ($section[$i*2+1]['pic'] == '') ? '/img/no_pic.jpg' : '/img/catalog/'.$section[$i*2+1]['pic'],
                            'PAGE_PREVIEW' => stripslashes($section[$i*2+1]['preview']),
                            'ID' => $section[$i*2+1]['id'],
                            'SEPARATOR' => ''
                        )
			        );

			        $this->tpl->parse('SECTION_SUB_ROW', '.section_sub_row');
			        //$this->tpl->parse('SECTION_SUB_EROW', 'null');
			    }
			    else {
			        //$this->tpl->parse('SECTION_SUB_ROW', 'null');
			        $this->tpl->parse('SECTION_SUB_EROW', '.section_sub_erow');
			    }

			    //$navbar = '';

                $this->tpl->assign(array('PAGES_TOP' => $navbar.'<div class="c_sep_h"><div><img src="/img/z.gif" width="1px" height="1px" alt="" /></div></div>', 'PAGES_BOTTOM' => $navbar.'<div class="c_sep_h"><div><img src="/img/z.gif" width="1px" height="1px" alt="" /></div></div>'));

			    $this->tpl->parse('SECTION_ROW', '.section_row');
			}*/

	        $this->tpl->parse('CONTENT', '.i_hits');
			$this->tpl->parse('CONTENT', '.hits');
	    }
	}
	
	private function getHitUrl($group, $hit) {
	    $tmp = '';
        
	    $flag = false;
	    
	    $size = sizeof($group);
	    
	    $level = $hit['level'];
	    
	    while(!$flag) {
	        for ($i=0; $i<$size; $i++) {
	            if ($group[$i]['id'] == $level) {
	                $tmp = $group[$i]['link'].'/'.$tmp;
	                $level = $group[$i]['level'];
	            }
	        }
	        
	        if ($level == 0) {
	            $tmp = '/'.$tmp;
	            $flag = true;
	        }
	    }
	    
	    return '/catalog'.$tmp.$hit['link'];	    
	}
	
	private function artikles() {
	    $this->tpl->define_dynamic('_artikle', 'artikle.tpl');
		
        $this->tpl->define_dynamic('artikle', '_artikle');
        $this->tpl->define_dynamic('artikle_item', 'artikle');
        
        $num_artikle = $this->cfg['settings']['num_artikle'];
        
        $artikles = $this->db->getAllRecords('SELECT * FROM `page` WHERE `lang` = "'.$this->lang.'" AND `type` = "page"'.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(''):(' AND `visibility` = "1"')).' ORDER BY `date` DESC, `name` LIMIT 0, '.$num_artikle);
		//echo '<!-- SELECT * FROM `page` WHERE `lang` = "'.$this->lang.'" AND `type` = "page"'.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(''):(' AND `visibility` = 1')).' ORDER BY `date` DESC, `name` LIMIT 0, '.$num_artikle.' -->';
		$size = sizeof($artikles);
		
		if ($size > 0) {
		    $this->groups = $this->db->getAllRecords('SELECT `id`, `name`, `link`, `level` FROM `page` WHERE `type` = "section" AND `lang` = "'.$this->lang.'"');
		    
		    for($i=0; $i<$size; $i++){
				$separator = '<div class="sep_h"><div><img src="/img/z.gif" alt="" width="1" height="1"></div></div>';
				
				$date = '<span>'.convertDate($artikles[$i]['date'], 'd.m.y').'</span> ';
				
				$this->tpl->assign(
					array(
						'ARTIKLE_ADRESS' => $this->getLink($artikles[$i]['level']).$artikles[$i]['link'],
						'ARTIKLE_DATE' => (isset($_SESSION['userData']) && $_SESSION['userData']['privilege'] == 'admin') ? $date.' &nbsp;|&nbsp; ' : '',
						'ARTIKLE_HEADER' => $artikles[$i]['name'],
						'ARTIKLE_PREVIEW' => $artikles[$i]['preview'],
						'SEPARATOR' => $separator
					)
				);
				
				$this->tpl->parse('ARTIKLE_ITEM', '.artikle_item');
			}
			
			$this->tpl->assign('ARTIKLE_H1', '��������� ������');
			
			$this->tpl->parse('CONTENT', '.i_artikle');
			$this->tpl->parse('CONTENT', '.artikle');
		}
	}
	
	private function getLink($id){
		$href = '';
		
		$stop = 0;
		
		while($stop != 1){
			foreach($this->groups as $group){
				if($group['id'] == $id){
					$href = $group['link'].'/'.$href;
					$id = $group['level'];
					break;
				}
			}
			
			if($id == 0) $stop = 1;
		}
		
		$href = '/'.$href;
		
		return $href;
	}
	
	private function topNews() {
	    $this->tpl->define_dynamic('_news', 'news.tpl');
		
        $this->tpl->define_dynamic('news', '_news');      
        $this->tpl->define_dynamic('news_item', 'news');        
        $this->tpl->define_dynamic('news_detail', '_news');
        
        $top = $this->db->getAllRecords('SELECT * FROM `news` WHERE `top` = "1" ORDER BY `date` DESC');
			    
	    $size = sizeof($top);
	    
	    if ($size > 0) {
	        for ($i=0; $i<$size; $i++) {
	            $separator = '<div class="sep_h"><div><img src="/img/z.gif" alt="" width="1" height="1"></div></div>';
			
				//$date = '<span>'.convertDate($top[$i]['date'], 'd.m.y').'</span> ';
				
				$pic = '';
    			if($top[$i]['pic'] != '' && file_exists('./img/news'.$this->langUrl.'/'.$top[$i]['pic'])){
    				//$pic = '<div class="nz_i"><img src="/img/news'.$this->langUrl.'/'.$new['pic'].'" width="160px" height="120px" alt="'.$new['name'].'" /></div>';
    				$pic = '<div class="n-img"><a href="/news/'.$top[$i]['link'].'" title="'.$top[$i]['name'].'"><img src="/img/news'.$this->langUrl.'/'.$top[$i]['pic'].'" alt="'.$top[$i]['name'].'" width="165px" height="165px"></a></div>';
    			}
				
				$this->tpl->assign(
					array(
						'NEWS_ID' => $top[$i]['id'],
						'NEWS_ADRESS' => $top[$i]['link'],
						'DATE' => '',
						'NEWS_HEADER' => $top[$i]['name'],
						'NEWS_PREVIEW' => $top[$i]['preview'],
						'ADM_EDIT' => '',
						'SEPARATOR' => $separator,
						'PIC' => $pic
					)
				);
				
				$this->tpl->parse('NEWS_ITEM', '.news_item');
	        }
	        $this->tpl->parse('CONTENT', '.news');
	    }
	}
	
	public function news($index = false){
		$this->tpl->define_dynamic('_news', 'news.tpl');
		 
        $this->tpl->define_dynamic('news', '_news');      
        $this->tpl->define_dynamic('news_item', 'news');        
        $this->tpl->define_dynamic('news_detail', '_news');
		$isAdmin = (isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin');
		$w = ' AND `date` <= UNIX_TIMESTAMP()';
        if(!isset($this->w[1]) || $this->w[1] == '') {
			$start = 0;
			$navbar = '';
			$pages = 1;
			
			if($index){
				$num_news = $this->cfg['settings']['num_lastnews'];
			}
			else{
				$num_news = $this->cfg['settings']['num_news'];
				if(!empty($this->page)){
					//$page = explode('=', $this->page);
					if(is_numeric($this->page) && $this->page > 0){
						$pages = $this->page;
						$start = $num_news * $pages - $num_news;
					}
				}
				
				//$query = 'SELECT count(`id`) FROM `news` WHERE `lang` = "'.$this->lang.'"'.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(''):(' AND `visibility` = 1'));
				//$this->db->setQuery($query);
				//$count = $this->db->loadResult();
				
				$count = $this->db->getResult('SELECT count(`id`) FROM `news` WHERE `lang` = "'.$this->lang.'"'.(($isAdmin)?(''):(' AND `visibility` = 1 '.$w)));
				
				//$count = $this->db->getNumRows();
				
				if($count > $num_news) $navbar = loadNavBar($count, $pages, $num_news, '/news/');
			}
			
			//$query = 'SELECT * FROM `news` WHERE `lang` = "'.$this->lang.'"'.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(''):(' AND `visibility` = 1')).' ORDER BY `date` DESC, `name` LIMIT '.$start.', '.$num_news;
			//$this->db->setQuery($query);
			//$news = $this->db->loadAssocList();
			
			//$size = sizeof($news);
			
			$news = $this->db->getAllRecords('SELECT * FROM `news` WHERE `lang` = "'.$this->lang.'"'.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(''):(' AND `visibility` = 1 '.$w)).''.(($index) ? (' AND `top` <> "1"  ') : ('')).' ORDER BY `date` DESC, `name` LIMIT '.$start.', '.$num_news);
		
			//echo 'SELECT * FROM `news` WHERE `lang` = "'.$this->lang.'"'.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(''):(' AND `visibility` = 1')).''.(($index) ? (" AND `top` <> '1'") : ('')).' ORDER BY `date` DESC, `name` LIMIT '.$start.', '.$num_news.'';
			$size = $this->db->getNumRows();
			
			$content = getAdminCreate('news', '', $this->langUrl);
			
			/*$size_top = 0;
			if ($index) {
			    $top = $this->db->getAllRecords('SELECT * FROM `news` WHERE `top` = "1" ORDER BY `date` DESC');
			    
			    $size_top = sizeof($top);
			}*/
			
			if($size > 0) {
			    /*if ($index && $size_top > 0) {
			        for ($i=0; $i<$size_top; $i++) {
			            $separator = '<div class="sep_h"><div><img src="/img/z.gif" alt="" width="1" height="1"></div></div>';
					
    					$date = '<span>'.convertDate($top[$i]['date'], 'd.m.y').'</span> ';
    					
    					$pic = '';
            			if($top[$i]['pic'] != '' && file_exists('./img/news'.$this->langUrl.'/'.$top[$i]['pic'])){
            				//$pic = '<div class="nz_i"><img src="/img/news'.$this->langUrl.'/'.$new['pic'].'" width="160px" height="120px" alt="'.$new['name'].'" /></div>';
            				$pic = '<div class="n-img"><a href="/news/'.$top[$i]['link'].'" title="'.$top[$i]['name'].'"><img src="/img/news'.$this->langUrl.'/'.$top[$i]['pic'].'" alt="'.$top[$i]['name'].'" width="165px" height="165px"></a></div>';
            			}
    					
    					$this->tpl->assign(
    						array(
    							'NEWS_ID' => $top[$i]['id'],
    							'NEWS_ADRESS' => $top[$i]['link'],
    							'DATE' => '',
    							'NEWS_HEADER' => $top[$i]['name'],
    							'NEWS_PREVIEW' => $top[$i]['preview'],
    							'ADM_EDIT' => '',
    							'SEPARATOR' => $separator,
    							'PIC' => $pic
    						)
    					);
    					
    					$this->tpl->parse('NEWS_ITEM', '.news_item');
			        }
			    }*/
			    $this->tpl->parse('NEWS_ITEM', 'null');
			   
				for($i=0; $i<$size; $i++){
					$separator = '<div class="sep_h"><div><img src="/img/z.gif" alt="" width="1" height="1"></div></div>';
					
					//$date = '<span>'.convertDate($news[$i]['date'], 'd.m.y').'</span> '.(($news[$i]['top'] == '0') ? (' &nbsp;|&nbsp; ') : (''));
					$date = (($news[$i]['top'] == '0') ? ('<span>'.convertDate($news[$i]['date'], 'd.m.y').'</span>  &nbsp;|&nbsp; ') : (''));
					
					$pic = '';
        			if($news[$i]['pic'] != '' && file_exists('./img/news'.$this->langUrl.'/'.$news[$i]['pic'])){
        				//$pic = '<div class="nz_i"><img src="/img/news'.$this->langUrl.'/'.$new['pic'].'" width="160px" height="120px" alt="'.$new['name'].'" /></div>';
        				$pic = '<div class="n-img"><a href="/news/'.$news[$i]['link'].'" title="'.$news[$i]['name'].'"><img src="/img/news'.$this->langUrl.'/'.$news[$i]['pic'].'" alt="'.$news[$i]['name'].'" width="165px" height="165px"></a></div>';
        			}
					
					$this->tpl->assign(
						array(
							'NEWS_ID' => $news[$i]['id'],
							'NEWS_ADRESS' => $news[$i]['link'],
							'DATE' => $date,
							'NEWS_HEADER' => $news[$i]['name'],
							'NEWS_PREVIEW' => $news[$i]['preview'],
							'ADM_EDIT' => (!$index) ? getAdminEdit($news[$i]['id'], 'news', '&nbsp;', $this->langUrl) : '',
							'SEPARATOR' => $separator,
							'PIC' => $pic
						)
					);
					
					$this->tpl->parse('NEWS_ITEM', '.news_item');
				}
				
				if($index){
					//$query = 'SELECT `metah1` FROM `meta_tags` WHERE `link` = "news" AND `lang` = "'.$this->lang.'"';
					//$this->db->setQuery($query);
					//$h1 = $this->db->loadResult();
					
					$h1 = $this->db->getResult('SELECT `metah1` FROM `meta_tags` WHERE `link` = "news" AND `lang` = "'.$this->lang.'"');
				
					$this->tpl->assign(array('PAGES_TOP' => '', 'PAGES_BOTTOM' => '', 'ADM_TYPE' => '', 'NEWS_H1' => $h1));
				}
				else{
					$this->tpl->assign(array('CONTENT' => $content != '' ? '<div id="body">'.$content.'</div>' : ''));
					$this->tpl->assign(array('PAGES_TOP' => $navbar.'<div class="c_sep_h"><div><img src="/img/z.gif" width="1px" height="1px" alt="" /></div></div>', 'PAGES_BOTTOM' => $navbar));
				}
				
				$this->tpl->parse('CONTENT', '.i_news');
				$this->tpl->parse('CONTENT', '.news');
			}
			else{
				if(!$index) $this->tpl->assign(array('CONTENT' => $content.$this->cfg['empty_section']));
			}
		}
		else{
			$link = mysql_real_escape_string(ru2lat($this->w[1]));
			//$query = 'SELECT * FROM `news` WHERE `link` = "'.$link.'" AND `lang` = "'.$this->lang.'"';
			//$this->db->setQuery($query);
			//if(!$this->db->loadObject($new)) return false;
			
			$new = $this->db->getRow('SELECT * FROM `news` WHERE `link` = "'.$link.'" AND `lang` = "'.$this->lang.'" '.$w.' ORDER BY `date`');
			
			if($this->db->getNumRows() != 1) return false;
			
			$date = convertDate($new['date'], 'd.m.y');
			
			$this->setMeta($new['metatitle'], $new['metakeywords'], $new['metadescription'], '<span>'.$date.'</span> | '.$new['metah1'].getAdminEdit($new['id'], 'news', '&nbsp;', $this->langUrl));
			$this->setWay(' &raquo; '.$date.' | '.$new['name']);
			
			$pic = '';
			if($new['pic'] != '' && file_exists('./img/news'.$this->langUrl.'/'.$new['pic'])){
				//$pic = '<div class="nz_i"><img src="/img/news'.$this->langUrl.'/'.$new['pic'].'" width="160px" height="120px" alt="'.$new['name'].'" /></div>';
				$pic = '<div class="n-img"><img src="/img/news'.$this->langUrl.'/'.$new['pic'].'" alt="'.$new['name'].'" width="165px" height="165px"></div>';
			}
			
			$this->tpl->assign(
				array(
					'NEWS_PREVIEW' => $new['preview'],
					'BODY' => $new['description'],
					'DATE' => '<span>'.$date.'</span>',
					'NEWS_TITLE' => $new['name'],
					'PIC' => $pic
				)
			);
			
			$this->tpl->parse('NEWS', 'null');
			$this->tpl->parse('CONTENT', 'news_detail');
		}
		
		return true;
	}
	
	
	
	public function items($index = false){
		$this->tpl->define_dynamic('_items', 'items.tpl');
		
        $this->tpl->define_dynamic('items', '_items');      
        $this->tpl->define_dynamic('items_item', 'items');        
        
        if(!isset($this->w[1]) || $this->w[1] == ''){
			$start = 0;
			$navbar = '';
			$pages = 1;
			$num_items = $this->cfg['settings']['num_artikle'];		
			
			
			$items = $this->db->getAllRecords('SELECT * FROM `page` WHERE `lang` = "'.$this->lang.'"'.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(' AND `type` ="page" '):(' AND `visibility` = "1" AND `date` <= UNIX_TIMESTAMP() AND `date` != "" AND `type` = "page"' )).' ORDER BY `date` DESC, `name` LIMIT '.$start.', '.$num_items );

			
			
			$size = $this->db->getNumRows();			
			
			if($size > 0){
			    
			    $this->tpl->parse('ITEMS_ITEM', 'null');
				for($i=0; $i<$size; $i++){
					$separator = '<div class="sep_h"><div><img src="/img/z.gif" alt="" width="1" height="1"></div></div>';
								
					
					$pic = '';
        								
					$this->tpl->assign(
						array(
							'ITEMS_ID' => $items[$i]['id'],
							'ITEMS_ADRESS' => $items[$i]['link'],
							'DATE' => '',
							'ITEMS_HEADER' => $items[$i]['name'],
							'ITEMS_PREVIEW' => $items[$i]['preview'],
							'ADM_EDIT' =>'',
							'SEPARATOR' => $separator,
							'PIC' => $pic
						)
					);
					
					$this->tpl->parse('ITEMS_ITEM', '.items_item');
				}
				
				if($index){ 
					//$query = 'SELECT `metah1` FROM `meta_tags` WHERE `link` = "news" AND `lang` = "'.$this->lang.'"';
					//$this->db->setQuery($query);
					//$h1 = $this->db->loadResult();
					
					$h1 = $this->db->getResult('SELECT `metah1` FROM `meta_tags` WHERE `link` = "page" AND `lang` = "'.$this->lang.'"');
					$this->tpl->assign('ITEMS_H1', $h1);
					//$this->tpl->assign(array('PAGES_TOP' => '', 'PAGES_BOTTOM' => '', 'ADM_TYPE' => '', 'ITEMS_H1' => $h1));
				}
				
				$this->tpl->parse('CONTENT', '.i_items');
				$this->tpl->parse('CONTENT', '.items');
			}
			
		}
		
		
		return true;
	}
	
	public function pages(){
		$isAdmin = (isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin');
		$link = mysql_real_escape_string(ru2lat(end($this->w)));
		//$query = 'SELECT * FROM `page` WHERE `link` = "'.$link.'" AND `lang` = "'.$this->lang.'"';
		//$this->db->setQuery($query);
		//$page = $this->db->loadAssocList();
		
		$page = $this->db->getRow('SELECT * FROM `page` WHERE `link` = "'.$link.'" '.(($isAdmin)?(''):(' AND `visibility` = "1" ')).' AND `lang` = "'.$this->lang.'" ORDER BY `date` DESC');
		
		//if($this->db->getNumRows() != 1){		
		if (!$page ) {
			er_404();
			exit;
		}
		
		//$page = $page[0];
		
		/*if(!$this->db->loadObject($page)){
			er_404();
			exit;
		}*/
		
		if(!$this->extractWay()){
			er_404();
			exit;
		}
		
		//$this->setHpic($page);
		//echo $page['name'];
		//$this->setWay($page['name']);
		$this->setMeta($page['metatitle'], $page['metakeywords'], $page['metadescription'], $page['metah1'].getAdminEdit($page['id'], 'pages', '&nbsp;', $this->langUrl), ' &raquo; '.$page['name']);
		
		$separator = '<div class="sep_h"><div><img src="/img/z.gif" width="1px" height="1px" alt="" /></div></div>';
		
		if($page['type'] == 'section') $this->admin = getAdminCreate('all', $page['id'], $this->langUrl);
		$this->admin = ($this->admin != '') ? $this->admin : '';
		
		$content = $this->admin.((!empty($page['tube']))?('<div class="tube">'.stripslashes($page['tube']).'</div>'):('')).$page['description'];
		
		//$this->tpl->assign(array('CONTENT' => $this->admin.$page->description.(($page->description != '')?($separator):(''))));
		$this->tpl->assign(array('CONTENT' => '<div id="body">'.$content.'</div>'));
		
		if($page['type'] == 'section') $this->drawGroups($page['id']);
		
		return true;
	}
	
	public function catalog() {
	    require_once('class/class.catalog.php');
	    
	    $catalog = new catalog($this->db, $this->tpl, $this->cfg, $this->userData, $this->w, $this->page);
	    
	    $this->extractWay('catalog');
	    
	    $this->SetMeta($catalog->metatitle, $catalog->metakeywords, $catalog->metadescription, $catalog->header, $catalog->way);
	    //$this->setWay($catalog->way);
	    
        return true;
	}
	
	
	private function drawPage($id = null, $options=array()){ 
		$url = $this->extractPageAdres();
		
		$start = 0;
		$navbar = '';
		$pages = 1;
		
		$num_pages = $this->cfg['settings']['num_page_items'];
		
		if(!empty($this->page)){
			//$page = explode('=', $this->page);
			if(is_numeric($this->page) && $this->page > 0){
				$pages = $this->page;
				$start = $num_pages * $pages - $num_pages;
			}
		}
		
		//$query = 'SELECT count(`id`) FROM `page` WHERE `level` = "'.$id.'" AND `lang` = "'.$this->lang.'"'.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(''):(' AND `visibility` = "1"'));
		//$this->db->setQuery($query);
		//$count = $this->db->loadResult();
		
		
	
		
		$count = $this->db->getResult('SELECT count(`id`) FROM `page` WHERE `id` = "'.$id.'" AND `lang` = "'.$this->lang.'"'.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(''):(' AND ( ( `visibility` = "1" AND type != "page")  OR ( `visibility` = "1" AND `date` <= UNIX_TIMESTAMP() AND type="page" AND `date` != "") )  ')));
		
		//$count = $this->db->getNumRows();
		
		if($count > $num_pages) $navbar = loadNavBar($count, $pages, $num_pages, $url);
		
		//$query = 'SELECT * FROM `page` WHERE `level` = '.$id.''.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(''):(' AND `visibility` = "1"')).' ORDER BY `type`, `position`, `name` LIMIT '.$start.', '.$num_pages;
		//$this->db->setQuery($query);
		//$groups = $this->db->loadAssocList();
		
		//$size = sizeof($groups);
		
		$groups = $this->db->getAllRecords('SELECT * FROM `page` WHERE `id` = '.$id.''.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(''):(' AND ( ( `visibility` = "1" AND type != "page")  OR ( `visibility` = "1" AND `date` <= UNIX_TIMESTAMP() AND type="page" AND `date` != "") ) ' )).' ORDER BY `position`,`date`  DESC LIMIT '.$start.', '.$num_pages);
	
		$size = $this->db->getNumRows();
		
		if($size > 0){			
			$this->tpl->define_dynamic('_section', 'section.tpl');
			$this->tpl->define_dynamic('section', '_section');
			$this->tpl->define_dynamic('section_row', 'section');
			
			for($i=0; $i<$size; $i++){
				$link = ($groups[$i]['type'] == 'link') ? $groups[$i]['link'] : $url.$groups[$i]['link'].(($groups[$i]['type'] == 'section')?('/'):(''));
				
				$types = getAdminTypes($groups[$i]['type'], $groups[$i]['id'], 'pages');
				
				$this->tpl->assign(
		            array(
			            'PAGE_ID' => $groups[$i]['id'],
			            'PAGE_ADRESS' => $link,
			            'PAGE_TYPE' => $types,
			            'PAGE_HEADER' => $groups[$i]['name'],
			            'PAGE_PREVIEW' => $groups[$i]['preview'],
			            'SEPARATOR' => (isset($options['isShowSeparator']) && $options['isShowSeparator'] === false ? '&nbsp;' : '<div class="c_sep_h"><div><img src="/img/z.gif" width="1px" height="1px" alt=""></div></div>')
		            )
	            );
	            $this->tpl->parse('SECTION_ROW', '.section_row');
			}
			
			$this->tpl->assign(array('PAGES_TOP' => $navbar.'<div class="sep_h"><div><img src="/img/z.gif" width="1px" height="1px" alt="" /></div></div>', 'PAGES_BOTTOM' => $navbar));
			
			$this->tpl->parse('CONTENT', '.section');
			
			return true;
		}
		
		return true;
	}
	
	private function drawGroups($id = null, $options=array()){ 
		$url = $this->extractPageAdres();
		
		$start = 0;
		$navbar = '';
		$pages = 1;
		
		$num_pages = $this->cfg['settings']['num_page_items'];
		
		if(!empty($this->page)){
			//$page = explode('=', $this->page);
			if(is_numeric($this->page) && $this->page > 0){
				$pages = $this->page;
				$start = $num_pages * $pages - $num_pages;
			}
		}
		
		//$query = 'SELECT count(`id`) FROM `page` WHERE `level` = "'.$id.'" AND `lang` = "'.$this->lang.'"'.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(''):(' AND `visibility` = "1"'));
		//$this->db->setQuery($query);
		//$count = $this->db->loadResult();
		
		
	
		
		$count = $this->db->getResult('SELECT count(`id`) FROM `page` WHERE `level` = "'.$id.'" AND `lang` = "'.$this->lang.'"'.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(''):(' AND ( ( `visibility` = "1" AND type != "page")  OR ( `visibility` = "1" AND `date` <= UNIX_TIMESTAMP() AND type="page" AND `date` != "") )  ')));
		
		//$count = $this->db->getNumRows();
		
		if($count > $num_pages) $navbar = loadNavBar($count, $pages, $num_pages, $url);
		
		//$query = 'SELECT * FROM `page` WHERE `level` = '.$id.''.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(''):(' AND `visibility` = "1"')).' ORDER BY `type`, `position`, `name` LIMIT '.$start.', '.$num_pages;
		//$this->db->setQuery($query);
		//$groups = $this->db->loadAssocList();
		
		//$size = sizeof($groups);
		
		$groups = $this->db->getAllRecords('SELECT * FROM `page` WHERE `level` = '.$id.''.((isset($this->userData['privilege']) && $this->userData['privilege'] == 'admin')?(''):(' AND ( ( `visibility` = "1" AND type != "page")  OR ( `visibility` = "1" AND `date` <= UNIX_TIMESTAMP() AND type="page" AND `date` != "") ) ' )).' ORDER BY `position`,`date`  DESC LIMIT '.$start.', '.$num_pages);
		
		$size = $this->db->getNumRows();
		
		if($size > 0){			
			$this->tpl->define_dynamic('_section', 'section.tpl');
			$this->tpl->define_dynamic('section', '_section');
			$this->tpl->define_dynamic('section_row', 'section');
			
			for($i=0; $i<$size; $i++){
				$link = ($groups[$i]['type'] == 'link') ? $groups[$i]['link'] : $url.$groups[$i]['link'].(($groups[$i]['type'] == 'section')?('/'):(''));
				
				$types = getAdminTypes($groups[$i]['type'], $groups[$i]['id'], 'pages');
				
				$this->tpl->assign(
		            array(
			            'PAGE_ID' => $groups[$i]['id'],
			            'PAGE_ADRESS' => $link,
			            'PAGE_TYPE' => $types,
			            'PAGE_HEADER' => $groups[$i]['name'],
			            'PAGE_PREVIEW' => $groups[$i]['preview'],
			            'SEPARATOR' => (isset($options['isShowSeparator']) && $options['isShowSeparator'] === false ? '&nbsp;' : '<div class="c_sep_h"><div><img src="/img/z.gif" width="1px" height="1px" alt=""></div></div>')
		            )
	            );
	            $this->tpl->parse('SECTION_ROW', '.section_row');
			}
			
			$this->tpl->assign(array('PAGES_TOP' => $navbar.'<div class="sep_h"><div><img src="/img/z.gif" width="1px" height="1px" alt="" /></div></div>', 'PAGES_BOTTOM' => $navbar));
			
			$this->tpl->parse('CONTENT', '.section');
			
			return true;
		}
		
		return true;
	}
	
	public function basket() {
		/*if ($_SESSION['userData']['privilege'] != 'master') {
			return false;
		}*/
	
		$this->tpl->define_dynamic('_basket', 'basket.tpl');
		
        $this->tpl->define_dynamic('basket', '_basket');      
        $this->tpl->define_dynamic('basket_item', 'basket');        
        $this->tpl->define_dynamic('basket_empty', 'basket');  
        $this->tpl->define_dynamic('delivery_items', 'basket');  
        
        
        $this->setMeta('�������', '�������', '�������', '�������');
		$this->setWay(' &raquo; �������');
        
		$deliveryValues = $this->db->getAllRecords('SELECT * FROM `delivery` WHERE `visibility` = "1"');
		if (count($deliveryValues) > 0) {
			for ($i = 0; $i < count($deliveryValues); $i++) {
				$this->tpl->assign(
						array(							
							'DELIVERY_NAME' =>$deliveryValues[$i]['name']
						)
				);
				$this->tpl->parse('DELIVERY_ITEMS', '.delivery_items');
			}
			
		} else {
			$this->tpl->parse('DELIVERY_ITEMS', 'null');
		}
        
		if (!empty($_SESSION['goods']) && !empty($_POST)) {
			//print_r($_POST);
			foreach ($_POST as $key => $value) {
				if ($key != 'x' && $key != 'y') {
					$item = explode('-', $key);
					
					if (isset($item[1]) && !isset($item[2])) {
						//echo $item[0].'<br />';
						if ($item[0] == 'del') {
							$this->removeGoods($item[1]);
						}
						elseif ($item[0] == 'count') {
							$this->updateCount($item[1], $value);
						}
					}
				}
			}
		}		
		$summ = 0;
		
		if (!empty($_SESSION['goods'])) {
			//print_r($_SESSION['goods']);
			$this->tpl->parse('BASKET_EMPTY', 'null');
			
			$size = sizeof($_SESSION['goods']);
			
			for ($i=0; $i<$size; $i++) {
				if (isset($_SESSION['goods'][$i]['id'])) {										
					$this->tpl->assign(
						array(
							'PRO' => stripslashes($_SESSION['goods'][$i]['pro']),
							'ARTIKUL' => $_SESSION['goods'][$i]['article'],
							'NAME' => stripslashes($_SESSION['goods'][$i]['name']),
							'COST' => number_format($_SESSION['goods'][$i]['price'], 2, ',', "'"),
							'COUNT' => $_SESSION['goods'][$i]['count'],
							'SUM' => number_format($_SESSION['goods'][$i]['price']*$_SESSION['goods'][$i]['count'], 2, ',', "'"),
							'ID' => $_SESSION['goods'][$i]['id'],
							'SEPARATOR' => $i<($size-1) ? '<div class="b_sep_h"><div><img src="/img/z.gif" width="1px" height="1px" alt="" /></div></div>' : '&nbsp;'
						)
					);
					
					$summ += $_SESSION['goods'][$i]['price']*$_SESSION['goods'][$i]['count'];					
					$this->tpl->parse('BASKET_ITEM', '.basket_item');
				}
			}
		}
		else {
			$this->tpl->parse('BASKET_ITEM', 'null');
			$this->tpl->parse('BASKET_EMPTY', '.basket_empty');
		}
		
		$fio = gp($_SESSION['orderuser'], 'fio', '');
		$email = gp($_SESSION['orderuser'], 'email', '');
		$city = gp($_SESSION['orderuser'], 'city', '');
		//$company = gp($_SESSION['orderuser'], 'company', '');
		$delivery_office = gp($_SESSION['orderuser'], 'delivery_office', '');
		$delivery_addres = gp($_SESSION['orderuser'], 'delivery_addres', '');
		$delivery = gp($_SESSION['orderuser'], 'delivery', '');
		$delivery_departament = gp($_SESSION['orderuser'], 'delivery_departament', '');
		$tel = gp($_SESSION['orderuser'], 'tel', '');
		$mtel = gp($_SESSION['orderuser'], 'mtel', '');
		$body = gp($_SESSION['orderuser'], 'body', '');
		$this->_err = gpm($_SESSION['orderuser'], 'err', '');
		
		$this->tpl->assign(
			array(
				'SUMM' => number_format($summ, 2, ',', "'"),
				'FIO' => $fio,
				'EMAIL' => $email,
				'CITY' => $city,
				'ADDR' => $delivery_addres,
				'DELIVERY_DEPARTAMENT' =>$delivery_departament,
				'DELIVERY_OFFICE' =>$delivery_office,
				'DELIVERY' =>$delivery,
				'TEL' => $tel,
				'MTEL' => $mtel,
				'BODY' => $body,
			)
		);
		
		if($this->_err){
			$this->tpl->assign(array('CONTENT' => page::ViewErr()));
		}
		
        $this->tpl->parse('CONTENT', '.basket');
        
        return true;
	}
	
	private function removeGoods($id) {
		//print_r($_SESSION['goods']);
		//echo $id;
		$size = sizeof($_SESSION['goods']);
		
		for ($i=0; $i<$size; $i++) {
			if ($_SESSION['goods'][$i]['id'] == $id) {
				//echo $id;
				unset($_SESSION['goods'][$i]);
			}
		}
		
		$tmp = $_SESSION['goods'];
		
		$_SESSION['goods'] = null;
		
		//$size = sizeof($tmp);
		
		for ($i=0; $i<$size; $i++) {
			if (isset($tmp[$i]['id'])) $_SESSION['goods'][] = $tmp[$i];
		}
	}
	
	private function updateCount($id, $count) {
		$size = sizeof($_SESSION['goods']);
		
		for ($i=0; $i<$size; $i++) {
			if (!empty($_SESSION['goods'][$i]) && $_SESSION['goods'][$i]['id'] == $id) {
				$_SESSION['goods'][$i]['count'] = $count;
			}
		}
	}
	
	public function order() {
		$this->setMeta('���������� ������', null, null, '���������� ������', ' &raquo; ���������� ������');
	    
	    if (empty($_SESSION['goods'])) {
	        $this->tpl->assign(array('CONTENT' => '<div id="body">���� ������� �����.</div>'));
	        return true;
	    }
	    
	    $this->tpl->define_dynamic('_order', 'order.tpl');
	    $this->tpl->define_dynamic('order', '_order');
	    $this->tpl->define_dynamic('order_item', 'order');
	    $this->tpl->define_dynamic('order_item', 'order');
	    $this->tpl->define_dynamic('order_delivery', 'order');
	    $this->tpl->define_dynamic('order_delivery_departament', 'order');
	    $this->tpl->define_dynamic('order_delivery_addres', 'order');
	    
	    
	    $fio = gp($_POST, 'fio', '');
        $email = gp($_POST, 'email', '');
        $city = gp($_POST, 'city', '');
        $delivery_office = gp($_POST, 'delivery_office', '0');
        $delivery = gp($_POST, 'delivery', '');
        $delivery1 = gp($_POST, 'delivery1', '');
        
        if (!empty($delivery1)) {
        	$delivery = $delivery1;
        }
        
        $delivery_departament = gp($_POST, 'delivery_department', '');
        $delivery_addres = gp($_POST, 'addr', '');        
        $tel = gp($_POST, 'tel', '');
        $mtel = gp($_POST, 'mtel', '');
        $body = gp($_POST, 'body', '');
	     
        if (!empty($_POST)) {
        	if ($fio == '') {
	            $this->_err .= '���� <b>"�.�.�."</b> �� ����� ���� ������!<br>';
	        }
	        
	        if ($email == '') {
	            $this->_err .= '���� <b>"E-mail"</b> �� ����� ���� ������!<br />';
	        }
	        else {
    	        if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email)) {
    	            $this->_err .= '<b>"E-mail"</b> ������ �����������!<br />';
    	        }
	        }
	        
	        if ($city == '') {
	            $this->_err .= '���� <b>"�����"</b> �� ����� ���� ������!<br>';
	        }
	        
	        if ($tel == '') {
	            $this->_err .= '���� <b>"�������"</b> �� ����� ���� ������!<br>';
	        }
	        
	        if ($body == '') {
	            $this->_err .= '���� <b>"�������������� ����������"</b> �� ����� ���� ������!<br>';
	        }
	      
	        if ($delivery_office == '1' && $delivery == 'home') {
	        	if ($delivery_addres == '' /*|| preg_match('/����� .* ���� .* �������� .* ����/i', $delivery_addres)*/) {
	            	$this->_err .= '���� <b>"����� ��������"</b> �� ����� ���� ������!<br>';
	        	}	
	        }
	        
        }
        
        if (!empty($_POST) && !$this->_err) {
	        //$this->db->setQuery('INSERT INTO `orders` (`fio`, `email`, `city`, `company`, `tel`, `mtel`, `body`, `date`) VALUES ("'.$fio.'", "'.$email.'", "'.$city.'", "'.$company.'" "'.$tel.'", "'.$mtel.'", "'.$body.'", "'.mktime().'")');
	        $this->db->setQuery("INSERT INTO `orders` (`id`, `fio`, `email`, `city`,  `delivery_office`,`delivery_departament`,`delivery_addres`, `delivery`, `tel`, `mtel`, `body`, `date`) VALUES ('', '".$fio."', '".$email."', '".$city."', '".$delivery_office."','".$delivery_departament."','".$delivery_addres."', '".$delivery."', '".$tel."', '".$mtel."', '".$body."', '".mktime()."');");
	        //print_r($_SESSION['goods']);
	        $size = sizeof($_SESSION['goods']);
            
	        $id = $this->db->getLastInsertId();
	        
		    $summa = 0;
		    
		    $goods = '<table border="1" cellspacing="0" cellpadding="0" width="100%">
	<tr>
	<td>�������������</td>
	<td>������������</td>
	<td>���� �� ��.</td>
	<td>����������</td>
	<td>�����</td>
	</tr>
	<tr>
	<td colspan="5">&nbsp;</td>
	</tr>
	';
		    
		    for ($i=0; $i<$size; $i++) {
		        //$this->db->setQuery('INSERT INTO `orders_goods` (`order_id`, `goods_id`, `count`, `cost`) VALUES ("'.$id.'", "'.$_SESSION['goods'][$i]['id'].'", '.$_SESSION['goods'][$i]['count'].', '.number_format($_SESSION['goods'][$i]['price'], 2, ',', "").')');
		        $this->db->setQuery('INSERT INTO `orders_goods` (`id`, `order_id`, `goods_id`, `count`, `cost`) VALUES ("", "'.$id.'", "'.$_SESSION['goods'][$i]['id'].'", "'.$_SESSION['goods'][$i]['count'].'", "'.$_SESSION['goods'][$i]['price'].'");');
		        
		        //$goods .= '<tr><td>'.$_SESSION['goods'][$i]['id'].'</td><td>'.$_SESSION['goods'][$i]['name'].'</td><td>'.$_SESSION['goods'][$i]['price'].' ���.</td><td>'.$_SESSION['goods'][$i]['count'].' ��.</td><td>'.round($_SESSION['goods'][$i]['price']*$_SESSION['goods'][$i]['count'], 2).' ���.</td></tr>';
		        
		        $goods .= '<tr>
				<td>'.stripslashes($_SESSION['goods'][$i]['pro']).'</td>
				<td>'.$_SESSION['goods'][$i]['article'].', '.stripslashes($_SESSION['goods'][$i]['name']).'</td>
				<td>'.number_format($_SESSION['goods'][$i]['price'], 2, ',', "'").'</td>
				<td>'.$_SESSION['goods'][$i]['count'].'</td>
				<td>'.number_format($_SESSION['goods'][$i]['price']*$_SESSION['goods'][$i]['count'], 2, ',', "'").'</td>
				</tr>
				
				<tr>
				<td colspan="5">&nbsp;</td>
				</tr>';
		        
		        $this->tpl->assign(
					array(
						'PRO' => stripslashes($_SESSION['goods'][$i]['pro']),
						'NAME' => stripslashes($_SESSION['goods'][$i]['name']),
						'ARTIKUL' => $_SESSION['goods'][$i]['article'],
						'COST' => number_format($_SESSION['goods'][$i]['price'], 2, ',', "'"),
						'COUNT' => $_SESSION['goods'][$i]['count'],
						'SUM' => number_format($_SESSION['goods'][$i]['price']*$_SESSION['goods'][$i]['count'], 2, ',', "'"),
						'ID' => $_SESSION['goods'][$i]['id'],
						'SEPARATOR' => $i<($size-1) ? '<div class="b_sep_h"><div><img src="/img/z.gif" width="1px" height="1px" alt="" /></div></div>' : '&nbsp;'
					)
				);
				
				$this->tpl->parse('ORDER_ITEM', '.order_item');
		        
		        $summa += ($_SESSION['goods'][$i]['price']*$_SESSION['goods'][$i]['count']);
		    }
		    
		    $goods .= '<tr>
	<td>����:</td>
	<td colspan="4" align="right">'.number_format($summa, 2, ',', "'").' ���</td>
	</tr></table>';
		    
		    $admin_mail = $this->cfg['settings']['admin_email'];
	    
		    require_once 'class/class.phpmailer.php';
			$mail = new PHPMailer();
			$mail->SetLanguage("ru", "lib/language/");
			$mail->IsMail();
			$mail->From = $email;
			$mail->FromName = $fio;
			$mail->AddAddress($admin_mail);
			//$mail->AddAddress($email);
			
			$mail->WordWrap = 50;                                 // set word wrap to 50 characters
			$mail->IsHTML(true);                                  // set email format to HTML
			
			$mail->Subject = "���������� ������ � �����!";
			$deliveryEmail = '';			
			$deliveryEmailBodyDeliveryMethod = ""; 
			$addressEmailBody = "����� ��������:  $delivery_addres <br />";
		
			if ($delivery_office == '1') {
				$deliveryEmailDepartament = (!empty($delivery_departament) ? "��������� ������ �������� : $delivery_departament<br /> " :'');
				
				if ($delivery == 'home') {
					$deliveryEmailBodyDeliveryMethod  = '�������� � ���� / ����� : ';
					$deliveryEmailDepartament = '';					
				} else {
					$addressEmailBody ='';
					$deliveryEmailBodyDeliveryMethod = $delivery;
				}
				$deliveryEmail .= "<br />������ �������� : $deliveryEmailBodyDeliveryMethod  <br />$deliveryEmailDepartament";				
				
					
			} else {
				$addressEmailBody = "";
				$deliveryEmail .= "���";
			}
			
			
			
			$_body = "���� ���������� ������: ".date('d-m-Y H:i')."<br /> �.�.�.: ".$fio." <br /> E-Mail: ".$email." <br /> �����: ".$city." <br />   <br /> �������: ".$tel." <br /> ��������� �������: ".$mtel." <br /> $deliveryEmail $addressEmailBody �������������� ����������: <br />".$body;
			
			$mail->Body    = $_body.'<br /><br />'.$goods;
			$mail->AltBody = $_body.'<br /><br />'.$goods;
			
			if(!$mail->Send()){
				$this->_err .= "<br>������: " . $mail->ErrorInfo;
			}
			else {
			    $mail->ClearAddresses();
			    $mail->AddAddress($email);
			    
			    if(!$mail->Send()){
	    			$this->_err .= "<br>������: " . $mail->ErrorInfo;
	    		}
			}
			
			
			
			$this->tpl->assign(
				array(
					'SUMM' => number_format($summa, 2, ',', "'"),
					'FIO' => $fio,
					'EMAIL' => $email,
					'CITY' => $city,
					'DELIVERY_OFFICE' => ($delivery_office == '1' ? '��' : '���') ,					
					'TEL' => $tel,
					'MTEL' => $mtel,
					'BODY' => $body,
					'ORDSEND' => $this->cfg['ORDERSEND']
				)
			);
			
			if ($delivery_office == '1') {
				if ($delivery == 'home') {
					
					$this->tpl->assign(
					array(	
						'DELIVERY' => '�������� � ���� / �����',
						'DELIVERY_ADDR' => $delivery_addres
						)
					);		
					$this->tpl->parse('ORDER_DELIVERY_ADDRES', '.order_delivery_addres');			
					$this->tpl->parse('ORDER_DELIVERY_DEPARTAMENT', 'null');
				} else {
					$this->tpl->assign(
					array(	
						'DELIVERY' => $delivery,
						'DELIVERY_DEPARTAMENT' => $delivery_departament,
						)
					);
					if (empty($delivery_departament)) {
						$this->tpl->parse('ORDER_DELIVERY_DEPARTAMENT', 'null');
					} else {
						$this->tpl->parse('ORDER_DELIVERY_DEPARTAMENT', '.order_delivery_departament');	
					}
					$this->tpl->parse('ORDER_DELIVERY_ADDRES', 'null');
					
				}
				
				
				$this->tpl->parse('ORDER_DELIVERY', '.order_delivery');
				
				
				
			} else {
				$this->tpl->parse('ORDER_DELIVERY', 'null');
				$this->tpl->parse('ORDER_DELIVERY_ADDRES', 'null');
				$this->tpl->parse('ORDER_DELIVERY_DEPARTAMENT', 'null');
			}
			
			
			$this->tpl->parse('CONTENT', '.order');
			
			$_SESSION['orderuser'] = null;
			$_SESSION['goods'] = null;
	    }
	    
	    if (empty($_POST) || $this->_err) {
	    	$_SESSION['orderuser']['err'] = $this->_err;
	    	$_SESSION['orderuser']['fio'] = $fio;
	    	$_SESSION['orderuser']['email'] = $email;
	    	$_SESSION['orderuser']['city'] = $city;
	    	$_SESSION['orderuser']['delivery_office'] = $delivery_office;
	    	$_SESSION['orderuser']['delivery'] = $delivery;
	    	$_SESSION['orderuser']['delivery_departament'] = $delivery_departament;
	    	$_SESSION['orderuser']['delivery_addres'] = $delivery_addres;
	    	//$_SESSION['orderuser']['company'] = $company;
	    	$_SESSION['orderuser']['tel'] = $tel;
	    	$_SESSION['orderuser']['mtel'] = $mtel;
	    	$_SESSION['orderuser']['body'] = $body;
	    	er_404('/basket');
	    }
	    
	    return true;
	}
	
	public function err_404(){
		//$page = null;
		//$query = 'SELECT * FROM `system_pages` WHERE `link` = "404" AND `lang` = "'.$this->lang.'"';
		//$this->db->setQuery($query);
		//if(!$this->db->loadObject($page)){
			//print '���� �������� ����������';
			//exit;
		//}
		
		$page = $this->db->getRow('SELECT * FROM `system_pages` WHERE `link` = "404" AND `lang` = "'.$this->lang.'"');
		
		if($this->db->getNumRows() != 1){
		    print '���� �������� ����������';
			exit;
		}
		
		$this->metatitle = $page['metatitle'];
		$this->setWay($page['name']);
		$this->header = $page['metah1'];
		$this->metakeywords = $page['metakeywords'];
		$this->metadescription = $page['metadescription'];
		
		$this->tpl->assign(array('CONTENT' => $page['description']));
		
		return true;
	}
	
	public function kontakti() {
		$this->tpl->define_dynamic('feedback', 'feedback.tpl');
		$admin_mail = $this->cfg['settings']['admin_email'];
		
		$fio = gp($_POST, 'fio', '');
		$email = gp($_POST, 'email', '');
		$tel = gp($_POST, 'tel', '');
		$body = gp($_POST, 'body', '');
		$mobile = gp($_POST, 'tel', '');
		$code = gp($_POST, 'captcha_str', '');
	
		if(!empty($_POST)){
			if (empty($fio)) $this->_err .= '<br>'.$this->cfg['empty_fio'].'!';
			if (!eregi("^[a-z0-9]+([-_\.]?[a-z0-9])+@[a-z0-9]+([-_\.]?[a-z0-9])+\.[a-z]{2,4}", $email)) $this->_err .= '<br>'.$this->cfg['wrong_email'].'';
			
			if (empty($mobile)) {
				$this->_err .= '<br>'.$this->cfg['empty_mobile'].'!';				
			}
			
			
			if (empty($body)) $this->_err .= '<br>'.$this->cfg['empty_text'].'!';
			if (empty($code) || !$code || $code !== $_SESSION['captcha_keystring']) $this->_err .= '<br>'.$this->cfg['error_captcha'].'!';
		
					
			if(empty($this->_err)){				
				require_once 'class/class.phpmailer.php';
				$mail = new PHPMailer();
				$mail->SetLanguage("ru", "lib/language/");
				$mail->IsMail();
				$mail->From = $email;
				$mail->FromName = $fio;
				$mail->AddAddress($admin_mail);
				
				$mail->WordWrap = 50;                                 // set word wrap to 50 characters
				$mail->IsHTML(true);                                  // set email format to HTML
				
				$mail->Subject = "������ � ����� http://".$_SERVER['HTTP_HOST']."/";
				$_body = "�.�.�.: $fio <br> E-Mail: ".$email." <br> �������: ".$mobile." <br> �������������� ����������: $body";
				
				$mail->Body    = $_body;
				$mail->AltBody = $_body;
				
				if(!$mail->Send()){
					$this->_err .= "<br>������: " . $mail->ErrorInfo;
				}
				else{
					//$this->db->query("INSERT INTO `feedback_db` (`id`,`date`, `fio`,`body`) VALUES (
					//'', '".date('Y-m-d H:i:s')."', '$fio', '$_body')");
				}				
			}
		}
		
		if(!empty($_POST) && !$this->_err){
			$content = $this->cfg['message_sent'].'!';
			$this->tpl->assign(array('CONTENT' => page::ViewMessage($content)));
		}
		
		if($this->_err){ 
			$this->tpl->assign(array('CONTENT' => page::ViewErr()));
		}
		
		if (empty($_POST) || $this->_err) {
			
			$vals = $this->db->getAllRecords("SELECT * FROM `page` WHERE `link`='kontakti' LIMIT 1");									
			$id = (isset($vals[0]['id']) && is_numeric($vals[0]['id']) ? $vals[0]['id']: -1);			
			$this->setMeta($vals[0]['metatitle'], $vals[0]['metakeywords'], $vals[0]['metadescription'], $vals[0]['metah1']);
			
			$id = (is_numeric($id) ? $id: -1);
			
			$this->drawPage($id, array('isShowSeparator'=>false));
			$this->tpl->assign(
				array(
					'FIO' => $fio,
					'EMAIL' => $email,
					'TEL' => $tel,
					//'CITY' => $city,
					//'COMPANY' => $company,
					//'PHONE' => $phone,
					//'FAX' => $fax,
					'MOBILE' => $mobile,
					'HREF' => '',
					'BODY' => $body,
					
					'SEPARATOR'=>'<div class="c_sep_h"><div><img src="/img/z.gif" width="1px" height="1px" alt=""></div></div>'
				)
			);
			$this->tpl->parse('CONTENT', '.feedback');						
		}
		
		return true;
	}
	
	public function sitemap(){
		$elems_h = $this->db->getAllRecords('SELECT * FROM `page` WHERE `level` = "0" AND `menu` = "horizontal" AND `visibility` = "1" AND `date` <= UNIX_TIMESTAMP() AND `date` != "0" AND `lang` = "'.$this->lang.'" ORDER BY `type`, `position`, `name`');
		
		$elems_v = $this->db->getAllRecords('SELECT * FROM `catalog` WHERE `level` = "0" AND `visibility` = "1" ORDER BY `type`, `position`, `name`');
		
		$size_h = sizeof($elems_h);
		$size_v = sizeof($elems_v);
		
		$sub = "";
		for($i=0; $i<$size_h; $i++){
			if($elems_h[$i]['type'] == "section")
				if($sub == "") $sub .= $elems_h[$i]['id'];
				else $sub .= ", ".$elems_h[$i]['id'];
		}
		
		if($sub != ''){
    		$submenu_page = $this->db->getAllRecords("SELECT `id`, `visibility`, `name`, `link`, `level`, `type` FROM `page` WHERE `level` IN (".$sub.") AND `visibility` = '1' AND `date` <= UNIX_TIMESTAMP() AND `date` != '0' ORDER BY `type`, `position`, `name`");
		}
		
		$sub = '';
		for($i=0; $i<$size_v; $i++){
			if($elems_v[$i]['type'] == "section")
				if($sub == "") $sub .= $elems_v[$i]['id'];
				else $sub .= ", ".$elems_v[$i]['id'];
		}
		
		if($sub != ''){
    		$submenu_cat = $this->db->getAllRecords("SELECT `id`, `visibility`, `name`, `link`, `level`, `type` FROM `catalog` WHERE `level` IN (".$sub.") AND `visibility` = '1' ORDER BY `type`, `position`, `name`");
		}
		//print_r($submenu_cat);
		//if($sub != ''){
    		//$submenu_page = $this->db->getAllRecords("SELECT `id`, `visibility`, `name`, `link`, `level`, `type` FROM `page` WHERE `level` IN (".$sub.") AND `visibility` = '1' ORDER BY `type`, `position`, `name`");
		//}
		
		//$first = array_merge($elems_h, $elems_v);
		
		$size = sizeof($elems_h);
		
		$map = '<div id="sitemap">';
		//$map .= "\n\t<li><a href='".$this->langUrl."/' title='".$this->cfg['WAY']."'>".
		//$this->cfg['WAY']."</a></li>";
		$map .= '<div class="map-f"><a href="'.$this->langUrl.'/" title="'.$this->cfg['WAY'].'">'.$this->cfg['WAY'].'</a></div>';
		
		for($i=0; $i<$size; $i++){
			//$map .= "\n\t<li><a href='".(($elems_h[$i]['type'] != 'link')?$this->langUrl.'/':'').$elems_h[$i]['link'].(($elems_h[$i]['type'] == 'section')?('/'):(''))."' title='".$elems_h[$i]['name']."'>".$elems_h[$i]['name']."</a></li>";
			$map .= '<div class="map-f"><a href="'.(($elems_h[$i]['type'] != 'link')?$this->langUrl.'/':'').$elems_h[$i]['link'].(($elems_h[$i]['type'] == 'section')?('/'):('')).'" title="'.$elems_h[$i]['name'].'">'.$elems_h[$i]['name'].'</a></div>';
			
			if($elems_h[$i]['type'] == 'section'){
				$sub = null;
				$sub = $this->getSubMenu($submenu_page, $elems_h[$i]['id']);
				
				if(sizeof($sub) > 0){
					$size_sub = sizeof($sub);
					
					//$map .= "\n\t<ul>";
					
					for($j=0; $j<$size_sub; $j++){
						$url = ($sub[$j]['type'] == 'link') ? $sub[$j]['link'] : $this->langUrl.'/'.$elems_h[$i]['link'].'/'.$sub[$j]['link'];
						//$map .= "\n\t\t<li><a href='".$url.(($sub[$j]['type'] == 'section')?('/'):(''))."' title='".$sub[$j]['name']."'>".$sub[$j]['name']."</a></li>";
						$map .= '<div class="map-s"><a href="'.$url.(($sub[$j]['type'] == 'section')?('/'):('')).'" title="'.$sub[$j]['name'].'">'.$sub[$j]['name'].'</a></div>';
					}
					
					//$map .= "\n\t</ul>";
				}
			}
		}
		
		$size = sizeof($elems_v);
		
		for($i=0; $i<$size; $i++){
			//$map .= "\n\t<li><a href='".(($elems_v[$i]['type'] != 'link')?$this->langUrl.'/':'').$elems_v[$i]['link'].(($elems_v[$i]['type'] == 'section')?('/'):(''))."' title='".$elems_v[$i]['name']."'>".$elems_v[$i]['name']."</a></li>";
			
			$map .= '<div class="map-f"><a href="'.(($elems_v[$i]['type'] != 'link')?$this->langUrl.'/catalog/':'').$elems_v[$i]['link'].(($elems_v[$i]['type'] == 'section')?('/'):('')).'" title="'.$elems_v[$i]['name'].'">'.$elems_v[$i]['name'].'</a></div>';
			
			if($elems_v[$i]['type'] == 'section'){
				$sub = null;
				$sub = $this->getSubMenu($submenu_cat, $elems_v[$i]['id']);
				
				if(sizeof($sub) > 0){
					$size_sub = sizeof($sub);
					
					//$map .= "\n\t<ul>";
					
					for($j=0; $j<$size_sub; $j++){
						$url = ($sub[$j]['type'] == 'link') ? $sub[$j]['link'] : $this->langUrl.'/catalog/'.$elems_v[$i]['link'].'/'.$sub[$j]['link'];
						//$map .= "\n\t\t<li><a href='".$url.(($sub[$j]['type'] == 'section')?('/'):(''))."' title='".$sub[$j]['name']."'>".$sub[$j]['name']."</a></li>";
						
						$map .= '<div class="map-s"><a href="'.$url.(($sub[$j]['type'] == 'section')?('/'):('')).'" title="'.$sub[$j]['name'].'">'.$sub[$j]['name'].'</a></div>';
					}
					
					//$map .= "\n\t</ul>";
				}
			}
		}
		
		$map .= "</div>";
		
		$this->tpl->assign(array('CONTENT' => $map));
		
		return true;
	}
	
	public function search(){
		require_once('class/class.search.php');
		
		$search = new search($this->tpl, $this->cfg, $this->db, $this->lang, $this->langUrl, $this->page);
		$search->makeSearch();
		//$search->makeSearchCat();
		
		return true;
	}
	
	public function enter(){
		$auth = new auth($this->db, $this->tpl, $this->cfg, $this->userData, $this->langUrl);
		
		if(!$auth->enter()){
			return false;
		}
		else{
			$this->SetMeta($auth->metatitle, $auth->metakeywords, $auth->metadescription, $auth->header);
			return true;
		}
	}
	
	public function admin() {
		require_once("class/class.admin.php");
		$admin = new admin($this->db, $this->tpl, $this->cfg, $this->userData, $this->w, $this->lang, $this->langUrl);
	
		@$method = $this->w[1];
		
		if(isset($this->w[1]) && (!method_exists($admin, $this->w[1]) || !is_callable(array($admin, $this->w[1])) || !$admin->$method())){
			return false;
		}
		else{
			$this->setMeta($admin->metatitle, $admin->metakeywords, $admin->metadescription, $admin->header, $admin->way);
			//$this->setWay($admin->way);
			return true;
		}
	}
	
	public function logout() {
		if (isset($_SESSION['userData'])) $_SESSION['userData'] = null;
		
		header("Location: /".$this->langUrl);
		
		return true;
	}
		
	public function setMeta($metatitle = null, $metakeywords = null, $metadescription = null, $header = null, $way = null){		
	    if($metatitle != null && $metakeywords == null && $metadescription == null && $header == null && $way == null){
			$this->metatitle = $metatitle;
			$this->metakeywords = $metatitle;
			$this->metadescription = $metatitle;
			$this->header = $metatitle;
			$this->way .= $metatitle;
		}
		else{
			if($metatitle) $this->metatitle = $metatitle;
			if($metakeywords) $this->metakeywords = $metakeywords;
			if($metadescription) $this->metadescription = $metadescription;
			if($header) $this->header = $header;
			if($way) $this->way .= $way;
		}
	}
	
	protected function setWay($way){
		$this->way .= $way;
	}
	
	private function extractWay($location = 'page'){
	   $size = sizeof($this->w) - 1;
	    
	    if($size > 0) {
	        $start = ($location == 'catalog') ? 1 : 0;
	        
	        $sub = '';
    	    for ($i=$start; $i<$size; $i++) {
    	        if ($sub == '') {
    	            $sub .= '"'.$this->w[$i].'"';
    	        }
    	        else {
    	            $sub .= ',"'.$this->w[$i].'"';
    	        }
    	    }
    	    
    	    $start = 0;
    	    $size = ($location == 'catalog') ? ($size - 1) : $size;
    	    
    	    //echo $sub.' - '.$size;
    	    if ($sub != '') {
    	        $items = $this->db->getAllRecords('SELECT `name`, `link` FROM `'.$location.'` WHERE `link` IN ('.$sub.') ORDER BY `id` ASC');
    	        //echo 'SELECT `name`, `link` FROM `'.$location.'` WHERE `link` IN ('.$sub.') ORDER BY `id` ASC';
    	        //print_r($items);
    	        if ($this->db->getNumRows() != $size) {
    	            echo '����<br />'.$this->db->getNumRows().'<br />'.$size;
    	        }
    	    }
    	    
    	    $url = ($location == 'catalog') ? '/catalog/' : '/';
    	    
    	    for ($i=$start; $i<$size; $i++) {
    	    	if (!isset($items[$i]['link'])) {return false;}
    	        $url .= $items[$i]['link'].'/';
    	        $this->way .= ' &raquo; <a href="'.$url.'">'.$items[$i]['name'].'</a>';
    	    }
	    }
	        
		/*$size = sizeof($this->w) - 1;
		
		$url = '/';
		
		$level = 0;
		
		for($i=0; $i<$size; $i++){
			$link = mysql_real_escape_string(ru2lat($this->w[$i]));
			$query = 'SELECT `name`, `type`, `id` FROM `page` WHERE `lang` = "'.$this->lang.'" AND `link` = "'.$link.'" AND `level` = '.$level;
			$this->db->setQuery($query);
			$w = $this->db->loadRow();
			
			$url .= $link.(($w[1] == 'section')?('/'):(''));
			$level = $w[2];
			
			
			if($w[0] != '') $this->setWay($w[0], $url);
			else{
				er_404();
				exit;
			}
		}*/
		
		return true;
	}
	
	protected function extractPageAdres(){
		$size = sizeof($this->w);
		
		$url = '/'.(($this->lang != 'ru')?$this->lang.'/':'');
		
		for($i=0; $i<$size; $i++){
			$url .= $this->w[$i].'/';
		}
		
		return $url;
	}
	
	private function getLookUps(){
		//$query = 'SELECT `key`, `value` FROM `lookups` WHERE `lang` = "'.$this->lang.'"';
		//$this->db->setQuery($query);
		//$lookupsArr = $this->db->loadAssocList();
		
		$lookupsArr = $this->db->getAllRecords('SELECT `key`, `value` FROM `lookups` WHERE `lang` = "'.$this->lang.'"');
		
		$size = $this->db->getNumRows();
		
		if($size > 0){
			foreach ($lookupsArr as $key => $value){
				$this->cfg[$value['key']] = stripslashes($value['value']);
			}
		}
		
		$this->setMeta($this->cfg['WAY'], null, null, null, '<a href="/" title="'.$this->cfg['WAY'].'">'.$this->cfg['WAY'].'</a>');
		//$this->setWay('<a href="/">'.$this->cfg['WAY'].'</a>');
		
		foreach($this->cfg as $key => $value){
			$this->tpl->assign(array($key => $value));
		}
		
		/*$query = 'SELECT * FROM `meta_tags` WHERE `lang` = "'.$this->lang.'"';
		$this->db->setQuery($query);
		$lookupsArr = $this->db->loadAssocList();
		
		for($i=0; $i<sizeof($lookupsArr); $i++){
			echo "INSERT INTO `meta_tags` ( `link`, `name`, `metatitle`, `metakeywords`, `metadescription`, `metah1`, `lang` ) VALUES ('".$lookupsArr[$i]['link']."', '".$lookupsArr[$i]['name']."', '".$lookupsArr[$i]['metatitle']."', '".$lookupsArr[$i]['metakeywords']."', '".$lookupsArr[$i]['metadescription']."', '".$lookupsArr[$i]['metah1']."', 'ua');";
			echo '<br />';
			//echo "INSERT INTO `meta_tags` ( `link`, `name`, `metatitle`, `metakeywords`, `metadescription`, `metah1`, `lang` ) VALUES ('".$lookupsArr[$i]['link']."', '".$lookupsArr[$i]['name']."', '".$lookupsArr[$i]['metatitle']."', '".$lookupsArr[$i]['metakeywords']."', '".$lookupsArr[$i]['metadescription']."', '".$lookupsArr[$i]['metah1']."', 'en');";
			//echo '<br />';
		}*/
		
		/*$query = "SELECT * FROM `lookups` WHERE `lang` = 'ru' ORDER BY `position` ASC, `key`";
		$this->db->setQuery($query);
		$lookupsArr = $this->db->loadAssocList();
		
		for($i=0; $i<sizeof($lookupsArr); $i++){
			echo $query = "INSERT INTO `lookups` ( `key`, `Name`, `value`, `lang`, `position` ) VALUES ('".$lookupsArr[$i]['key']."', '".$lookupsArr[$i]['Name']."', '".$lookupsArr[$i]['value']."', 'ua', '".($i+1+100)."');";
			$this->db->setQuery($query);
			$this->db->query();
			echo '<br />';
		}*/
	}
	
	private function getSettings(){
		//$query = 'SELECT `key`, `value` FROM `settings`';
		//$this->db->setQuery($query);
		//$settings = $this->db->loadAssocList();
		
		$settings = $this->db->getAllRecords('SELECT `key`, `value` FROM `settings`');
		
		foreach($settings as $set){
			$this->cfg['settings'][$set['key']] = $set['value'];
		}
	}
	
	private function load_meta_tags(){
		if(isset($this->w[0])){
			$link = mysql_real_escape_string(ru2lat($this->w[0]));
			
			if($link == 'index' || $link == '404') return true;
			
			//$query = 'SELECT * FROM `meta_tags` WHERE `link` = "'.$link.'" AND `lang` = "'.$this->lang.'"';
			//$this->db->setQuery($query);
			//if($this->db->loadObject($tags)){
				//$this->setWay($tags->metah1, '/'.$tags->link.'/');
				//$this->setMeta($tags->metatitle, $tags->metakeywords, $tags->metadescription, $tags->metah1);
				//if($tags->hpic != '') $this->hpic = $tags->hpic;
			//}
			
			$tags = $this->db->getRow('SELECT * FROM `meta_tags` WHERE `link` = "'.$link.'" AND `lang` = "'.$this->lang.'"');
			
			if($this->db->getNumRows() == 1){
			    $adm = '';
			    if(isset($_SESSION['userData']['privilege']) && ($_SESSION['userData']['privilege'] == 'admin' || $_SESSION['userData']['privilege'] == 'master')){
			        $adm = '&nbsp;&nbsp;&nbsp;<a href="/admin/edit_meta_tags/'.$tags['id'].'" title="�������������"><img src="/img/admin_icons/edit.png" width="12" height="12"></a>';
			        //echo '1';
			    }
			    //$this->setWay(' &raquo; <a href="/'.$tags['link'].'/">'.$tags['metah1'].'</a>');
				$this->setMeta($tags['metatitle'], $tags['metakeywords'], $tags['metadescription'], $tags['metah1'].$adm, ' &raquo; <a href="/'.$tags['link'].'/">'.$tags['metah1'].'</a>');				
				$this->tpl->assign('CONTENT', '<div id="body">'.(isset($tags['body']) ? stripslashes($tags['body']) : '').'</div>');
			}
		}
		return true;
	}
	
	protected function ViewErr() {
		return '<div id="err">'.$this->_err.'</div>';
	}

	protected function ViewMessage($message) {
		return "<div style='padding: 20px 5px 10px 50px;'>".$message.'</div>';
	}
	
}

?>