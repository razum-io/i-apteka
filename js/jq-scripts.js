jQuery(function(){
	jQuery('.goods-length').click(function(){
		var val = jQuery(this).val();
		if (isNaN(val) || val <= 1) {
			jQuery(this).val('');
		}
	}).blur(function(){
		var val = jQuery(this).val();
		if (isNaN(val) || val <= 1) {
			jQuery(this).val('1');
		}
	});
});