function g1(){
	id=1;
}

function g2(){
	id=2;
}

function g3(){
	id=3;
}

function getSuggestion(e, suggest){
	var unicode = e.keyCode ? e.keyCode : e.charCode;
	if((unicode >= 65 && unicode <= 90) || unicode == 8){
		
		if(suggest != ""){
			$.get("./php/getsuggest.php", { search: suggest }, function(data){
				$('#suggestbox'+id).html(data);
				if($('#suggestbox'+id).is(":visible") == false){
					$('#suggestbox'+id).show();
					$('#search_input'+id).css('border-bottom-left-radius', 0);
				}
			});
		}else{
			$('#suggestbox'+id).html("").hide();
			$('#search_input'+id).css('border-bottom-left-radius', 5);
		}
	}
}

function putSuggestion(element){
	$('#search_input'+id).val($(element).html()).css('border-bottom-left-radius', 5);
	$('#suggestbox'+id).hide();
}
