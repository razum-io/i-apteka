			Как подключить текстовый редактор + загрузчик картинок к полю <TEXTAREA> инструкция от Скряги Д.А.
			
	1)Берем папку elrte
		1.1)Переходим в elrte/elfinder/connectors/php/
		1.2) открываем файл connector.php 
		1.3) находим строчку "$opts = array(" и в первом параметре root меняем путь к папке в которой будут храниться файлы можно абсолютный (ШТТП://....)
				либо относительный путь в формате (../../files) относительно данного файла, формат (/files) не подойдет
		1.4) Во втором параметре 'URL'  - пишем путь к той же папке только абсолютный (http://САЙТ.ОЛОЛО/admin/elrte/elfinder/files/)
	
	2)Папку elrte кладем в папку админки
	
	3)Чтоб подключить редактор в файлы необходимо:
		3.1)либо подключать в каждом файле код либо сделать файл шапки и подключать файл, или если он у вас уже есть добавить/вставить туда код подключения js и CSS -файлов 
			ВАЖНО: возможны конфликты с уже подключенными файлами (тут уж сами разбирайтесь)
			
	вот этот код:
	
	<!-- jQuery and jQuery UI -->
	<script src="elrte/elfinder/js/jquery-1.4.1.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="elrte/elfinder/js/jquery-ui-1.7.2.custom.min.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="elrte/css/smoothness/jquery-ui-1.8.13.custom.css" type="text/css" media="screen" charset="utf-8">

	<link rel="stylesheet" href="elrte/elfinder/css/elfinder.css" type="text/css" media="screen" title="no title" charset="utf-8">
	
	<!-- elRTE -->
	<script src="elrte/js/elrte.min.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="elrte/css/elrte.min.css" type="text/css" media="screen" charset="utf-8">

	<!-- elRTE translation messages -->

	<script src="elrte/js/jquery.backgroundPosition.js" type="text/javascript" charset="utf-8"></script>
	
	<script src="elrte/elfinder/js/elfinder.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="elrte/elfinder/js/i18n/elfinder.ru.js" type="text/javascript" charset="utf-8"></script>
	
	
	
	4)И последнее, в каждом файле или созданой шапке треба подлючать код инициализации самого редактора
		я ставил его в самый низ страницы после тега </body>
		
		и каждой <textarea> дописать - id="editor", чтоб получилось <textarea id="editor"> 
		
		если <textarea> больше одного треба дописывать в каждый  тег этот ид + какой то уникальный идентификатор (я например делал так)
		<textarea id="editor">
		<textarea id="editor2">
		<textarea id="editor3">
		...
		<textarea id="editorN">
		
		а в код инициализации
		добавлять строки 
		$('#editor').elrte(opts);
		$('#editor2').elrte(opts);
		$('#editor3').elrte(opts);
		...
		$('#editorN').elrte(opts);
		в примере она одна
		
		
		4.1) в строке url:   в коде пишем адрес файла connector.php 
		
		
		
		КОД ИНИЦИАЛИЗАЦИИ:
		
		<script type="text/javascript" charset="utf-8">
		$().ready(function() {
			 
				$('#elFinder a').hover(
					function () {
						$('#elFinder a').animate({
							'background-position' : '0 -45px'
						}, 300);
					},
					function () {
						$('#elFinder a').delay(400).animate({
							'background-position' : '0 0'
						}, 300);
					}
				);

			$('#elFinder a').delay(800).animate({'background-position' : '0 0'}, 300);
			
			var opts = {
				absoluteURLs: false,
				cssClass : 'el-rte',
				lang     : 'ru',
				height   : 420,
				toolbar  : 'maxi',
				cssfiles : ['css/elrte-inner.css'],
				fmOpen : function(callback) {
					$('<div id="myelfinder" />').elfinder({
						url : 'elrte/elfinder/connectors/php/connector.php'',
						lang : 'en',
						dialog : { width : 900, modal : true, title : 'elFinder - file manager for web' },
						closeOnEditorCallback : true,
						editorCallback : callback
					})
				}
			}
			$('#editor').elrte(opts);
			
		})
		</script>
	