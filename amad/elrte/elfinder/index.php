<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>elFinder</title>
	<!--
	<script type='text/javascript' src='http://getfirebug.com/releases/lite/1.2/firebug-lite-compressed.js'></script>
	-->
	<link rel="stylesheet" href="js/ui-themes/base/ui.all.css" type="text/css" media="screen" title="no title" charset="utf-8">
	<link rel="stylesheet" href="css/elfinder.css" type="text/css" media="screen" title="no title" charset="utf-8">

	<script src="js/jquery-1.4.1.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="js/jquery-ui-1.7.2.custom.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="js/elfinder.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="js/i18n/elfinder.ru.js" type="text/javascript" charset="utf-8"></script>


	<style type="text/css">
		#open{
			width: 100px;
			position:relative;
			display: -moz-inline-stack;
			display: inline-block;
			vertical-align: top;
			zoom: 1;
			*display: inline;
			margin:0 3px 3px 0;
			padding:1px 0;
			text-align:center;
			border:1px solid #ccc;
			background-color:#eee;
			margin:1em .5em;
			padding:.3em .7em;
			border-radius:5px; 
			-moz-border-radius:5px; 
			-webkit-border-radius:5px;
			cursor:pointer;
		}
	</style>
	

		<script type="text/javascript" charset="utf-8">
		$().ready(function() {
			
		
			// window.console.log(f)
			$('#open').click(function() {
				$('<div id="finder" />').elfinder({
					url : 'connectors/php/connector.php',
					lang : 'ru',
					rememberLastDir : true,
					contextmenu : {
						cwd : ['reload', 'delim', 'mkdir', 'mkfile', 'upload', 'delim', 'paste', 'delim', 'info'], 
						file : ['select', 'open', 'delim', 'copy', 'cut', 'rm', 'delim', 'duplicate', 'rename'], 
						group : ['copy', 'cut', 'rm', 'delim', 'archive', 'extract', 'delim', 'info'] 
					 },
										
					dialog : { title : 'File manager', width : 500, modal : true },
					editorCallback : function(url) {
						//alert(url);
						vv='<img src="'+url+'" alt="" >';
						$('#image').html(vv);
						$('#url').html(url);
						
						lati = document.getElementById("myID"); 
						lati.value = url;
						
					},
					closeOnEditorCallback : true,
				})
				
			});
			
			
			
			
			
			
		});
	</script>

</head>
<body>

<div id="image"></div>

<div id="url" style="color:red; font-size:20px;"></div>

<input type="hidden" size="100" id="myID" value="">
<br /><br />
	<button type="button" id="open">Добавить/Выбрать</button><br />
	<button type="button" id="MyBid"  onclick="window.opener.myFunc (document.getElementById ('myID').value);self.close ()" >Загрузить картинку в обработчик</button>
	
	
	

</body>
</html>
