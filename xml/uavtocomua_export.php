<?
ini_set('display_errors', 'on');
require_once '../cfg/cfg.php';
require_once PATH.'lib/funcs.php';
require_once PATH.'lib/rewrite.php';
require_once PATH.'lib/class.Template.php';
require_once PATH.'lib/class.Mysql.php';
require_once PATH.'lib/class.Content.php';
require_once PATH.'lib/class.Catalog.php';
require_once PATH.'lib/class.Auth.php';

session_start();
//var_dump($_POST);
$lang = (isset($_SESSION['lang']) && in_array($_SESSION['lang'], $cfg['settings']['langs'])) ? $_SESSION['lang']:'rus';

$db = new DB($cfg['db']['host'], $cfg['db']['user'], $cfg['db']['pass'], $cfg['db']['name']);
read_settings();
$tpl = new Template('tpl/');
$tpl->define_dynamic('cart', 'cart.tpl');
$tpl->define_dynamic('null', 'cart');
$userData = (isset($_SESSION['userData'])) ? $_SESSION['userData']:null;

$cat_groups = $db->queryAllRecords("SELECT * FROM `groups` ORDER BY `position` ASC");
        
$catalog = new Catalog($db, $tpl, $cfg, $userData, $lang, $cat_groups);

$items = $db->queryAllRecords("SELECT SQL_CACHE * FROM `goods` WHERE `index` = '1' ORDER BY RAND() LIMIT 0,4");
$goods = '';
foreach ($items as $item) {
$pic = (!empty($item['code'])) ? 'http://uamag.com.ua/goods/small/'.$item['code'].'.jpg':'';
	$goods .= 
	"\t<item>
	\t\t<id>".$item['id']."</id>
	\t\t<name>".
str_ireplace(array('&','<','>'), "", str_ireplace('/', "", str_ireplace('<br ', "\n", str_ireplace('<br /', "\n", str_ireplace('<br />', "\n", htmlspecialchars(iconv("CP1251", "UTF-8", $item['name'])))))))
."</name>
	\t\t<name2>".
str_ireplace(array('&','<','>'), "", str_ireplace('/', "", str_ireplace('<br ', "\n", str_ireplace('<br /', "\n", str_ireplace('<br />', "\n", htmlspecialchars(iconv("CP1251", "UTF-8", $item['name2'])))))))
."</name2>
	\t\t<link>http://uamag.com.ua".$catalog->extractAdress($item['group_id']).$item['adress_str']."</link>
	\t\t<pic>$pic</pic>
	\t</item>
	";
}
header('Content-type: text/xml');
//header('Content-encoding: windows-1251');
header('Content-encoding: UTF-8');

//echo "<?xml version=\"1.0\" encoding=\"UTF-8\"

echo "<?xml version='1.0' encoding='UTF-8'?>
<rss version=\"2.0\">
   <channel>
      <title>Uamag</title>
      <link>http://uamag.com.ua/</link>

\t".$goods."
    </channel>
</rss>";

/*
header('Content-type: text/xml');
header('Content-encoding: windows-1251');
echo '<?xml version="1.0" encoding="windows-1251"?>
<auto-catalog>
  <creation-date>'.date('Y-m-d H:i:s').' GMT+2</creation-date>
  <host>uamag.com.ua</host>
 
  <offers>
    '.$offers.'
  </offers>
</auto-catalog>';*/