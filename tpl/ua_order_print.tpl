<!-- BDP: order_form -->
<script>
      var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
    
</script>
<div class="type_elem">
  
 
  <div class="basket_table2">
  
  <div class="print print_logo">
   <p>Телефон: +38 (067) 433 33 53<br />www.ikwell.com.ua<br />e-mail: admin@ikwell.com.ua</p>
  </div>
  <p class="print"><span class="right"><strong>Накладна № {ORDER_GOODS_NO_ZAKAZA}</strong><br />Від  «25» серпня 2011 року</span><span>Поставщик:</span> <strong>ТОВ «I.K.ВЕЛ»</strong><br />Адреса: <strong>49000, м. Дніпропетровськ, Вул.Селянський узвіз, 3-А</strong><br />Р/С: <strong>26004229031800 в АТ «Укрсиббанк» МФО 351005</strong><br />код ЄДРПОУ: <strong>36439904</strong><br />ІНН: <strong>364399004632</strong><br />Свід.пл.ПДВ: <strong>№100221024</strong></p>
  <p class="print"><span>Покупець:</span> <strong>{ORDER_GOODS_FIO}</strong><br />Адреса: <strong>{ORDER_GOODS_ADR}</strong><br />e-mail: <strong>{ORDER_GOODS_EMAIL}</strong><br />Телефон мобільный: <strong>{ORDER_GOODS_MPHONE}</strong><br />Телефон домашній/робочий: <strong>{ORDER_GOODS_PHONE}</strong><br />Точка самовивозу: <strong>{ORDER_GOODS_TVIVOZA}</strong><br />Додаткова інформація: <strong>{ORDER_GOODS_INFO}</strong></p>
  <table>
   <tr>
    <th class="print_t">№</th>
    <th>Назва</th>
    <th>Виробник</th>
    <th>Ціна</th>
    <th>Кіл-ть</th>
    <th>Сума (грн.)</th>
   </tr>



<!-- BDP: order_goods_list -->
  <tr>
    <td class="print_t">{ORDER_GOODS_NP}</td>
    <td>{ORDER_GOODS_NAME}</td>
    <td>{ORDER_GOODS_PRODUCER}</td>
    <td>{ORDER_GOODS_COST}</td>
    <td>{ORDER_GOODS_COUNT} шт.</td>
    <td>{ORDER_GOODS_COST_SUMM}</td>
   </tr>
   <tr>

<!-- EDP: order_goods_list -->

  </table> 
   
  <p class="total forsite">Всього: <span>{ORDER_GOODS_TOTAL} грн</span><br /><a href="/{REGION_URL}order/printOrder" >Друкувати заказ</a><br /><a href="#" onclick="print_preview(); return false;">Друкувати накладну</a></p>
  <!--<p class="print print_total">Всего на сумму: <strong>{ORDER_GOODS_TOTAL} грн.</strong><br />Заказано единиц товара: <strong>{ORDER_GOODS_NUM_STR}</strong></p>-->
    <div class="print print_sign">Відправив<br /><img src="/img/sign.gif" width="293" height="12" alt="" /></div>
  <div class="print print_sign print_sign2">Прийняв<br /><img src="/img/sign.gif" width="293" height="12" alt="" /></div>
  <div class="clear"></div>
  </div>
  
<!-- EDP: order_form -->

<!-- BDP: order_empty -->
<h2>У вас немає заказів</h2>
<!-- EDP: order_empty -->

<!-- BDP: order_success -->
<h2>Заказ виконаний</h2>
<!-- EDP: order_success -->

  
