<!-- BDP: order_form -->
<div class="type_elem">
  
 
  <div class="basket_table2">
  
  <div class="print print_logo">
   <p>Телефон: +38 (067) 433 33 53<br />www.ikwell.com.ua<br />e-mail: admin@ikwell.com.ua</p>
  </div>
  <p class="print"><span class="right"><strong>Накладная № {ORDER_GOODS_NO_ZAKAZA}</strong><br />От  «25» августа 2011 года</span><span>Поставщик:</span> <strong>ТОВ «I.K.ВЕЛ»</strong><br />Адрес: <strong>49000, м. Дніпропетровськ, Вул.Селянський узвіз, 3-А</strong><br />Р/С: <strong>26004229031800 в АТ «Укрсиббанк» МФО 351005</strong><br />код ЄДРПОУ: <strong>36439904</strong><br />ІНН: <strong>364399004632</strong><br />Свід.пл.ПДВ: <strong>№100221024</strong></p>
  <p class="print"><span>Покупатель:</span> <strong>{ORDER_GOODS_FIO}</strong><br />Адрес: <strong>{ORDER_GOODS_ADR}</strong><br />e-mail: <strong>{ORDER_GOODS_EMAIL}</strong><br />Телефон мобильный: <strong>{ORDER_GOODS_MPHONE}</strong><br />Телефон домашний/рабочий: <strong>{ORDER_GOODS_PHONE}</strong><br />Точка самовывоза: <strong>{ORDER_GOODS_TVIVOZA}</strong><br />Дополнительная информация о заказе: <strong>{ORDER_GOODS_INFO}</strong></p>
  <p class="forsite">На указанный Вами «e-mail» отправлено письмо с подтверждением о покупке.<br />Вскоре заказ будет обработан нашим менеджером.</p>
  <p class="forsite">Номер заказа: <strong>{ORDER_GOODS_NO_ZAKAZA}</strong><br />Дата заказа: <strong>{ORDER_GOODS_DATA_ZAKAZA}</strong><br />Ф.И.О.: <strong>{ORDER_GOODS_FIO}</strong><br />Адрес: <strong>{ORDER_GOODS_ADR}</strong><br />e-mail: <strong>{ORDER_GOODS_EMAIL}</strong><br />Телефон мобильный: <strong>{ORDER_GOODS_MPHONE}</strong><br />Телефон домашний/рабочий: <strong>{ORDER_GOODS_PHONE}</strong><br />Точка самовывоза: <strong>{ORDER_GOODS_TVIVOZA}</strong><br />Дополнительная информация о заказе: <strong>{ORDER_GOODS_INFO}</strong></p>
  <table>
   <tr>
    <th class="print_t">№</th>
    <th>Наименование</th>
    <th>Производитель</th>
    <th>Цена</th>
    <th>Кол-во</th>
    <th>Сумма (грн.)</th>
   </tr>



<!-- BDP: order_goods_list -->
  <tr>
    <td class="print_t">{ORDER_GOODS_NP}</td>
    <td>{ORDER_GOODS_NAME}</td>
    <td>{ORDER_GOODS_PRODUCER}</td>
    <td>{ORDER_GOODS_COST}</td>
    <td>{ORDER_GOODS_COUNT} шт.</td>
    <td>{ORDER_GOODS_COST_SUMM}</td>
   </tr>

<!-- EDP: order_goods_list -->

  </table> 
   
  <p class="total forsite">Итого: <span>{ORDER_GOODS_TOTAL} грн</span><br /><a href="/{REGION_URL}order/printOrder" target="_blank">Напечатать заказ</a><br /><a href="/{REGION_URL}order/printBill" target="_blank">Напечатать накладную</a></p>
      <div class="print print_sign">Отпустил<br /><img src="/img/sign.gif" width="293" height="12" alt="" /></div>
  <div class="print print_sign print_sign2">Получил<br /><img src="/img/sign.gif" width="293" height="12" alt="" /></div>
  <div class="clear"></div>
  </div>
  </div>
<!-- EDP: order_form -->

<!-- BDP: order_empty -->
<h2>У вас нет заказов</h2>
<!-- EDP: order_empty -->

<!-- BDP: order_success -->
<h2>Заказ выполнен</h2>
<!-- EDP: order_success -->

 
