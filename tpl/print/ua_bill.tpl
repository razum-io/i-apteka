<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>Index</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/css/style.css" /> 
<script type="text/javascript" src="/js/jquery-1.4.2.min.js"></script> 
<script type="text/javascript" src="/js/js_func.js"></script>
<link rel="alternate stylesheet" type="text/css" href="/css/print.css" media="screen" title="Print Preview" />
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print" />
<script type="text/javascript" src="/js/print.js"></script>
<script>
// Switch the stylesheet
	setActiveStyleSheet('Print Preview');
	// Print the page
	window.print();
</script>
</head>
<body>


<div class="wraper">


 <div class="right_column">
 <div class="type_elem">
  <div class="basket_table2">
  <h1 class="forsite">Дякуємо за заказ!</h1>
  <div class="print print_logo">
      <img src="/img/logo2.jpg" class="logo_print">
   <p>{PRINT_HEADER}</p>
  </div>
  <p class="print"><span class="right"><strong>Накладна № {PRINT_ORDER_NUMBER}</strong><br />від  {PRINT_ORDER_DATE}</span>{PRINT_INFO}</p>
  <p class="print"><span>Покупець:</span> <strong>{PRINT_ORDER_FIO}</strong><br />Адреса: <strong>{PRINT_ORDER_ADR}</strong><br />e-mail: <strong>{PRINT_ORDER_EMAIL}</strong><br />Телефон мобільний: <strong>{PRINT_ORDER_MPHONE}</strong><br />Телефон домашній/робочий: <strong>{PRINT_ORDER_PHONE}</strong><br />Точка самовивозу: <strong>{PRINT_ORDER_TVIVOZA}</strong><br />Додаткова інформація: <strong>{PRINT_ORDER_INFO}</strong></p>
  <table>
   <tr>
    <th class="print_t">№</th>
    <th>Назва</th>
    <th>Виробник</th>
    <th>Ціна</th>
    <th>Кіл-ть</th>
    <th>Сума (грн.)</th>
   </tr>
     <!-- BDP: item_row -->
   <tr>
    <td class="print_t">{PRINT_ORDER_GOODS_NO}</td>
    <td>{PRINT_ORDER_GOODS_NAME}</td>
    <td>{PRINT_ORDER_GOODS_PRODUCER}</td>
    <td>{PRINT_ORDER_GOODS_COST}</td>
    <td>{PRINT_ORDER_GOODS_COUNT} шт.</td>
    <td>{PRINT_ORDER_GOODS_SUMM}</td>
   </tr>
  <!-- EDP: item_row -->


  </table>
  
  <p class="print print_total">Всього на суму: <strong>{PRINT_ORDER_TOTAL_SUMM} грн.</strong></p>
  
  <div class="print print_sign">Відправив<br /><img src="/img/sign.gif" width="293" height="12" alt="" /></div>
  <div class="print print_sign print_sign2">Прийняв<br /><img src="/img/sign.gif" width="293" height="12" alt="" /></div>
  <div class="clear"></div>
  </div>
 </div>
 </div>
</div>

</body>
</html>