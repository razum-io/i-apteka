<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML><HEAD><TITLE>{TITLE}</TITLE>
<meta name="keywords" content="{KEYWORDS}">
<meta name="description" content="{DESCRIPTION}">
<META http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="document-state" content="dynamic">
<meta name="revisit" content="7 days">
<meta name="revisit-after" content="7 days">
<META NAME="Resourse-type" CONTENT="document">
<META NAME="Robots" CONTENT="index,follow">
<META NAME="Rating" CONTENT="general">
<META NAME="Distribution" CONTENT="global">
<META NAME="Classification" CONTENT="">
<META NAME="Category" CONTENT="">
<meta http-equiv="Pragma" content="token">
<meta http-equiv="Cache-Control" content="token">
<META NAME="Copyright" CONTENT="2011 deluxe.dp.ua">
<LINK href="/css/style.css" type="text/css" rel=stylesheet>
<link  rel="stylesheet" type="text/css" href="/css/lightbox.css" />
<link  rel="stylesheet" type="text/css" href="/css/calendar-blue.css" />
{CANONICAL_URL}
<LINK REL="shortcut icon" HREF="/favicon.ico" type="image/x-icon">

<!--[if lte IE 6]>  
<script defer type="text/javascript" src="/js/pngfix.js" mce_src="/js/pngfix.js"></script>
<![endif]-->
<!-- АВТОКОМЛИТТЕР 	1 ЧАСТЬ	-->
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script>var $j = jQuery.noConflict();</script>
<!-- /АВТОКОМЛИТТЕР -->

<link  rel="stylesheet" type="text/css" href="/css/lightbox.css" />
<link  rel="stylesheet" type="text/css" href="/css/calendar-blue.css" />
<link rel="alternate stylesheet" type="text/css" href="/css/print.css" media="screen" title="Print Preview" />
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print" />
<script type="text/javascript" src="/js/print.js"></script>

<script type="text/javascript" src="/js/jquery.js"></script>


<!-- BDP: adminjslib -->
<script type="text/javascript" src="/js/swfobject.js"></script>
    <script type="text/javascript" src="/js/jquery.uploadify.v2.1.4.min.js"></script>

    	<script type="text/javascript">
            
            var objDump = function (obj) {
                var ret = '';
                for(o in obj) {
                    ret += 'Key: '+o+' Val: '+obj[o]+"\n";
                }
                return ret;
            }
            
    		jQuery(document).ready(function() {
    			jQuery("#uploadify").uploadify({
    				
    				'expressInstall': '/modules/uploadify/expressInstall.swf',
    				'uploader': '/modules/uploadify/uploadify.swf',
    				'script': '/modules/uploadify/uploadify.php',
    				'folder': '/upload',
    				'cancelImg': '/modules/uploadify/cancel.png',
    				
    				'queueID'        : 'fileQueue',
    				'fileDesc'       : 'jpg, gif, png',
    				'fileExt'        : '*.jpg;*.gif;*.png;*.jpeg',
    				'auto'           : false,
    				'multi'          : true,
                                'onAllComplate': function(t1, t2) {
                                  alert(objDump(t2));  
                                },
    				'onComplete'   : function(event,queueID,fileObj,response,data) {
                                    alert(response);
    									if (response != 'OK') {
    										jQuery('#response').append(response);
    									}
    								 },
    				onError: function (event, queueID, fileObj, errorObj) {
                                  
    							 err = '';
    					         if (errorObj.status == 404) {
    					            err += 'Could not find upload script.';
    					         } else if (errorObj.type === "HTTP") {
    					            err += 'error '+errorObj.type+": "+errorObj.status;
    					         } else if (errorObj.type ==="File Size") {
    					            err += fileObj.name+' '+errorObj.type+' Limit: '+Math.round(errorObj.sizeLimit/1024)+'KB';
    					         } else {
    					            err += 'error '+errorObj.type+": "+errorObj.text;
    					         }
                                                  
    					      }
                                             
    				/*
    				*/
    			});
    		});
    	</script>



<!-- EDP: adminjslib -->


<script>
    


jQuery(document).ready(function() {	

	//select all the a tag with name equal to modal
	jQuery('a[name=modal]').click(function(e) {
		//Cancel the link behavior
		e.preventDefault();
		//Get the A tag
		var id = jQuery(this).attr('href');
	
		//Get the screen height and width
		var maskHeight = jQuery(document).height();
		var maskWidth = jQuery(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		jQuery('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		jQuery('#mask').fadeIn(1000);	
		jQuery('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = jQuery(window).height();
		var winW = jQuery(window).width();
              
		//Set the popup window to center
		jQuery(id).css('top',  winH/2-jQuery(id).height()/2);
		jQuery(id).css('left', winW/2-jQuery(id).width()/2);
	
		//transition effect
		jQuery(id).fadeIn(2000); 
	
	});
	
        
        
        var id = '#dialog';
	
		//Get the screen height and width
		var maskHeight = jQuery(document).height();
		var maskWidth = jQuery(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		jQuery('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		jQuery('#mask').fadeIn(1000);	
		jQuery('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = jQuery(window).height();
		var winW = jQuery(window).width();
              
		//Set the popup window to center
		jQuery('#dialog').css('top',  winH/2-jQuery('#dialog').height()/2);
		jQuery('#dialog').css('left', winW/2-jQuery('#dialog').width()/2);
	
		//transition effect
		jQuery('#dialog').fadeIn(2000); 
        
	//if close button is clicked
	jQuery('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();
		jQuery('#mask, .window').hide();
	});		
	
	//if mask is clicked
	jQuery('#mask').click(function () {
		jQuery(this).hide();
		jQuery('.window').hide();
	});			
	
});



function goToRegion() {
    //alert(document.getElementById('go_to_region').options[document.getElementById('go_to_region').selectedIndex].value);
    location.href=document.getElementById('go_to_region').options[document.getElementById('go_to_region').selectedIndex].value;
}

</script>

<link href="/css/lightbox/jquery.lightbox-0.5.css" rel="stylesheet" type="text/css" />
	 	
<script type="text/javascript" src="/js/lightbox/jquery.lightbox-0.5.pack.js"></script>
		<script type="text/javascript">
	jQuery(function(){
	    
		jQuery("a[href$='jpg']").lightBox();
	});
	</script>    

<script>
    function add2Fav (x){
        if (document.all  && !window.opera) {
             if (typeof window.external == "object") {
                window.external.AddFavorite (document.location, document.title);
                return true;
              }
              else return false;

        }
        else{
            x.href=document.location;
            x.title=document.title;
            x.rel = "sidebar";
            return true;
        }
    }
</script>

{PROBA_CATALOG}

        <script type="text/javascript" src="/js/jq-scripts.js"></script>
        <script type="text/javascript" src="/js/jq-order-form.js"></script>
        
        <script src="/js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
        <script src="/js/lightbox.js" type="text/javascript"></script>
        <script type="text/javascript" src="/js/scripts.js"></script>
        <script type="text/javascript" src="/js/js_func.js"></script>
        <link rel="stylesheet" href="/css/jquery-ui.css">
        <link rel="stylesheet" href="/css/jquery.selectBox.css">
        <script type="text/javascript" src="/js/jquery_ui/jquery.ui.core.min.js"></script>
        <script type="text/javascript" src="/js/jquery_ui/jquery.ui.widget.min.js"></script>
        <script type="text/javascript" src="/js/ui.checkbox.js"></script>
        <!--<script type="text/javascript" src="/js/jquery.selectBox.min.js"></script>-->
        <script type="text/javascript" src="/js/site.js"></script>
       <script type="text/javascript" src="/js/prototype.js"></script>	
		
<!-- АВТОКОМЛИТТЕР 	2 ЧАСТЬ	-->
				<!--		jquery.min.js подключаемый вверху с командой $j = jQuery.noConflict() для устранения конфликта скриптов
							в файле js_autocompl.js меняем значок $ на $j-->
        <script type="text/javascript" src="/js/jquery1.8.1.js.js"></script>
   <!--     <script type="text/javascript" src="/js/js_autocompl.js"></script>
 /АВТОКОМЛИТТЕР -->



<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38638352-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  
<script>
  (function() {
    var cx = '001074390252114108699:zlmbuectvpk';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
        
		
		<!-- BUYME -->
		<script>
var b1c_lang = 'ua'; 
</script>
		
<script src="/buyme/js/buyme.js"></script>
<link rel="stylesheet" type="text/css" href="/buyme/templates/default/style.css">
		<!-- /BUYME -->	
	
<script type="text/javascript">
var suggest_count = 0;
var input_initial_value = '';
var suggest_selected = 0;
var num_cat = g1('{NUMBER}');

//alert(num_cat);

$j(window).load(function(){

    // читаем ввод с клавиатуры
    $j("#search_box").keyup(function(I){
        // определяем какие действия нужно делать при нажатии на клавиатуру
        switch(I.keyCode) {
		
		
            // игнорируем нажатия на эти клавишы
            case 13:  // enter
            case 27:  // escape
            case 38:  // стрелка вверх
            case 40:  // стрелка вниз
            break;
 
            default:
                // производим поиск только при вводе более 2х символов
                if($j(this).val().length>2){
                    input_initial_value = $j(this).val();
                    // производим AJAX запрос к /ajax/ajax.php, передаем ему GET query, в который мы помещаем наш запрос
                    $j.get("/tpl/php/ajax.php", { "search":$j(this).val(), "number":num_cat },function(data){
                        //php скрипт возвращает нам строку, ее надо распарсить в массив.
                        // возвращаемые данные: ['test','test 1','test 2','test 3']
                        var list = eval("("+data+")");
                        suggest_count = list.length;
                        if(suggest_count > 0){
                            // перед показом слоя подсказки, его обнуляем
                            $j("#search_advice_wrapper").html("").show();
                            for(var i in list){
								var isn=isNaN(i);
                                if(list[i] != '' && isn==false){
                                    // добавляем слою позиции
                                    $j('#search_advice_wrapper').append('<div class="advice_variant">'+list[i]+'</div>');
                                }
                            }
                        }
                    }, 'html');
                }
            break;
        }
    });
 
    //считываем нажатие клавиш, уже после вывода подсказки
    $j("#search_box").keydown(function(I){
        switch(I.keyCode) {
            // по нажатию клавиш прячем подсказку
            case 13: var hr=document.getElementById('link'+suggest_selected); if( hr!=null ) { location.href=hr; return false; break;} else {  form.submit(); }
	// enter - переходим по ссылке
			// по нажатию клавиш прячем подсказку
            case 27: // escape
                $j('#search_advice_wrapper').hide();
				suggest_selected = 0;
                return false;
            break;
            // делаем переход по подсказке стрелочками клавиатуры
            case 38: // стрелка вверх
            case 40: // стрелка вниз
                I.preventDefault();
                if(suggest_count){
                    //делаем выделение пунктов в слое, переход по стрелочкам
                    key_activate( I.keyCode-39 );
                }
            break;
        }
    });
 
    // делаем обработку клика по подсказке
    $j('.advice_variant').live('click',function(){
        // ставим текст в input поиска
        $j('#search_box').val($j(this).text());
        // прячем слой подсказки
        $j('#search_advice_wrapper').fadeOut(350).html('');
		suggest_selected = 0;
    });
 
    // если кликаем в любом месте сайта, нужно спрятать подсказку
    $j('html').click(function(){
        $j('#search_advice_wrapper').hide();
		$j('#search_advice_wrapper div').eq(suggest_selected-1).removeClass('active');
		suggest_selected = 0;

    });
    // если кликаем на поле input и есть пункты подсказки, то показываем скрытый слой
    $j('#search_box').click(function(event){
        //alert(suggest_count);
        if(suggest_count)
            $j('#search_advice_wrapper').show();
			$j('#search_advice_wrapper div').eq(suggest_selected-1).removeClass('active');
        event.stopPropagation();
    });
});
 
function key_activate(n){
    $j('#search_advice_wrapper div').eq(suggest_selected-1).removeClass('active');
 
    if(n == 1 && suggest_selected < suggest_count){
        suggest_selected++;
    }else if(n == -1 && suggest_selected > 0){
        suggest_selected--;
    }
 
    if( suggest_selected > 0){
        $j('#search_advice_wrapper div').eq(suggest_selected-1).addClass('active');
        $j("#search_box").val( $j('#search_advice_wrapper div').eq(suggest_selected-1).text() );
    } else {
        $j("#search_box").val( input_initial_value );
    }
}

function g1(c){
  var string =c+"";
		if (string.length == 1){
			ccf='0'+c;
		}
		else {
			ccf=c;
			}
	return ccf;
}
</script>
</HEAD>
<body>
   
{PROMPTED_SELECT_REGION}

<div class="wraper zindex">
 <div class="header">
  <!--<a class="logo" href="/"><img src="/img/logo.jpg" width="242" height="95" alt="{LOGOALT}" /></a>-->
  <a class="logo" href="/"><img src="/img/logo.jpg" width="195" height="95" alt="{LOGOALT}" /></a>
  <p class="top_text">{RGTELHEADER}</p>

  <!--<div class="enter_exit"><a href="#">Мой кабинет</a><span>|</span><a href="#">Выйти</a></div>-->
  <!--<div class="enter_exit"><a href="#">Регистрация</a><span>|</span><a href="#">Войти</a></div>-->
  <!--<div class="busket busket_bg">
   <p class="tit"><a href="#">Ваша Корзина</a></p>
   <p><a href="#">Товаров:</a><span><a href="#">15 шт.</a></span></p>
   <p><a href="#">На сумму:</a><span><a href="#">15 500 грн.</a></span></p>
   <p class="order"><a href="#">Оформить заказ</a></p>
  </div>-->


<div class="busket_block" id="busket-block"> 
{BASKET_START}        
</div>
        
  <!--<div class="busket">
   <p class="tit"><a href="#">Ваша Корзина</a></p>
   <p><a href="#">Товаров:</a><span><a href="#">0 шт.</a></span></p> 
   <p><a href="#">На сумму:</a><span><a href="#">0 грн.</a></span></p>
   <p class="center"><a href="#">Корзина пуста</a></p>
  </div>-->
  <!--<div class="lng"><span>рус</span> | <a href="#">укр</a></div>-->
  <div class="alpha">{ABC}</div>
  <div class="search_block">
  
     	<div class="search_area">
        <form action="/{REGION_URL}search/" method="GET">
            <input type="text" name="term1" id="search_box" value="Пошук товара по назвi" onFocus='value="";' onKeyUp='javascript:getSuggestion(event, this.value);' autocomplete="off">
			<input type="submit" value="Знайти" class="button" />       
	   </form>
        <div id="search_advice_wrapper"></div>
    </div>
  <!--
  <form method="get" action="/{REGION_URL}search/">
  <input size="70" type='txt' id='search_input1' value="Пошук товара по назвi" onFocus='g1({NUMBER}); value="";' onKeyUp='javascript:getSuggestion(event, this.value);' name="term1"/>
  <input type="submit" value="Знайти" class="button" />
		<ul style="background:#87CEFA" id='suggestbox1' class='sugg_box' ></ul>
		</form>
		-->
		</div>


	
  <div class="region">
   Ваш регіон: {LIST_REGIONS}
  </div>
    <div class="lang" >
 <a href="/?lang_cook=0" >Рос.</a>
  </div>
  
  <div class="top_menu_block">
  <table>  
   <tr> 		 
        <!-- BDP: horisontal -->
        <td class=""><div class="rel" style="width:80px"><a href="{MENU_HREF}" title="{MENU_NAME}">{MENU_NAME}</a></div></td>	
	<!-- EDP: horisontal -->
   </tr>
  </table>
  </div>
  
  
  <ul class="pic_menu">
   <li><a class="home" href="/{REGION_URL}" alt="{TITLEMAIN}" title="{TITLEMAIN}">&nbsp;</a></li>
   <li class="pad"><a href="/{REGION_URL}sitemap/" alt="{TITLEMAP}" title="{TITLEMAP}" class="sitemap">&nbsp;</a></li>
   <li><a href="#" title="{TITLEAI}" alt="{TITLEAI}" onClick="add2Fav (this)" class="email">&nbsp;</a></li>
  </ul>
  <div class="clear"></div>
 </div>
</div>
 <div class="top_banner_list">
  <ul>
   <li>{TOPBANLEFT}</li>
   <li>{TOPBANRIGHT}</li>
  </ul>
  <div class="clear"></div>
 </div>
<div class="wraper">
<div class="main_menu">
 <ul class="sf-menu">
  <li><a href="/{REGION_URL}catalog/lekarstvennie-preparati/">Лікарські препарати</a>{CATALOG_TOP_MENU_LP}</li>
  <li><a href="/{REGION_URL}catalog/kosmetika/">Косметика</a>{CATALOG_TOP_MENU_KOSMETIKA}</li>
  <li><a href="/{REGION_URL}catalog/biologicheski-aktivnie-dobavki-bad/">Біологічно активні домішки (БАД)</a>{CATALOG_TOP_MENU_BAD}</li>
  <li><a href="/{REGION_URL}catalog/izdeliya-meditcinskogo-naznacheniya/">Засоби медичного призначения</a>{CATALOG_TOP_MENU_IMN}</li>
  <li><a href="/{REGION_URL}catalog/meditcinskaya-tehnika">Медична техніка</a>{CATALOG_TOP_MENU_MT}</li>
  <li><a href="/{REGION_URL}catalog/soputstvuyushie-tovari/">Супутні товари</a>{CATALOG_TOP_MENU_ST}</li>
  <li class="last"><a href="/{REGION_URL}catalog/vitamini/">Інше</a>{CATALOG_TOP_MENU_VITAMIN}</li>
 </ul>
 <div class="clear"></div>
</div>
<div class="clear"></div>
<div class="pager">
{WAY}
 </div>
 <div class="left_column">
  <div class="nofont"><a href="/apteki"><img src="/img/banner3_1.jpg" width="240" height="284" alt=""/></a></div>  
  <div class="left_menu">
      
       <!-- BDP: administration -->
 <div class="title-header">
 	<div class="tit">Администрирование</div>
 </div>
 
 <ul class="left_menu">
 	<li><a href='{BASE_PATH}admin/editpage/mainpage' title="Главная страница">Главная страница</a></li>
 	<li><a href='{BASE_PATH}admin/menu/horisontal' title="Горизонтальное меню">Горизонтальное меню</a></li>
 	<li><a href='{BASE_PATH}news' title="Новости">Новости</a></li>
 	<li><a href='{BASE_PATH}admin/settings' title="Настройки сайта">Настройки сайта</a></li>
        
 	<li><a href='{BASE_PATH}admin/lookups' title="Редактируемые поля сайтов">Редактируемые поля сайта</a></li>
        <li><a href='{BASE_PATH}{REGION_URL}admin/lookupsRegion' title="Редактируемые поля регионов">Редактируемые поля регионов</a></li>
        <li><a href='{BASE_PATH}{REGION_URL}admin/listPoll' title="Голосование">Голосование</a></li>
 	<li><a href='{BASE_PATH}admin/metatags' title="Мета-Теги">Мета-Теги</a></li>
        <li><a href='{BASE_PATH}admin/powerRegion' title="Вкл./Выкл. Регионов">Вкл./Выкл. Регионов</a></li>
        <li><a href='#' title="">____________________________</a></li>
        <li><a href='{BASE_PATH}admin/orders/' title="База заказов">База заказов</a></li>
        <li><a href='{BASE_PATH}{REGION_URL}admin/uploadImgCatalogGoods/' title="Загрузка изображений">Загрузка изображений к товарам</a></li>
        <li><a href='{BASE_PATH}{REGION_URL}admin/instructions/' title="Инструкции">Инструкции</a></li>
        <li><a href='{BASE_PATH}{REGION_URL}admin/listSections/' title="Информация о разделах">Информация о разделах</a></li>
        <li><a href='{BASE_PATH}{REGION_URL}admin/standartText/' title="Шаблонный текст">Шаблонный текст</a></li>
        <li><a href='{BASE_PATH}{REGION_URL}admin/import/' title="Импорт товаров">Импорт товаров</a></li> 	
        <li><a href='#' title="">____________________________</a></li>
 	<li><a href='{BASE_PATH}logout' title="Выход">Выход</a></li>
 </ul>	
                        
  <br /> <br />
  
  <!-- EDP: administration -->
      
   <div class="tit">Лікарські препарати</div>
   
   
   
   
   <ul>
<!-- BDP: catalog_left_menu -->
    <li>
        <a href='{CATALOG_LEFT_MENU_HREF}' title='{CATALOG_LEFT_MENU_NAME}'>
            {CATALOG_LEFT_MENU_NAME}
        </a>
    </li>
<!-- EDP: catalog_left_menu -->
   </ul>
  </div>
  <div class="nofont">{LEFTBANFOOT}</div>  
  <div id="pollDiv">
      {POLL}
  </div>

  
  <script>
function FormClick () {
  var str = jQuery("#pollForm").serialize();
  jQuery.post("/poll.php", str, function(data) {
    jQuery("#pollDiv").html(data);
  });
}
</script>
  
  
  
  

                    <span class="corner top left lt"></span>
                    <span class="corner top right rt"></span>
                    <span class="corner bottom left lb"></span>
                    <span class="corner bottom right rb"></span>
                
  
  
 </div>
 <div class="right_column">
{GOOGLE_SEARCH}
 <div class="type_elem">

     
    

  <div class="clear"></div>
    <!-- BDP: p_header -->

  <h1>{HEADER}</h1>
 
 <!-- EDP: p_header -->
  {CONTENT}
  
  
 </div>
 </div>
</div>
<div class="clear"></div>
<div class="footer_wrap">
 <div class="footer">
  <div class="left">
      {RFADR}
  </div>
  <div class="right">
   <p>{RFTEXT}</p>
  </div>
  <div class="clear"></div>
  
{ALLRR}
<div class="rules">
   <p>{ALLRR2}<span>
    <!-- <a href="http://www.deluxe.dp.ua/" target="_blank">создание сайта<br /><img src="/img/deluxe.gif" width="80" height="18" alt="" /></a> --> </span></p>
  </div>

  <div class="clear"></div>
 </div>
</div>
{ALERT_REGION}

</body>
</HTML>

