<!-- BDP: sectionsi -->

 <div class="order_history">
     <a title="Добавить информацию о разделе" href="/{REGION_URL}admin/addSectionsInfo/"><img border="0" alt="Добавить" src="/img/admin_icons/add_page.png">Добавить информацию о разделе</a>
   <table>
    <tr class="hnone">     

     <th>Название</th>
     <th></th>
     <th></th>     
    </tr>
    <!-- BDP: sectionsi_list -->
    <tr>
     <td><a href="/{REGION_URL}admin/editSectionsInfo/{SECTION_ID}">{SECTION_NAME}</a></td>   
     <td><a title="Редактировать информацию о разделе" href="/{REGION_URL}admin/editSectionsInfo/{SECTION_ID}"><img width="12" height="12" alt="Редактировать  информацию о разделе" src="/img/admin_icons/edit.png"></a></td> 
     <td><a onclick="return confirm('Вы уверены что хотите удалить информацию о разделе?'); return false;" title="Удалить" href="/{REGION_URL}admin/deleteSectionsInfo/{SECTION_ID}"><img width="12" height="12" alt="Удалить" src="/img/admin_icons/delete.png"></a></td>
    </tr>
    <!-- EDP: sectionsi_list -->
    
   </table>
  </div>
  
  <!-- BDP: sectionsi_empty -->
  Нет данных
  <!-- EDP: sectionsi_empty -->
  
<!-- EDP: sectionsi -->
