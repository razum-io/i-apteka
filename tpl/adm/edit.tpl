﻿<!-- BDP: start -->

<form enctype="multipart/form-data" method="post" action="">
<table Border=0 CellSpacing=0 CellPadding=0 Width="100%" Align="" vAlign="" class="admin-table">
<!-- EDP: start -->

<!-- BDP: js -->
<script type="text/javascript" src="/js/jq-auto-fill.js"></script>
<!-- EDP: js -->


<!-- BDP: mce -->
<script language="javascript" type="text/javascript" src="/js/tiny_mce/filemanager/jscripts/mcfilemanager.js"></script>
<script language="javascript" type="text/javascript" src="/js/tiny_mce/imagemanager/jscripts/mcimagemanager.js"></script>
<script type="text/javascript" src="/js/tiny_mce/tiny_mce_gzip.js"></script>
<script type="text/javascript">
tinyMCE_GZ.init({
    plugins : "style,layer,table,save,advhr,advimage,advlink,preview,zoom,media,contextmenu,paste,directionality,noneditable,visualchars,nonbreaking",
    themes : 'advanced',
    languages : 'ru',
    disk_cache : true,
    debug : false
});
</script>
<script language="javascript" type="text/javascript">
    tinyMCE.init({
        mode : "specific_textareas",
        editor_selector : "mceEditor",
        language : "ru",
        mode : "textareas",
        theme : "advanced",
        plugins : "layer,table,save,advhr,advimage,advlink,preview,zoom,media,contextmenu,paste,directionality,noneditable,visualchars, nonbreaking",
        theme_advanced_buttons2_add : "separator,preview",
        theme_advanced_buttons2_add_before: "cut,copy,pastetext,pasteword,separator",
        theme_advanced_buttons3_add_before : "tablecontrols,separator",
        theme_advanced_buttons3_add : "media,advhr,separator,visualchars,nonbreaking",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_path_location : "bottom",
        plugin_insertdate_dateFormat : "%Y-%m-%d",
        plugin_insertdate_timeFormat : "%H:%M:%S",
        extended_valid_elements : "hr[class|width|size|noshade],font[face|size|color],span[class|align|style]",
        file_browser_callback : "mcFileManager.filebrowserCallBack",
        theme_advanced_resize_horizontal : true,
        theme_advanced_resizing : true,
        apply_source_formatting : false,
        relative_urls : false,
        add_unload_trigger : true,
        strict_loading_mode : true
    });
</script>
<!-- EDP: mce -->

<!-- BDP: settings -->
 <!-- BDP: settings_item -->
<tr>
  <td width="220" valign="middle">{NAME}: &nbsp;&nbsp;&nbsp;</td><td><textarea rows="1" style="width: 100%;" name="settings_{KEY}" >{VALUE}</textarea><br></td>
</tr>
 <!-- EDP: settings_item -->
<!-- EDP: settings -->

<!-- BDP: settings_region -->
 <!-- BDP: settings_region_item -->
<tr>
    <td width="220" valign="middle">{NAME}: &nbsp;&nbsp;&nbsp;</td><td><input type="checkbox" name="{KEY}" value="1" {VALUE} > <br></td>
</tr>
 <!-- EDP: settings_region_item -->
<!-- EDP: settings_region -->


<!-- BDP: lookups -->
    <!-- BDP: lookups_item -->
    <tr>
        <td width="220" valign="middle">{LOOK_NAME}: &nbsp;&nbsp;&nbsp; <textarea rows="4" style="width: 100%;" name="{LOOK_KEY}">{LOOK_VALUE}</textarea></td>
        <td width="220" valign="middle">{LOOK_NAME_UA}: &nbsp;&nbsp;&nbsp; <textarea rows="4" style="width: 100%;" name="{LOOK_KEY_UA}">{LOOK_VALUE_UA}</textarea></td>
		
    </tr>
    <!-- EDP: lookups_item -->
<!-- EDP: lookups -->

<!-- BDP: adress -->
<tr>
     <td>Адрес в URL <small><font color="red">[</font>A-Z<font color="red">]</font><font color="red">[</font>a-z<font color="red">]</font><font color="red">[</font>0-9<font color="red">]</font><font color="red">[</font>-<font color="red">]</font><font color="red">[</font>_<font color="red">]</font></small>:</td><td><input type=text size=50 name="adm_href" value='{ADM_HREF}' class='admin-input-text'  id='admin-input-url'><br></td>
</tr>
<!-- EDP: adress -->

<!-- BDP: visible -->
<tr>
    <td>Отображать</td>
    <td>
  	<select name="visible">{VISIBLE_S}
  	</select>
    </td>
</tr>
<!-- EDP: visible -->

<!-- BDP: top -->
<tr>
    <td>Выводить на главную</td>
    <td>
  	<select name="top">{TOP_S}
  	</select>
    </td>
</tr>
<!-- EDP: top -->

<!-- BDP: header -->
<tr>
     <td>Header: &nbsp;&nbsp;&nbsp;</td><td><input type="text" name="header" value='{ADM_HEADER}' class='admin-input-text'  id='admin-input-header'><br></td>
</tr>
<tr>
     <td>Header(ua): &nbsp;&nbsp;&nbsp;</td><td><input type="text" name="header_ua" value='{ADM_HEADER_UA}' class='admin-input-text'  id='admin-input-header'><br></td>
</tr>
<!-- EDP: header -->

<!-- BDP: meta -->
<tr>
     <td>Header: &nbsp;&nbsp;&nbsp;</td><td><input type="text" size=50 name="header" value='{ADM_HEADER}' class='admin-input-text' id='admin-input-header'><br></td>
</tr>
<tr>
     <td>Header (ua): &nbsp;&nbsp;&nbsp;</td><td><input type="text" size=50 name="header_ua" value='{ADM_HEADER_UA}' class='admin-input-text' id='admin-input-header'><br></td>
</tr>
<tr>
     <td>Title: &nbsp;&nbsp;&nbsp;</td><td><input type="text" size=50 name="title" value='{ADM_TITLE}' class='admin-input-text' id='admin-input-title'> <br></td>
</tr>
<tr>
     <td>Title (UA): &nbsp;&nbsp;&nbsp;</td><td><input type="text" size=50 name="title_ua" value='{ADM_TITLE_UA}' class='admin-input-text' id='admin-input-title'> <br></td>
</tr>
<tr>
     <td>Keywords: &nbsp;&nbsp;&nbsp;</td><td><input type="text" size=50 name="keywords" value='{ADM_KEYWORDS}' class='admin-input-text' id='admin-input-keyword'><br></td>
</tr>
<tr>
	  <td>Keywords (UA): &nbsp;&nbsp;&nbsp;</td><td><input type="text" size=50 name="keywords_ua" value='{ADM_KEYWORDS_UA}' class='admin-input-text' id='admin-input-keyword'><br></td>
</tr>
<tr>
     <td>Description: &nbsp;&nbsp;&nbsp;</td><td><input type="text" size=50 name="description" value='{ADM_DESCRIPTION}' class='admin-input-text' id='admin-input-description'><br></td>
</tr>
<tr>
     <td>Description (UA): &nbsp;&nbsp;&nbsp;</td><td><input type="text" size=50 name="description_ua" value='{ADM_DESCRIPTION_UA}' class='admin-input-text' id='admin-input-description'><br></td>
</tr>
<!-- EDP: meta -->

<!-- BDP: news -->
<tr>
    <td>Топ новость</td>
    <td>
  	<select name="topnews">{ADM_TOPNEWS}
  	</select>
    </td>
</tr>
<link rel="stylesheet" type="text/css" media="all" href="/css/calendar-blue.css" title="winter" />
  <script type="text/javascript" src="/js/calendar.js"></script>
  <script type="text/javascript" src="/js/calendar-ru.js"></script>
  <script type="text/javascript" src="/js/calendar-setup.js"></script>
<tr>
     <td>Дата: &nbsp;&nbsp;&nbsp;</td><td><input type="text" size="10" name="date" value="{ADM_DATE}" id="date_i" readonly="1" /> 
     <img src="/img/calendar.gif" id="trigger" style="cursor: pointer; border: 1px solid red;" title="Date selector"
      onmouseover="this.style.background='red';" onmouseout="this.style.background=''" /></td>
</tr>
<script type="text/javascript">

    Calendar.setup({
        inputField     :    "date_i",     // id of the input field
        //ifFormat       :    "%Y-%m-%d",      // format of the input field
        ifFormat       :    "%d.%m.%Y",      // format of the input field
        button         :    "trigger",  // trigger for the calendar (button ID)
        align          :    "B1",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
</script>

<tr>
     <td colspan=2><b>Анонс:</b> &nbsp;&nbsp;&nbsp;</td>
</tr>
<tr>
     <td colspan=2><textarea class="mceEditor" rows=10 style="width: 100%;" name="preview">{ADM_PREVIEW}</textarea></td>
</tr>

<tr>
     <td colspan=2>Анонс (UA)<textarea class="mceEditor" rows=10 style="width: 100%;" name="preview_ua">{ADM_PREVIEW_UA}</textarea></td>
</tr>
<!-- EDP: news -->

<!-- BDP: name -->
<tr>
    <td>Название:</td><td><input type="text" size="50" name="name" value="{ADM_NAME}" class='admin-input-text'  id='admin-input-name'></td>
</tr>
<!-- EDP: name -->

<!-- BDP: artikul -->
<tr>
    <td>Артикул:</td><td><input type="text" size="50" name="artikul" value="{ADM_ARTIKUL}" ></td>
</tr>
<!-- EDP: artikul -->

<!-- BDP: proizvoditel -->
<tr>
    <td>Производитель:</td><td><input type="text" size="50" name="proizvoditel" value="{ADM_PROISVODITEL}" class='admin-input-text' ></td>
</tr>
<!-- EDP: proizvoditel -->

<!-- BDP: cost -->
<tr>
    <td>Цена:</td><td><input type="text" size="50" name="cost" value="{ADM_COST}" ></td>
</tr>
<!-- EDP: cost -->

<!-- BDP: cost_old -->
<tr>
    <td>Цена в магазинах:</td><td><input type="text" size="50" name="cost_old" value="{ADM_COST_OLD}" ></td>
</tr>
<!-- EDP: cost_old -->


<!-- BDP: pos -->
<tr>
    <td>Позиция:</td><td><input type="text" size="50" name="position" value="{ADM_POSITION}"></td>
</tr>
<!-- EDP: pos -->

<!-- BDP: model -->
<tr>
    <td>Модель:</td><td><input type="text" size="50" name="model" value="{ADM_MODEL}"  class='admin-input-text' /></td>
</tr>
<!-- EDP: model -->


<!-- BDP: scale -->
<tr>
    <td>Масштаб:</td><td><input type="text" size="50" name="scale" value="{ADM_SCALE}"  class='admin-input-text' /></td>
</tr>
<!-- EDP: scale -->


<!-- BDP: preview -->
<tr>
     <td colspan=2><b>Краткое описание:</b> &nbsp;&nbsp;&nbsp;</td>
</tr>
<tr>
     <td colspan="2"><textarea class="mceEditor" rows=25 style="width: 100%;" name="preview">{ADM_PREVIEW}</textarea></td>
</tr>
<tr>
     <td colspan="2">Краткое описание (UA)<textarea class="mceEditor" rows=25 style="width: 100%;" name="preview_ua">{ADM_PREVIEW_UA}</textarea></td>
</tr>
<!-- EDP: preview -->

<!-- BDP: body -->
<tr>
     <td colspan=2><b>Текст:</b> &nbsp;&nbsp;&nbsp;</td>
</tr>
<tr>
     <td colspan="2"><textarea class="mceEditor" rows=25 style="width: 100%;" name="body">{ADM_BODY}</textarea></td>
</tr>
<tr>
     <td colspan="2">Текст (UA)<textarea class="mceEditor" rows=25 style="width: 100%;" name="body_ua">{ADM_BODY_UA}</textarea></td>
</tr>
<!-- EDP: body -->

<!-- BDP: pic -->
<tr>
	
    <td align="left">Картинка: 
    <!-- BDP: show_pic -->  
    	<br />  		
	  	<img src="{ADM_IMG_SRC}" alt="{ADM_IMG_ALT}" title="{ADM_IMG_TITLE}" />	  		
	  	<br />	  
	  	<input name="dell_pic" type="checkbox" style="width: 5px;" onclick="if (this.checked) {this.checked = confirm('Вы уверены что хотите удалить изображение?'); }"> Удалить изображение
	<!-- EDP: show_pic -->
    </td><td valign="bottom">    	
		<input type="file" name="pic">
		
    </td>
</tr>
<!-- EDP: pic -->

<!-- BDP: goods_pic -->
<tr>
	
    <td align="left">Картинка: (Не меньше: 420px X 420px)
    <!-- BDP: show_goods_pic -->  
    	<br />  		
	  	<img src="{ADM_IMG_SRC}" alt="{ADM_IMG_ALT}" title="{ADM_IMG_TITLE}" />	  		
	  	<br />	  
	  	<input name="dell_pic" type="checkbox" style="width: 5px;" onclick="if (this.checked) {this.checked = confirm('Вы уверены что хотите удалить изображение?'); }"> Удалить изображение
	<!-- EDP: show_goods_pic -->
    </td><td valign="bottom">    	
		<input type="file" name="pic">
		
    </td>
</tr>
<!-- EDP: goods_pic -->

<!-- BDP: catalog_upload_pic -->
<tr>
    <td colspan='2'>    
   Добавить изображения к товарам каталога (Не меньше: 295px X 295px). <br>
   Имена файлов картинок должны совпадать с артикулом товара. 
    </td>
</tr>

<tr>
    <td colspan="2">
	
    <table class="adm-gallery">
</tr>    
		<tr>
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 1:</td><td> <input type="file" name="catalog_upload_pic1"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="catalog_upload_title1"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="catalog_upload_alt1"></td></tr>
    			
    			</table>
    		</td>	
    			
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 2:</td><td> <input type="file" name="catalog_upload_pic2"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="catalog_upload_title2"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="catalog_upload_alt2"></td></tr>
    			
    			</table>
    		</td>
		</tr>
		<tr>
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 3:</td><td> <input type="file" name="catalog_upload_pic3"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="catalog_upload_title3"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="catalog_upload_alt3"></td></tr>
    			</table>
    		</td>
    		
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 4:</td><td> <input type="file" name="catalog_upload_pic4"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="catalog_upload_title4"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="catalog_upload_alt4"></td></tr>
    			</table>
    		</td>
    		
		</tr>
		<tr>	
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 5:</td><td> <input type="file" name="catalog_upload_pic5"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="catalog_upload_title5"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="catalog_upload_alt5"></td></tr>
    			</table>
    		</td>
    		
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 6:</td><td> <input type="file" name="catalog_upload_pic6"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="catalog_upload_title6"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="catalog_upload_alt6"></td></tr>
    			</table>
    		</td>
    		
		</tr>
    </table>
    

    
    </td>
</tr>

<!-- EDP: catalog_upload_pic --> 


<!-- BDP: gallery_pic -->
<tr>
    <td colspan='2'>    
   Добавить изображения в минигалерею
    </td>
</tr>

<tr>
    <td colspan="2">
	<div class="goods_full">
	 <div class="details_list">
	 	<ul>
			<!-- BDP: gallery_pic_list --> 
    		<li style="font-size: 12px"><a href="#"><img src="/img/catalog/gallery/small/{G_SRC}" width="100" height="100" alt="{G_ALT}" title="{G_TITLE}" /></a>
    		<br />
	  			<input name="dell_gallery_pic[{G_ID}]" type="checkbox" style="width: 5px;" onclick="if (this.checked) {this.checked = confirm('Вы уверены что хотите удалить изображение?'); }"> Удалить
    		</li>
    		<!-- EDP: gallery_pic_list -->
    	</ul>
    </div>
    </div>
    <table class="adm-gallery">
</tr>    
		<tr>
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 1:</td><td> <input type="file" name="gallery_pic1"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="gallery_title1"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="gallery_alt1"></td></tr>
    			
    			</table>
    		</td>	
    			
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 2:</td><td> <input type="file" name="gallery_pic2"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="gallery_title2"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="gallery_alt2"></td></tr>
    			
    			</table>
    		</td>
		</tr>
		<tr>
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 3:</td><td> <input type="file" name="gallery_pic3"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="gallery_title3"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="gallery_alt3"></td></tr>
    			</table>
    		</td>
    		
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 4:</td><td> <input type="file" name="gallery_pic4"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="gallery_title4"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="gallery_alt4"></td></tr>
    			</table>
    		</td>
    		
		</tr>
		<tr>	
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 5:</td><td> <input type="file" name="gallery_pic5"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="gallery_title5"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="gallery_alt5"></td></tr>
    			</table>
    		</td>
    		
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 6:</td><td> <input type="file" name="gallery_pic6"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="gallery_title6"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="gallery_alt6"></td></tr>
    			</table>
    		</td>
    		
		</tr>
    </table>
    

    
    </td>
</tr>

<!-- EDP: gallery_pic --> 

<!-- BDP: banners_html --> 
<tr>
	<td>Имя (используется только для удобства в администрировании)</td><td><input type="text" name="name"  class='admin-input-text' value="{ADM_BANNER_NAME}"></td>
<tr>
<tr>				
	<td>Расположение на сайте</td><td>
	<select name="layout">
		<option value="top" {ADM_BANNER_LAYOUT_TOP}>Вверху (не более двух)</option>
		<option value="left" {ADM_BANNER_LAYOUT_LEFT}>Слева под меню</option>
		<option value="bottom" {ADM_BANNER_LAYOUT_BOTTOM}>Внизу (Наши партнеры)</option>
	</select>
	</td>
</tr>
<tr>				
	<td>Порядок вывода</td><td><input type="text" name="position" value="{ADM_BANNER_POSITION}"></td>
</tr>
<tr>				
	<td>Выводить</td><td>
	<select name="show_as">
		<option value="all" {ADM_BANNER_SHOW_AS_ALL}>На всех страницах сайта</option>
		<option value="index" {ADM_BANNER_SHOW_AS_INDEX}>Только на главной</option>
		<option value="hide" {ADM_BANNER_SHOW_AS_HIDE}>Скрыть</option>
		
	</select>
	</td>
</tr>
<tr>				
	<td>Порядок вывода</td><td><input type="text" name="position" value="{ADM_BANNER_POSITION}"></td>
</tr>	
<tr>
     <td colspan=2><b>HTML код</b> &nbsp;&nbsp;&nbsp;</td>
</tr>
<tr>
     <td colspan="2"><textarea rows=25 style="width: 100%;" name="body">{ADM_BODY}</textarea></td>
</tr>

<!-- EDP: banners_html --> 


<!-- BDP: banners_pic -->
<tr>
	<td>Имя (используется только для удобства в администрировании)</td><td><input type="text" name="name"  class='admin-input-text' value="{ADM_BANNER_NAME}"></td>
<tr>
<tr>
	<td>Укажите файл</td><td><input type="file" name="pic" ></td>
</tr>
<tr>	
	<td>Ссылка (можно не указывать для FLASH файлов <i>.swf</i>)</td><td><input type="text" name="href" value="{ADM_BANNER_HREF}"  class='admin-input-text'></td>
</tr>
<tr>			
	<td>Альтернативный текст (тег Alt)</td><td><input type="text" name="alt" value="{ADM_BANNER_ALT}"  class='admin-input-text'></td>
</tr>
<tr>				
	<td>Текст для всплывающей подсказки (тег Title)</td><td><input type="text" name="title" value="{ADM_BANNER_TITLE}"  class='admin-input-text'></td>
</tr>
<tr>				
	<td>Расположение на сайте</td><td>
	<select name="layout">
		<option value="top" {ADM_BANNER_LAYOUT_TOP}>Вверху (не более двух)</option>
		<option value="left" {ADM_BANNER_LAYOUT_LEFT}>Слева под меню</option>
		<option value="bottom" {ADM_BANNER_LAYOUT_BOTTOM}>Внизу (Наши партнеры)</option>
	</select>
	</td>
</tr>
<tr>				
	<td>Порядок вывода</td><td><input type="text" name="position" value="{ADM_BANNER_POSITION}"></td>
</tr>
<tr>				
	<td>Выводить</td><td>
	<select name="show_as">
		<option value="all" {ADM_BANNER_SHOW_AS_ALL}>На всех страницах сайта</option>
		<option value="index" {ADM_BANNER_SHOW_AS_INDEX}>Только на главной</option>
		<option value="hide" {ADM_BANNER_SHOW_AS_HIDE}>Скрыть</option>
		
	</select>
	</td>
</tr>
<!-- EDP: banners_pic --> 


<!-- BDP: gallery_foreshortening_pic -->
<tr>
    <td colspan="2">Добавить изображения товара в другом ракурсе (Не меньше: 420px X 420px)</td>
</tr>
<tr>
    <td colspan="2">
    <div class="goods_full">
	 <div class="details_list">
	 	<ul>
			<!-- BDP: gallery_foreshortening_pic_list --> 
    		<li style="font-size: 12px"><a href="#"><img src="/img/catalog/foreshortening/small/{F_SRC}" width="100" height="100" alt="{F_ALT}" title="{F_TITLE}" /></a>
    		<br />
	  			<input name="dell_foreshortening_pic[{F_ID}]" type="checkbox" style="width: 5px;" onclick="if (this.checked) {this.checked = confirm('Вы уверены что хотите удалить изображение?'); }"> Удалить
    		</li>
    		<!-- EDP: gallery_foreshortening_pic_list -->
    	</ul>
    </div>
    </div>
    <table class="adm-gallery">
</tr>
<tr>
    
    <td colspan='2'>    
   Добавить картинки
    </td>
</tr>
		<tr>
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 1:</td><td> <input type="file" name="gallery_foreshortening_pic1"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="gallery_foreshortening_text1"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="gallery_foreshortening_alt1"></td></tr>
    			
    			</table>
    		</td>	
    			
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 2:</td><td> <input type="file" name="gallery_foreshortening_pic2"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="gallery_foreshortening_text2"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="gallery_foreshortening_alt2"></td></tr>
    			
    			</table>
    		</td>
		</tr>
		<tr>
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 3:</td><td> <input type="file" name="gallery_foreshortening_pic3"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="gallery_foreshortening_text3"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="gallery_foreshortening_alt3"></td></tr>
    			</table>
    		</td>
    		
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 4:</td><td> <input type="file" name="gallery_foreshortening_pic4"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="gallery_foreshortening_text4"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="gallery_foreshortening_alt4"></td></tr>
    			</table>
    		</td>
    		
		</tr>
		<tr>	
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 5:</td><td> <input type="file" name="gallery_foreshortening_pic5"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="gallery_works_text5"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="gallery_foreshortening_alt5"></td></tr>
    			</table>
    		</td>
    		
    		<td>
    			<table width='100%'>
    			<tr><td>Картинка 6:</td><td> <input type="file" name="gallery_foreshortening_pic6"></td></tr>
    			<tr><td>Подпись (title):</td><td> <input type="text" name="gallery_foreshortening_text6"></td></tr>
    			<tr><td>Альтерн. текст  (alt):</td><td> <input type="text" name="gallery_foreshortening_alt6"></td></tr>
    			</table>
    		</td>
    		
		</tr>
    </table>
    

    
    </td>
</tr>

<!-- EDP: gallery_foreshortening_pic --> 


<!-- BDP: pic_alt_title_info -->
<tr>
    <td colspan="2"> <i>Если не заполнять поля title и alt для картинки, добавится текст с названия</i></td>
</tr>
<!-- EDP: pic_alt_title_info -->

<!-- BDP: pic_title -->
<tr>
    <td>Подпись (title):</td><td><input type="text" name="pic_title" class='admin-input-text' value="{ADM_PIC_TITLE}" /></td>
</tr>
<!-- EDP: pic_title -->

<!-- BDP: pic_alt -->
<tr>
    <td>Альтерн. текст  (alt):</td><td><input type="text" name="pic_alt" class='admin-input-text' value="{ADM_PIC_ALT}" /></td>
</tr>
<!-- EDP: pic_alt -->




<!-- BDP: featured_products -->
<tr>
     <td colspan=2><b>Рекомендованные товары: </b> <i>(Укажите артикулы товаров через запятую)</i>&nbsp;&nbsp;&nbsp;</td>
</tr>
<tr>
     <td colspan="2"><textarea rows=5 style="width: 100%;" name="featured_products">{ADM_FEATURED_PRODUCTS}</textarea></td>
</tr>
<!-- EDP: featured_products -->

<!-- BDP: used_complete -->
<tr class="additional">
     <td colspan=2><b>Комплект: </b> <i>(Укажите артикулы товаров через запятую)</i>&nbsp;&nbsp;&nbsp;</td>
</tr>
<tr class="additional">
     <td colspan="2"><textarea rows=5 style="width: 100%;" name="used_complete">{ADM_USED_COMPLETE}</textarea></td>
</tr>
<!-- EDP: used_complete -->


<!-- BDP: import -->
<tr>
    <td>Файл импорта (.xls):</td><td><input type="file" name="file"></td>
</tr>
<!-- EDP: import -->

<!-- BDP: upload_catalog_pics -->
<table Border=0 CellSpacing=0 CellPadding=0 Width="100%" Align="" vAlign="">
<tr>
    <td colspan="2" class="p10p">
    Файлы изображений не должны быть меньше 420px X 420px
<div id="fileQueue"></div>
<input type="file" name="uploadify" id="uploadify" />
<p><a href="javascript://" onClick="jQuery('#uploadify').uploadifyUpload();">Начать загрузку</a> | <a href="javascript:jQuery('#uploadify').uploadifyClearQueue();">Отменить все загрузки</a></p>

   <div id="response"></div>
    
    </td>
</tr>
</table>
<!-- EDP: upload_catalog_pics -->

<!-- BDP: delete_spare_catalog_pics -->



<!-- BDP: availability -->
<tr>
    <td>Наличие на складе</td><td>
    <select name="availability">
		{AVAILABILITY_S}
	</select>

    </td>
</tr>
<!-- EDP: availability -->


<!-- BDP: status -->
<tr>
    <td>Статус</td><td>
    <select name="status">
		{STATUS_S}
	</select>

    </td>
</tr>
<!-- EDP: status -->

<!-- BDP: is_new -->
<tr>
    <td>Новинка</td><td>
    <select name="is_new">
		{IS_NEW_S}
	</select>

    </td>
</tr>
<!-- EDP: is_new -->

<!-- BDP: hit -->
<tr>
    <td>Хит</td><td>
    <select name="hit">
		{HIT_S}
	</select>

    </td>
</tr>
<!-- EDP: hit -->


<!-- BDP: action -->
<tr>
    <td>Акция</td><td>
    <select name="hit">
		{HIT_S}
	</select>

    </td>
</tr>
<!-- EDP: action -->

<!-- BDP: section_s -->
<tr>
    <td>Разделы</td><td>
    <select name="sections">
		{SECTIONS_S}
	</select>

    </td>
</tr>
<!-- EDP: section_s -->

<!-- BDP: cat_setting -->
<tr>
    <td>Отображать товары без картинок</td><td>
    <select name="is_show_empty_pic">
		{IS_SHOW_EMPTY_PIC_S}
	</select>

    </td>
</tr>

<tr>
    <td>Отображать товары без цены или с нулевой ценой</td><td>
    <select name="is_show_empty_price">
		{IS_SHOW_EMPTY_PRICE_S}
	</select>

    </td>
</tr>

<tr>
    <td>Отображать хиты</td><td>
    <select name="is_show_hits">
		{IS_SHOW_EMPTY_HITS_S}
	</select>
    </td>
</tr>

<tr>
    <td>Отображать новинки</td><td>
    <select name="is_show_new">
		{IS_SHOW_EMPTY_NEW_S}
	</select>
    </td>
</tr>

<tr>
    <td>Отображать акции</td><td>
    <select name="is_show_actions">
		{IS_SHOW_EMPTY_ACTIONS_S}
	</select>
    </td>
</tr>


<tr>
    <td>Количество новинок на главной</td><td>
     	<input type="text" name="new_index_length" value ="{NEW_INDEX_LENGTH}" />
    </td>
</tr>

<tr>
    <td>Количество хитов на главной</td><td>
     	<input type="text" name="hits_index_length" value ="{HITS_INDEX_LENGTH}" />
    </td>
</tr>

<tr>
    <td>Количество акционных товаров на главной</td><td>
     	<input type="text" name="action_index_length" value ="{ACTION_INDEX_LENGTH}" />
    </td>
</tr>

<!-- EDP: cat_setting -->






<!-- BDP: instructions_a --> 
<tr>
	<td>Артикул: </td><td><input type="text" name="art"  class='admin-input-text' value="{ADM_INSTRUCTION_ART}"></td>
<tr>
<tr>
	<td>Название: </td><td><input type="text" name="name"  class='admin-input-text' value="{ADM_INSTRUCTION_NAME}"></td>
<tr>
<tr>
    
     <td colspan="2">Описание препарата:<br/><textarea class="mceEditor" rows=25 style="width: 100%;" name="text">{ADM_INSTRUCTION_TEXT}</textarea></td>
</tr>
<tr>
    
     <td colspan="2">Текст инструкции:<br/><textarea class="mceEditor" rows=25 style="width: 100%;" name="body">{ADM_INSTRUCTION_BODY}</textarea></td>
</tr>

<tr>
	<td>Title: </td><td><input type="text" name="title"  class='admin-input-text' value="{ADM_INSTRUCTION_TITLE}"></td>
<tr>
<tr>
	<td>Keywords: </td><td><input type="text" name="keywords"  class='admin-input-text' value="{ADM_INSTRUCTION_KEYWORDS}"></td>
<tr>    
<tr>
	<td>Description: </td><td><input type="text" name="description"  class='admin-input-text' value="{ADM_INSTRUCTION_DESCRIPTION}"></td>
<tr>    

<!-- EDP: instructions_a --> 


<!-- BDP: section_a --> 
<tr>
	<td>Название: <input type="text" name="name"  class='admin-input-text' value="{ADM_SECTION_NAME}"></td><td>Название (UA): <input type="text" name="name_ua"  class='admin-input-text' value="{ADM_SECTION_NAME_UA}"></td>
<tr>

<tr>
    
     <td colspan="2">Краткое описание:<br/><textarea class="mceEditor" rows=25 style="width: 100%;" name="short_text">{ADM_SECTION_BODY}</textarea></td>
</tr>

<tr>
    
     <td colspan="2">Краткое описание (UA):<br/><textarea class="mceEditor" rows=25 style="width: 100%;" name="short_text_ua">{ADM_SECTION_BODY_UA}</textarea></td>
</tr>

<tr>
	<td>Title: <input type="text" name="title"  class='admin-input-text' value="{ADM_SECTION_TITLE}"></td><td>Title: (UA) <input type="text" name="title_ua"  class='admin-input-text' value="{ADM_SECTION_TITLE_UA}"></td>
<tr>
<tr>
	<td>Keywords: <input type="text" name="keywords"  class='admin-input-text' value="{ADM_SECTION_KEYWORDS}"> </td><td>Keywords: (UA)<input type="text" name="keywords_ua"  class='admin-input-text' value="{ADM_SECTION_KEYWORDS_UA}"></td>
<tr>    
<tr>
	<td>Description: <input type="text" name="description"  class='admin-input-text' value="{ADM_SECTION_DESCRIPTION}"> </td><td>Description: (UA)<input type="text" name="description_ua"  class='admin-input-text' value="{ADM_SECTION_DESCRIPTION_UA}"></td>
<tr>    

<!-- EDP: section_a --> 

<!-- BDP: poll_b --> 
<tr>
	<td colspan="2">Результат:</td>
<tr>
<tr>
	<td>{ADM_A_Q1}: </td><td><input type="text" name="a1"  class='admin-input-text' value="{ADM_POLL_Q1P}"></td>
<tr>
<tr>
	<td>{ADM_A_Q2}: </td><td><input type="text" name="a2"  class='admin-input-text' value="{ADM_POLL_Q2P}"></td>
<tr>
<tr>
	<td>{ADM_A_Q3}: </td><td><input type="text" name="a3"  class='admin-input-text' value="{ADM_POLL_Q3P}"></td>
<tr>
<tr>
	<td>{ADM_A_Q4}: </td><td><input type="text" name="a4"  class='admin-input-text' value="{ADM_POLL_Q4P}"></td>
<tr>
<tr>
	<td>{ADM_A_Q5}: </td><td><input type="text" name="a5"  class='admin-input-text' value="{ADM_POLL_Q5P}"></td>
<tr>
<tr>
	<td>{ADM_A_Q6}: </td><td><input type="text" name="a6"  class='admin-input-text' value="{ADM_POLL_Q6P}"></td>
<tr>
<tr>
	<td>{ADM_A_Q7}: </td><td><input type="text" name="a7"  class='admin-input-text' value="{ADM_POLL_Q7P}"></td>
<tr>    
<tr>
	<td>{ADM_A_Q8}: </td><td><input type="text" name="a8"  class='admin-input-text' value="{ADM_POLL_Q8P}"></td>
<tr>    
<tr>
	<td>{ADM_A_Q9}: </td><td><input type="text" name="a9"  class='admin-input-text' value="{ADM_POLL_Q9P}"></td>
<tr>    
<tr>
	<td>{ADM_A_Q10}: </td><td><input type="text" name="a10"  class='admin-input-text' value="{ADM_POLL_Q10P}"></td>
<tr>    
<tr>
	<td>{ADM_A_Q11}: </td><td><input type="text" name="a11"  class='admin-input-text' value="{ADM_POLL_Q11P}"></td>
<tr>    
<tr>
	<td>{ADM_A_Q12}: </td><td><input type="text" name="a12"  class='admin-input-text' value="{ADM_POLL_Q12P}"></td>
<tr>    
<tr>
	<td>{ADM_A_Q13}: </td><td><input type="text" name="a13"  class='admin-input-text' value="{ADM_POLL_Q13P}"></td>
<tr>    
<tr>
	<td>{ADM_A_Q14}: </td><td><input type="text" name="a14"  class='admin-input-text' value="{ADM_POLL_Q14P}"></td>
<tr>    
<tr>
	<td>{ADM_A_Q15}: </td><td><input type="text" name="a15"  class='admin-input-text' value="{ADM_POLL_Q15P}"></td>
<tr>    
<tr>
	<td>{ADM_A_Q16}: </td><td><input type="text" name="a16"  class='admin-input-text' value="{ADM_POLL_Q16P}"></td>
<tr>    
<tr>
	<td>{ADM_A_Q17}: </td><td><input type="text" name="a17"  class='admin-input-text' value="{ADM_POLL_Q17P}"></td>
<tr>    
<tr>
	<td>{ADM_A_Q18}: </td><td><input type="text" name="a18"  class='admin-input-text' value="{ADM_POLL_Q18P}"></td>
<tr>    
<tr>
	<td>{ADM_A_Q19}: </td><td><input type="text" name="a19"  class='admin-input-text' value="{ADM_POLL_Q19P}"></td>
<tr>    
<tr>
	<td>{ADM_A_Q20}: </td><td><input type="text" name="a20"  class='admin-input-text' value="{ADM_POLL_Q20P}"></td>
<tr>    
    
<tr><td colspan="2" style="text-align: center;">__________________________________________________________</td></tr>
<!-- EDP: poll_b --> 
<!-- BDP: poll_a --> 



<tr>
	<td>Вопрос: <input type="text" name="name"  class='admin-input-text' value="{ADM_POLL_NAME}"></td><td></td>
<tr>
<tr>
	<td>Вопрос (UA): <input type="text" name="name_ua"  class='admin-input-text' value="{ADM_POLL_NAME_UA}"></td><td></td>
<tr>
<tr>
	<td colspan="2" style="text-align: center">Варианты ответов</td>
<tr>
    
<tr>
	<td>1: (RU) <input type="text" name="q1"  class='admin-input-text' value="{ADM_POLL_Q1}"> </td><td> (UA)  <input type="text" name="q1_ua"  class='admin-input-text' value="{ADM_POLL_Q1_UA}"></td>
<tr>
<tr>
	<td>2: (RU) <input type="text" name="q2"  class='admin-input-text' value="{ADM_POLL_Q2}"> </td><td> (UA)  <input type="text" name="q2_ua"  class='admin-input-text' value="{ADM_POLL_Q2_UA}"></td>
<tr>
<tr>
	<td>3: (RU) <input type="text" name="q3"  class='admin-input-text' value="{ADM_POLL_Q3}"> </td><td> (UA)  <input type="text" name="q3_ua"  class='admin-input-text' value="{ADM_POLL_Q3_UA}"></td>
<tr>
<tr>
	<td>4: (RU) <input type="text" name="q4"  class='admin-input-text' value="{ADM_POLL_Q4}"> </td><td> (UA)  <input type="text" name="q4_ua"  class='admin-input-text' value="{ADM_POLL_Q4_UA}"></td>
<tr>
<tr>
	<td>5: (RU) <input type="text" name="q5"  class='admin-input-text' value="{ADM_POLL_Q5}"> </td><td>  (UA) <input type="text" name="q5_ua"  class='admin-input-text' value="{ADM_POLL_Q5_UA}"></td>
<tr>
<tr>
	<td>6: (RU) <input type="text" name="q6"  class='admin-input-text' value="{ADM_POLL_Q6}"> </td><td> (UA)  <input type="text" name="q6_ua"  class='admin-input-text' value="{ADM_POLL_Q6_UA}"></td>
<tr>
<tr>
	<td>7: (RU) <input type="text" name="q7"  class='admin-input-text' value="{ADM_POLL_Q7}"> </td><td> (UA)  <input type="text" name="q7_ua"  class='admin-input-text' value="{ADM_POLL_Q7_UA}"></td>
<tr>
<tr>
	<td>8: (RU) <input type="text" name="q8"  class='admin-input-text' value="{ADM_POLL_Q8}"> </td><td> (UA)  <input type="text" name="q8_ua"  class='admin-input-text' value="{ADM_POLL_Q8_UA}"></td>
<tr>
<tr>
	<td>9: (RU) <input type="text" name="q9"  class='admin-input-text' value="{ADM_POLL_Q9}"> </td><td> (UA)  <input type="text" name="q9_ua"  class='admin-input-text' value="{ADM_POLL_Q9_UA}"></td>
<tr>
<tr>
	<td>10: (RU) <input type="text" name="q10"  class='admin-input-text' value="{ADM_POLL_Q10}"> </td><td>  (UA) <input type="text" name="q10_ua"  class='admin-input-text' value="{ADM_POLL_Q10_UA}"></td>
<tr>
<tr>
	<td>11: (RU) <input type="text" name="q11"  class='admin-input-text' value="{ADM_POLL_Q11}"> </td><td>  (UA) <input type="text" name="q11_ua"  class='admin-input-text' value="{ADM_POLL_Q11_UA}"></td>
<tr>
<tr>
	<td>12: (RU) <input type="text" name="q12"  class='admin-input-text' value="{ADM_POLL_Q12}"> </td><td> (UA)  <input type="text" name="q12_ua"  class='admin-input-text' value="{ADM_POLL_Q12_UA}"></td>
<tr>
<tr>
	<td>13: (RU) <input type="text" name="q13"  class='admin-input-text' value="{ADM_POLL_Q13}"> </td><td>  (UA) <input type="text" name="q13_ua"  class='admin-input-text' value="{ADM_POLL_Q13_UA}"></td>
<tr>
<tr>
	<td>14: (RU) <input type="text" name="q14"  class='admin-input-text' value="{ADM_POLL_Q14}"> </td><td> (UA)  <input type="text" name="q14_ua"  class='admin-input-text' value="{ADM_POLL_Q14_UA}"></td>
<tr>
<tr>
	<td>15: (RU) <input type="text" name="q15"  class='admin-input-text' value="{ADM_POLL_Q15}"> </td><td> (UA)  <input type="text" name="q15_ua"  class='admin-input-text' value="{ADM_POLL_Q15_UA}"></td>
<tr>
<tr>
	<td>16: (RU) <input type="text" name="q16"  class='admin-input-text' value="{ADM_POLL_Q16}"> </td><td> (UA)  <input type="text" name="q16_ua"  class='admin-input-text' value="{ADM_POLL_Q16_UA}"></td>
<tr>
<tr>
	<td>17: (RU) <input type="text" name="q17"  class='admin-input-text' value="{ADM_POLL_Q17}"> </td><td>  (UA)  <input type="text" name="q17_ua"  class='admin-input-text' value="{ADM_POLL_Q17_UA}"></td>
<tr>
<tr>
	<td>18: (RU) <input type="text" name="q18"  class='admin-input-text' value="{ADM_POLL_Q18}"> </td><td> (UA) <input type="text" name="q18_ua"  class='admin-input-text' value="{ADM_POLL_Q18_UA}"></td>
<tr>
<tr>
	<td>19: (RU) <input type="text" name="q19"  class='admin-input-text' value="{ADM_POLL_Q19}"> </td><td>  (UA)  <input type="text" name="q19_ua"  class='admin-input-text' value="{ADM_POLL_Q19_UA}"></td>
<tr>
<tr>
	<td>20: (RU) <input type="text" name="q20"  class='admin-input-text' value="{ADM_POLL_Q20}"> </td><td>  (UA)  <input type="text" name="q20_ua"  class='admin-input-text' value="{ADM_POLL_Q20_UA}"></td>
<tr>

<tr>
    <td>Статус: </td><td>
        <select name="poll_status">
            <option value="1" {ADM_POLL_ACT}>Активен</option>
            <option value="0" {ADM_POLL_NOACT}>Не активен</option>
        </select>
<tr>

    
<!-- EDP: poll_a --> 

<!-- BDP: standart_text --> 

<tr>
    <td colspan="2">Вставте <b>&lbrace;preparation&rbrace;</b> в то место где должно отображаться название препарата.</td>
<tr>
<tr>
     <td colspan="2">Текст:<br/><textarea class="mceEditor" rows=25 style="width: 100%;" name="body">{ADM_STANDART_TEXT_BODY}</textarea></td>
</tr>
<!-- EDP: standart_text --> 




<!-- BDP: end -->
<input type="hidden" name="HTTP_REFERER" value="{REFERER}">
<tr>
    <td colspan=2><br><br><center><input class="button" value="Сохранить" type="submit"></center></td>
</tr>
</table>
</form>
<!-- EDP: end -->
