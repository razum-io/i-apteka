﻿<!-- BDP: orders -->
  <div class="order_history">
   <table>
    <tr class="hnone">
     <th>№</th>
     <th>Ф.И.О.</th>
     <th>Адрес</th>
     <th>Дата <a href="#"><img src="images/data.gif" width="16" height="16" alt="" /></a></th>
     <th></th>
    </tr>
    <!-- BDP: orders_list -->
    <tr>
    <td><a href="/admin/order_detail/{ADM_ORDER_ID}">{ADM_ORDER_NO}</a></td>    
     <td>{ADM_ORDER_FIO}</td>
     <td>{ADM_ORDER_ADR}</td>
     <td>{ADM_ORDER_DATE}</td>
     <td></td>
    </tr>
    <!-- EDP: orders_list -->
    
   </table>
  </div>
<!-- EDP: orders -->

<!-- BDP: orders_empty -->
<h2>Заказов не найдено</h2>
<!-- EDP: orders_empty -->

<!-- BDP: order_detail -->

  <div class="order_review">
   <div class="w_del">
    <table>
     <tr>
      <td class="width">№</td>
      <td>{ORDER_DETAIL_ID1}</td>
     </tr>
     <tr>
      <td class="width">Ф.И.О.:</td>
      <td>{ORDER_DETAIL_FIO}</td>
     </tr>
     <tr>
      <td class="width">E-mail:</td>
      <td><a href="mailto:{ORDER_DETAIL_MAIL}">{ORDER_DETAIL_MAIL}</a></td>
     </tr>
     <tr>
      <td class="width">Дата:</td>
      <td>{ORDER_DETAIL_DATE}</td>
     </tr>
     <tr>
      <td class="width">Адрес:</td>
      <td>{ORDER_DETAIL_ADR}</td>
     </tr>
     <tr>
      <td class="width">Телефон:</td>
      <td>{ORDER_DETAIL_PHONE}</td>
     </tr>
     <tr>
      <td class="width">Моб. телефон:</td>
      <td>{ORDER_DETAIL_MPHONE}</td>
     </tr>
     <tr>
      <td class="width">Точка самовывоза:</td>
      <td>{ORDER_DETAIL_TVIVOZA}</td>
     </tr>
     
     <td class="width">Дополнительно:</td>
      <td>{ORDER_DETAIL_INFO}</td>
     </tr>
    </table>
    <table>
        <tr><td>Название</td><td>Артикул</td><td>Производитель</td><td>Цена за единицу</td><td>Количество</td><td>Сумма</td></tr>
     <!-- BDP: order_detail_list -->
        <tr>     
      <td>{ORDER_DETAIL_LIST_NAME}</td>
      <td>{ORDER_DETAIL_LIST_ARTIKUL}</td>
      <td>{ORDER_DETAIL_LIST_PRODUCER}</td>
      <td>{ORDER_DETAIL_LIST_COST} грн.</td>
      <td>{ORDER_DETAIL_LIST_COUNT} шт.</td>
      <td>{ORDER_DETAIL_LIST_SUMM} грн.</td>
     </tr>     

     <!-- EDP: order_detail_list -->
     
      <!-- BDP: order_detail_list_empty -->
      <tr>
      <td colspan="2" align="center">Заказов не найдено</td>      
     </tr>
      <!-- EDP: order_detail_list_empty -->
      <tr><td colspan="7">Итого: {ORDER_SUMM_ALL}</td></tr>
    </table>
   </div>
  </div>
<!-- EDP: order_detail -->
