<!-- BDP: instructions -->

 <div class="order_history">
     <a title="Добавить инструкцию" href="/{REGION_URL}admin/addInstruction/"><img border="0" alt="Добавить" src="/img/admin_icons/add_page.png">Добавить инструкцию</a>
   <table>
    <tr class="hnone">     
     <th>Акртикул</th>    
     <th>Название</th>
     <th></th>
     <th></th>     
    </tr>
    <!-- BDP: instructions_list -->
    <tr>
     <td>{INSTRUCTION_ART}</td>
     <td><a href="/{REGION_URL}admin/editInstruction/{INSTRUCTION_ID}">{INSTRUCTION_NAME}</a></td>   
     <td><a title="Редактировать инструкцию" href="/{REGION_URL}admin/editInstruction/{INSTRUCTION_ID}"><img width="12" height="12" alt="Редактировать инструкцию" src="/img/admin_icons/edit.png"></a></td> 
     <td><a onclick="return confirm('Вы уверены что хотите удалить инструкцию?'); return false;" title="Удалить инструкцию" href="/{REGION_URL}admin/deleteInstruction/{INSTRUCTION_ID}"><img width="12" height="12" alt="Удалить" src="/img/admin_icons/delete.png"></a></td>
    </tr>
    <!-- EDP: instructions_list -->
    
   </table>
  </div>
  
  <!-- BDP: instructions_empty -->
  Нет данных
  <!-- EDP: instructions_empty -->
  
<!-- EDP: instructions -->
