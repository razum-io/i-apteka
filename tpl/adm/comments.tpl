<!-- BDP: comments -->
<div class="goods_full">
   <div class="comment_block">
    <form method="post" action="">
        <table>
            <tr>
                <td>Видимость</td>
                <td><select name="visible"> 
                        <option value="1" {COMMENT_FIO_VISIBLE}>Видимый</option>
                        <option value="0"  {COMMENT_FIO_HIDDEN}>Скрытый</option>
                    </select></td>
             </tr>
            <tr>
                <td>Имя и фамилия</td>
                <td><input type="text" name ="fio" value ="{COMMENT_FIO}"/></td>
             </tr>
             <tr>
                <td>Период эксплуатации</td>
                <td><input class="small" type="text" name="period_of_operation" value="{COMMENT_PERIOD_OF_OPERATION}" /></td>
             </tr>
             <tr>    
                <td>Достоинства</td>
                <td><textarea name="dignity">{COMMENT_DIGNITY}</textarea></td>
             </tr>
             <tr>
                <td>Недостатки</td>
                <td><textarea name="shortcomings">{COMMENT_SHORTCOMMINGS}</textarea></td>
             </tr>
             <tr>
                <td>Рекомендации</td>
                <td><textarea name="recommendations">{COMMENT_RECOMENDATIONS}</textarea></td>
             </tr>
             <tr>
                 <td>Вывод</td>
                 <td><textarea name="conclusion">{COMMENT_CONCLUSION}</textarea></td>
             </tr>
             <tr>
                  <td></td>
                    <td>                        
                        <p><input type="submit"  value="Отправить" /></p>
                    </td>
                </tr>
            </table>
          </form> 
     </div>
</div> 
<!-- EDP: comments -->