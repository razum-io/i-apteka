<!-- BDP: main_index -->
<h1>{INDEX_HEADER}</h1>
{INDEX_BODY}
<!-- EDP: main_index -->

<!-- BDP: main_news -->

  

 <h2>Новости</h2>
 <div class="all"><a href="/news/">Все новости</a></div>
<!-- EDP: main_news -->

<!-- BDP: main_new_goods -->
 <h2><a href="/catalog/novelty" title="Новинки">Новинки</a></h2>
 <div class="goods_list">
 {NEW_CATALOG_ITEMS}
 </div>
<!-- EDP: main_new_goods -->

<!-- BDP: main_hit_goods -->
 <h2><a href="/catalog/hits" title="Хиты">Хиты</a></h2>
 <div class="goods_list">
 {HIT_CATALOG_ITEMS}
 </div>
<!-- EDP: main_hit_goods -->

<!-- BDP: main_actions_goods -->
 <h2><a href="/catalog/actions" title="Акции">Акции</a></h2>
 <div class="goods_list">
 {ACT_CATALOG_ITEMS}
 </div>
<!-- EDP: main_actions_goods -->