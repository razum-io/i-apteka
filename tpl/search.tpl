<!-- BDP: search -->

<div class="div-search">
 
	Поисковая фраза: <strong>{SEARCH_QUERY}</strong>
	<br />
	Найдено результатов: <strong>{SEARCH_COUNT}</strong>

	<ul class="partisions_list">
    	<!-- BDP: search_row -->
    	<li>
        	<span><a href="{SEARCH_HREF}" title="{SEARCH_NAME}">{SEARCH_NAME}</a></span>
        	<p>{SEARCH_PREVIEW}</p>
        	<div class="clear"></div>
    	</li>
		<!-- EDP: search_row -->
	</ul>
</div>
<!-- EDP: search -->
