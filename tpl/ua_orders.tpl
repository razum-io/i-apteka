<!-- BDP: orders_empty -->
<h2>Заказів не знайдено</h2>
<!-- EDP: orders_empty -->

<!-- BDP: order_detail -->

  <div class="order_review">
   <div class="w_del">
      
    <table>
     <tr>
      <td class="width"><b>№</b></td>
      <td>{ORDER_DETAIL_ID1}</td>
     </tr>
     <tr>
      <td class="width"><b>П.І.Б.</b></td>
      <td>{ORDER_DETAIL_FIO}</td>
     </tr>
     <tr>
      <td class="width"><b>Дата</b></td>
      <td>{ORDER_DETAIL_DATE}</td>
     </tr>
     <tr>
      <td class="width"><b>Місто</b></td>
      <td>{ORDER_DETAIL_CITY}</td>
     </tr>
     <tr>
      <td class="width"><b>Телефон</b></td>
      <td>{ORDER_DETAIL_PHONE}</td>
     </tr>
     <tr>
      <td class="width"><b>Моб. телефон</b></td>
      <td>{ORDER_DETAIL_MOBAIL_PHONE}</td>
     </tr>
     <tr>
      <td class="width"><b>Тип доставки</b></td>
      <td>{ORDER_DETAIL_DELIVERY_TYPE}</td>
     </tr>
     <!-- BDP: order_detail_service -->
     <td class="width"><b>Служба доставки</b></td>
      <td>{ORDER_DETAIL_DELIVERY_SERVICE}</td>
     </tr>
     <!-- EDP: order_detail_service -->
     <td class="width"><b>Додатково:</b></td>
      <td>{ORDER_DETAIL_BODY}</td>
     </tr>
     <!-- BDP: order_detail_list -->
     <tr>
     <td colspan="2"><hr /></td>
     </tr>
     <tr>
      <td class="width"><b>Назва</b></td>
      <td>{ORDER_DETAIL_LIST_NAME}</td>
     </tr>
    
     <tr>
      <td class="width"><b>Артикул</b></td>
      <td>{ORDER_DETAIL_LIST_ARTIKUL}</td>
     </tr>  
         <tr>
      <td class="width"><b>Ціна за одиницю</b></td>
      <td>{ORDER_DETAIL_LIST_COST} грн.</td>
     </tr>  
      <tr>
      <td class="width"><b>Кількість</b></td>
      <td>{ORDER_DETAIL_LIST_COUNT} шт.</td>
     </tr>
     <tr>
      <td class="width"><b>Сума</b></td>
      <td>{ORDER_DETAIL_LIST_SUMM} грн.</td>
     </tr>     
     <tr>
     
     </tr>
     <!-- EDP: order_detail_list -->
     
      <!-- BDP: order_detail_list_empty -->
      <tr>
      <td colspan="2" align="center">Заказів не знайдено</td>      
     </tr>
      <!-- EDP: order_detail_list_empty -->
     
     <tr>
      <td class="width"><b>Вся сума</b></td>
      <td>{ORDER_DETAIL_SUMM} грн.</td>
     </tr>
    </table>
   </div>
   
   
  </div>
<!-- EDP: order_detail -->
