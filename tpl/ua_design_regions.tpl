<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML><HEAD><TITLE>{TITLE}</TITLE>
<meta name="keywords" content="{KEYWORDS}">
<meta name="description" content="{DESCRIPTION}">
<META http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="document-state" content="dynamic">
<meta name="revisit" content="7 days">
<meta name="revisit-after" content="7 days">
<META NAME="Resourse-type" CONTENT="document">
<META NAME="Robots" CONTENT="index,follow">
<META NAME="Rating" CONTENT="general">
<META NAME="Distribution" CONTENT="global">
<META NAME="Classification" CONTENT="">
<META NAME="Category" CONTENT="">
<meta http-equiv="Pragma" content="token">
<meta http-equiv="Cache-Control" content="token">
<META NAME="Copyright" CONTENT="2011 deluxe.dp.ua">
<LINK href="/css/style.css" type="text/css" rel=stylesheet>
<link  rel="stylesheet" type="text/css" href="/css/lightbox.css" />
<link  rel="stylesheet" type="text/css" href="/css/calendar-blue.css" />

<LINK REL="shortcut icon" HREF="/favicon.ico" type="image/x-icon">

<!--[if lte IE 6]>  
<script defer type="text/javascript" src="/js/pngfix.js" mce_src="/js/pngfix.js"></script>
<![endif]-->

<link  rel="stylesheet" type="text/css" href="/css/lightbox.css" />
<link  rel="stylesheet" type="text/css" href="/css/calendar-blue.css" />

<script type="text/javascript" src="/js/jquery.js"></script>


<!-- BDP: adminjslib -->
<script type="text/javascript" src="/js/swfobject.js"></script>
    <script type="text/javascript" src="/js/jquery.uploadify.v2.1.4.min.js"></script>

    	<script type="text/javascript">
            
            var objDump = function (obj) {
                var ret = '';
                for(o in obj) {
                    ret += 'Key: '+o+' Val: '+obj[o]+"\n";
                }
                return ret;
            }
            
    		jQuery(document).ready(function() {
    			jQuery("#uploadify").uploadify({
    				
    				'expressInstall': '/modules/uploadify/expressInstall.swf',
    				'uploader': '/modules/uploadify/uploadify.swf',
    				'script': '/modules/uploadify/uploadify.php',
    				'folder': '/upload',
    				'cancelImg': '/modules/uploadify/cancel.png',
    				
    				'queueID'        : 'fileQueue',
    				'fileDesc'       : 'jpg, gif, png',
    				'fileExt'        : '*.jpg;*.gif;*.png;*.jpeg',
    				'auto'           : false,
    				'multi'          : true,
                                'onAllComplate': function(t1, t2) {
                                  alert(objDump(t2));  
                                },
    				'onComplete'   : function(event,queueID,fileObj,response,data) {
                                    alert(response);
    									if (response != 'OK') {
    										jQuery('#response').append(response);
    									}
    								 },
    				onError: function (event, queueID, fileObj, errorObj) {
                                  
    							 err = '';
    					         if (errorObj.status == 404) {
    					            err += 'Could not find upload script.';
    					         } else if (errorObj.type === "HTTP") {
    					            err += 'error '+errorObj.type+": "+errorObj.status;
    					         } else if (errorObj.type ==="File Size") {
    					            err += fileObj.name+' '+errorObj.type+' Limit: '+Math.round(errorObj.sizeLimit/1024)+'KB';
    					         } else {
    					            err += 'error '+errorObj.type+": "+errorObj.text;
    					         }
                                                  
    					      }
                                             
    				/*
    				*/
    			});
    		});
    	</script>



<!-- EDP: adminjslib -->

<script type="text/javascript" src="/js/jq-scripts.js"></script>

<script type="text/javascript" src="/js/jq-order-form.js"></script>
<!--<script type="text/javascript" src="/js/prototype.js"></script>-->

<script src="/js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
<script src="/js/lightbox.js" type="text/javascript"></script>

<script type="text/javascript" src="/js/scripts.js"></script>
<script type="text/javascript" src="/js/js_func.js"></script>

        <link rel="stylesheet" href="/css/jquery-ui.css">
        <link rel="stylesheet" href="/css/jquery.selectBox.css">
        <script type="text/javascript" src="/js/jquery_ui/jquery.ui.core.min.js"></script>
        <script type="text/javascript" src="/js/jquery_ui/jquery.ui.widget.min.js"></script>
        <script type="text/javascript" src="/js/ui.checkbox.js"></script>
        <script type="text/javascript" src="/js/jquery.selectBox.min.js"></script>
        <script type="text/javascript" src="/js/site.js"></script>

</HEAD>
<body>

    <div class="wraper zindex">
 <div class="header">
  <a class="logo" href="#"><img src="/img/logo.jpg" width="242" height="95" alt="" /></a>
  <p class="top_text"><span>0 (800) 888-85-89</span><br />Всеукраїнська довідкова служба<br />аптек ООО «I.K.Well»</p>
  <div class="enter_exit"><a href="#">Мій кабінет</a><span>|</span><a href="#">Вийти</a></div>
  <!--<div class="enter_exit"><a href="#">Регистрация</a><span>|</span><a href="#">Войти</a></div>-->
  <div class="busket busket_bg">
   <p class="tit"><a href="#">Ваш Кошик</a></p>
   <p><a href="#">Товарів:</a><span><a href="#">15 шт.</a></span></p>
   <p><a href="#">На суму:</a><span><a href="#">15 500 грн.</a></span></p>
   <p class="order"><a href="#">Оформити заказ</a></p>
  </div>
  <!--<div class="busket">
   <p class="tit"><a href="#">Ваша Корзина</a></p>
   <p><a href="#">Товаров:</a><span><a href="#">0 шт.</a></span></p> 
   <p><a href="#">На сумму:</a><span><a href="#">0 грн.</a></span></p>
   <p class="center"><a href="#">Корзина пуста</a></p>
  </div>-->
  <div class="lng"><span>рос</span> | <a href="#">укр</a></div>
  <div class="alpha"><a href="#">А</a><a href="#">Б</a><a href="#">В</a><a href="#">Г</a><a href="#">Д</a><a href="#">Е</a><a href="#">Ё</a><a href="#">Ж</a><a href="#">З</a><a href="#">И</a><a href="#">Й</a><a href="#">К</a><a href="#">Л</a><a href="#">М</a><a href="#">Н</a><a href="#">О</a><a href="#">П</a><a href="#">Р</a><a href="#">С</a><a href="#">Т</a><a href="#">У</a><a href="#">Ф</a><a href="#">Х</a><a href="#">Ц</a><a href="#">Ч</a><a href="#">Ш</a><a href="#">Щ</a><a href="#">Э</a><a href="#">Ю</a><a href="#">Я</a><a href="#">0-9</a><a href="#">A-Z</a></div>
  <div class="search_block"><input type="text" value="Пошук препарату за назвою" id="email" /><input type="submit" value="Знайти" class="button" /></div>
  <div class="region">
   Ваш регион: {LIST_REGIONS} <!--<a href="#" id="choose">Выбрать регион</a>
   <div class="region_list">
    <ul>
     <li><a href="#">г. Киев</a></li>
     <li><a href="#">Днепропетровская обл.</a></li>
     <li><a href="#">Донецкая область</a></li>
     <li><a href="#">Запорожская область</a></li>
     <li><a href="#">Полтавская область</a></li>
     <li><a href="#">Харьковская область</a></li>
     <li><a href="#">Киевская область</a></li>
     <li><a href="#">Одесская область</a></li>
     <li><a href="#">Луганская область</a></li>
     <li><a href="#">г. Севастополь</a></li>
     <li><a href="#">Николаевская область</a></li>
    </ul>
   </div>-->
  </div>
  <div class="top_menu_block">
  <table>  
   <tr> 		 
        <!-- BDP: horisontal -->
        <td class=""><div class="rel"><a href="{MENU_HREF}" title="{MENU_NAME}">{MENU_NAME}</a></div></td>	
	<!-- EDP: horisontal -->
	<!--<td class="first"><a href="#" title="">Наши аптеки</a></td>
	<td><a href="#" title="">Новости</a></td> 
	<td><a href="#" title="">Бонус-Дисконт</a></td>
	<td><a href="#" title="">Как заказать</a></td>
	<td><a href="#" title="">Оплата</a></td>
	<td class="last"><a href="#" title="">Книга отзывов</a></td>-->
   </tr>
  </table>
  </div>
  <ul class="pic_menu">
   <li class="active"><a class="home" href="#" title="">&nbsp;</a></li>
   <li class="pad"><a href="#" title="" class="sitemap">&nbsp;</a></li>
   <li><a href="#" title="" class="email">&nbsp;</a></li>
  </ul>
  <div class="clear"></div>
 </div>
</div>
 <div class="top_banner_list">
  <ul>
   <li><a href="#">Вітаміни для всієї родини</a></li>
   <li><a href="#">Консультація</a></li>
  </ul>
  <div class="clear"></div>
 </div>
<div class="wraper">
<div class="main_menu">
 <ul class="sf-menu">
  <li><a href="#">Лікарські препарати</a>
   <ul>
    <li>
     <ul>
      <li><a href="#">Анальгетики-антипиретики / Спазмолітики / НПВС</a></li>
      <li><a href="#">Антациди/Противиразкові</a></li>
      <li><a href="#">Антигістамінні препарати</a></li>
      <li><a href="#">Антидіабетичні засоби</a></li>
      <li><a href="#">Антимикробні препарати/Антибіотики</a></li>
      <li><a href="#">Антипротозойні препарати</a></li>
      <li><a href="#">Антиревматичні препарати локальної дії</a></li>
      <li><a href="#">Антисептичні / Ранозаживляющие препарати</a></li>
      <li><a href="#">Биогенні стимулятори</a></li>
      <li><a href="#">Вакцини</a></li>
      <li><a href="#">Венотонизируючі препарати</a></li>
      <li><a href="#">Вітаміни / Полівітаміни / Тонізуючі засоби</a></li>
      <li><a href="#">Впливаючі на систему крові та кровотворення</a></li>
      <li><a href="#">Впливаючі на тонус та скорочувальну активність міометрія</a></li>
      <li><a href="#">Гинекологичні засоби Гомеопатичні препарати</a></li>
      <li><a href="#">Гормони. препарати, впливаючі на обмін речовин</a></li>
      <li><a href="#">Діючі на нервову систему</a></li>
      <li><a href="#">Дерматологічні засоби</a></li>
      <li><a href="#">Для контрацепції</a></li>
     </ul>
    </li>
    <li>
     <ul>
      <li><a href="#">Для корекції полової функції у чоловіків</a></li>
      <li><a href="#">Для лікування аденоми передміхурової залози</a></li>
      <li><a href="#">Для лікування алкогольної залежності</a></li>
      <li><a href="#">Для лікування бронхо-легеневої системи</a></li>
      <li><a href="#">Для лікування в отоларінгології</a></li>
      <li><a href="#">Для лікування в офтальмології</a></li>
      <li><a href="#">Для лікування в урології, нефрології, проктології</a></li>
      <li><a href="#">Для лікування вестибулярних порушень/Ноотропні</a></li>
      <li><a href="#">Для лікування опорно-рухової системи</a></li>
      <li><a href="#">Для лікування органів травлення/Ферменти</a></li>
      <li><a href="#">Для лікування печінчки і жовчних шляхів</a></li>
      <li><a href="#">Для нормализаціїї мікрофлори кишечника</a></li>
      <li><a href="#">Для схуднення</a></li>
      <li><a href="#">Інші товари</a></li>
      <li><a href="#">Лікувальна косметика</a></li>
      <li><a href="#">Сечогінні препарати</a></li>
      <li><a href="#">Заспокійливі засоби</a></li>
      <li><a href="#">Стоматологичні засоби</a></li>
      <li><a href="#">Трави, сбори, фиточаї</a></li>
      <li><a href="#">Иммуномодулятори</a></li>
     </ul>
    </li>
    <li>
     <ul>
      <li><a href="#">Муколітичні и відхаркувальні засоби</a></li>
      <li><a href="#">Онкологичні препарати</a></li>
      <li><a href="#">Протирвотні</a></li>
      <li><a href="#">Перев'язувальні матеріали, лейкопластири, шприци, тести, інше</a></li>
      <li><a href="#">Противирусні засоби</a></li>
      <li><a href="#">Протигрибкові, Протипаразитарні засоби</a></li>
      <li><a href="#">Протидиарейні, для лікування инфекцій кишечника</a></li>
      <li><a href="#">Протизачаточні засоби</a></li>
      <li><a href="#">Протипростудні/Для лікування ОРВІ и гриппу</a></li>
      <li><a href="#">Протитуберкулезні</a></li>
      <li><a href="#">Інші засоби</a></li>
      <li><a href="#">Раствори для в/в инфузій/В/м ин’єкцій</a></li>
      <li><a href="#">Рентгеноконтрастні засоби</a></li>
      <li><a href="#">Сердцево-судинні засоби</a></li>
      <li><a href="#">Проносні засоби</a></li>
      <li><a href="#">Снотворні</a></li>
      <li><a href="#">Сорбенти. препарати для лікування харчових і алкогольних отрруєнь</a></li>
      <li><a href="#">Засоби для хирургії, анестезиології, реаниматології</a></li>
      <li><a href="#">Засоби, укріпляючі імунітет</a></li>
     </ul>
    </li>
   </ul>
  </li>
  <li><a href="#">Вітаміни</a></li>
  <li><a href="#">Биологічно активні добавки (БАД)</a></li>
  <li><a href="#">Вироби Медичного призначения</a></li>
  <li><a href="#">Медична техніка</a></li>
  <li><a href="#">Сіпутні товари</a></li>
  <li><a href="#">Косметика</a></li>
 </ul>
 <div class="clear"></div>
</div>
<div class="clear"></div>
<div class="pager">
 <a href="#">Головна</a><span>&nbsp;</span><a href="#">Харків</a><span>&nbsp;</span><a href="#">Каталог продукції</a><span>&nbsp;</span>Бальзам Вигор 500мл
</div>
 <div class="left_column">
  <div class="nofont"><a href="#"><img src="/img/banner3.jpg" width="240" height="284" alt="" /></a></div>  
  <div class="left_menu">
   <div class="tit">Лекарственні препарати</div>
   <ul>
      <li><a href="#">Анальгетики-антипиретики / Спазмолитики / НПВС</a></li>
      <li><a href="#">Антациды/Протиязвенні</a></li>
      <li><a href="#">Антигистаминні препарати</a></li>
      <li><a href="#">Антидиабетичні засоби</a></li>
      <li><a href="#">Антимикробні препарати/Антибиотики</a></li>
      <li><a href="#">Антипротозойні препарати</a></li>
      <li><a href="#">Антиревматичні препарати местного действия</a></li>
      <li><a href="#">Антисептичні / Ранозаживляющие препарати</a></li>
      <li><a href="#">Биогенні стимуляторы</a></li>
      <li><a href="#">Вакцины</a></li>
      <li><a href="#">Венотонизирующие препарати</a></li>
      <li><a href="#">Витамины / Поливитамины / Тонизирующие засоби</a></li>
      <li><a href="#">Влияющие на систему крови и кроветворение</a></li>
      <li><a href="#">Влияющие на тонус и сократительную активность миометрия</a></li>
      <li><a href="#">Гинекологичні засобиГомеопатичні препарати</a></li>
      <li><a href="#">Гормоны. препарати, влияющие на обмен веществ</a></li>
      <li><a href="#">Действующие на нервную систему</a></li>
      <li><a href="#">Дерматологичні засоби</a></li>
      <li><a href="#">Для контрацепции</a></li>
      <li><a href="#">Для коррекции половой функции у мужчин</a></li>
      <li><a href="#">Для лікування аденомы предстательной железы</a></li>
      <li><a href="#">Для лікування алкогольной зависимости</a></li>
      <li><a href="#">Для лікування бронхо-легочной системи</a></li>
      <li><a href="#">Для лікування в отоларингології</a></li>
      <li><a href="#">Для лікування в офтальмології</a></li>
      <li><a href="#">Для лікування в урології, нефрології, проктології</a></li>
      <li><a href="#">Для лікування вестибулярных нарушений/Ноотропні</a></li>
      <li><a href="#">Для лікування опорно-двигательной системи</a></li>
      <li><a href="#">Для лікування органов пищеварения/Ферменты</a></li>
      <li><a href="#">Для лікування печени и желчевыводящих путей</a></li>
      <li><a href="#">Для нормализации микрофлоры кишечника</a></li>
      <li><a href="#">Для похудения</a></li>
      <li><a href="#">Другие товары</a></li>
      <li><a href="#">Лечебная косметика</a></li>
      <li><a href="#">Мочегонні препарати</a></li>
      <li><a href="#">Успокаивающие засоби</a></li>
      <li><a href="#">Стоматологичні засоби</a></li>
      <li><a href="#">Травы, сборы, фиточаи</a></li>
      <li><a href="#">Иммуномодуляторы</a></li>
      <li><a href="#">Муколитичні и отхаркивающие засоби</a></li>
      <li><a href="#">Онкологичні препарати</a></li>
      <li><a href="#">От укачивания/Протирвотні</a></li>
      <li><a href="#">Перевязочні материалы, лейкопластыри, шприцы, тесты, другое</a></li>
      <li><a href="#">Противирусні засоби</a></li>
      <li><a href="#">Протигрибковые, Протипаразитарні засоби</a></li>
      <li><a href="#">Протидиарейні, для лікування инфекций кишечника</a></li>
      <li><a href="#">Протизачаточні засоби</a></li>
      <li><a href="#">Протипростудні/Для лікування ОРВИ и гриппа</a></li>
      <li><a href="#">Протитуберкулезні</a></li>
      <li><a href="#">інші засоби</a></li>
      <li><a href="#">Растворы для в/в инфузий/В/м иньекций</a></li>
      <li><a href="#">Рентгеноконтрастні засоби</a></li>
      <li><a href="#">Сердечно-сосудистые засоби</a></li>
      <li><a href="#">Слабительні засоби</a></li>
      <li><a href="#">Снотворні</a></li>
      <li><a href="#">Сорбенты. препарати для лікування пищевых и алкогольных отравлений</a></li>
      <li><a href="#">засоби для хирургии, анестезиології, реаниматології</a></li>
      <li><a href="#">засоби, укрепляющие иммунитет</a></li>
   </ul>
  </div>
  <div class="nofont"><a href="#"><img src="/img/banner4.jpg" width="240" height="402" alt="" /></a></div>  
  <div class="poll_tit">Как политик должен отвечать за невыполнение своих предвыборных обещаний?</div>
  <div class="poll_block">
   <form action="#" method="post">
   <ul>
    <li>
		<div>
        <input class="ui-helper-hidden-accessible" value="1" id="radio_1" name="poll[]" type="radio">
        <label class="ui-radio" for="radio_1">Добровольной отставкой</label>
        </div>
	</li>
    <li>
	                        <div>
                            <input class="ui-helper-hidden-accessible" value="2" id="radio_2" name="poll[]" type="radio">
                            <label class="ui-radio" for="radio_2">Публичным извинением</label>
                        </div>
	</li>
    <li>
	                        <div>
                            <input class="ui-helper-hidden-accessible" value="3" id="radio_3" name="poll[]" type="radio">
                            <label class="ui-radio" for="radio_3">Денежным штрафом</label>
                        </div>
	</li>
    <li>
	                        <div>
                            <input class="ui-helper-hidden-accessible" value="4" id="radio_4" name="poll[]" type="radio">
                            <label class="ui-radio" for="radio_4">Запретом на участие в следующих выборах</label>
                        </div>
	</li>
    <li>
	                        <div>
                            <input class="ui-helper-hidden-accessible" value="5" id="radio_5" name="poll[]" type="radio">
                            <label class="ui-radio ui-radio-state-checked ui-radio-checked" for="radio_5">Никак. Обещания — это просто слова...</label>
                        </div>
	</li>
   </ul>
   </form>
   <div class="clear"></div>
   <div class="button_wrap"><input type="submit" value="Ответить" class="button" /></div>
  </div>
  
  <div class="bottom_corn"></div>
  
  
  
  

                    <span class="corner top left lt"></span>
                    <span class="corner top right rt"></span>
                    <span class="corner bottom left lb"></span>
                    <span class="corner bottom right rb"></span>
                
  
  
 </div>
 <div class="right_column">
 <div class="type_elem">
  <h2>Новинки</h2>
  <div class="all"><a href="#">Все новинки</a></div>
  <div class="goods_list">
   <ul>
    <li>
     <ul> 
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальным маслом и маслом зародышей пшеницы 250мл</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальным маслом и маслом зародышей</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальицы 250мл</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальицы 250мл</a></div>
      </li>
     </ul>
    </li>
    <li>
     <ul>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
     </ul>   
    </li>
    <li>
     <ul>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
     </ul>
    </li>
   </ul>
   <ul>
    <li>
     <ul>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальным маслом и маслом зародышей пшеницы 250мл</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальным маслом и маслом зародышей</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальицы 250мл</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальицы 250мл</a></div>
      </li>
     </ul>
    </li>
    <li>
     <ul>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
     </ul>   
    </li>
    <li>
     <ul>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
     </ul>
    </li>
   </ul>
   <div class="clear"></div>
  </div>
  <h2>Хиты</h2>
  <div class="all"><a href="#">Все хиты</a></div>
  <div class="goods_list">
   <ul>
    <li>
     <ul>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальным маслом и маслом зародышей пшеницы 250мл</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальным маслом и маслом зародышей</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальицы 250мл</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальицы 250мл</a></div>
      </li>
     </ul>
    </li>
    <li>
     <ul>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
     </ul>   
    </li>
    <li>
     <ul>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
     </ul>
    </li>
   </ul>
   <ul>
    <li>
     <ul>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальным маслом и маслом зародышей пшеницы 250мл</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальным маслом и маслом зародышей</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальицы 250мл</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальицы 250мл</a></div>
      </li>
     </ul>
    </li>
    <li>
     <ul>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
     </ul>   
    </li>
    <li>
     <ul>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
     </ul>
    </li>
   </ul>
   <div class="clear"></div>
  </div>
  <h2>Акции</h2>
  <div class="all"><a href="#">Все акции</a></div>
  <div class="goods_list">
   <ul>
    <li>
     <ul>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальным маслом и маслом зародышей пшеницы 250мл</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальным маслом и маслом зародышей</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальицы 250мл</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальицы 250мл</a></div>
      </li>
     </ul>
    </li>
    <li>
     <ul>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
     </ul>   
    </li>
    <li>
     <ul>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
     </ul>
    </li>
   </ul>
   <ul>
    <li>
     <ul>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальным маслом и маслом зародышей пшеницы 250мл</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальным маслом и маслом зародышей</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальицы 250мл</a></div>
      </li>
      <li>
       <div class="img"><img src="/img/img2.jpg" width="146" height="146" alt="" /></div>
       <div class="tit"><a href="#">НФ Шампунь с миндальицы 250мл</a></div>
      </li>
     </ul>
    </li>
    <li>
     <ul>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
      <li>
       <p>Натурфарм косметика</p>
       <p class="price">Цена:<span>43,70 грн</span></p>
      </li>
     </ul>   
    </li>
    <li>
     <ul>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
      <li>
       <div class="buy"><input type="text" />шт.<a href="#">Заказать</a></div>
      </li>
     </ul>
    </li>
   </ul>
   <div class="clear"></div>
  </div>
  <h2>Новости</h2>
  <div class="all"><a href="#">Все новости</a></div>
  <ul class="news_list">
   <li>22.06.11<span><a href="#">Больным ожирением и курильщикам отказывают в медицинской помощи</a></span><p><img src="/img/img.jpg" width="84" height="73" alt="" class="float_left" />Британские медики решили себя обезопасить, так как отказываются проводить хирургичні операции людям, страдающим ожирением и тем, кто имеет вредную привычку курить.</p><p><a href="#">Подробнее</a></p></li>
   <li>18.05.11<span><a href="#">Рынок фармацевтических средств в Казахстане по-прежнему остается рынком импорта</a></span><p>Импорт лекарственных средств в Казахстан растет, а экспорт снижается с 2007 года. Об этом говорится в последнем обзоре ATFBank Research «Фармацевтический рынок Казахстана растет в 2,5 раза быстрей ВВП».</p><p><a href="#">Подробнее</a></p></li>
  </ul>
  <div class="clear"></div>
  <h1>Интернет-аптека ООО «I.K.Well»</h1>
  <p><img src="/img/img.jpg" width="680" height="347" alt="" /></p>
  <p>Уважаемые посетители интернет-аптеки ООО «I.K.Well»!, мы рады приветствовать вас на своем сайте. Наша интернет-аптека создана для удобства поиска лекарств, а также сопутствующих товарных групп по всей Украине. Вам необходимо лишь сделать заказ лекарств, доставка по городу – это одна из услуг, предоставляемых нашей аптекой. Специально для вас, мы планируем продолжать развитие торговли лекарствами через интернет и по телефону. Благодаря нашему интернет-магазину, доставка лекарств по Украине стала нормой. Вам стоит один раз попробовать сделать заказ с доставкой медикаментов по г. Харьков, и вы в полной мере оцените удобство данной услуги. Наша аптека поможет вам осуществлять заказ легальных таблеток, лекарств по действительно выгодным ценам. Доставка медикаментов по г. Харьков, как и любой продукции, представленной на нашем сайте возможна при покупке от 100 грн. Вам не надо тратить время на утомительный поиск лекарств в аптеках. Не сомневайтесь в качестве приобретаемых товаров, представленных в нашей аптеке онлайн. Тех кто занят или не может выйти из дома, выручит наша услуга – доставка лекарств по г. Харьков.</p>
  <p>Мы рады помогать вам быть здоровыми и жизнерадостными!<br />С уважением,<br />коллектив ООО «I.K.Well»</p>
  <div class="clear"></div>  
  <h1>Типовые элементы</h1>
  <h1>Ссылка:</h1>
  <p><a href="#">Активная</a><br /><a href="#">При наведении</a><br /><a href="#">Посещённая</a></p>
  <h1>Кнопка</h1>
  <p><input type="submit" value="Ответить" class="button" /></p>   
  <h1>Маркированый список:</h1>
  <ul>
   <li><a href="#">О нас</a></li>
   <li><a href="#">Новости</a></li>
   <li><a href="#">Сервис</a></li>
   <li><a href="#">Информация</a>
    <ul>
     <li><a href="#">Статья №1</a></li>
     <li><a href="#">Статья №2</a></li>
     <li><a href="#">Статья №3</a></li>
    </ul>
   </li>
   <li><a href="#">Контакты</a></li>
  </ul>
  <h1>H1 - H6:</h1>
  <h1>Заголовок Н1</h1>
  <h2>Заголовок Н2</h2>
  <h3>Заголовок Н3</h3>
  <h4>Заголовок Н4</h4>
  <h5>Заголовок Н5</h5>
  <h6>Заголовок Н6</h6>
  <h1>Поле ввода для текста:</h1>
  <p>Имя <input type="text" /></p>
  <h1>Выпадающий список:</h1>
  <p><select><option>&nbsp;</option></select></p>
  
                          <div>
                            <select name="s" id="s">
                                <option value="0">Акции</option>
                                <option value="1">Условия и цены</option>
                                <option value="2">Доставка</option>
                                <option value="3">Предоставляемые информационні сервисы</option>
                            </select>
                        </div><br><br>
  
  <h1>Таблица</h1>
  <table>
   <tr>
    <th>№</th>
    <th>Продукт</th>
    <th>Вода</th>
    <th>Жиры</th>
    <th>Белки</th>
    <th>Углеводы</th>
    <th>ККАЛ</th>
   </tr>
   <tr>
    <td>1</td>
    <td><a href="#">Брынза из коровьего молока</a></td>
    <td>52</td>
    <td>17,9</td>
    <td>20,1</td>
    <td>0</td>
    <td>260</td>
   </tr>
   <tr>
    <td>2</td>
    <td><a href="#">Брынза из коровьего молока</a></td>
    <td>52</td>
    <td>17,9</td>
    <td>20,1</td>
    <td>0</td>
    <td>260</td>
   </tr>
   <tr>
    <td>3</td>
    <td><a href="#">Брынза из коровьего молока</a></td>
    <td>52</td>
    <td>17,9</td>
    <td>20,1</td>
    <td>0</td>
    <td>260</td>
   </tr>
  </table>
 </div>
 </div>
</div>
<div class="clear"></div>
<div class="footer_wrap">
 <div class="footer">
  <div class="left">
   <p>Украина<br />г. Харьков, (057) 728 41 45	</p>
   <p>Всеукраинская справочная служба<br />аптек ООО «I.K.Well» <span>0 (800) 888-85-89</span></p>
  </div>
  <div class="right">
   <p>Уважаемые посетители интернет-аптеки ООО «I.K.Well»!, мы рады приветствовать вас на своем сайте. Наша интернет-аптека создана для удобства поиска лекарств, а также сопутствующих товарных групп по всей Украине. Вам необходимо лишь сделать заказ лекарств, доставка по городу – это одна из услуг, предоставляемых нашей аптекой. Специально для вас, мы планируем продолжать развитие торговли лекарствами через интернет и по телефону. Благодаря нашему интернет-магазину, доставка лекарств по Украине стала нормой. Вам стоит один раз попробовать сделать заказ с доставкой медикаментов по г. Харьков, и вы в полной мере оцените удобство данной </p>
  </div>
  <div class="clear"></div>
  <div class="copy">
   <p>&copy; 2010 ООО «I.K.Well» Все права защищены</p>
  </div>
  <div class="rules">
   <p><a href="#">Правила использования информации</a><span><a href="http://www.deluxe.dp.ua/" target="_blank">создание сайта<br /><img src="/img/deluxe.gif" width="80" height="18" alt="" /></a></span></p>
  </div>
  <div class="clear"></div>
 </div>
</div>

</body>
</HTML>