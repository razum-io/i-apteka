﻿<!-- BDP: basket -->

  <div class="basket_table">
  <form method="post" action="/basket" name="basket_form1">
  <table>
   <tr>
    <th>Наименование</th>
    <th>Производитель</th>
    <th>Цена</th>
    <th>Кол-во</th>
    <th>Сумма (грн.)</th>
    <th>Удалить</th>
   </tr>
   <!-- BDP: basket_items -->
   <tr class="{PRINT_REG}">
    <td id="td1_goods_{BASKET_ITEM_ID}"><a href="#">{BASKET_ITEM_NAME}</a></td>
    <td id="td2_goods_{BASKET_ITEM_ID}">{BASKET_ITEM_PRODUCER}</td>
    <td id="td3_goods_{BASKET_ITEM_ID}">{BASKET_ITEM_COST}</td>
    <td id="td4_goods_{BASKET_ITEM_ID}"><input type="text" onKeyUp="addToBasket2('{BASKET_ITEM_REGION}','goods_{BASKET_ITEM_ID}', this, 1, event);" name="count_id[{BASKET_ITEM_ID}]" value="{BASKET_ITEM_COUNT}" onclick="value=''" />шт.</td>
    <td id="td5_goods_{BASKET_ITEM_ID}"><div id="span_goods_{BASKET_ITEM_ID}{BASKET_ITEM_REGION}">{BASKET_ITEM_SUMM}</div></td>
    <td id="td6_goods_{BASKET_ITEM_ID}"><a href="/{REGION_URL}basket/{BASKET_ITEM_BUTTON_CLASS}/{BASKET_ITEM_ARTIKUL}" >{BASKET_ITEM_BUTTON_TEXT}</a></td>
   </tr>
   <!-- EDP: basket_items -->
  </table>
      
      
  </form>
      
  <p class="total">Итого: <span id="all-summ">{BASKET_ITEM_ALL_SUMM} грн.</span><br /><!--<a href="#">Применить</a>--></p>
  </div>
  <div class="order_block">
   <h2>Оформление заказа</h2>
   {BASKET_FORM_ERR}
   <p><span>*</span> — Поля необходимые для заполнения.</p>
   <form name="user_info" action="/{REGION_URL}basket/" method="post">
     
   <table>
    <tr>
     <td>Ф.И.О.<span>*</span>:</td>
     <td><input type="text" name="fio" value="{BASKET_FORM_FIO}" /></td>
    </tr>
    <tr>
     <td>Адрес<span>*</span>:</td>
     <td><textarea name="adr">{BASKET_FORM_ADR}</textarea></td>
    </tr>
    <tr>
     <td>e-mail<span>*</span>:</td>
     <td><input type="text" name="email" value="{BASKET_FORM_EMAIL}" /></td>
    </tr>
    <tr>
     <td>Тел. моб.<span>*</span>:</td>
     <td>	 <input type="text" name="mphone" required pattern="[0-9_-+]{13}" placeholder="+38(___) ___ __ __" id="user_phone" title="Формат: +380(096) 999 99 99" value="{BASKET_FORM_MPHONE}" /></td>
    </tr>
    <tr>
     <td>Тел. дом./раб.:</td>
     <td><input type="text" name="phone" value="{BASKET_FORM_PHONE}" /></td>
    </tr>
    
    {BASKET_TVIVOZA}
    
    <tr>
     <td>Дополнительная информация о заказе:</td>
     <td><textarea name="info">{BASKET_FORM_INFO}</textarea></td>
    </tr>
   </table>
   <p class="right"><input type="submit" value="Заказать" name="go" class="button" /></p>
   </form>
  </div>
</div>

<!-- EDP: basket -->


<!-- BDP: basket_empty -->
<h2>Заказов не найдено</h2>
<!-- EDP: basket_empty -->