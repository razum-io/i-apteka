<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML><HEAD><TITLE>{TITLE}</TITLE>
<meta name="keywords" content="{KEYWORDS}">
<meta name="description" content="{DESCRIPTION}">
<META http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="document-state" content="dynamic">
<meta name="revisit" content="7 days">
<meta name="revisit-after" content="7 days">
<META NAME="Resourse-type" CONTENT="document">
<META NAME="Robots" CONTENT="index,follow">
<META NAME="Rating" CONTENT="general">
<META NAME="Distribution" CONTENT="global">
<META NAME="Classification" CONTENT="">
<META NAME="Category" CONTENT="">
<meta http-equiv="Pragma" content="token">
<meta http-equiv="Cache-Control" content="token">
<META NAME="Copyright" CONTENT="2011 deluxe.dp.ua">
<LINK href="/css/style.css" type="text/css" rel=stylesheet>
<link  rel="stylesheet" type="text/css" href="/css/lightbox.css" />
<link  rel="stylesheet" type="text/css" href="/css/calendar-blue.css" />

<LINK REL="shortcut icon" HREF="/favicon.ico" type="image/x-icon">

<!--[if lte IE 6]>  
<script defer type="text/javascript" src="/js/pngfix.js" mce_src="/js/pngfix.js"></script>
<![endif]-->

<link  rel="stylesheet" type="text/css" href="/css/lightbox.css" />
<link  rel="stylesheet" type="text/css" href="/css/calendar-blue.css" />

<script type="text/javascript" src="/js/jquery.js"></script>


<!-- BDP: adminjslib -->
<script type="text/javascript" src="/js/swfobject.js"></script>
    <script type="text/javascript" src="/js/jquery.uploadify.v2.1.4.min.js"></script>

    	<script type="text/javascript">
            
            var objDump = function (obj) {
                var ret = '';
                for(o in obj) {
                    ret += 'Key: '+o+' Val: '+obj[o]+"\n";
                }
                return ret;
            }
            
    		jQuery(document).ready(function() {
    			jQuery("#uploadify").uploadify({
    				
    				'expressInstall': '/modules/uploadify/expressInstall.swf',
    				'uploader': '/modules/uploadify/uploadify.swf',
    				'script': '/modules/uploadify/uploadify.php',
    				'folder': '/upload',
    				'cancelImg': '/modules/uploadify/cancel.png',
    				
    				'queueID'        : 'fileQueue',
    				'fileDesc'       : 'jpg, gif, png',
    				'fileExt'        : '*.jpg;*.gif;*.png;*.jpeg',
    				'auto'           : false,
    				'multi'          : true,
                                'onAllComplate': function(t1, t2) {
                                  alert(objDump(t2));  
                                },
    				'onComplete'   : function(event,queueID,fileObj,response,data) {
                                    alert(response);
    									if (response != 'OK') {
    										jQuery('#response').append(response);
    									}
    								 },
    				onError: function (event, queueID, fileObj, errorObj) {
                                  
    							 err = '';
    					         if (errorObj.status == 404) {
    					            err += 'Could not find upload script.';
    					         } else if (errorObj.type === "HTTP") {
    					            err += 'error '+errorObj.type+": "+errorObj.status;
    					         } else if (errorObj.type ==="File Size") {
    					            err += fileObj.name+' '+errorObj.type+' Limit: '+Math.round(errorObj.sizeLimit/1024)+'KB';
    					         } else {
    					            err += 'error '+errorObj.type+": "+errorObj.text;
    					         }
                                                  
    					      }
                                             
    				/*
    				*/
    			});
    		});
    	</script>



<!-- EDP: adminjslib -->

<script type="text/javascript" src="/js/jq-scripts.js"></script>

<script type="text/javascript" src="/js/jq-order-form.js"></script>
<script type="text/javascript" src="/js/prototype.js"></script>

<script src="/js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
<script src="/js/lightbox.js" type="text/javascript"></script>

<script type="text/javascript" src="/js/scripts.js"></script>
<script type="text/javascript" src="/js/js_func.js"></script>

        <link rel="stylesheet" href="/css/jquery-ui.css">
        <link rel="stylesheet" href="/css/jquery.selectBox.css">
        <script type="text/javascript" src="/js/jquery_ui/jquery.ui.core.min.js"></script>
        <script type="text/javascript" src="/js/jquery_ui/jquery.ui.widget.min.js"></script>
        <script type="text/javascript" src="/js/ui.checkbox.js"></script>
        <script type="text/javascript" src="/js/jquery.selectBox.min.js"></script>
        <script type="text/javascript" src="/js/site.js"></script>

</HEAD>
<body>

<div class="wraper">
 <div class="header">
 
	 <div class="top_menu_block">
		  <ul class="pic_menu">
		   <li class="active"><a class="home" href="/" title="{TITLEMAIN}">&nbsp;</a></li>
		   <li><a href="/sitemap" title="{TITLEMAP}" class="sitemap">&nbsp;</a></li>
		   <li style="padding-right:0"><a href="#" onclick="add2Fav (this, 'http://{SERVER_NAME}/');" title="{TITLEFEED}" class="email">&nbsp;</a></li>
		  
		  </ul>
	 <div class="h-menu">

	  <table class="w100pr"> 
	   <tr>
		<td class="first0"></td>
		 <!-- BDP: horisontal -->
		 <td class=""><div class="rel"><a href="{MENU_HREF}" title="{MENU_NAME}">{MENU_NAME}</a></div></td>	
		<!-- EDP: horisontal -->	
	    <td class="last0"></td> 
	   </tr>
	  </table>
 	 </div>
  	</div>
  	<br class="clear">
  <div class="logo">
   <a href="/"  alt="{LOGOALT}" title="{LOGOALT}"><img src="/img/logo.gif" alt="{LOGOALT}" title="{LOGOALT}" width="184" height="102" /></a>
  </div>
  <div class="block2_heder">
  <div class="top_text2"><div class="text">{SLG2}</div>
  <div class="busket_block" id="busket-block"> 
  <!-- BDP: basket_block -->
  <script type="text/javascript">
  window.onload = function() {
    getBasket();	
  }
  
	</script> 
	<!-- EDP: basket_block -->
        {SHOW_SEARCH_TEXT_IF_NO_STORE}
  </div>
  </div>


  <div class="phone"><div class="tel">{SLG2_PHONES}</div>
    	<div class="search_block">
   			<form action="/search/" method="get">
  
   			 <p><img src="/img/input_left.gif" width="5" height="22" alt="" /><input value="Поиск товаров" name="q" onclick="value=''" onfocus="if (this.value=='введите запрос'){this.value=''}" onblur="if (this.value==''){this.value='введите запрос'}" type="text" /><img src="/img/input_right.gif" width="5" height="22" alt="" /><input type="image" src="/img/search.gif" class="button"/></p>
  
   			 </form>
   
  		</div>
 </div>
 
  </div>
  </div><!-- block2_heder -->
  
  <div class="login_block"> 
  
  </div>  
<!-- BDP: index_top_bnnrs -->
<div class="top_bnr">
    
    <span id="left">{TOP_BANNER_LEFT}</span>
	<span id="right">{TOP_BANNER_RIGHT}</span>
    
    
</div>
<!-- EDP: index_top_bnnrs -->

 
 <div class="pager">{WAY}</div>
 <div class="columns_wrap">
 <div class="left_column">
  
 <!-- BDP: administration -->
 <div class="title-header">
 	<h2>Администрирование</h2>
 </div>
 
 <ul class="left_menu">
 	<li><h3><a href='{BASE_PATH}admin/editpage/mainpage' title="Главная страница">Главная страница</a></h3></li>
 	<li><h3><a href='{BASE_PATH}admin/menu/horisontal' title="Горизонтальное меню">Горизонтальное меню</a></h3></li>
 	
 	<li><h3><a href='{BASE_PATH}news' title="Новости">Новости</a></h3></li>
         <!-- BDP: show_order --> 
 	<li><h3><a href='{BASE_PATH}admin/orders/' title="База заказов">База заказов</a></h3></li>
 	<li><h3><a href='{BASE_PATH}admin/deliveryservice/' title="Службы доставки">Службы доставки</a></h3></li>
         <!-- EDP: show_order --> 
 	<li><h3><a href='{BASE_PATH}admin/import/' title="Импорт товаров">Импорт товаров</a></h3></li> 	
 	<li><h3><a href='{BASE_PATH}admin/loadcatpics/' title="Загрузка картинок каталога">Загрузка картинок каталога</a></h3></li>
 	<li><h3><a href='{BASE_PATH}admin/settings' title="Настройки сайта">Настройки сайта</a></h3></li>
 	<li><h3><a href='{BASE_PATH}admin/banners/' title="Баннеры">Баннеры</a></h3></li>
 	<li><h3><a href='{BASE_PATH}admin/lookups' title="Редактируемые поля">Редактируемые поля</a></h3></li>
 	<li><h3><a href='{BASE_PATH}admin/metatags' title="Мета-Теги">Мета-Теги</a></h3></li>
 	<li><h3><a href='{BASE_PATH}logout' title="Выход">Выход</a></h3></li>
 </ul>	
                        
  <br /> <br />
  
  <!-- EDP: administration -->
 <div class="title-header">
 <h2><a href="/catalog">{CATTITLE}</a></h2>
 </div>
   
  
  <ul class="left_menu">
  <!-- BDP: catalog_menu_new_goods -->
   	<li><h3><a href="/catalog/novelty" title="Новинки">Новинки</a></h3></li>
  <!-- EDP: catalog_menu_new_goods -->
  
  <!-- BDP: catalog_menu_hit_goods -->
   	<li><h3><img src="/img/main_menu_row.jpg" style="width: 9px; height: 9px;" /> <a href="/catalog/hits" title="Хиты"> Хиты</a></h3></li>
  <!-- EDP: catalog_menu_hit_goods -->
  
  <!-- BDP: catalog_menu_action_goods -->
   	<li><h3><a href="/catalog/actions" title="Акции">Акции</a></h3></li>
  <!-- EDP: catalog_menu_action_goods -->
   
  <!-- BDP: catalog_menu -->
                             
  	<!-- BDP: catalog_menu_complex -->
  		<li><h3><img src="/img/main_menu_row.jpg" style="width: 9px; height: 9px;" /> <a href='/catalog{CATALOG_MENU_HREF}' title='{CATALOG_MENU_NAME}'> {CATALOG_MENU_NAME}</a></h3>    	
         	<ul>
        	<!-- BDP: catalog_menu_complex_sub -->
        		<li><img src="/img/{CATALOG_MENU_IMG}" style="width: 6px; height: 7px;" /> <a href="/catalog{CATALOG_MENU_HREF}" class="{CATALOG_MENU_CL}" title='{CATALOG_MENU_NAME}'> {CATALOG_MENU_NAME}</a></li>            	
           <!-- EDP: catalog_menu_complex_sub -->
            </ul>
       </li>
       <!-- EDP: catalog_menu_complex -->
       
       <!-- BDP: catalog_menu_single -->
       	<li><h3><img src="/img/main_menu_row.jpg" style="width: 9px; height: 9px;" /> <a href='/catalog{CATALOG_MENU_HREF}' title='{CATALOG_MENU_NAME}'> {CATALOG_MENU_NAME}</a></h3></li>
       <!-- EDP: catalog_menu_single -->
   <!-- EDP: catalog_menu -->
   
   
  </ul>
  
  <div class="clear"></div>
  <p class="banner">{LEFT_BANNER}</p>
 </div>
 <div class="right_column">
 
  <!-- BDP: p_header -->
     <h1>{HEADER}</h1>
 <!-- EDP: p_header -->
 <div class="main-content">
 {CONTENT}
 </div>                   



   </div>
 <div class="footer">
 <div class="footer-line"></div>
  <div class="left">
   <p>
   <div class="div-logo">
   
   <img src="/img/logo-bot.gif" width="88" height="31" alt="{LOGO_BOT_ALT}" title="{LOGO_BOTT_TITLE}" />
   </div>
   <div class="adress">{ADRES}</div>  </div>
  <div class="right">
   <p>{SLG3}</p>
  </div>
  <div class="clear" style="border-bottom: 2px solid #c9c9c9; padding-top: 20px;"></div>
  <div class="left">
   <p class="cs"><a href="http://bingo.in.ua/" alt="Bingo! Создание сайтов" title="Bingo! Создание сайтов" target="_blank">создание сайтов</a></p>
   <p><a href="http://bingo.in.ua/"  target="_blank" alt="Bingo! Создание сайтов" title="Bingo! Создание сайтов"    ><img src="/img/bingo-logo.gif" width="85" height="29" class="bingo"  alt="Bingo! Создание сайтов" title="Bingo! Создание сайтов" /></a></p>
  </div>
  <div class="right">
   <p class="partner">Наши партнеры</p>   
   <ul>
   {PARTNERS}
    
   </ul>
  </div>
  <div class="clear"></div>
 </div>
</div>
</body>
</HTML>